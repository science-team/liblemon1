<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="page">
    <name>index</name>
    <title>LEMON Documentation</title>
    <filename>index</filename>
    <docanchor file="index">howtoread</docanchor>
    <docanchor file="index">intro</docanchor>
    <docanchor file="index">whatis</docanchor>
  </compound>
  <compound kind="file">
    <name>arg_parser_demo.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/demo/</path>
    <filename>a00301</filename>
    <includes id="a00317" name="arg_parser.h" local="no" imported="no">lemon/arg_parser.h</includes>
  </compound>
  <compound kind="file">
    <name>graph_to_eps_demo.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/demo/</path>
    <filename>a00302</filename>
    <includes id="a00375" name="list_graph.h" local="no" imported="no">lemon/list_graph.h</includes>
    <includes id="a00368" name="graph_to_eps.h" local="no" imported="no">lemon/graph_to_eps.h</includes>
    <includes id="a00384" name="math.h" local="no" imported="no">lemon/math.h</includes>
  </compound>
  <compound kind="file">
    <name>lgf_demo.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/demo/</path>
    <filename>a00303</filename>
    <includes id="a00394" name="smart_graph.h" local="no" imported="no">lemon/smart_graph.h</includes>
    <includes id="a00373" name="lgf_reader.h" local="no" imported="no">lemon/lgf_reader.h</includes>
    <includes id="a00374" name="lgf_writer.h" local="no" imported="no">lemon/lgf_writer.h</includes>
  </compound>
  <compound kind="file">
    <name>adaptors.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00315</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <includes id="a00399" name="tolerance.h" local="no" imported="no">lemon/tolerance.h</includes>
    <class kind="class">lemon::ReverseDigraph</class>
    <class kind="class">lemon::SubDigraph</class>
    <class kind="class">lemon::SubGraph</class>
    <class kind="class">lemon::FilterNodes</class>
    <class kind="class">lemon::FilterArcs</class>
    <class kind="class">lemon::FilterEdges</class>
    <class kind="class">lemon::Undirector</class>
    <class kind="class">lemon::Undirector::CombinedArcMap</class>
    <class kind="class">lemon::Orienter</class>
    <class kind="class">lemon::ResidualDigraph</class>
    <class kind="class">lemon::ResidualDigraph::ResidualCapacity</class>
    <class kind="class">lemon::SplitNodes</class>
    <class kind="class">lemon::SplitNodes::CombinedNodeMap</class>
    <class kind="class">lemon::SplitNodes::CombinedArcMap</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>arg_parser.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00317</filename>
    <includes id="a00318" name="assert.h" local="no" imported="no">lemon/assert.h</includes>
    <class kind="class">lemon::ArgParser</class>
    <class kind="class">lemon::ArgParser::RefType</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>assert.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00318</filename>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
    <namespace>lemon</namespace>
    <member kind="define">
      <type>#define</type>
      <name>LEMON_ASSERT</name>
      <anchorfile>a00451.html</anchorfile>
      <anchor>gf78cf5572d91896ceecdd970f55601bc</anchor>
      <arglist>(exp, msg)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LEMON_DEBUG</name>
      <anchorfile>a00451.html</anchorfile>
      <anchor>gcbefc04b0335938603649e96af183843</anchor>
      <arglist>(exp, msg)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>base.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00319</filename>
    <includes id="a00399" name="tolerance.h" local="no" imported="no">lemon/tolerance.h</includes>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <namespace>lemon</namespace>
    <member kind="variable">
      <type>const Invalid</type>
      <name>INVALID</name>
      <anchorfile>a00406.html</anchorfile>
      <anchor>0f04de8e6be7bc21ed685c651571d9fe</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>bfs.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00320</filename>
    <includes id="a00375" name="list_graph.h" local="no" imported="no">lemon/list_graph.h</includes>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <includes id="a00388" name="path.h" local="no" imported="no">lemon/path.h</includes>
    <class kind="struct">lemon::BfsDefaultTraits</class>
    <class kind="class">lemon::Bfs</class>
    <class kind="struct">lemon::Bfs::SetPredMap</class>
    <class kind="struct">lemon::Bfs::SetDistMap</class>
    <class kind="struct">lemon::Bfs::SetReachedMap</class>
    <class kind="struct">lemon::Bfs::SetProcessedMap</class>
    <class kind="struct">lemon::Bfs::SetStandardProcessedMap</class>
    <class kind="struct">lemon::BfsWizardDefaultTraits</class>
    <class kind="class">lemon::BfsWizardBase</class>
    <class kind="class">lemon::BfsWizard</class>
    <class kind="struct">lemon::BfsVisitor</class>
    <class kind="struct">lemon::BfsVisitDefaultTraits</class>
    <class kind="class">lemon::BfsVisit</class>
    <class kind="struct">lemon::BfsVisit::SetReachedMap</class>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>BfsWizard&lt; BfsWizardBase&lt; GR &gt; &gt;</type>
      <name>bfs</name>
      <anchorfile>a00436.html</anchorfile>
      <anchor>g437c3771dd4592f7d7a2d93fa2e5f54c</anchor>
      <arglist>(const GR &amp;digraph)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>bin_heap.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00321</filename>
    <class kind="class">lemon::BinHeap</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>windows.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/bits/</path>
    <filename>a00336</filename>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>cbc.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00338</filename>
    <includes id="a00339" name="cbc.h" local="yes" imported="no">cbc.h</includes>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>cbc.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00339</filename>
    <includes id="a00378" name="lp_base.h" local="no" imported="no">lemon/lp_base.h</includes>
    <class kind="class">lemon::CbcMip</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>circulation.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00340</filename>
    <includes id="a00399" name="tolerance.h" local="no" imported="no">lemon/tolerance.h</includes>
    <includes id="a00361" name="elevator.h" local="no" imported="no">lemon/elevator.h</includes>
    <class kind="struct">lemon::CirculationDefaultTraits</class>
    <class kind="class">lemon::Circulation</class>
    <class kind="struct">lemon::Circulation::SetFlowMap</class>
    <class kind="struct">lemon::Circulation::SetElevator</class>
    <class kind="struct">lemon::Circulation::SetStandardElevator</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>clp.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00342</filename>
    <includes id="a00378" name="lp_base.h" local="no" imported="no">lemon/lp_base.h</includes>
    <class kind="class">lemon::ClpLp</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>color.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00343</filename>
    <includes id="a00344" name="color.h" local="no" imported="no">lemon/color.h</includes>
    <namespace>lemon</namespace>
    <member kind="variable">
      <type>const Color</type>
      <name>WHITE</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>ga574c6748d637031ff114ee5396f371d</anchor>
      <arglist>(1, 1, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>BLACK</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g887e77777b0cdd4bd98cd8582eab747d</anchor>
      <arglist>(0, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>RED</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g3aab05ed4f1fa1188cb5cec4a951da36</anchor>
      <arglist>(1, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>GREEN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g9d50a7cbc8c947f88556291754b964df</anchor>
      <arglist>(0, 1, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>BLUE</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g8d1bd8aebf1ea19b34a359b95afb2271</anchor>
      <arglist>(0, 0, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>YELLOW</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g7b3b82796993ff082f39aeaca4f74be9</anchor>
      <arglist>(1, 1, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>MAGENTA</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gd9c523e1fcd76b6e97a7e8f6c89a6d09</anchor>
      <arglist>(1, 0, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>CYAN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g6b139d00115defc76ec508dff90c91fd</anchor>
      <arglist>(0, 1, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>GREY</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gccc4c0904a38839f9554cde971bb4963</anchor>
      <arglist>(0, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_RED</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g1c9259e9d2ec9a44ea6bf855d3dd3917</anchor>
      <arglist>(.5, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_GREEN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gbc9fc012aa002dd8e311065b3115969c</anchor>
      <arglist>(0,.5, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_BLUE</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gc56cc8c77b9fcb56e308652d922d08f0</anchor>
      <arglist>(0, 0,.5)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_YELLOW</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g5dbe8dbe7b513bc66228981dcad165f7</anchor>
      <arglist>(.5,.5, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_MAGENTA</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>ga3457b13dd61099a849816a02ba55efc</anchor>
      <arglist>(.5, 0,.5)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_CYAN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g1814dea6aea83b28d9137adaa4d8b937</anchor>
      <arglist>(0,.5,.5)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>color.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00344</filename>
    <includes id="a00384" name="math.h" local="no" imported="no">lemon/math.h</includes>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <class kind="class">lemon::Color</class>
    <class kind="class">lemon::Palette</class>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>Color</type>
      <name>distantColor</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gafbc768b20325e1e5048dcd2b10bc4e7</anchor>
      <arglist>(const Color &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>Color</type>
      <name>distantBW</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g22a184708d82339466dd08286e01a756</anchor>
      <arglist>(const Color &amp;c)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>concept_check.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00345</filename>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>void</type>
      <name>function_requires</name>
      <anchorfile>a00406.html</anchorfile>
      <anchor>1928f6d1b464ab2e048c3c41f4f93389</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>checkConcept</name>
      <anchorfile>a00406.html</anchorfile>
      <anchor>6addf2ab7b723e4f2663c08b6a463033</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>digraph.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/concepts/</path>
    <filename>a00346</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00382" name="maps.h" local="no" imported="no">lemon/concepts/maps.h</includes>
    <includes id="a00345" name="concept_check.h" local="no" imported="no">lemon/concept_check.h</includes>
    <includes id="a00348" name="graph_components.h" local="no" imported="no">lemon/concepts/graph_components.h</includes>
    <class kind="class">lemon::concepts::Digraph</class>
    <class kind="class">lemon::concepts::Digraph::Node</class>
    <class kind="class">lemon::concepts::Digraph::NodeIt</class>
    <class kind="class">lemon::concepts::Digraph::Arc</class>
    <class kind="class">lemon::concepts::Digraph::OutArcIt</class>
    <class kind="class">lemon::concepts::Digraph::InArcIt</class>
    <class kind="class">lemon::concepts::Digraph::ArcIt</class>
    <class kind="class">lemon::concepts::Digraph::NodeMap</class>
    <class kind="class">lemon::concepts::Digraph::ArcMap</class>
    <namespace>lemon</namespace>
    <namespace>lemon::concepts</namespace>
  </compound>
  <compound kind="file">
    <name>graph.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/concepts/</path>
    <filename>a00347</filename>
    <includes id="a00348" name="graph_components.h" local="no" imported="no">lemon/concepts/graph_components.h</includes>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <class kind="class">lemon::concepts::Graph</class>
    <class kind="class">lemon::concepts::Graph::Node</class>
    <class kind="class">lemon::concepts::Graph::NodeIt</class>
    <class kind="class">lemon::concepts::Graph::Edge</class>
    <class kind="class">lemon::concepts::Graph::EdgeIt</class>
    <class kind="class">lemon::concepts::Graph::IncEdgeIt</class>
    <class kind="class">lemon::concepts::Graph::Arc</class>
    <class kind="class">lemon::concepts::Graph::ArcIt</class>
    <class kind="class">lemon::concepts::Graph::OutArcIt</class>
    <class kind="class">lemon::concepts::Graph::InArcIt</class>
    <class kind="class">lemon::concepts::Graph::NodeMap</class>
    <class kind="class">lemon::concepts::Graph::ArcMap</class>
    <class kind="class">lemon::concepts::Graph::EdgeMap</class>
    <namespace>lemon</namespace>
    <namespace>lemon::concepts</namespace>
  </compound>
  <compound kind="file">
    <name>graph_components.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/concepts/</path>
    <filename>a00348</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00382" name="maps.h" local="no" imported="no">lemon/concepts/maps.h</includes>
    <class kind="class">lemon::concepts::GraphItem</class>
    <class kind="class">lemon::concepts::BaseDigraphComponent</class>
    <class kind="class">lemon::concepts::BaseGraphComponent</class>
    <class kind="class">lemon::concepts::BaseGraphComponent::Edge</class>
    <class kind="class">lemon::concepts::IDableDigraphComponent</class>
    <class kind="class">lemon::concepts::IDableGraphComponent</class>
    <class kind="class">lemon::concepts::GraphItemIt</class>
    <class kind="class">lemon::concepts::GraphIncIt</class>
    <class kind="class">lemon::concepts::IterableDigraphComponent</class>
    <class kind="class">lemon::concepts::IterableGraphComponent</class>
    <class kind="class">lemon::concepts::AlterableDigraphComponent</class>
    <class kind="class">lemon::concepts::AlterableGraphComponent</class>
    <class kind="class">lemon::concepts::GraphMap</class>
    <class kind="class">lemon::concepts::MappableDigraphComponent</class>
    <class kind="class">lemon::concepts::MappableDigraphComponent::NodeMap</class>
    <class kind="class">lemon::concepts::MappableDigraphComponent::ArcMap</class>
    <class kind="class">lemon::concepts::MappableGraphComponent</class>
    <class kind="class">lemon::concepts::MappableGraphComponent::EdgeMap</class>
    <class kind="class">lemon::concepts::ExtendableDigraphComponent</class>
    <class kind="class">lemon::concepts::ExtendableGraphComponent</class>
    <class kind="class">lemon::concepts::ErasableDigraphComponent</class>
    <class kind="class">lemon::concepts::ErasableGraphComponent</class>
    <class kind="class">lemon::concepts::ClearableDigraphComponent</class>
    <class kind="class">lemon::concepts::ClearableGraphComponent</class>
    <namespace>lemon</namespace>
    <namespace>lemon::concepts</namespace>
  </compound>
  <compound kind="file">
    <name>heap.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/concepts/</path>
    <filename>a00349</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00345" name="concept_check.h" local="no" imported="no">lemon/concept_check.h</includes>
    <class kind="class">lemon::concepts::Heap</class>
    <namespace>lemon</namespace>
    <namespace>lemon::concepts</namespace>
  </compound>
  <compound kind="file">
    <name>connectivity.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00351</filename>
    <includes id="a00356" name="dfs.h" local="no" imported="no">lemon/dfs.h</includes>
    <includes id="a00320" name="bfs.h" local="no" imported="no">lemon/bfs.h</includes>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <includes id="a00315" name="adaptors.h" local="no" imported="no">lemon/adaptors.h</includes>
    <includes id="a00346" name="digraph.h" local="no" imported="no">lemon/concepts/digraph.h</includes>
    <includes id="a00347" name="graph.h" local="no" imported="no">lemon/concepts/graph.h</includes>
    <includes id="a00345" name="concept_check.h" local="no" imported="no">lemon/concept_check.h</includes>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>bool</type>
      <name>connected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g0adc4e80ce6ab29c538d9f2e99c23f5b</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g5c5a64cc9a7819f0cda6f94ac85d4495</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>connectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g9a6eab5d28f4dcc406609f1bc52bde78</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>stronglyConnected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>ga2cf073c58bca9a33472d75a904e709f</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countStronglyConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g6cd9776000cff151b9a73cc7d43cfee6</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>stronglyConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g155081f6d53c0c7d0ab96d229350004c</anchor>
      <arglist>(const Digraph &amp;digraph, NodeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>stronglyConnectedCutArcs</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g188f683e6621295b4cc6d4a9053a5216</anchor>
      <arglist>(const Digraph &amp;digraph, ArcMap &amp;cutMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countBiNodeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g355802e6b150b1f7ca1870380d32a14f</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>biNodeConnected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gc893f803a3736c18cc21a07ef29d0a0e</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biNodeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g04846c92189759d768aed68b713304b3</anchor>
      <arglist>(const Graph &amp;graph, EdgeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biNodeConnectedCutNodes</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g850219005a529d78a429f67b72b4828c</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;cutMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countBiEdgeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g0c00ba5193699d8ed03105c0c214b375</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>biEdgeConnected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>ga4116e769789e5eb612e45771e1044a7</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biEdgeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g8858bd6adf73e7cb0762058ecc636f79</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biEdgeConnectedCutEdges</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gfc18050604b516d4583e7595c5476078</anchor>
      <arglist>(const Graph &amp;graph, EdgeMap &amp;cutMap)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>dag</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g05008850803855835013e430a44f1402</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>topologicalSort</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g67cdef21788938be90604624f5af8569</anchor>
      <arglist>(const Digraph &amp;digraph, NodeMap &amp;order)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>checkedTopologicalSort</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g87256d2b4d6902b607c0deecfebcaa07</anchor>
      <arglist>(const Digraph &amp;digraph, NodeMap &amp;order)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>acyclic</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gcec2620760ea875a3ab17d6b26086499</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>tree</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g311ec89ea618e3453a14488dd3f75173</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>bipartite</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gcf98bfb7e52e8fa13bd9eac09703803f</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>bipartitePartitions</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gc6a525e6f95ec1d1847270befb55f12a</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;partMap)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>loopFree</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g96d709ca1f43a692fd9bde016eb05879</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>parallelFree</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g3fc80473f0dae370509c95322997d2f4</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>simpleGraph</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g4270a55a61dcc189e25331c7387eed98</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>core.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00352</filename>
    <includes id="a00318" name="assert.h" local="no" imported="no">lemon/assert.h</includes>
    <class kind="struct">lemon::Invalid</class>
    <class kind="class">lemon::DigraphCopy</class>
    <class kind="class">lemon::GraphCopy</class>
    <class kind="class">lemon::ConArcIt</class>
    <class kind="class">lemon::ConEdgeIt</class>
    <class kind="class">lemon::DynArcLookUp</class>
    <class kind="class">lemon::ArcLookUp</class>
    <class kind="class">lemon::AllArcLookUp</class>
    <namespace>lemon</namespace>
    <member kind="define">
      <type>#define</type>
      <name>DIGRAPH_TYPEDEFS</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gc618f30ace596c69836144bfdcc9112c</anchor>
      <arglist>(Digraph)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TEMPLATE_DIGRAPH_TYPEDEFS</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g14ef90d8fd45b56f1cc2c5023c76c4b1</anchor>
      <arglist>(Digraph)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GRAPH_TYPEDEFS</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gb8c497e4257836f4669b8922237d830b</anchor>
      <arglist>(Graph)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TEMPLATE_GRAPH_TYPEDEFS</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gb0e6cf49071e60eda3e15e2b0e0d8310</anchor>
      <arglist>(Graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countItems</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g8b8052427edea53441499450c5416ef4</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countNodes</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g85863e6534999fb3acd27b93228b4798</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countArcs</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g5e562e0a53b64eca38a7a053d157dfa9</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countEdges</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gf8263f7f605f41abf9953693e81d6df5</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countOutArcs</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>ga448f6d1089195d57a7cb7649f5234ed</anchor>
      <arglist>(const Graph &amp;g, const typename Graph::Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countInArcs</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g494e3b2d5eeaed01cbe4958f1a6bc334</anchor>
      <arglist>(const Graph &amp;g, const typename Graph::Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countIncEdges</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g8e4f35523ceb5e8ec55262a6c9bc810b</anchor>
      <arglist>(const Graph &amp;g, const typename Graph::Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>DigraphCopy&lt; From, To &gt;</type>
      <name>digraphCopy</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g59011124e7760c273e82d6029313943c</anchor>
      <arglist>(const From &amp;from, To &amp;to)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy&lt; From, To &gt;</type>
      <name>graphCopy</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g7994b98b1f2edc832624300f6f221b6b</anchor>
      <arglist>(const From &amp;from, To &amp;to)</arglist>
    </member>
    <member kind="function">
      <type>Graph::Arc</type>
      <name>findArc</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g34cdf06e17726835e18d323cebf28a6f</anchor>
      <arglist>(const Graph &amp;g, typename Graph::Node u, typename Graph::Node v, typename Graph::Arc prev=INVALID)</arglist>
    </member>
    <member kind="function">
      <type>Graph::Edge</type>
      <name>findEdge</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gd5e63432e1c9daddda31824bc1c3693f</anchor>
      <arglist>(const Graph &amp;g, typename Graph::Node u, typename Graph::Node v, typename Graph::Edge p=INVALID)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>counter.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00353</filename>
    <class kind="class">lemon::Counter</class>
    <class kind="class">lemon::NoCounter</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>cplex.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00354</filename>
    <includes id="a00355" name="cplex.h" local="no" imported="no">lemon/cplex.h</includes>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>cplex.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00355</filename>
    <includes id="a00378" name="lp_base.h" local="no" imported="no">lemon/lp_base.h</includes>
    <class kind="class">lemon::CplexEnv</class>
    <class kind="class">lemon::CplexEnv::LicenseError</class>
    <class kind="class">lemon::CplexBase</class>
    <class kind="class">lemon::CplexLp</class>
    <class kind="class">lemon::CplexMip</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>dfs.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00356</filename>
    <includes id="a00375" name="list_graph.h" local="no" imported="no">lemon/list_graph.h</includes>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <includes id="a00388" name="path.h" local="no" imported="no">lemon/path.h</includes>
    <class kind="struct">lemon::DfsDefaultTraits</class>
    <class kind="class">lemon::Dfs</class>
    <class kind="struct">lemon::Dfs::SetPredMap</class>
    <class kind="struct">lemon::Dfs::SetDistMap</class>
    <class kind="struct">lemon::Dfs::SetReachedMap</class>
    <class kind="struct">lemon::Dfs::SetProcessedMap</class>
    <class kind="struct">lemon::Dfs::SetStandardProcessedMap</class>
    <class kind="struct">lemon::DfsWizardDefaultTraits</class>
    <class kind="class">lemon::DfsWizardBase</class>
    <class kind="class">lemon::DfsWizard</class>
    <class kind="struct">lemon::DfsVisitor</class>
    <class kind="struct">lemon::DfsVisitDefaultTraits</class>
    <class kind="class">lemon::DfsVisit</class>
    <class kind="struct">lemon::DfsVisit::SetReachedMap</class>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>DfsWizard&lt; DfsWizardBase&lt; GR &gt; &gt;</type>
      <name>dfs</name>
      <anchorfile>a00436.html</anchorfile>
      <anchor>g1926710bf80700bdeab0fb2e16830999</anchor>
      <arglist>(const GR &amp;digraph)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>dijkstra.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00357</filename>
    <includes id="a00375" name="list_graph.h" local="no" imported="no">lemon/list_graph.h</includes>
    <includes id="a00321" name="bin_heap.h" local="no" imported="no">lemon/bin_heap.h</includes>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <includes id="a00388" name="path.h" local="no" imported="no">lemon/path.h</includes>
    <class kind="struct">lemon::DijkstraDefaultOperationTraits</class>
    <class kind="struct">lemon::DijkstraDefaultTraits</class>
    <class kind="class">lemon::Dijkstra</class>
    <class kind="struct">lemon::Dijkstra::SetPredMap</class>
    <class kind="struct">lemon::Dijkstra::SetDistMap</class>
    <class kind="struct">lemon::Dijkstra::SetProcessedMap</class>
    <class kind="struct">lemon::Dijkstra::SetStandardProcessedMap</class>
    <class kind="struct">lemon::Dijkstra::SetHeap</class>
    <class kind="struct">lemon::Dijkstra::SetStandardHeap</class>
    <class kind="struct">lemon::Dijkstra::SetOperationTraits</class>
    <class kind="struct">lemon::DijkstraWizardDefaultTraits</class>
    <class kind="class">lemon::DijkstraWizardBase</class>
    <class kind="class">lemon::DijkstraWizard</class>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>DijkstraWizard&lt; DijkstraWizardBase&lt; GR, LEN &gt; &gt;</type>
      <name>dijkstra</name>
      <anchorfile>a00437.html</anchorfile>
      <anchor>g6aa57523fe00e2b8fe2f5cd17dd15cea</anchor>
      <arglist>(const GR &amp;digraph, const LEN &amp;length)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>dim2.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00358</filename>
    <class kind="class">lemon::dim2::Point</class>
    <class kind="class">lemon::dim2::Box</class>
    <class kind="class">lemon::dim2::XMap</class>
    <class kind="class">lemon::dim2::ConstXMap</class>
    <class kind="class">lemon::dim2::YMap</class>
    <class kind="class">lemon::dim2::ConstYMap</class>
    <class kind="class">lemon::dim2::NormSquareMap</class>
    <namespace>lemon</namespace>
    <namespace>lemon::dim2</namespace>
  </compound>
  <compound kind="file">
    <name>dimacs.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00359</filename>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
    <class kind="struct">lemon::DimacsDescriptor</class>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>DimacsDescriptor</type>
      <name>dimacsType</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>gd16c7910d72fa073be372881e1b3b087</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsMin</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>ga024f017fecd676aae6a003661181f71</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, LowerMap &amp;lower, CapacityMap &amp;capacity, CostMap &amp;cost, SupplyMap &amp;supply, typename CapacityMap::Value infty=0, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsMax</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>g9504e359ce76c85263ccd7b87fc8dd60</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, CapacityMap &amp;capacity, typename Digraph::Node &amp;s, typename Digraph::Node &amp;t, typename CapacityMap::Value infty=0, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsSp</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>gf24991c9c8644dd9427ac69028672901</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, LengthMap &amp;length, typename Digraph::Node &amp;s, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsCap</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>gf6e66dd7b4241ac27f143a1c97f05fe8</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, CapacityMap &amp;capacity, typename CapacityMap::Value infty=0, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsMat</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>g76a92aefd2b18d6d43c450a7e5930f5f</anchor>
      <arglist>(std::istream &amp;is, Graph &amp;g, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>writeDimacsMat</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>g268c7554ea91945bf8f29e308fef5e81</anchor>
      <arglist>(std::ostream &amp;os, const Digraph &amp;g, std::string comment=&quot;&quot;)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>edge_set.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00360</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <class kind="class">lemon::ListArcSet</class>
    <class kind="class">lemon::ListEdgeSet</class>
    <class kind="class">lemon::SmartArcSet</class>
    <class kind="class">lemon::SmartEdgeSet</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>elevator.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00361</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <class kind="class">lemon::Elevator</class>
    <class kind="class">lemon::LinkedElevator</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>error.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00362</filename>
    <class kind="class">lemon::Exception</class>
    <class kind="class">lemon::IoError</class>
    <class kind="class">lemon::FormatError</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>euler.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00363</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00315" name="adaptors.h" local="no" imported="no">lemon/adaptors.h</includes>
    <includes id="a00351" name="connectivity.h" local="no" imported="no">lemon/connectivity.h</includes>
    <class kind="class">lemon::DiEulerIt</class>
    <class kind="class">lemon::EulerIt</class>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>bool</type>
      <name>eulerian</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gebb99bb6b830b9ef4836ee803fd62e52</anchor>
      <arglist>(const GR &amp;g)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>full_graph.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00364</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <class kind="class">lemon::FullDigraph</class>
    <class kind="class">lemon::FullGraph</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>glpk.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00365</filename>
    <includes id="a00366" name="glpk.h" local="no" imported="no">lemon/glpk.h</includes>
    <includes id="a00318" name="assert.h" local="no" imported="no">lemon/assert.h</includes>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>glpk.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00366</filename>
    <includes id="a00378" name="lp_base.h" local="no" imported="no">lemon/lp_base.h</includes>
    <class kind="class">lemon::GlpkBase</class>
    <class kind="class">lemon::GlpkLp</class>
    <class kind="class">lemon::GlpkMip</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>gomory_hu.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00367</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00390" name="preflow.h" local="no" imported="no">lemon/preflow.h</includes>
    <includes id="a00345" name="concept_check.h" local="no" imported="no">lemon/concept_check.h</includes>
    <includes id="a00382" name="maps.h" local="no" imported="no">lemon/concepts/maps.h</includes>
    <class kind="class">lemon::GomoryHu</class>
    <class kind="class">lemon::GomoryHu::MinCutNodeIt</class>
    <class kind="class">lemon::GomoryHu::MinCutEdgeIt</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>graph_to_eps.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00368</filename>
    <includes id="a00384" name="math.h" local="no" imported="no">lemon/math.h</includes>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00358" name="dim2.h" local="no" imported="no">lemon/dim2.h</includes>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <includes id="a00344" name="color.h" local="no" imported="no">lemon/color.h</includes>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
    <class kind="struct">lemon::DefaultGraphToEpsTraits</class>
    <class kind="class">lemon::GraphToEps</class>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>GraphToEps&lt; DefaultGraphToEpsTraits&lt; GR &gt; &gt;</type>
      <name>graphToEps</name>
      <anchorfile>a00454.html</anchorfile>
      <anchor>gc54dccfb4d46de3d6457ea3b1cd62671</anchor>
      <arglist>(GR &amp;g, std::ostream &amp;os=std::cout)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; DefaultGraphToEpsTraits&lt; GR &gt; &gt;</type>
      <name>graphToEps</name>
      <anchorfile>a00454.html</anchorfile>
      <anchor>g807a9417676a0810788049f75d9e671e</anchor>
      <arglist>(GR &amp;g, const char *file_name)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; DefaultGraphToEpsTraits&lt; GR &gt; &gt;</type>
      <name>graphToEps</name>
      <anchorfile>a00454.html</anchorfile>
      <anchor>gd3c8aa0b4653e9e8eaa57f363083cd5c</anchor>
      <arglist>(GR &amp;g, const std::string &amp;file_name)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>grid_graph.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00369</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00358" name="dim2.h" local="no" imported="no">lemon/dim2.h</includes>
    <includes id="a00318" name="assert.h" local="no" imported="no">lemon/assert.h</includes>
    <class kind="class">lemon::GridGraph</class>
    <class kind="class">lemon::GridGraph::IndexMap</class>
    <class kind="class">lemon::GridGraph::ColMap</class>
    <class kind="class">lemon::GridGraph::RowMap</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>hao_orlin.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00370</filename>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00399" name="tolerance.h" local="no" imported="no">lemon/tolerance.h</includes>
    <class kind="class">lemon::HaoOrlin</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>hypercube_graph.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00371</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00318" name="assert.h" local="no" imported="no">lemon/assert.h</includes>
    <class kind="class">lemon::HypercubeGraph</class>
    <class kind="class">lemon::HypercubeGraph::HyperMap</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>kruskal.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00372</filename>
    <includes id="a00400" name="unionfind.h" local="no" imported="no">lemon/unionfind.h</includes>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>Value</type>
      <name>kruskal</name>
      <anchorfile>a00443.html</anchorfile>
      <anchor>ga0b63cab19a2f168772a13d7c59449eb</anchor>
      <arglist>(const Graph &amp;g, const In &amp;in, Out &amp;out)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>lgf_reader.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00373</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00374" name="lgf_writer.h" local="no" imported="no">lemon/lgf_writer.h</includes>
    <includes id="a00345" name="concept_check.h" local="no" imported="no">lemon/concept_check.h</includes>
    <includes id="a00382" name="maps.h" local="no" imported="no">lemon/concepts/maps.h</includes>
    <class kind="class">lemon::DigraphReader</class>
    <class kind="class">lemon::GraphReader</class>
    <class kind="class">lemon::SectionReader</class>
    <class kind="class">lemon::LgfContents</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>lgf_writer.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00374</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <includes id="a00345" name="concept_check.h" local="no" imported="no">lemon/concept_check.h</includes>
    <includes id="a00382" name="maps.h" local="no" imported="no">lemon/concepts/maps.h</includes>
    <class kind="class">lemon::DigraphWriter</class>
    <class kind="class">lemon::GraphWriter</class>
    <class kind="class">lemon::SectionWriter</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>list_graph.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00375</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
    <class kind="class">lemon::ListDigraph</class>
    <class kind="class">lemon::ListDigraph::Snapshot</class>
    <class kind="class">lemon::ListGraph</class>
    <class kind="class">lemon::ListGraph::Snapshot</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>lp.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00376</filename>
    <namespace>lemon</namespace>
    <member kind="define">
      <type>#define</type>
      <name>LEMON_DEFAULT_LP</name>
      <anchorfile>a00446.html</anchorfile>
      <anchor>g459ae538832b3817b7692a81de79d744</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LEMON_DEFAULT_MIP</name>
      <anchorfile>a00446.html</anchorfile>
      <anchor>ge4ce37e43b0032f13b3efa0e0b0af640</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GlpkLp</type>
      <name>Lp</name>
      <anchorfile>a00446.html</anchorfile>
      <anchor>g8c6461f78849b26ae8be11062410d043</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GlpkMip</type>
      <name>Mip</name>
      <anchorfile>a00446.html</anchorfile>
      <anchor>gd4ee17c56e133b01b9d75eb2cefd4d7f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>lp_base.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00377</filename>
    <includes id="a00378" name="lp_base.h" local="no" imported="no">lemon/lp_base.h</includes>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>lp_base.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00378</filename>
    <includes id="a00384" name="math.h" local="no" imported="no">lemon/math.h</includes>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
    <includes id="a00318" name="assert.h" local="no" imported="no">lemon/assert.h</includes>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <class kind="class">lemon::LpBase</class>
    <class kind="class">lemon::LpBase::Col</class>
    <class kind="class">lemon::LpBase::ColIt</class>
    <class kind="class">lemon::LpBase::Row</class>
    <class kind="class">lemon::LpBase::RowIt</class>
    <class kind="class">lemon::LpBase::Expr</class>
    <class kind="class">lemon::LpBase::Expr::CoeffIt</class>
    <class kind="class">lemon::LpBase::Expr::ConstCoeffIt</class>
    <class kind="class">lemon::LpBase::Constr</class>
    <class kind="class">lemon::LpBase::DualExpr</class>
    <class kind="class">lemon::LpBase::DualExpr::CoeffIt</class>
    <class kind="class">lemon::LpBase::DualExpr::ConstCoeffIt</class>
    <class kind="class">lemon::LpSolver</class>
    <class kind="class">lemon::MipSolver</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>lp_skeleton.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00379</filename>
    <includes id="a00380" name="lp_skeleton.h" local="no" imported="no">lemon/lp_skeleton.h</includes>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>lp_skeleton.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00380</filename>
    <includes id="a00378" name="lp_base.h" local="no" imported="no">lemon/lp_base.h</includes>
    <class kind="class">lemon::SkeletonSolverBase</class>
    <class kind="class">lemon::LpSkeleton</class>
    <class kind="class">lemon::MipSkeleton</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>maps.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00381</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <class kind="class">lemon::MapBase</class>
    <class kind="class">lemon::NullMap</class>
    <class kind="class">lemon::ConstMap</class>
    <class kind="class">lemon::ConstMap&lt; K, Const&lt; V, v &gt; &gt;</class>
    <class kind="class">lemon::IdentityMap</class>
    <class kind="class">lemon::RangeMap</class>
    <class kind="class">lemon::SparseMap</class>
    <class kind="class">lemon::ComposeMap</class>
    <class kind="class">lemon::CombineMap</class>
    <class kind="class">lemon::FunctorToMap</class>
    <class kind="class">lemon::MapToFunctor</class>
    <class kind="class">lemon::ConvertMap</class>
    <class kind="class">lemon::ForkMap</class>
    <class kind="class">lemon::AddMap</class>
    <class kind="class">lemon::SubMap</class>
    <class kind="class">lemon::MulMap</class>
    <class kind="class">lemon::DivMap</class>
    <class kind="class">lemon::ShiftMap</class>
    <class kind="class">lemon::ShiftWriteMap</class>
    <class kind="class">lemon::ScaleMap</class>
    <class kind="class">lemon::ScaleWriteMap</class>
    <class kind="class">lemon::NegMap</class>
    <class kind="class">lemon::NegWriteMap</class>
    <class kind="class">lemon::AbsMap</class>
    <class kind="class">lemon::TrueMap</class>
    <class kind="class">lemon::FalseMap</class>
    <class kind="class">lemon::AndMap</class>
    <class kind="class">lemon::OrMap</class>
    <class kind="class">lemon::NotMap</class>
    <class kind="class">lemon::NotWriteMap</class>
    <class kind="class">lemon::EqualMap</class>
    <class kind="class">lemon::LessMap</class>
    <class kind="class">lemon::LoggerBoolMap</class>
    <class kind="class">lemon::IdMap</class>
    <class kind="class">lemon::IdMap::InverseMap</class>
    <class kind="class">lemon::CrossRefMap</class>
    <class kind="class">lemon::CrossRefMap::ValueIterator</class>
    <class kind="class">lemon::CrossRefMap::InverseMap</class>
    <class kind="class">lemon::RangeIdMap</class>
    <class kind="class">lemon::RangeIdMap::InverseMap</class>
    <class kind="class">lemon::SourceMap</class>
    <class kind="class">lemon::TargetMap</class>
    <class kind="class">lemon::ForwardMap</class>
    <class kind="class">lemon::BackwardMap</class>
    <class kind="class">lemon::InDegMap</class>
    <class kind="class">lemon::OutDegMap</class>
    <class kind="class">lemon::PotentialDifferenceMap</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>maps.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/concepts/</path>
    <filename>a00382</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00345" name="concept_check.h" local="no" imported="no">lemon/concept_check.h</includes>
    <class kind="class">lemon::concepts::ReadMap</class>
    <class kind="class">lemon::concepts::WriteMap</class>
    <class kind="class">lemon::concepts::ReadWriteMap</class>
    <class kind="class">lemon::concepts::ReferenceMap</class>
    <namespace>lemon</namespace>
    <namespace>lemon::concepts</namespace>
  </compound>
  <compound kind="file">
    <name>matching.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00383</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00400" name="unionfind.h" local="no" imported="no">lemon/unionfind.h</includes>
    <includes id="a00321" name="bin_heap.h" local="no" imported="no">lemon/bin_heap.h</includes>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <class kind="class">lemon::MaxMatching</class>
    <class kind="class">lemon::MaxWeightedMatching</class>
    <class kind="class">lemon::MaxWeightedMatching::BlossomIt</class>
    <class kind="class">lemon::MaxWeightedPerfectMatching</class>
    <class kind="class">lemon::MaxWeightedPerfectMatching::BlossomIt</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>math.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00384</filename>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>bool</type>
      <name>isNaN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g78d99e4135230ce52d67f269ed1d464f</anchor>
      <arglist>(double v)</arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>E</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g72bb95cfe3f4109af43a989e478a2d61</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LOG2E</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g9cef1ca3f697ed0afa15e6ce2658b9cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LOG10E</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g044b3cac2493404bbd6bb04cf61dc38d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LN2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g64f768a3649a214be5a8b9d13acc30fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LN10</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gb62596f975434ae5b1dde456a64c455a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>PI</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gcf20630e5d2a9696928fe77b0726013c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>PI_2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g8d3eb5ff33b365b02e3d7065f2ecba48</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>PI_4</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g5a75e78cd42171bf864e2bad56639318</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>SQRT2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gc6586fa2865c0cc54dd89b93a0da1d17</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>SQRT1_2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g1794f2ffbd3e762771a25847b905918c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>min_cost_arborescence.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00385</filename>
    <includes id="a00375" name="list_graph.h" local="no" imported="no">lemon/list_graph.h</includes>
    <includes id="a00321" name="bin_heap.h" local="no" imported="no">lemon/bin_heap.h</includes>
    <includes id="a00318" name="assert.h" local="no" imported="no">lemon/assert.h</includes>
    <class kind="struct">lemon::MinCostArborescenceDefaultTraits</class>
    <class kind="class">lemon::MinCostArborescence</class>
    <class kind="struct">lemon::MinCostArborescence::SetArborescenceMap</class>
    <class kind="struct">lemon::MinCostArborescence::SetPredMap</class>
    <class kind="class">lemon::MinCostArborescence::DualIt</class>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>CostMap::Value</type>
      <name>minCostArborescence</name>
      <anchorfile>a00443.html</anchorfile>
      <anchor>g93a653979756bb0294981b82710a9dda</anchor>
      <arglist>(const Digraph &amp;digraph, const CostMap &amp;cost, typename Digraph::Node source, ArborescenceMap &amp;arborescence)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>nauty_reader.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00386</filename>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>readNautyGraph</name>
      <anchorfile>a00456.html</anchorfile>
      <anchor>g6f9f96dc6de37a5317cb6df5a1971600</anchor>
      <arglist>(Graph &amp;graph, std::istream &amp;is=std::cin)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>network_simplex.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00387</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00384" name="math.h" local="no" imported="no">lemon/math.h</includes>
    <class kind="class">lemon::NetworkSimplex</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>path.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00388</filename>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00389" name="path.h" local="no" imported="no">lemon/concepts/path.h</includes>
    <class kind="class">lemon::Path</class>
    <class kind="class">lemon::Path::ArcIt</class>
    <class kind="class">lemon::SimplePath</class>
    <class kind="class">lemon::SimplePath::ArcIt</class>
    <class kind="class">lemon::ListPath</class>
    <class kind="class">lemon::ListPath::ArcIt</class>
    <class kind="class">lemon::StaticPath</class>
    <class kind="class">lemon::StaticPath::ArcIt</class>
    <class kind="class">lemon::PathNodeIt</class>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>void</type>
      <name>pathCopy</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gcbf036a4279aaf3b2945cee2401984ba</anchor>
      <arglist>(const From &amp;from, To &amp;to)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copyPath</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gc8f2f7858c2a4a263abe861108decf65</anchor>
      <arglist>(To &amp;to, const From &amp;from)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>checkPath</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gd2ae6bbf77a7859394591ad8507e3232</anchor>
      <arglist>(const Digraph &amp;digraph, const Path &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>Digraph::Node</type>
      <name>pathSource</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gf2f386ebf2ada943f47b153d1d70ccd9</anchor>
      <arglist>(const Digraph &amp;digraph, const Path &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>Digraph::Node</type>
      <name>pathTarget</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gea6d1bb44b8be7929be3c7aded1317f9</anchor>
      <arglist>(const Digraph &amp;digraph, const Path &amp;path)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>path.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/concepts/</path>
    <filename>a00389</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00345" name="concept_check.h" local="no" imported="no">lemon/concept_check.h</includes>
    <class kind="class">lemon::concepts::Path</class>
    <class kind="class">lemon::concepts::Path::ArcIt</class>
    <class kind="class">lemon::concepts::PathDumper</class>
    <class kind="class">lemon::concepts::PathDumper::ArcIt</class>
    <class kind="class">lemon::concepts::PathDumper::RevArcIt</class>
    <namespace>lemon</namespace>
    <namespace>lemon::concepts</namespace>
  </compound>
  <compound kind="file">
    <name>preflow.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00390</filename>
    <includes id="a00399" name="tolerance.h" local="no" imported="no">lemon/tolerance.h</includes>
    <includes id="a00361" name="elevator.h" local="no" imported="no">lemon/elevator.h</includes>
    <class kind="struct">lemon::PreflowDefaultTraits</class>
    <class kind="class">lemon::Preflow</class>
    <class kind="struct">lemon::Preflow::SetFlowMap</class>
    <class kind="struct">lemon::Preflow::SetElevator</class>
    <class kind="struct">lemon::Preflow::SetStandardElevator</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>radix_sort.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00391</filename>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>void</type>
      <name>radixSort</name>
      <anchorfile>a00444.html</anchorfile>
      <anchor>gef447cc5a062480029a922daea269a89</anchor>
      <arglist>(Iterator first, Iterator last, Functor functor)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>stableRadixSort</name>
      <anchorfile>a00444.html</anchorfile>
      <anchor>g5f0a601b83783dc471f628eb2a1795c7</anchor>
      <arglist>(Iterator first, Iterator last, Functor functor)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>random.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00392</filename>
    <includes id="a00393" name="random.h" local="no" imported="no">lemon/random.h</includes>
    <namespace>lemon</namespace>
    <member kind="variable">
      <type>Random</type>
      <name>rnd</name>
      <anchorfile>a00406.html</anchorfile>
      <anchor>f55e529932608e88737901e3404d1d0e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>random.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00393</filename>
    <includes id="a00384" name="math.h" local="no" imported="no">lemon/math.h</includes>
    <includes id="a00358" name="dim2.h" local="no" imported="no">lemon/dim2.h</includes>
    <class kind="class">lemon::Random</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>smart_graph.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00394</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
    <class kind="class">lemon::SmartDigraphBase</class>
    <class kind="class">lemon::SmartDigraph</class>
    <class kind="class">lemon::SmartDigraph::Snapshot</class>
    <class kind="class">lemon::SmartGraph</class>
    <class kind="class">lemon::SmartGraph::Snapshot</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>soplex.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00395</filename>
    <includes id="a00396" name="soplex.h" local="no" imported="no">lemon/soplex.h</includes>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>soplex.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00396</filename>
    <includes id="a00378" name="lp_base.h" local="no" imported="no">lemon/lp_base.h</includes>
    <class kind="class">lemon::SoplexLp</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>suurballe.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00397</filename>
    <includes id="a00321" name="bin_heap.h" local="no" imported="no">lemon/bin_heap.h</includes>
    <includes id="a00388" name="path.h" local="no" imported="no">lemon/path.h</includes>
    <includes id="a00375" name="list_graph.h" local="no" imported="no">lemon/list_graph.h</includes>
    <includes id="a00381" name="maps.h" local="no" imported="no">lemon/maps.h</includes>
    <class kind="class">lemon::Suurballe</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>time_measure.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00398</filename>
    <class kind="class">lemon::TimeStamp</class>
    <class kind="class">lemon::Timer</class>
    <class kind="class">lemon::TimeReport</class>
    <class kind="class">lemon::NoTimeReport</class>
    <namespace>lemon</namespace>
    <member kind="function">
      <type>TimeStamp</type>
      <name>runningTimeTest</name>
      <anchorfile>a00450.html</anchorfile>
      <anchor>g43f5d1e50bae7c25870c29c0e08d3c09</anchor>
      <arglist>(F f, double min_time=10, unsigned int *num=NULL, TimeStamp *full_time=NULL)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>tolerance.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00399</filename>
    <class kind="class">lemon::Tolerance</class>
    <class kind="class">lemon::Tolerance&lt; float &gt;</class>
    <class kind="class">lemon::Tolerance&lt; double &gt;</class>
    <class kind="class">lemon::Tolerance&lt; long double &gt;</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>unionfind.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>a00400</filename>
    <includes id="a00352" name="core.h" local="no" imported="no">lemon/core.h</includes>
    <class kind="class">lemon::UnionFind</class>
    <class kind="class">lemon::UnionFindEnum</class>
    <class kind="class">lemon::UnionFindEnum::ClassIt</class>
    <class kind="class">lemon::UnionFindEnum::ItemIt</class>
    <class kind="class">lemon::ExtendFindEnum</class>
    <class kind="class">lemon::ExtendFindEnum::ClassIt</class>
    <class kind="class">lemon::ExtendFindEnum::ItemIt</class>
    <class kind="class">lemon::HeapUnionFind</class>
    <class kind="class">lemon::HeapUnionFind::ItemIt</class>
    <class kind="class">lemon::HeapUnionFind::ClassIt</class>
    <namespace>lemon</namespace>
  </compound>
  <compound kind="file">
    <name>test_tools.h</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/test/</path>
    <filename>a00401</filename>
    <member kind="define">
      <type>#define</type>
      <name>check</name>
      <anchorfile>a00401.html</anchorfile>
      <anchor>1acdc408ee02ffd13dfbc7457fa01383</anchor>
      <arglist>(rc, msg)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>dimacs-solver.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/tools/</path>
    <filename>a00402</filename>
    <includes id="a00394" name="smart_graph.h" local="no" imported="no">lemon/smart_graph.h</includes>
    <includes id="a00359" name="dimacs.h" local="no" imported="no">lemon/dimacs.h</includes>
    <includes id="a00374" name="lgf_writer.h" local="no" imported="no">lemon/lgf_writer.h</includes>
    <includes id="a00398" name="time_measure.h" local="no" imported="no">lemon/time_measure.h</includes>
    <includes id="a00317" name="arg_parser.h" local="no" imported="no">lemon/arg_parser.h</includes>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
    <includes id="a00357" name="dijkstra.h" local="no" imported="no">lemon/dijkstra.h</includes>
    <includes id="a00390" name="preflow.h" local="no" imported="no">lemon/preflow.h</includes>
    <includes id="a00383" name="matching.h" local="no" imported="no">lemon/matching.h</includes>
    <includes id="a00387" name="network_simplex.h" local="no" imported="no">lemon/network_simplex.h</includes>
  </compound>
  <compound kind="file">
    <name>dimacs-to-lgf.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/tools/</path>
    <filename>a00403</filename>
    <includes id="a00394" name="smart_graph.h" local="no" imported="no">lemon/smart_graph.h</includes>
    <includes id="a00359" name="dimacs.h" local="no" imported="no">lemon/dimacs.h</includes>
    <includes id="a00374" name="lgf_writer.h" local="no" imported="no">lemon/lgf_writer.h</includes>
    <includes id="a00317" name="arg_parser.h" local="no" imported="no">lemon/arg_parser.h</includes>
    <includes id="a00362" name="error.h" local="no" imported="no">lemon/error.h</includes>
  </compound>
  <compound kind="file">
    <name>lgf-gen.cc</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/tools/</path>
    <filename>a00404</filename>
    <includes id="a00375" name="list_graph.h" local="no" imported="no">lemon/list_graph.h</includes>
    <includes id="a00393" name="random.h" local="no" imported="no">lemon/random.h</includes>
    <includes id="a00358" name="dim2.h" local="no" imported="no">lemon/dim2.h</includes>
    <includes id="a00320" name="bfs.h" local="no" imported="no">lemon/bfs.h</includes>
    <includes id="a00353" name="counter.h" local="no" imported="no">lemon/counter.h</includes>
    <includes id="a00397" name="suurballe.h" local="no" imported="no">lemon/suurballe.h</includes>
    <includes id="a00368" name="graph_to_eps.h" local="no" imported="no">lemon/graph_to_eps.h</includes>
    <includes id="a00374" name="lgf_writer.h" local="no" imported="no">lemon/lgf_writer.h</includes>
    <includes id="a00317" name="arg_parser.h" local="no" imported="no">lemon/arg_parser.h</includes>
    <includes id="a00363" name="euler.h" local="no" imported="no">lemon/euler.h</includes>
    <includes id="a00384" name="math.h" local="no" imported="no">lemon/math.h</includes>
    <includes id="a00372" name="kruskal.h" local="no" imported="no">lemon/kruskal.h</includes>
    <includes id="a00398" name="time_measure.h" local="no" imported="no">lemon/time_measure.h</includes>
  </compound>
  <compound kind="page">
    <name>coding_style</name>
    <title>LEMON Coding Style</title>
    <filename>coding_style</filename>
    <docanchor file="coding_style">cs-files</docanchor>
    <docanchor file="coding_style">header-template</docanchor>
    <docanchor file="coding_style">cs-loc-var</docanchor>
    <docanchor file="coding_style">cs-func</docanchor>
    <docanchor file="coding_style">cs-funcs</docanchor>
    <docanchor file="coding_style">cs-excep</docanchor>
    <docanchor file="coding_style">cs-class</docanchor>
    <docanchor file="coding_style">naming_conv</docanchor>
    <docanchor file="coding_style">pri-loc-var</docanchor>
  </compound>
  <compound kind="page">
    <name>lgf-format</name>
    <title>LEMON Graph Format (LGF)</title>
    <filename>lgf-format</filename>
  </compound>
  <compound kind="page">
    <name>license</name>
    <title>License Terms</title>
    <filename>license</filename>
  </compound>
  <compound kind="page">
    <name>migration</name>
    <title>Migration from the 0.x Series</title>
    <filename>migration</filename>
    <docanchor file="migration">migration-search</docanchor>
    <docanchor file="migration">migration-lgf</docanchor>
    <docanchor file="migration">migration-graph</docanchor>
    <docanchor file="migration">migration-other</docanchor>
    <docanchor file="migration">migration-error</docanchor>
  </compound>
  <compound kind="page">
    <name>min_cost_flow</name>
    <title>Minimum Cost Flow Problem</title>
    <filename>min_cost_flow</filename>
    <docanchor file="min_cost_flow">mcf_def</docanchor>
    <docanchor file="min_cost_flow">mcf_algs</docanchor>
    <docanchor file="min_cost_flow">mcf_leq</docanchor>
    <docanchor file="min_cost_flow">mcf_dual</docanchor>
    <docanchor file="min_cost_flow">mcf_eq</docanchor>
  </compound>
  <compound kind="page">
    <name>named-param</name>
    <title>Named Parameters</title>
    <filename>named-param</filename>
    <docanchor file="named-param">named-templ-param</docanchor>
    <docanchor file="named-param">named-func-param</docanchor>
    <docanchor file="named-param">named-templ-func-param</docanchor>
    <docanchor file="named-param">traits-classes</docanchor>
  </compound>
  <compound kind="group">
    <name>datas</name>
    <title>Data Structures</title>
    <filename>a00427.html</filename>
    <subgroup>graphs</subgroup>
    <subgroup>maps</subgroup>
    <subgroup>paths</subgroup>
    <subgroup>auxdat</subgroup>
  </compound>
  <compound kind="group">
    <name>graphs</name>
    <title>Graph Structures</title>
    <filename>a00428.html</filename>
    <class kind="class">lemon::ListArcSet</class>
    <class kind="class">lemon::ListEdgeSet</class>
    <class kind="class">lemon::SmartArcSet</class>
    <class kind="class">lemon::SmartEdgeSet</class>
    <class kind="class">lemon::FullDigraph</class>
    <class kind="class">lemon::FullGraph</class>
    <class kind="class">lemon::GridGraph</class>
    <class kind="class">lemon::HypercubeGraph</class>
    <class kind="class">lemon::ListDigraph</class>
    <class kind="class">lemon::ListGraph</class>
    <class kind="class">lemon::SmartDigraph</class>
    <class kind="class">lemon::SmartGraph</class>
    <subgroup>graph_adaptors</subgroup>
    <file>edge_set.h</file>
    <file>full_graph.h</file>
    <file>grid_graph.h</file>
    <file>hypercube_graph.h</file>
    <file>list_graph.h</file>
    <file>smart_graph.h</file>
  </compound>
  <compound kind="group">
    <name>graph_adaptors</name>
    <title>Adaptor Classes for Graphs</title>
    <filename>a00429.html</filename>
    <class kind="class">lemon::ReverseDigraph</class>
    <class kind="class">lemon::SubDigraph</class>
    <class kind="class">lemon::SubGraph</class>
    <class kind="class">lemon::FilterNodes</class>
    <class kind="class">lemon::FilterArcs</class>
    <class kind="class">lemon::FilterEdges</class>
    <class kind="class">lemon::Undirector</class>
    <class kind="class">lemon::Orienter</class>
    <class kind="class">lemon::ResidualDigraph</class>
    <class kind="class">lemon::SplitNodes</class>
    <file>adaptors.h</file>
    <member kind="function">
      <type>ReverseDigraph&lt; const DGR &gt;</type>
      <name>reverseDigraph</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g218b793f2cd2640ee82e84e4e1570544</anchor>
      <arglist>(const DGR &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>SubDigraph&lt; const DGR, NF, AF &gt;</type>
      <name>subDigraph</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g062ef0ddcd96bf483cde2358f40b46da</anchor>
      <arglist>(const DGR &amp;digraph, NF &amp;node_filter, AF &amp;arc_filter)</arglist>
    </member>
    <member kind="function">
      <type>SubGraph&lt; const GR, NF, EF &gt;</type>
      <name>subGraph</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>gc662b1d237b24d56a04d3bd7a5a05774</anchor>
      <arglist>(const GR &amp;graph, NF &amp;node_filter, EF &amp;edge_filter)</arglist>
    </member>
    <member kind="function">
      <type>FilterNodes&lt; const GR, NF &gt;</type>
      <name>filterNodes</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g5dbf1ee0dfe1ad897d9b9f50dcc38ad0</anchor>
      <arglist>(const GR &amp;graph, NF &amp;node_filter)</arglist>
    </member>
    <member kind="function">
      <type>FilterArcs&lt; const DGR, AF &gt;</type>
      <name>filterArcs</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>ge354ced20aee296dfd57b9f647b1d71b</anchor>
      <arglist>(const DGR &amp;digraph, AF &amp;arc_filter)</arglist>
    </member>
    <member kind="function">
      <type>FilterEdges&lt; const GR, EF &gt;</type>
      <name>filterEdges</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g5fb6134ad6eb75a7f57dfffdb7953695</anchor>
      <arglist>(const GR &amp;graph, EF &amp;edge_filter)</arglist>
    </member>
    <member kind="function">
      <type>Undirector&lt; const DGR &gt;</type>
      <name>undirector</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g1dbbd6d361a509c0ed5ed9e2e9d2b1a3</anchor>
      <arglist>(const DGR &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>Orienter&lt; const GR, DM &gt;</type>
      <name>orienter</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g42eb576a82ec3211c968b00ab4c11698</anchor>
      <arglist>(const GR &amp;graph, DM &amp;direction)</arglist>
    </member>
    <member kind="function">
      <type>ResidualDigraph&lt; DGR, CM, FM &gt;</type>
      <name>residualDigraph</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g7d15101ba4085bf76c55ac167a2e7934</anchor>
      <arglist>(const DGR &amp;digraph, const CM &amp;capacity_map, FM &amp;flow_map)</arglist>
    </member>
    <member kind="function">
      <type>SplitNodes&lt; DGR &gt;</type>
      <name>splitNodes</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g89acf78045eaa7bbb164e4bc397638ed</anchor>
      <arglist>(const DGR &amp;digraph)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>maps</name>
    <title>Maps</title>
    <filename>a00430.html</filename>
    <class kind="class">lemon::MapBase</class>
    <class kind="class">lemon::NullMap</class>
    <class kind="class">lemon::ConstMap</class>
    <class kind="class">lemon::ConstMap&lt; K, Const&lt; V, v &gt; &gt;</class>
    <class kind="class">lemon::IdentityMap</class>
    <class kind="class">lemon::RangeMap</class>
    <class kind="class">lemon::SparseMap</class>
    <class kind="class">lemon::TrueMap</class>
    <class kind="class">lemon::FalseMap</class>
    <class kind="class">lemon::LoggerBoolMap</class>
    <subgroup>graph_maps</subgroup>
    <subgroup>map_adaptors</subgroup>
    <file>maps.h</file>
    <member kind="function">
      <type>NullMap&lt; K, V &gt;</type>
      <name>nullMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>gdc125dd59f361134ee8e5a59802ea2e2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>ConstMap&lt; K, V &gt;</type>
      <name>constMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g19fc5eeb6094d25f6c72e273e0b1e776</anchor>
      <arglist>(const V &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>ConstMap&lt; K, Const&lt; V, v &gt; &gt;</type>
      <name>constMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g1004623cdfc30601d45b45eff02294d0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>IdentityMap&lt; T &gt;</type>
      <name>identityMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g74d66a12052a62f85aaffa4fcdc17cb9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>RangeMap&lt; V &gt;</type>
      <name>rangeMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g6648195c0df738a0c3101524e55243dc</anchor>
      <arglist>(int size=0, const V &amp;value=V())</arglist>
    </member>
    <member kind="function">
      <type>RangeMap&lt; V &gt;</type>
      <name>rangeMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g9b43968e329c49b394424cdfd56e61a1</anchor>
      <arglist>(const std::vector&lt; V &gt; &amp;vector)</arglist>
    </member>
    <member kind="function">
      <type>SparseMap&lt; K, V, Compare &gt;</type>
      <name>sparseMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g22355d327475f1a36b63b7270b9f07b2</anchor>
      <arglist>(const V &amp;value=V())</arglist>
    </member>
    <member kind="function">
      <type>SparseMap&lt; K, V, Compare &gt;</type>
      <name>sparseMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g22955912798ba75ea8bca22e408b8807</anchor>
      <arglist>(const std::map&lt; K, V, Compare &gt; &amp;map, const V &amp;value=V())</arglist>
    </member>
    <member kind="function">
      <type>TrueMap&lt; K &gt;</type>
      <name>trueMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g9346464be299b8b31ec5319144ea5302</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>FalseMap&lt; K &gt;</type>
      <name>falseMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g30371344e499cec60db176b5b35f6cd0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>LoggerBoolMap&lt; Iterator &gt;</type>
      <name>loggerBoolMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g21ca379ec2c92eccd71b76df0a9eee8c</anchor>
      <arglist>(Iterator it)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>graph_maps</name>
    <title>Graph Maps</title>
    <filename>a00431.html</filename>
    <class kind="class">lemon::IdMap</class>
    <class kind="class">lemon::CrossRefMap</class>
    <class kind="class">lemon::RangeIdMap</class>
    <class kind="class">lemon::SourceMap</class>
    <class kind="class">lemon::TargetMap</class>
    <class kind="class">lemon::ForwardMap</class>
    <class kind="class">lemon::BackwardMap</class>
    <class kind="class">lemon::InDegMap</class>
    <class kind="class">lemon::OutDegMap</class>
    <class kind="class">lemon::PotentialDifferenceMap</class>
    <member kind="function">
      <type>SourceMap&lt; GR &gt;</type>
      <name>sourceMap</name>
      <anchorfile>a00431.html</anchorfile>
      <anchor>gee3916bbb46530ddc62cf39e1801325c</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>TargetMap&lt; GR &gt;</type>
      <name>targetMap</name>
      <anchorfile>a00431.html</anchorfile>
      <anchor>g96e38231848a32a3a8ec5626b2e68b3c</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>ForwardMap&lt; GR &gt;</type>
      <name>forwardMap</name>
      <anchorfile>a00431.html</anchorfile>
      <anchor>g9b42700e1bf75a2d81827c5d8e63cafb</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>BackwardMap&lt; GR &gt;</type>
      <name>backwardMap</name>
      <anchorfile>a00431.html</anchorfile>
      <anchor>g6e0ca8c5fa9c4bfb97a0d1bc8296fd54</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>PotentialDifferenceMap&lt; GR, POT &gt;</type>
      <name>potentialDifferenceMap</name>
      <anchorfile>a00431.html</anchorfile>
      <anchor>g06e9797d62087031a2a1baedefafbadf</anchor>
      <arglist>(const GR &amp;gr, const POT &amp;potential)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>map_adaptors</name>
    <title>Map Adaptors</title>
    <filename>a00432.html</filename>
    <class kind="class">lemon::ComposeMap</class>
    <class kind="class">lemon::CombineMap</class>
    <class kind="class">lemon::FunctorToMap</class>
    <class kind="class">lemon::MapToFunctor</class>
    <class kind="class">lemon::ConvertMap</class>
    <class kind="class">lemon::ForkMap</class>
    <class kind="class">lemon::AddMap</class>
    <class kind="class">lemon::SubMap</class>
    <class kind="class">lemon::MulMap</class>
    <class kind="class">lemon::DivMap</class>
    <class kind="class">lemon::ShiftMap</class>
    <class kind="class">lemon::ShiftWriteMap</class>
    <class kind="class">lemon::ScaleMap</class>
    <class kind="class">lemon::ScaleWriteMap</class>
    <class kind="class">lemon::NegMap</class>
    <class kind="class">lemon::NegWriteMap</class>
    <class kind="class">lemon::AbsMap</class>
    <class kind="class">lemon::AndMap</class>
    <class kind="class">lemon::OrMap</class>
    <class kind="class">lemon::NotMap</class>
    <class kind="class">lemon::NotWriteMap</class>
    <class kind="class">lemon::EqualMap</class>
    <class kind="class">lemon::LessMap</class>
    <member kind="function">
      <type>ComposeMap&lt; M1, M2 &gt;</type>
      <name>composeMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g168cb183ff15d3610151ab5044c70ce2</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>CombineMap&lt; M1, M2, F, V &gt;</type>
      <name>combineMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gb2f07629cac63828dbdab26e754e0767</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2, const F &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>FunctorToMap&lt; F, K, V &gt;</type>
      <name>functorToMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g963668011830914036e0246f7ee060ac</anchor>
      <arglist>(const F &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>MapToFunctor&lt; M &gt;</type>
      <name>mapToFunctor</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g9141e0ff40567f9d640db2c22485c28a</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>ConvertMap&lt; M, V &gt;</type>
      <name>convertMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g47b4b4266339a3a8178c64d70958246d</anchor>
      <arglist>(const M &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>ForkMap&lt; M1, M2 &gt;</type>
      <name>forkMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g09ff2fa37a6ca994c6d79a21942a50d2</anchor>
      <arglist>(M1 &amp;m1, M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>AddMap&lt; M1, M2 &gt;</type>
      <name>addMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gbb8ca6bab7c32b12e1671497ac774d5d</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>SubMap&lt; M1, M2 &gt;</type>
      <name>subMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gd1a93a51897caa4c921635a9fa51cef9</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>MulMap&lt; M1, M2 &gt;</type>
      <name>mulMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>ge53cd3bc6adcb7381407f33f065142a5</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>DivMap&lt; M1, M2 &gt;</type>
      <name>divMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g5ed3699166ac2cb68e69d4722ebc4df7</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>ShiftMap&lt; M, C &gt;</type>
      <name>shiftMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g179d41790ba45a63238666fd8437f763</anchor>
      <arglist>(const M &amp;m, const C &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>ShiftWriteMap&lt; M, C &gt;</type>
      <name>shiftWriteMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gd25e797238b5fe75bec91c1937c2f99d</anchor>
      <arglist>(M &amp;m, const C &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>ScaleMap&lt; M, C &gt;</type>
      <name>scaleMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g50af6b7bd9b56fa08bdda3e4e84a9541</anchor>
      <arglist>(const M &amp;m, const C &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>ScaleWriteMap&lt; M, C &gt;</type>
      <name>scaleWriteMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>ga23c144c639d693478a6660cf388cf87</anchor>
      <arglist>(M &amp;m, const C &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>NegMap&lt; M &gt;</type>
      <name>negMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g8d293b8674b498629cd4f5cd73431330</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>NegWriteMap&lt; M &gt;</type>
      <name>negWriteMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gb8b568a6081147ff0df4d6720095a80d</anchor>
      <arglist>(M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>AbsMap&lt; M &gt;</type>
      <name>absMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>ga3f892034fca5ba41151aebdba69e41c</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>AndMap&lt; M1, M2 &gt;</type>
      <name>andMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g33f98a39433f441c03b1945d2de4403f</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>OrMap&lt; M1, M2 &gt;</type>
      <name>orMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g87d0aa36f6a8a59b23f692d03376761d</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>NotMap&lt; M &gt;</type>
      <name>notMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g03804878900222f6828e1ee4f921a083</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>NotWriteMap&lt; M &gt;</type>
      <name>notWriteMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g6d3efe02540c14e706214993a061111e</anchor>
      <arglist>(M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>EqualMap&lt; M1, M2 &gt;</type>
      <name>equalMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gb064d43b2f6ccb5ea5fe99ba5f53015d</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>LessMap&lt; M1, M2 &gt;</type>
      <name>lessMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g6d57040954dfe0188f02540fe8cf3319</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>paths</name>
    <title>Path Structures</title>
    <filename>a00433.html</filename>
    <class kind="class">lemon::Path</class>
    <class kind="class">lemon::SimplePath</class>
    <class kind="class">lemon::ListPath</class>
    <class kind="class">lemon::StaticPath</class>
    <class kind="class">lemon::PathNodeIt</class>
    <file>path.h</file>
    <member kind="function">
      <type>void</type>
      <name>pathCopy</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gcbf036a4279aaf3b2945cee2401984ba</anchor>
      <arglist>(const From &amp;from, To &amp;to)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copyPath</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gc8f2f7858c2a4a263abe861108decf65</anchor>
      <arglist>(To &amp;to, const From &amp;from)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>checkPath</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gd2ae6bbf77a7859394591ad8507e3232</anchor>
      <arglist>(const Digraph &amp;digraph, const Path &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>Digraph::Node</type>
      <name>pathSource</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gf2f386ebf2ada943f47b153d1d70ccd9</anchor>
      <arglist>(const Digraph &amp;digraph, const Path &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>Digraph::Node</type>
      <name>pathTarget</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gea6d1bb44b8be7929be3c7aded1317f9</anchor>
      <arglist>(const Digraph &amp;digraph, const Path &amp;path)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>auxdat</name>
    <title>Auxiliary Data Structures</title>
    <filename>a00434.html</filename>
    <class kind="class">lemon::BinHeap</class>
    <class kind="class">lemon::Elevator</class>
    <class kind="class">lemon::LinkedElevator</class>
    <class kind="class">lemon::UnionFind</class>
    <class kind="class">lemon::UnionFindEnum</class>
    <class kind="class">lemon::ExtendFindEnum</class>
    <class kind="class">lemon::HeapUnionFind</class>
    <file>bin_heap.h</file>
    <file>elevator.h</file>
    <file>unionfind.h</file>
  </compound>
  <compound kind="group">
    <name>algs</name>
    <title>Algorithms</title>
    <filename>a00435.html</filename>
    <subgroup>search</subgroup>
    <subgroup>shortest_path</subgroup>
    <subgroup>max_flow</subgroup>
    <subgroup>min_cost_flow_algs</subgroup>
    <subgroup>min_cut</subgroup>
    <subgroup>graph_properties</subgroup>
    <subgroup>matching</subgroup>
    <subgroup>spantree</subgroup>
    <subgroup>auxalg</subgroup>
  </compound>
  <compound kind="group">
    <name>search</name>
    <title>Graph Search</title>
    <filename>a00436.html</filename>
    <class kind="class">lemon::Bfs</class>
    <class kind="class">lemon::BfsVisit</class>
    <class kind="class">lemon::Dfs</class>
    <class kind="class">lemon::DfsVisit</class>
    <file>bfs.h</file>
    <file>dfs.h</file>
    <member kind="function">
      <type>BfsWizard&lt; BfsWizardBase&lt; GR &gt; &gt;</type>
      <name>bfs</name>
      <anchorfile>a00436.html</anchorfile>
      <anchor>g437c3771dd4592f7d7a2d93fa2e5f54c</anchor>
      <arglist>(const GR &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>DfsWizard&lt; DfsWizardBase&lt; GR &gt; &gt;</type>
      <name>dfs</name>
      <anchorfile>a00436.html</anchorfile>
      <anchor>g1926710bf80700bdeab0fb2e16830999</anchor>
      <arglist>(const GR &amp;digraph)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>shortest_path</name>
    <title>Shortest Path Algorithms</title>
    <filename>a00437.html</filename>
    <class kind="class">lemon::Dijkstra</class>
    <class kind="class">lemon::Suurballe</class>
    <file>dijkstra.h</file>
    <file>suurballe.h</file>
    <member kind="function">
      <type>DijkstraWizard&lt; DijkstraWizardBase&lt; GR, LEN &gt; &gt;</type>
      <name>dijkstra</name>
      <anchorfile>a00437.html</anchorfile>
      <anchor>g6aa57523fe00e2b8fe2f5cd17dd15cea</anchor>
      <arglist>(const GR &amp;digraph, const LEN &amp;length)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>max_flow</name>
    <title>Maximum Flow Algorithms</title>
    <filename>a00438.html</filename>
    <class kind="class">lemon::Circulation</class>
    <class kind="class">lemon::Preflow</class>
    <file>circulation.h</file>
    <file>preflow.h</file>
  </compound>
  <compound kind="group">
    <name>min_cost_flow_algs</name>
    <title>Minimum Cost Flow Algorithms</title>
    <filename>a00439.html</filename>
    <class kind="class">lemon::NetworkSimplex</class>
    <file>network_simplex.h</file>
  </compound>
  <compound kind="group">
    <name>min_cut</name>
    <title>Minimum Cut Algorithms</title>
    <filename>a00440.html</filename>
    <class kind="class">lemon::GomoryHu</class>
    <class kind="class">lemon::HaoOrlin</class>
    <file>gomory_hu.h</file>
    <file>hao_orlin.h</file>
  </compound>
  <compound kind="group">
    <name>graph_properties</name>
    <title>Connectivity and Other Graph Properties</title>
    <filename>a00441.html</filename>
    <class kind="class">lemon::EulerIt</class>
    <file>connectivity.h</file>
    <file>euler.h</file>
    <member kind="function">
      <type>bool</type>
      <name>connected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g0adc4e80ce6ab29c538d9f2e99c23f5b</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g5c5a64cc9a7819f0cda6f94ac85d4495</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>connectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g9a6eab5d28f4dcc406609f1bc52bde78</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>stronglyConnected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>ga2cf073c58bca9a33472d75a904e709f</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countStronglyConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g6cd9776000cff151b9a73cc7d43cfee6</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>stronglyConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g155081f6d53c0c7d0ab96d229350004c</anchor>
      <arglist>(const Digraph &amp;digraph, NodeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>stronglyConnectedCutArcs</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g188f683e6621295b4cc6d4a9053a5216</anchor>
      <arglist>(const Digraph &amp;digraph, ArcMap &amp;cutMap)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>biNodeConnected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gc893f803a3736c18cc21a07ef29d0a0e</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countBiNodeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g355802e6b150b1f7ca1870380d32a14f</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biNodeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g04846c92189759d768aed68b713304b3</anchor>
      <arglist>(const Graph &amp;graph, EdgeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biNodeConnectedCutNodes</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g850219005a529d78a429f67b72b4828c</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;cutMap)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>biEdgeConnected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>ga4116e769789e5eb612e45771e1044a7</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countBiEdgeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g0c00ba5193699d8ed03105c0c214b375</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biEdgeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g8858bd6adf73e7cb0762058ecc636f79</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biEdgeConnectedCutEdges</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gfc18050604b516d4583e7595c5476078</anchor>
      <arglist>(const Graph &amp;graph, EdgeMap &amp;cutMap)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>dag</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g05008850803855835013e430a44f1402</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>topologicalSort</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g67cdef21788938be90604624f5af8569</anchor>
      <arglist>(const Digraph &amp;digraph, NodeMap &amp;order)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>checkedTopologicalSort</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g87256d2b4d6902b607c0deecfebcaa07</anchor>
      <arglist>(const Digraph &amp;digraph, NodeMap &amp;order)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>acyclic</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gcec2620760ea875a3ab17d6b26086499</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>tree</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g311ec89ea618e3453a14488dd3f75173</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>bipartite</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gcf98bfb7e52e8fa13bd9eac09703803f</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>bipartitePartitions</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gc6a525e6f95ec1d1847270befb55f12a</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;partMap)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>loopFree</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g96d709ca1f43a692fd9bde016eb05879</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>parallelFree</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g3fc80473f0dae370509c95322997d2f4</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>simpleGraph</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g4270a55a61dcc189e25331c7387eed98</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>eulerian</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gebb99bb6b830b9ef4836ee803fd62e52</anchor>
      <arglist>(const GR &amp;g)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>matching</name>
    <title>Matching Algorithms</title>
    <filename>a00442.html</filename>
    <class kind="class">lemon::MaxMatching</class>
    <class kind="class">lemon::MaxWeightedMatching</class>
    <class kind="class">lemon::MaxWeightedPerfectMatching</class>
    <file>matching.h</file>
  </compound>
  <compound kind="group">
    <name>spantree</name>
    <title>Minimum Spanning Tree Algorithms</title>
    <filename>a00443.html</filename>
    <class kind="class">lemon::MinCostArborescence</class>
    <file>kruskal.h</file>
    <file>min_cost_arborescence.h</file>
    <member kind="function">
      <type>Value</type>
      <name>kruskal</name>
      <anchorfile>a00443.html</anchorfile>
      <anchor>ga0b63cab19a2f168772a13d7c59449eb</anchor>
      <arglist>(const Graph &amp;g, const In &amp;in, Out &amp;out)</arglist>
    </member>
    <member kind="function">
      <type>CostMap::Value</type>
      <name>minCostArborescence</name>
      <anchorfile>a00443.html</anchorfile>
      <anchor>g93a653979756bb0294981b82710a9dda</anchor>
      <arglist>(const Digraph &amp;digraph, const CostMap &amp;cost, typename Digraph::Node source, ArborescenceMap &amp;arborescence)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>auxalg</name>
    <title>Auxiliary Algorithms</title>
    <filename>a00444.html</filename>
    <file>radix_sort.h</file>
    <member kind="function">
      <type>void</type>
      <name>radixSort</name>
      <anchorfile>a00444.html</anchorfile>
      <anchor>gef447cc5a062480029a922daea269a89</anchor>
      <arglist>(Iterator first, Iterator last, Functor functor)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>stableRadixSort</name>
      <anchorfile>a00444.html</anchorfile>
      <anchor>g5f0a601b83783dc471f628eb2a1795c7</anchor>
      <arglist>(Iterator first, Iterator last, Functor functor)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>gen_opt_group</name>
    <title>General Optimization Tools</title>
    <filename>a00445.html</filename>
    <subgroup>lp_group</subgroup>
  </compound>
  <compound kind="group">
    <name>lp_group</name>
    <title>Lp and Mip Solvers</title>
    <filename>a00446.html</filename>
    <class kind="class">lemon::CbcMip</class>
    <class kind="class">lemon::ClpLp</class>
    <class kind="class">lemon::CplexBase</class>
    <class kind="class">lemon::CplexLp</class>
    <class kind="class">lemon::CplexMip</class>
    <class kind="class">lemon::GlpkBase</class>
    <class kind="class">lemon::GlpkLp</class>
    <class kind="class">lemon::GlpkMip</class>
    <class kind="class">lemon::LpBase</class>
    <class kind="class">lemon::LpSolver</class>
    <class kind="class">lemon::MipSolver</class>
    <class kind="class">lemon::LpSkeleton</class>
    <class kind="class">lemon::MipSkeleton</class>
    <class kind="class">lemon::SoplexLp</class>
    <file>cbc.h</file>
    <file>glpk.h</file>
    <file>lp.h</file>
    <file>lp_base.h</file>
    <member kind="define">
      <type>#define</type>
      <name>LEMON_DEFAULT_LP</name>
      <anchorfile>a00446.html</anchorfile>
      <anchor>g459ae538832b3817b7692a81de79d744</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LEMON_DEFAULT_MIP</name>
      <anchorfile>a00446.html</anchorfile>
      <anchor>ge4ce37e43b0032f13b3efa0e0b0af640</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GlpkLp</type>
      <name>Lp</name>
      <anchorfile>a00446.html</anchorfile>
      <anchor>g8c6461f78849b26ae8be11062410d043</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GlpkMip</type>
      <name>Mip</name>
      <anchorfile>a00446.html</anchorfile>
      <anchor>gd4ee17c56e133b01b9d75eb2cefd4d7f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>utils</name>
    <title>Tools and Utilities</title>
    <filename>a00447.html</filename>
    <subgroup>gutils</subgroup>
    <subgroup>misc</subgroup>
    <subgroup>exceptions</subgroup>
  </compound>
  <compound kind="group">
    <name>gutils</name>
    <title>Basic Graph Utilities</title>
    <filename>a00448.html</filename>
    <class kind="class">lemon::DigraphCopy</class>
    <class kind="class">lemon::GraphCopy</class>
    <class kind="class">lemon::ConArcIt</class>
    <class kind="class">lemon::ConEdgeIt</class>
    <class kind="class">lemon::DynArcLookUp</class>
    <class kind="class">lemon::ArcLookUp</class>
    <class kind="class">lemon::AllArcLookUp</class>
    <member kind="define">
      <type>#define</type>
      <name>DIGRAPH_TYPEDEFS</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gc618f30ace596c69836144bfdcc9112c</anchor>
      <arglist>(Digraph)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TEMPLATE_DIGRAPH_TYPEDEFS</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g14ef90d8fd45b56f1cc2c5023c76c4b1</anchor>
      <arglist>(Digraph)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GRAPH_TYPEDEFS</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gb8c497e4257836f4669b8922237d830b</anchor>
      <arglist>(Graph)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TEMPLATE_GRAPH_TYPEDEFS</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gb0e6cf49071e60eda3e15e2b0e0d8310</anchor>
      <arglist>(Graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countItems</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g8b8052427edea53441499450c5416ef4</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countNodes</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g85863e6534999fb3acd27b93228b4798</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countArcs</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g5e562e0a53b64eca38a7a053d157dfa9</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countEdges</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gf8263f7f605f41abf9953693e81d6df5</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countOutArcs</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>ga448f6d1089195d57a7cb7649f5234ed</anchor>
      <arglist>(const Graph &amp;g, const typename Graph::Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countInArcs</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g494e3b2d5eeaed01cbe4958f1a6bc334</anchor>
      <arglist>(const Graph &amp;g, const typename Graph::Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countIncEdges</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g8e4f35523ceb5e8ec55262a6c9bc810b</anchor>
      <arglist>(const Graph &amp;g, const typename Graph::Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>DigraphCopy&lt; From, To &gt;</type>
      <name>digraphCopy</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g59011124e7760c273e82d6029313943c</anchor>
      <arglist>(const From &amp;from, To &amp;to)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy&lt; From, To &gt;</type>
      <name>graphCopy</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g7994b98b1f2edc832624300f6f221b6b</anchor>
      <arglist>(const From &amp;from, To &amp;to)</arglist>
    </member>
    <member kind="function">
      <type>Graph::Arc</type>
      <name>findArc</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g34cdf06e17726835e18d323cebf28a6f</anchor>
      <arglist>(const Graph &amp;g, typename Graph::Node u, typename Graph::Node v, typename Graph::Arc prev=INVALID)</arglist>
    </member>
    <member kind="function">
      <type>Graph::Edge</type>
      <name>findEdge</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gd5e63432e1c9daddda31824bc1c3693f</anchor>
      <arglist>(const Graph &amp;g, typename Graph::Node u, typename Graph::Node v, typename Graph::Edge p=INVALID)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>misc</name>
    <title>Miscellaneous Tools</title>
    <filename>a00449.html</filename>
    <class kind="class">lemon::ArgParser</class>
    <class kind="class">lemon::Color</class>
    <class kind="class">lemon::Palette</class>
    <class kind="class">lemon::dim2::Point</class>
    <class kind="class">lemon::dim2::Box</class>
    <class kind="class">lemon::dim2::XMap</class>
    <class kind="class">lemon::dim2::ConstXMap</class>
    <class kind="class">lemon::dim2::YMap</class>
    <class kind="class">lemon::dim2::ConstYMap</class>
    <class kind="class">lemon::dim2::NormSquareMap</class>
    <class kind="class">lemon::Random</class>
    <class kind="class">lemon::Tolerance</class>
    <class kind="class">lemon::Tolerance&lt; float &gt;</class>
    <class kind="class">lemon::Tolerance&lt; double &gt;</class>
    <class kind="class">lemon::Tolerance&lt; long double &gt;</class>
    <subgroup>timecount</subgroup>
    <file>arg_parser.h</file>
    <file>color.h</file>
    <file>dim2.h</file>
    <file>math.h</file>
    <file>random.h</file>
    <file>tolerance.h</file>
    <file>test_tools.h</file>
    <member kind="function">
      <type>Color</type>
      <name>distantColor</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gafbc768b20325e1e5048dcd2b10bc4e7</anchor>
      <arglist>(const Color &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>Color</type>
      <name>distantBW</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g22a184708d82339466dd08286e01a756</anchor>
      <arglist>(const Color &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isNaN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g78d99e4135230ce52d67f269ed1d464f</anchor>
      <arglist>(double v)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>makePoint</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gf2251ac71816d6d1a01977a41c1e6f5b</anchor>
      <arglist>(const T &amp;x, const T &amp;y)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>operator*</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gbfc65ded0f794f4e5903048252a8cc79</anchor>
      <arglist>(const T &amp;u, const Point&lt; T &gt; &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g2dd3eccf5dece76c03bc6d1c2f348643</anchor>
      <arglist>(std::istream &amp;is, Point&lt; T &gt; &amp;z)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g551d5cc0501d3499faff3e3ba728f32a</anchor>
      <arglist>(std::ostream &amp;os, const Point&lt; T &gt; &amp;z)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>rot90</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g3e6760608b4dfa6eb771a7c02bee5892</anchor>
      <arglist>(const Point&lt; T &gt; &amp;z)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>rot180</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g908068d1858d89712f7b55c3a0f2a375</anchor>
      <arglist>(const Point&lt; T &gt; &amp;z)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>rot270</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g729ffd49ccc19dcd23cb057b8c550e0a</anchor>
      <arglist>(const Point&lt; T &gt; &amp;z)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gd9dd0880428a70a38bc5d4daa1930da7</anchor>
      <arglist>(std::istream &amp;is, Box&lt; T &gt; &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>geec02f85273c8923fcb14c9a38df5eb1</anchor>
      <arglist>(std::ostream &amp;os, const Box&lt; T &gt; &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>XMap&lt; M &gt;</type>
      <name>xMap</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gd9409d1cd73ef6041dfd7e2ef985caad</anchor>
      <arglist>(M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>ConstXMap&lt; M &gt;</type>
      <name>xMap</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g6149b65448f13dabba937f9decbd6f20</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>YMap&lt; M &gt;</type>
      <name>yMap</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g42c015a3cec9a5cdaf8c76340e8a7570</anchor>
      <arglist>(M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>ConstYMap&lt; M &gt;</type>
      <name>yMap</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g45e163c72ccfdc68808cc69f803449ed</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>NormSquareMap&lt; M &gt;</type>
      <name>normSquareMap</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g6120711d506a51dddca09c0ea5607cb3</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>WHITE</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>ga574c6748d637031ff114ee5396f371d</anchor>
      <arglist>(1, 1, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>BLACK</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g887e77777b0cdd4bd98cd8582eab747d</anchor>
      <arglist>(0, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>RED</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g3aab05ed4f1fa1188cb5cec4a951da36</anchor>
      <arglist>(1, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>GREEN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g9d50a7cbc8c947f88556291754b964df</anchor>
      <arglist>(0, 1, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>BLUE</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g8d1bd8aebf1ea19b34a359b95afb2271</anchor>
      <arglist>(0, 0, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>YELLOW</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g7b3b82796993ff082f39aeaca4f74be9</anchor>
      <arglist>(1, 1, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>MAGENTA</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gd9c523e1fcd76b6e97a7e8f6c89a6d09</anchor>
      <arglist>(1, 0, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>CYAN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g6b139d00115defc76ec508dff90c91fd</anchor>
      <arglist>(0, 1, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>GREY</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gccc4c0904a38839f9554cde971bb4963</anchor>
      <arglist>(0, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_RED</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g1c9259e9d2ec9a44ea6bf855d3dd3917</anchor>
      <arglist>(.5, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_GREEN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gbc9fc012aa002dd8e311065b3115969c</anchor>
      <arglist>(0,.5, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_BLUE</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gc56cc8c77b9fcb56e308652d922d08f0</anchor>
      <arglist>(0, 0,.5)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_YELLOW</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g5dbe8dbe7b513bc66228981dcad165f7</anchor>
      <arglist>(.5,.5, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_MAGENTA</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>ga3457b13dd61099a849816a02ba55efc</anchor>
      <arglist>(.5, 0,.5)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_CYAN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g1814dea6aea83b28d9137adaa4d8b937</anchor>
      <arglist>(0,.5,.5)</arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>E</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g72bb95cfe3f4109af43a989e478a2d61</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LOG2E</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g9cef1ca3f697ed0afa15e6ce2658b9cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LOG10E</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g044b3cac2493404bbd6bb04cf61dc38d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LN2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g64f768a3649a214be5a8b9d13acc30fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LN10</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gb62596f975434ae5b1dde456a64c455a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>PI</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gcf20630e5d2a9696928fe77b0726013c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>PI_2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g8d3eb5ff33b365b02e3d7065f2ecba48</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>PI_4</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g5a75e78cd42171bf864e2bad56639318</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>SQRT2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gc6586fa2865c0cc54dd89b93a0da1d17</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>SQRT1_2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g1794f2ffbd3e762771a25847b905918c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>timecount</name>
    <title>Time Measuring and Counting</title>
    <filename>a00450.html</filename>
    <class kind="class">lemon::Counter</class>
    <class kind="class">lemon::NoCounter</class>
    <class kind="class">lemon::TimeStamp</class>
    <class kind="class">lemon::Timer</class>
    <class kind="class">lemon::TimeReport</class>
    <class kind="class">lemon::NoTimeReport</class>
    <file>counter.h</file>
    <file>time_measure.h</file>
    <member kind="function">
      <type>TimeStamp</type>
      <name>runningTimeTest</name>
      <anchorfile>a00450.html</anchorfile>
      <anchor>g43f5d1e50bae7c25870c29c0e08d3c09</anchor>
      <arglist>(F f, double min_time=10, unsigned int *num=NULL, TimeStamp *full_time=NULL)</arglist>
    </member>
    <member kind="friend">
      <type>friend std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>a00450.html</anchorfile>
      <anchor>g7a8fc6a89c07865a42b13d6a2bbcbbcb</anchor>
      <arglist>(std::ostream &amp;os, const TimeStamp &amp;t)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>exceptions</name>
    <title>Exceptions</title>
    <filename>a00451.html</filename>
    <class kind="class">lemon::Exception</class>
    <class kind="class">lemon::IoError</class>
    <class kind="class">lemon::FormatError</class>
    <file>assert.h</file>
    <file>error.h</file>
    <member kind="define">
      <type>#define</type>
      <name>LEMON_ASSERT</name>
      <anchorfile>a00451.html</anchorfile>
      <anchor>gf78cf5572d91896ceecdd970f55601bc</anchor>
      <arglist>(exp, msg)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LEMON_DEBUG</name>
      <anchorfile>a00451.html</anchorfile>
      <anchor>gcbefc04b0335938603649e96af183843</anchor>
      <arglist>(exp, msg)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>io_group</name>
    <title>Input-Output</title>
    <filename>a00452.html</filename>
    <subgroup>lemon_io</subgroup>
    <subgroup>eps_io</subgroup>
    <subgroup>dimacs_group</subgroup>
    <subgroup>nauty_group</subgroup>
  </compound>
  <compound kind="group">
    <name>lemon_io</name>
    <title>LEMON Graph Format</title>
    <filename>a00453.html</filename>
    <class kind="class">lemon::DigraphReader</class>
    <class kind="class">lemon::GraphReader</class>
    <class kind="class">lemon::SectionReader</class>
    <class kind="class">lemon::LgfContents</class>
    <class kind="class">lemon::DigraphWriter</class>
    <class kind="class">lemon::GraphWriter</class>
    <class kind="class">lemon::SectionWriter</class>
    <file>lgf_reader.h</file>
    <file>lgf_writer.h</file>
    <member kind="friend" protection="private">
      <type>friend DigraphReader&lt; TDGR &gt;</type>
      <name>digraphReader</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>g2f805968e7f23de95b1fd2452be1b6b9</anchor>
      <arglist>(TDGR &amp;digraph, std::istream &amp;is)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend GraphReader&lt; TGR &gt;</type>
      <name>graphReader</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>gf9cd86326f2bf28c0b7957ad0b94fc0b</anchor>
      <arglist>(TGR &amp;graph, std::istream &amp;is)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend SectionReader</type>
      <name>sectionReader</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>g919ce27a20ac3228c90a85310a64fa4c</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend DigraphWriter&lt; TDGR &gt;</type>
      <name>digraphWriter</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>gef02039f61d7e740650a1e8e6274257d</anchor>
      <arglist>(const TDGR &amp;digraph, std::ostream &amp;os)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend GraphWriter&lt; TGR &gt;</type>
      <name>graphWriter</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>g68ea72296c298c8b15e51ee6e3de6b4b</anchor>
      <arglist>(const TGR &amp;graph, std::ostream &amp;os)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend SectionWriter</type>
      <name>sectionWriter</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>g6bc4578acf71f56b06729191b8463779</anchor>
      <arglist>(std::ostream &amp;os)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>eps_io</name>
    <title>Postscript Exporting</title>
    <filename>a00454.html</filename>
    <file>graph_to_eps.h</file>
    <member kind="function">
      <type>GraphToEps&lt; DefaultGraphToEpsTraits&lt; GR &gt; &gt;</type>
      <name>graphToEps</name>
      <anchorfile>a00454.html</anchorfile>
      <anchor>gc54dccfb4d46de3d6457ea3b1cd62671</anchor>
      <arglist>(GR &amp;g, std::ostream &amp;os=std::cout)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; DefaultGraphToEpsTraits&lt; GR &gt; &gt;</type>
      <name>graphToEps</name>
      <anchorfile>a00454.html</anchorfile>
      <anchor>g807a9417676a0810788049f75d9e671e</anchor>
      <arglist>(GR &amp;g, const char *file_name)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; DefaultGraphToEpsTraits&lt; GR &gt; &gt;</type>
      <name>graphToEps</name>
      <anchorfile>a00454.html</anchorfile>
      <anchor>gd3c8aa0b4653e9e8eaa57f363083cd5c</anchor>
      <arglist>(GR &amp;g, const std::string &amp;file_name)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>dimacs_group</name>
    <title>DIMACS format</title>
    <filename>a00455.html</filename>
    <class kind="struct">lemon::DimacsDescriptor</class>
    <file>dimacs.h</file>
    <member kind="function">
      <type>DimacsDescriptor</type>
      <name>dimacsType</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>gd16c7910d72fa073be372881e1b3b087</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsMin</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>ga024f017fecd676aae6a003661181f71</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, LowerMap &amp;lower, CapacityMap &amp;capacity, CostMap &amp;cost, SupplyMap &amp;supply, typename CapacityMap::Value infty=0, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsMax</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>g9504e359ce76c85263ccd7b87fc8dd60</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, CapacityMap &amp;capacity, typename Digraph::Node &amp;s, typename Digraph::Node &amp;t, typename CapacityMap::Value infty=0, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsSp</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>gf24991c9c8644dd9427ac69028672901</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, LengthMap &amp;length, typename Digraph::Node &amp;s, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsCap</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>gf6e66dd7b4241ac27f143a1c97f05fe8</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, CapacityMap &amp;capacity, typename CapacityMap::Value infty=0, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsMat</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>g76a92aefd2b18d6d43c450a7e5930f5f</anchor>
      <arglist>(std::istream &amp;is, Graph &amp;g, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>writeDimacsMat</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>g268c7554ea91945bf8f29e308fef5e81</anchor>
      <arglist>(std::ostream &amp;os, const Digraph &amp;g, std::string comment=&quot;&quot;)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>nauty_group</name>
    <title>NAUTY Format</title>
    <filename>a00456.html</filename>
    <file>nauty_reader.h</file>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>readNautyGraph</name>
      <anchorfile>a00456.html</anchorfile>
      <anchor>g6f9f96dc6de37a5317cb6df5a1971600</anchor>
      <arglist>(Graph &amp;graph, std::istream &amp;is=std::cin)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>concept</name>
    <title>Concepts</title>
    <filename>a00457.html</filename>
    <class kind="class">lemon::concepts::Heap</class>
    <class kind="class">lemon::concepts::Path</class>
    <class kind="class">lemon::concepts::PathDumper</class>
    <subgroup>graph_concepts</subgroup>
    <subgroup>map_concepts</subgroup>
    <file>heap.h</file>
    <file>path.h</file>
  </compound>
  <compound kind="group">
    <name>graph_concepts</name>
    <title>Graph Structure Concepts</title>
    <filename>a00458.html</filename>
    <class kind="class">lemon::concepts::Digraph</class>
    <class kind="class">lemon::concepts::Graph</class>
    <file>digraph.h</file>
    <file>graph.h</file>
    <file>graph_components.h</file>
  </compound>
  <compound kind="group">
    <name>map_concepts</name>
    <title>Map Concepts</title>
    <filename>a00459.html</filename>
    <class kind="class">lemon::concepts::ReadMap</class>
    <class kind="class">lemon::concepts::WriteMap</class>
    <class kind="class">lemon::concepts::ReadWriteMap</class>
    <class kind="class">lemon::concepts::ReferenceMap</class>
    <file>maps.h</file>
  </compound>
  <compound kind="group">
    <name>demos</name>
    <title>Demo Programs</title>
    <filename>a00460.html</filename>
    <file>arg_parser_demo.cc</file>
    <file>graph_to_eps_demo.cc</file>
    <file>lgf_demo.cc</file>
    <docanchor file="a00460">demoprograms</docanchor>
  </compound>
  <compound kind="group">
    <name>tools</name>
    <title>Standalone Utility Applications</title>
    <filename>a00461.html</filename>
    <file>dimacs-solver.cc</file>
    <file>dimacs-to-lgf.cc</file>
    <file>lgf-gen.cc</file>
  </compound>
  <compound kind="namespace">
    <name>lemon</name>
    <filename>a00406.html</filename>
    <namespace>lemon::concepts</namespace>
    <namespace>lemon::dim2</namespace>
    <class kind="class">lemon::ReverseDigraph</class>
    <class kind="class">lemon::SubDigraph</class>
    <class kind="class">lemon::SubGraph</class>
    <class kind="class">lemon::FilterNodes</class>
    <class kind="class">lemon::FilterArcs</class>
    <class kind="class">lemon::FilterEdges</class>
    <class kind="class">lemon::Undirector</class>
    <class kind="class">lemon::Orienter</class>
    <class kind="class">lemon::ResidualDigraph</class>
    <class kind="class">lemon::SplitNodes</class>
    <class kind="class">lemon::ArgParser</class>
    <class kind="struct">lemon::BfsDefaultTraits</class>
    <class kind="class">lemon::Bfs</class>
    <class kind="struct">lemon::BfsWizardDefaultTraits</class>
    <class kind="class">lemon::BfsWizardBase</class>
    <class kind="class">lemon::BfsWizard</class>
    <class kind="struct">lemon::BfsVisitor</class>
    <class kind="struct">lemon::BfsVisitDefaultTraits</class>
    <class kind="class">lemon::BfsVisit</class>
    <class kind="class">lemon::BinHeap</class>
    <class kind="class">lemon::CbcMip</class>
    <class kind="struct">lemon::CirculationDefaultTraits</class>
    <class kind="class">lemon::Circulation</class>
    <class kind="class">lemon::ClpLp</class>
    <class kind="class">lemon::Color</class>
    <class kind="class">lemon::Palette</class>
    <class kind="struct">lemon::Invalid</class>
    <class kind="class">lemon::DigraphCopy</class>
    <class kind="class">lemon::GraphCopy</class>
    <class kind="class">lemon::ConArcIt</class>
    <class kind="class">lemon::ConEdgeIt</class>
    <class kind="class">lemon::DynArcLookUp</class>
    <class kind="class">lemon::ArcLookUp</class>
    <class kind="class">lemon::AllArcLookUp</class>
    <class kind="class">lemon::Counter</class>
    <class kind="class">lemon::NoCounter</class>
    <class kind="class">lemon::CplexEnv</class>
    <class kind="class">lemon::CplexBase</class>
    <class kind="class">lemon::CplexLp</class>
    <class kind="class">lemon::CplexMip</class>
    <class kind="struct">lemon::DfsDefaultTraits</class>
    <class kind="class">lemon::Dfs</class>
    <class kind="struct">lemon::DfsWizardDefaultTraits</class>
    <class kind="class">lemon::DfsWizardBase</class>
    <class kind="class">lemon::DfsWizard</class>
    <class kind="struct">lemon::DfsVisitor</class>
    <class kind="struct">lemon::DfsVisitDefaultTraits</class>
    <class kind="class">lemon::DfsVisit</class>
    <class kind="struct">lemon::DijkstraDefaultOperationTraits</class>
    <class kind="struct">lemon::DijkstraDefaultTraits</class>
    <class kind="class">lemon::Dijkstra</class>
    <class kind="struct">lemon::DijkstraWizardDefaultTraits</class>
    <class kind="class">lemon::DijkstraWizardBase</class>
    <class kind="class">lemon::DijkstraWizard</class>
    <class kind="struct">lemon::DimacsDescriptor</class>
    <class kind="class">lemon::ListArcSet</class>
    <class kind="class">lemon::ListEdgeSet</class>
    <class kind="class">lemon::SmartArcSet</class>
    <class kind="class">lemon::SmartEdgeSet</class>
    <class kind="class">lemon::Elevator</class>
    <class kind="class">lemon::LinkedElevator</class>
    <class kind="class">lemon::Exception</class>
    <class kind="class">lemon::IoError</class>
    <class kind="class">lemon::FormatError</class>
    <class kind="class">lemon::DiEulerIt</class>
    <class kind="class">lemon::EulerIt</class>
    <class kind="class">lemon::FullDigraph</class>
    <class kind="class">lemon::FullGraph</class>
    <class kind="class">lemon::GlpkBase</class>
    <class kind="class">lemon::GlpkLp</class>
    <class kind="class">lemon::GlpkMip</class>
    <class kind="class">lemon::GomoryHu</class>
    <class kind="struct">lemon::DefaultGraphToEpsTraits</class>
    <class kind="class">lemon::GraphToEps</class>
    <class kind="class">lemon::GridGraph</class>
    <class kind="class">lemon::HaoOrlin</class>
    <class kind="class">lemon::HypercubeGraph</class>
    <class kind="class">lemon::DigraphReader</class>
    <class kind="class">lemon::GraphReader</class>
    <class kind="class">lemon::SectionReader</class>
    <class kind="class">lemon::LgfContents</class>
    <class kind="class">lemon::DigraphWriter</class>
    <class kind="class">lemon::GraphWriter</class>
    <class kind="class">lemon::SectionWriter</class>
    <class kind="class">lemon::ListDigraph</class>
    <class kind="class">lemon::ListGraph</class>
    <class kind="class">lemon::LpBase</class>
    <class kind="class">lemon::LpSolver</class>
    <class kind="class">lemon::MipSolver</class>
    <class kind="class">lemon::SkeletonSolverBase</class>
    <class kind="class">lemon::LpSkeleton</class>
    <class kind="class">lemon::MipSkeleton</class>
    <class kind="class">lemon::MapBase</class>
    <class kind="class">lemon::NullMap</class>
    <class kind="class">lemon::ConstMap</class>
    <class kind="class">lemon::ConstMap&lt; K, Const&lt; V, v &gt; &gt;</class>
    <class kind="class">lemon::IdentityMap</class>
    <class kind="class">lemon::RangeMap</class>
    <class kind="class">lemon::SparseMap</class>
    <class kind="class">lemon::ComposeMap</class>
    <class kind="class">lemon::CombineMap</class>
    <class kind="class">lemon::FunctorToMap</class>
    <class kind="class">lemon::MapToFunctor</class>
    <class kind="class">lemon::ConvertMap</class>
    <class kind="class">lemon::ForkMap</class>
    <class kind="class">lemon::AddMap</class>
    <class kind="class">lemon::SubMap</class>
    <class kind="class">lemon::MulMap</class>
    <class kind="class">lemon::DivMap</class>
    <class kind="class">lemon::ShiftMap</class>
    <class kind="class">lemon::ShiftWriteMap</class>
    <class kind="class">lemon::ScaleMap</class>
    <class kind="class">lemon::ScaleWriteMap</class>
    <class kind="class">lemon::NegMap</class>
    <class kind="class">lemon::NegWriteMap</class>
    <class kind="class">lemon::AbsMap</class>
    <class kind="class">lemon::TrueMap</class>
    <class kind="class">lemon::FalseMap</class>
    <class kind="class">lemon::AndMap</class>
    <class kind="class">lemon::OrMap</class>
    <class kind="class">lemon::NotMap</class>
    <class kind="class">lemon::NotWriteMap</class>
    <class kind="class">lemon::EqualMap</class>
    <class kind="class">lemon::LessMap</class>
    <class kind="class">lemon::LoggerBoolMap</class>
    <class kind="class">lemon::IdMap</class>
    <class kind="class">lemon::CrossRefMap</class>
    <class kind="class">lemon::RangeIdMap</class>
    <class kind="class">lemon::SourceMap</class>
    <class kind="class">lemon::TargetMap</class>
    <class kind="class">lemon::ForwardMap</class>
    <class kind="class">lemon::BackwardMap</class>
    <class kind="class">lemon::InDegMap</class>
    <class kind="class">lemon::OutDegMap</class>
    <class kind="class">lemon::PotentialDifferenceMap</class>
    <class kind="class">lemon::MaxMatching</class>
    <class kind="class">lemon::MaxWeightedMatching</class>
    <class kind="class">lemon::MaxWeightedPerfectMatching</class>
    <class kind="struct">lemon::MinCostArborescenceDefaultTraits</class>
    <class kind="class">lemon::MinCostArborescence</class>
    <class kind="class">lemon::NetworkSimplex</class>
    <class kind="class">lemon::Path</class>
    <class kind="class">lemon::SimplePath</class>
    <class kind="class">lemon::ListPath</class>
    <class kind="class">lemon::StaticPath</class>
    <class kind="class">lemon::PathNodeIt</class>
    <class kind="struct">lemon::PreflowDefaultTraits</class>
    <class kind="class">lemon::Preflow</class>
    <class kind="class">lemon::Random</class>
    <class kind="class">lemon::SmartDigraphBase</class>
    <class kind="class">lemon::SmartDigraph</class>
    <class kind="class">lemon::SmartGraph</class>
    <class kind="class">lemon::SoplexLp</class>
    <class kind="class">lemon::Suurballe</class>
    <class kind="class">lemon::TimeStamp</class>
    <class kind="class">lemon::Timer</class>
    <class kind="class">lemon::TimeReport</class>
    <class kind="class">lemon::NoTimeReport</class>
    <class kind="class">lemon::Tolerance</class>
    <class kind="class">lemon::Tolerance&lt; float &gt;</class>
    <class kind="class">lemon::Tolerance&lt; double &gt;</class>
    <class kind="class">lemon::Tolerance&lt; long double &gt;</class>
    <class kind="class">lemon::UnionFind</class>
    <class kind="class">lemon::UnionFindEnum</class>
    <class kind="class">lemon::ExtendFindEnum</class>
    <class kind="class">lemon::HeapUnionFind</class>
    <member kind="typedef">
      <type>GlpkLp</type>
      <name>Lp</name>
      <anchorfile>a00446.html</anchorfile>
      <anchor>g8c6461f78849b26ae8be11062410d043</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GlpkMip</type>
      <name>Mip</name>
      <anchorfile>a00446.html</anchorfile>
      <anchor>gd4ee17c56e133b01b9d75eb2cefd4d7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>BfsWizard&lt; BfsWizardBase&lt; GR &gt; &gt;</type>
      <name>bfs</name>
      <anchorfile>a00436.html</anchorfile>
      <anchor>g437c3771dd4592f7d7a2d93fa2e5f54c</anchor>
      <arglist>(const GR &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>Color</type>
      <name>distantColor</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gafbc768b20325e1e5048dcd2b10bc4e7</anchor>
      <arglist>(const Color &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>Color</type>
      <name>distantBW</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g22a184708d82339466dd08286e01a756</anchor>
      <arglist>(const Color &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>function_requires</name>
      <anchorfile>a00406.html</anchorfile>
      <anchor>1928f6d1b464ab2e048c3c41f4f93389</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>checkConcept</name>
      <anchorfile>a00406.html</anchorfile>
      <anchor>6addf2ab7b723e4f2663c08b6a463033</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>connected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g0adc4e80ce6ab29c538d9f2e99c23f5b</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g5c5a64cc9a7819f0cda6f94ac85d4495</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>connectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g9a6eab5d28f4dcc406609f1bc52bde78</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>stronglyConnected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>ga2cf073c58bca9a33472d75a904e709f</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countStronglyConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g6cd9776000cff151b9a73cc7d43cfee6</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>stronglyConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g155081f6d53c0c7d0ab96d229350004c</anchor>
      <arglist>(const Digraph &amp;digraph, NodeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>stronglyConnectedCutArcs</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g188f683e6621295b4cc6d4a9053a5216</anchor>
      <arglist>(const Digraph &amp;digraph, ArcMap &amp;cutMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countBiNodeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g355802e6b150b1f7ca1870380d32a14f</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>biNodeConnected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gc893f803a3736c18cc21a07ef29d0a0e</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biNodeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g04846c92189759d768aed68b713304b3</anchor>
      <arglist>(const Graph &amp;graph, EdgeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biNodeConnectedCutNodes</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g850219005a529d78a429f67b72b4828c</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;cutMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countBiEdgeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g0c00ba5193699d8ed03105c0c214b375</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>biEdgeConnected</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>ga4116e769789e5eb612e45771e1044a7</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biEdgeConnectedComponents</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g8858bd6adf73e7cb0762058ecc636f79</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;compMap)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biEdgeConnectedCutEdges</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gfc18050604b516d4583e7595c5476078</anchor>
      <arglist>(const Graph &amp;graph, EdgeMap &amp;cutMap)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>dag</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g05008850803855835013e430a44f1402</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>topologicalSort</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g67cdef21788938be90604624f5af8569</anchor>
      <arglist>(const Digraph &amp;digraph, NodeMap &amp;order)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>checkedTopologicalSort</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g87256d2b4d6902b607c0deecfebcaa07</anchor>
      <arglist>(const Digraph &amp;digraph, NodeMap &amp;order)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>acyclic</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gcec2620760ea875a3ab17d6b26086499</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>tree</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g311ec89ea618e3453a14488dd3f75173</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>bipartite</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gcf98bfb7e52e8fa13bd9eac09703803f</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>bipartitePartitions</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gc6a525e6f95ec1d1847270befb55f12a</anchor>
      <arglist>(const Graph &amp;graph, NodeMap &amp;partMap)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>loopFree</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g96d709ca1f43a692fd9bde016eb05879</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>parallelFree</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g3fc80473f0dae370509c95322997d2f4</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>simpleGraph</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>g4270a55a61dcc189e25331c7387eed98</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countItems</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g8b8052427edea53441499450c5416ef4</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countNodes</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g85863e6534999fb3acd27b93228b4798</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countArcs</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g5e562e0a53b64eca38a7a053d157dfa9</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countEdges</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gf8263f7f605f41abf9953693e81d6df5</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countOutArcs</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>ga448f6d1089195d57a7cb7649f5234ed</anchor>
      <arglist>(const Graph &amp;g, const typename Graph::Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countInArcs</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g494e3b2d5eeaed01cbe4958f1a6bc334</anchor>
      <arglist>(const Graph &amp;g, const typename Graph::Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countIncEdges</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g8e4f35523ceb5e8ec55262a6c9bc810b</anchor>
      <arglist>(const Graph &amp;g, const typename Graph::Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>DigraphCopy&lt; From, To &gt;</type>
      <name>digraphCopy</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g59011124e7760c273e82d6029313943c</anchor>
      <arglist>(const From &amp;from, To &amp;to)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy&lt; From, To &gt;</type>
      <name>graphCopy</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g7994b98b1f2edc832624300f6f221b6b</anchor>
      <arglist>(const From &amp;from, To &amp;to)</arglist>
    </member>
    <member kind="function">
      <type>Graph::Arc</type>
      <name>findArc</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>g34cdf06e17726835e18d323cebf28a6f</anchor>
      <arglist>(const Graph &amp;g, typename Graph::Node u, typename Graph::Node v, typename Graph::Arc prev=INVALID)</arglist>
    </member>
    <member kind="function">
      <type>Graph::Edge</type>
      <name>findEdge</name>
      <anchorfile>a00448.html</anchorfile>
      <anchor>gd5e63432e1c9daddda31824bc1c3693f</anchor>
      <arglist>(const Graph &amp;g, typename Graph::Node u, typename Graph::Node v, typename Graph::Edge p=INVALID)</arglist>
    </member>
    <member kind="function">
      <type>DfsWizard&lt; DfsWizardBase&lt; GR &gt; &gt;</type>
      <name>dfs</name>
      <anchorfile>a00436.html</anchorfile>
      <anchor>g1926710bf80700bdeab0fb2e16830999</anchor>
      <arglist>(const GR &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>DijkstraWizard&lt; DijkstraWizardBase&lt; GR, LEN &gt; &gt;</type>
      <name>dijkstra</name>
      <anchorfile>a00437.html</anchorfile>
      <anchor>g6aa57523fe00e2b8fe2f5cd17dd15cea</anchor>
      <arglist>(const GR &amp;digraph, const LEN &amp;length)</arglist>
    </member>
    <member kind="function">
      <type>DimacsDescriptor</type>
      <name>dimacsType</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>gd16c7910d72fa073be372881e1b3b087</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsMin</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>ga024f017fecd676aae6a003661181f71</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, LowerMap &amp;lower, CapacityMap &amp;capacity, CostMap &amp;cost, SupplyMap &amp;supply, typename CapacityMap::Value infty=0, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsMax</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>g9504e359ce76c85263ccd7b87fc8dd60</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, CapacityMap &amp;capacity, typename Digraph::Node &amp;s, typename Digraph::Node &amp;t, typename CapacityMap::Value infty=0, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsSp</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>gf24991c9c8644dd9427ac69028672901</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, LengthMap &amp;length, typename Digraph::Node &amp;s, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsCap</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>gf6e66dd7b4241ac27f143a1c97f05fe8</anchor>
      <arglist>(std::istream &amp;is, Digraph &amp;g, CapacityMap &amp;capacity, typename CapacityMap::Value infty=0, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readDimacsMat</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>g76a92aefd2b18d6d43c450a7e5930f5f</anchor>
      <arglist>(std::istream &amp;is, Graph &amp;g, DimacsDescriptor desc=DimacsDescriptor())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>writeDimacsMat</name>
      <anchorfile>a00455.html</anchorfile>
      <anchor>g268c7554ea91945bf8f29e308fef5e81</anchor>
      <arglist>(std::ostream &amp;os, const Digraph &amp;g, std::string comment=&quot;&quot;)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>eulerian</name>
      <anchorfile>a00441.html</anchorfile>
      <anchor>gebb99bb6b830b9ef4836ee803fd62e52</anchor>
      <arglist>(const GR &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; DefaultGraphToEpsTraits&lt; GR &gt; &gt;</type>
      <name>graphToEps</name>
      <anchorfile>a00454.html</anchorfile>
      <anchor>gc54dccfb4d46de3d6457ea3b1cd62671</anchor>
      <arglist>(GR &amp;g, std::ostream &amp;os=std::cout)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; DefaultGraphToEpsTraits&lt; GR &gt; &gt;</type>
      <name>graphToEps</name>
      <anchorfile>a00454.html</anchorfile>
      <anchor>g807a9417676a0810788049f75d9e671e</anchor>
      <arglist>(GR &amp;g, const char *file_name)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; DefaultGraphToEpsTraits&lt; GR &gt; &gt;</type>
      <name>graphToEps</name>
      <anchorfile>a00454.html</anchorfile>
      <anchor>gd3c8aa0b4653e9e8eaa57f363083cd5c</anchor>
      <arglist>(GR &amp;g, const std::string &amp;file_name)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>kruskal</name>
      <anchorfile>a00443.html</anchorfile>
      <anchor>ga0b63cab19a2f168772a13d7c59449eb</anchor>
      <arglist>(const Graph &amp;g, const In &amp;in, Out &amp;out)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isNaN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g78d99e4135230ce52d67f269ed1d464f</anchor>
      <arglist>(double v)</arglist>
    </member>
    <member kind="function">
      <type>CostMap::Value</type>
      <name>minCostArborescence</name>
      <anchorfile>a00443.html</anchorfile>
      <anchor>g93a653979756bb0294981b82710a9dda</anchor>
      <arglist>(const Digraph &amp;digraph, const CostMap &amp;cost, typename Digraph::Node source, ArborescenceMap &amp;arborescence)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>readNautyGraph</name>
      <anchorfile>a00456.html</anchorfile>
      <anchor>g6f9f96dc6de37a5317cb6df5a1971600</anchor>
      <arglist>(Graph &amp;graph, std::istream &amp;is=std::cin)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>pathCopy</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gcbf036a4279aaf3b2945cee2401984ba</anchor>
      <arglist>(const From &amp;from, To &amp;to)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copyPath</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gc8f2f7858c2a4a263abe861108decf65</anchor>
      <arglist>(To &amp;to, const From &amp;from)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>checkPath</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gd2ae6bbf77a7859394591ad8507e3232</anchor>
      <arglist>(const Digraph &amp;digraph, const Path &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>Digraph::Node</type>
      <name>pathSource</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gf2f386ebf2ada943f47b153d1d70ccd9</anchor>
      <arglist>(const Digraph &amp;digraph, const Path &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>Digraph::Node</type>
      <name>pathTarget</name>
      <anchorfile>a00433.html</anchorfile>
      <anchor>gea6d1bb44b8be7929be3c7aded1317f9</anchor>
      <arglist>(const Digraph &amp;digraph, const Path &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>radixSort</name>
      <anchorfile>a00444.html</anchorfile>
      <anchor>gef447cc5a062480029a922daea269a89</anchor>
      <arglist>(Iterator first, Iterator last, Functor functor)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>stableRadixSort</name>
      <anchorfile>a00444.html</anchorfile>
      <anchor>g5f0a601b83783dc471f628eb2a1795c7</anchor>
      <arglist>(Iterator first, Iterator last, Functor functor)</arglist>
    </member>
    <member kind="function">
      <type>TimeStamp</type>
      <name>runningTimeTest</name>
      <anchorfile>a00450.html</anchorfile>
      <anchor>g43f5d1e50bae7c25870c29c0e08d3c09</anchor>
      <arglist>(F f, double min_time=10, unsigned int *num=NULL, TimeStamp *full_time=NULL)</arglist>
    </member>
    <member kind="variable">
      <type>const Invalid</type>
      <name>INVALID</name>
      <anchorfile>a00406.html</anchorfile>
      <anchor>0f04de8e6be7bc21ed685c651571d9fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>WHITE</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>ga574c6748d637031ff114ee5396f371d</anchor>
      <arglist>(1, 1, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>BLACK</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g887e77777b0cdd4bd98cd8582eab747d</anchor>
      <arglist>(0, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>RED</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g3aab05ed4f1fa1188cb5cec4a951da36</anchor>
      <arglist>(1, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>GREEN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g9d50a7cbc8c947f88556291754b964df</anchor>
      <arglist>(0, 1, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>BLUE</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g8d1bd8aebf1ea19b34a359b95afb2271</anchor>
      <arglist>(0, 0, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>YELLOW</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g7b3b82796993ff082f39aeaca4f74be9</anchor>
      <arglist>(1, 1, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>MAGENTA</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gd9c523e1fcd76b6e97a7e8f6c89a6d09</anchor>
      <arglist>(1, 0, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>CYAN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g6b139d00115defc76ec508dff90c91fd</anchor>
      <arglist>(0, 1, 1)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>GREY</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gccc4c0904a38839f9554cde971bb4963</anchor>
      <arglist>(0, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_RED</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g1c9259e9d2ec9a44ea6bf855d3dd3917</anchor>
      <arglist>(.5, 0, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_GREEN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gbc9fc012aa002dd8e311065b3115969c</anchor>
      <arglist>(0,.5, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_BLUE</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gc56cc8c77b9fcb56e308652d922d08f0</anchor>
      <arglist>(0, 0,.5)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_YELLOW</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g5dbe8dbe7b513bc66228981dcad165f7</anchor>
      <arglist>(.5,.5, 0)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_MAGENTA</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>ga3457b13dd61099a849816a02ba55efc</anchor>
      <arglist>(.5, 0,.5)</arglist>
    </member>
    <member kind="variable">
      <type>const Color</type>
      <name>DARK_CYAN</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g1814dea6aea83b28d9137adaa4d8b937</anchor>
      <arglist>(0,.5,.5)</arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>E</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g72bb95cfe3f4109af43a989e478a2d61</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LOG2E</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g9cef1ca3f697ed0afa15e6ce2658b9cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LOG10E</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g044b3cac2493404bbd6bb04cf61dc38d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LN2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g64f768a3649a214be5a8b9d13acc30fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>LN10</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gb62596f975434ae5b1dde456a64c455a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>PI</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gcf20630e5d2a9696928fe77b0726013c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>PI_2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g8d3eb5ff33b365b02e3d7065f2ecba48</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>PI_4</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g5a75e78cd42171bf864e2bad56639318</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>SQRT2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gc6586fa2865c0cc54dd89b93a0da1d17</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const long double</type>
      <name>SQRT1_2</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g1794f2ffbd3e762771a25847b905918c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>Random</type>
      <name>rnd</name>
      <anchorfile>a00406.html</anchorfile>
      <anchor>f55e529932608e88737901e3404d1d0e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ReverseDigraph</name>
    <filename>a00228.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>DGR</type>
      <name>Digraph</name>
      <anchorfile>a00228.html</anchorfile>
      <anchor>6435ad6a242e3d323a0dfffd053d9584</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ReverseDigraph</name>
      <anchorfile>a00228.html</anchorfile>
      <anchor>1c19d8f05c205125bfc52b57d9115483</anchor>
      <arglist>(DGR &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>ReverseDigraph&lt; const DGR &gt;</type>
      <name>reverseDigraph</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g218b793f2cd2640ee82e84e4e1570544</anchor>
      <arglist>(const DGR &amp;digraph)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SubDigraph</name>
    <filename>a00281.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>DGR</type>
      <name>Digraph</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>6435ad6a242e3d323a0dfffd053d9584</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>NF</type>
      <name>NodeFilterMap</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>4eb9c95d5fc082e8c24367f6534fdc33</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>AF</type>
      <name>ArcFilterMap</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>059cdfd6d771a5bc57e8aaee120c3407</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SubDigraph</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>a586b064493ed16cd99bf22e5f6bcdd5</anchor>
      <arglist>(DGR &amp;digraph, NF &amp;node_filter, AF &amp;arc_filter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>status</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>711dc8f20e5cc54beb6b593b477e39da</anchor>
      <arglist>(const Node &amp;n, bool v) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>status</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>7ede39359354abf6eb518f569ff527cc</anchor>
      <arglist>(const Arc &amp;a, bool v) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>status</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>88f03c67cb6b5cae80863297d6af6ad7</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>status</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>68a0220715672c9fb921375db6ea1f6c</anchor>
      <arglist>(const Arc &amp;a) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>disable</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>399546f119b51777efca621d172ce72b</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>disable</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>c03cc82fca9d6d0bd1afa0189c249189</anchor>
      <arglist>(const Arc &amp;a) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enable</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>6908dd7a97e89feaebd42f28b79cbda5</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enable</name>
      <anchorfile>a00281.html</anchorfile>
      <anchor>2f8dabdc2a72b3ca71d4aedd8c82dd7b</anchor>
      <arglist>(const Arc &amp;a) const </arglist>
    </member>
    <member kind="function">
      <type>SubDigraph&lt; const DGR, NF, AF &gt;</type>
      <name>subDigraph</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g062ef0ddcd96bf483cde2358f40b46da</anchor>
      <arglist>(const DGR &amp;digraph, NF &amp;node_filter, AF &amp;arc_filter)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SubGraph</name>
    <filename>a00282.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>NF</type>
      <name>NodeFilterMap</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>4eb9c95d5fc082e8c24367f6534fdc33</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>EF</type>
      <name>EdgeFilterMap</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>c2f0212f10cc7aa26b76d2a113e963c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SubGraph</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>cafef764806c2916e10afb85630bedb5</anchor>
      <arglist>(GR &amp;graph, NF &amp;node_filter, EF &amp;edge_filter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>status</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>711dc8f20e5cc54beb6b593b477e39da</anchor>
      <arglist>(const Node &amp;n, bool v) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>status</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>459d5d31aca26a7ae65e12e67206bbea</anchor>
      <arglist>(const Edge &amp;e, bool v) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>status</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>88f03c67cb6b5cae80863297d6af6ad7</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>status</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>1d0451e1488401c6b1e557abd13e6f58</anchor>
      <arglist>(const Edge &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>disable</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>399546f119b51777efca621d172ce72b</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>disable</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>dbed4d766b7574f13f51d572cc3a2312</anchor>
      <arglist>(const Edge &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enable</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>6908dd7a97e89feaebd42f28b79cbda5</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enable</name>
      <anchorfile>a00282.html</anchorfile>
      <anchor>7e856a820c2c09a70f940d7e237b5c37</anchor>
      <arglist>(const Edge &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>SubGraph&lt; const GR, NF, EF &gt;</type>
      <name>subGraph</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>gc662b1d237b24d56a04d3bd7a5a05774</anchor>
      <arglist>(const GR &amp;graph, NF &amp;node_filter, EF &amp;edge_filter)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::FilterNodes</name>
    <filename>a00116.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>FilterNodes</name>
      <anchorfile>a00116.html</anchorfile>
      <anchor>3acbedd2d3b60c3e7c8c1913ba47d8cd</anchor>
      <arglist>(GR &amp;graph, NF &amp;node_filter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>status</name>
      <anchorfile>a00116.html</anchorfile>
      <anchor>711dc8f20e5cc54beb6b593b477e39da</anchor>
      <arglist>(const Node &amp;n, bool v) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>status</name>
      <anchorfile>a00116.html</anchorfile>
      <anchor>88f03c67cb6b5cae80863297d6af6ad7</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>disable</name>
      <anchorfile>a00116.html</anchorfile>
      <anchor>399546f119b51777efca621d172ce72b</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enable</name>
      <anchorfile>a00116.html</anchorfile>
      <anchor>6908dd7a97e89feaebd42f28b79cbda5</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>FilterNodes&lt; const GR, NF &gt;</type>
      <name>filterNodes</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g5dbf1ee0dfe1ad897d9b9f50dcc38ad0</anchor>
      <arglist>(const GR &amp;graph, NF &amp;node_filter)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::FilterArcs</name>
    <filename>a00114.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>DGR</type>
      <name>Digraph</name>
      <anchorfile>a00114.html</anchorfile>
      <anchor>6435ad6a242e3d323a0dfffd053d9584</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>AF</type>
      <name>ArcFilterMap</name>
      <anchorfile>a00114.html</anchorfile>
      <anchor>059cdfd6d771a5bc57e8aaee120c3407</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FilterArcs</name>
      <anchorfile>a00114.html</anchorfile>
      <anchor>9273e96f98a5aa99ea93fb0e770e8e4e</anchor>
      <arglist>(DGR &amp;digraph, ArcFilterMap &amp;arc_filter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>status</name>
      <anchorfile>a00114.html</anchorfile>
      <anchor>7ede39359354abf6eb518f569ff527cc</anchor>
      <arglist>(const Arc &amp;a, bool v) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>status</name>
      <anchorfile>a00114.html</anchorfile>
      <anchor>68a0220715672c9fb921375db6ea1f6c</anchor>
      <arglist>(const Arc &amp;a) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>disable</name>
      <anchorfile>a00114.html</anchorfile>
      <anchor>c03cc82fca9d6d0bd1afa0189c249189</anchor>
      <arglist>(const Arc &amp;a) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enable</name>
      <anchorfile>a00114.html</anchorfile>
      <anchor>2f8dabdc2a72b3ca71d4aedd8c82dd7b</anchor>
      <arglist>(const Arc &amp;a) const </arglist>
    </member>
    <member kind="function">
      <type>FilterArcs&lt; const DGR, AF &gt;</type>
      <name>filterArcs</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>ge354ced20aee296dfd57b9f647b1d71b</anchor>
      <arglist>(const DGR &amp;digraph, AF &amp;arc_filter)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::FilterEdges</name>
    <filename>a00115.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00115.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>EF</type>
      <name>EdgeFilterMap</name>
      <anchorfile>a00115.html</anchorfile>
      <anchor>c2f0212f10cc7aa26b76d2a113e963c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FilterEdges</name>
      <anchorfile>a00115.html</anchorfile>
      <anchor>857bb05088a80cbe464ce993c9744037</anchor>
      <arglist>(GR &amp;graph, EF &amp;edge_filter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>status</name>
      <anchorfile>a00115.html</anchorfile>
      <anchor>459d5d31aca26a7ae65e12e67206bbea</anchor>
      <arglist>(const Edge &amp;e, bool v) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>status</name>
      <anchorfile>a00115.html</anchorfile>
      <anchor>1d0451e1488401c6b1e557abd13e6f58</anchor>
      <arglist>(const Edge &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>disable</name>
      <anchorfile>a00115.html</anchorfile>
      <anchor>dbed4d766b7574f13f51d572cc3a2312</anchor>
      <arglist>(const Edge &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enable</name>
      <anchorfile>a00115.html</anchorfile>
      <anchor>7e856a820c2c09a70f940d7e237b5c37</anchor>
      <arglist>(const Edge &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>FilterEdges&lt; const GR, EF &gt;</type>
      <name>filterEdges</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g5fb6134ad6eb75a7f57dfffdb7953695</anchor>
      <arglist>(const GR &amp;graph, EF &amp;edge_filter)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Undirector</name>
    <filename>a00294.html</filename>
    <templarg>DGR</templarg>
    <class kind="class">lemon::Undirector::CombinedArcMap</class>
    <member kind="typedef">
      <type>DGR</type>
      <name>Digraph</name>
      <anchorfile>a00294.html</anchorfile>
      <anchor>6435ad6a242e3d323a0dfffd053d9584</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Undirector</name>
      <anchorfile>a00294.html</anchorfile>
      <anchor>b3b1215366b82b7585ffe3950ba8cb3f</anchor>
      <arglist>(DGR &amp;digraph)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static CombinedArcMap&lt; FW, BK &gt;</type>
      <name>combinedArcMap</name>
      <anchorfile>a00294.html</anchorfile>
      <anchor>8e30f443a5a546e3698f3bf2f44b7ae1</anchor>
      <arglist>(FW &amp;forward, BK &amp;backward)</arglist>
    </member>
    <member kind="function">
      <type>Undirector&lt; const DGR &gt;</type>
      <name>undirector</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g1dbbd6d361a509c0ed5ed9e2e9d2b1a3</anchor>
      <arglist>(const DGR &amp;digraph)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Undirector::CombinedArcMap</name>
    <filename>a00052.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>Parent::Arc</type>
      <name>Key</name>
      <anchorfile>a00052.html</anchorfile>
      <anchor>13e85d8e9431f7a6a51c1e2e15807dcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>FW::Value</type>
      <name>Value</name>
      <anchorfile>a00052.html</anchorfile>
      <anchor>7f806cb53c6fe517966417f73431ac65</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CombinedArcMap</name>
      <anchorfile>a00052.html</anchorfile>
      <anchor>fc72a99a501025589a75334d5099653e</anchor>
      <arglist>(FW &amp;forward, BK &amp;backward)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00052.html</anchorfile>
      <anchor>8b4f088ec51d022ac424e2ad03345528</anchor>
      <arglist>(const Key &amp;e, const Value &amp;a)</arglist>
    </member>
    <member kind="function">
      <type>ConstReturnValue</type>
      <name>operator[]</name>
      <anchorfile>a00052.html</anchorfile>
      <anchor>b5916e41ffc89ab269fc0e850d5cff04</anchor>
      <arglist>(const Key &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>ReturnValue</type>
      <name>operator[]</name>
      <anchorfile>a00052.html</anchorfile>
      <anchor>6c15088cce9ae56b72a0e3e53c206c38</anchor>
      <arglist>(const Key &amp;e)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Orienter</name>
    <filename>a00204.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00204.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>DM</type>
      <name>DirectionMap</name>
      <anchorfile>a00204.html</anchorfile>
      <anchor>039bf501bb852a36dd281ca096295e4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Orienter</name>
      <anchorfile>a00204.html</anchorfile>
      <anchor>b2c793dd60c1389f60cb7ac1751395ac</anchor>
      <arglist>(GR &amp;graph, DM &amp;direction)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reverseArc</name>
      <anchorfile>a00204.html</anchorfile>
      <anchor>6ac584128e0ad8f56eb1e98dc6e25988</anchor>
      <arglist>(const Arc &amp;a)</arglist>
    </member>
    <member kind="function">
      <type>Orienter&lt; const GR, DM &gt;</type>
      <name>orienter</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g42eb576a82ec3211c968b00ab4c11698</anchor>
      <arglist>(const GR &amp;graph, DM &amp;direction)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ResidualDigraph</name>
    <filename>a00226.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <class kind="class">lemon::ResidualDigraph::ResidualCapacity</class>
    <member kind="typedef">
      <type>DGR</type>
      <name>Digraph</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>6435ad6a242e3d323a0dfffd053d9584</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CM</type>
      <name>CapacityMap</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>a445690c7204e6db4ddd20f5e865098b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>FM</type>
      <name>FlowMap</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>2939a0a237c77039e7d80faa9eec9ccd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TL</type>
      <name>Tolerance</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>5baae3db2e79c32027a17896b1204403</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ResidualDigraph</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>a70d15d1d33bf517fd1181d5320b2ec8</anchor>
      <arglist>(const DGR &amp;digraph, const CM &amp;capacity, FM &amp;flow, const TL &amp;tolerance=Tolerance())</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>residualCapacity</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>33766028cc266e37ad553aaebd09d351</anchor>
      <arglist>(const Arc &amp;a) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>augment</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>9cba43ffb906a3c3677bbf7317d30549</anchor>
      <arglist>(const Arc &amp;a, const Value &amp;v) const </arglist>
    </member>
    <member kind="function">
      <type>ResidualCapacity</type>
      <name>residualCapacity</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>7bc9d9321c8c66b1e4e0f33eda33f707</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>forward</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>7e6872f27247ac469eca9bbec6faa4b9</anchor>
      <arglist>(const Arc &amp;a)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>backward</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>fd7c3914c21d626a6bcb348ca1d158ef</anchor>
      <arglist>(const Arc &amp;a)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Arc</type>
      <name>forward</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>05a82dec247b71ac0a5b42b9f8581f39</anchor>
      <arglist>(const typename Digraph::Arc &amp;a)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Arc</type>
      <name>backward</name>
      <anchorfile>a00226.html</anchorfile>
      <anchor>434f8ac6881d17f56a29ae0053466bd9</anchor>
      <arglist>(const typename Digraph::Arc &amp;a)</arglist>
    </member>
    <member kind="function">
      <type>ResidualDigraph&lt; DGR, CM, FM &gt;</type>
      <name>residualDigraph</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g7d15101ba4085bf76c55ac167a2e7934</anchor>
      <arglist>(const DGR &amp;digraph, const CM &amp;capacity_map, FM &amp;flow_map)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ResidualDigraph::ResidualCapacity</name>
    <filename>a00225.html</filename>
    <member kind="typedef">
      <type>Arc</type>
      <name>Key</name>
      <anchorfile>a00225.html</anchorfile>
      <anchor>49324d309ab1fbcdfd1e07ecc5f48b39</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CapacityMap::Value</type>
      <name>Value</name>
      <anchorfile>a00225.html</anchorfile>
      <anchor>f751c878eda966b90fa4fdf5f1915eff</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ResidualCapacity</name>
      <anchorfile>a00225.html</anchorfile>
      <anchor>a18fb5b24fd7da1fa4f38406019f16ca</anchor>
      <arglist>(const ResidualDigraph&lt; DGR, CM, FM, TL &gt; &amp;adaptor)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00225.html</anchorfile>
      <anchor>d1162507b5caa110bbdebdf9f732f842</anchor>
      <arglist>(const Arc &amp;a) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SplitNodes</name>
    <filename>a00279.html</filename>
    <templarg></templarg>
    <class kind="class">lemon::SplitNodes::CombinedArcMap</class>
    <class kind="class">lemon::SplitNodes::CombinedNodeMap</class>
    <member kind="function">
      <type></type>
      <name>SplitNodes</name>
      <anchorfile>a00279.html</anchorfile>
      <anchor>56f3680d312f11bc9ab3d92e707076a6</anchor>
      <arglist>(const DGR &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>inNode</name>
      <anchorfile>a00279.html</anchorfile>
      <anchor>77de114f3028c8969526fd4b4e7ccb6d</anchor>
      <arglist>(const Node &amp;n)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>outNode</name>
      <anchorfile>a00279.html</anchorfile>
      <anchor>c153d4477370e4ccd4a85fe68d448c1d</anchor>
      <arglist>(const Node &amp;n)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>origArc</name>
      <anchorfile>a00279.html</anchorfile>
      <anchor>b8fc5ac8e1d333a751a2a4fceb94208b</anchor>
      <arglist>(const Arc &amp;a)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>bindArc</name>
      <anchorfile>a00279.html</anchorfile>
      <anchor>fa9ff5bc582ba23c4d1ee971c622c1c1</anchor>
      <arglist>(const Arc &amp;a)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Node</type>
      <name>inNode</name>
      <anchorfile>a00279.html</anchorfile>
      <anchor>a75e68bdea87e2f7a9e0127fc038a138</anchor>
      <arglist>(const DigraphNode &amp;n)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Node</type>
      <name>outNode</name>
      <anchorfile>a00279.html</anchorfile>
      <anchor>5b34aa374711534d2c56a2253e21c71b</anchor>
      <arglist>(const DigraphNode &amp;n)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Arc</type>
      <name>arc</name>
      <anchorfile>a00279.html</anchorfile>
      <anchor>2e84b05c0825d772b21ededdfb7a83e1</anchor>
      <arglist>(const DigraphNode &amp;n)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Arc</type>
      <name>arc</name>
      <anchorfile>a00279.html</anchorfile>
      <anchor>deda0d2dde8caca551ceb4279023b291</anchor>
      <arglist>(const DigraphArc &amp;a)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static CombinedNodeMap&lt; IN, OUT &gt;</type>
      <name>combinedNodeMap</name>
      <anchorfile>a00279.html</anchorfile>
      <anchor>449c2170966f4ef35f884ca2ad688ee0</anchor>
      <arglist>(IN &amp;in_map, OUT &amp;out_map)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static CombinedArcMap&lt; ArcMap, NodeMap &gt;</type>
      <name>combinedArcMap</name>
      <anchorfile>a00279.html</anchorfile>
      <anchor>be674d0ba5dfbfb40cf1bae27aed53c6</anchor>
      <arglist>(ArcMap &amp;arc_map, NodeMap &amp;node_map)</arglist>
    </member>
    <member kind="function">
      <type>SplitNodes&lt; DGR &gt;</type>
      <name>splitNodes</name>
      <anchorfile>a00429.html</anchorfile>
      <anchor>g89acf78045eaa7bbb164e4bc397638ed</anchor>
      <arglist>(const DGR &amp;digraph)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SplitNodes::CombinedArcMap</name>
    <filename>a00053.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>Arc</type>
      <name>Key</name>
      <anchorfile>a00053.html</anchorfile>
      <anchor>49324d309ab1fbcdfd1e07ecc5f48b39</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>AM::Value</type>
      <name>Value</name>
      <anchorfile>a00053.html</anchorfile>
      <anchor>9451425a7d8c5c2fb3079bd0a3483297</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CombinedArcMap</name>
      <anchorfile>a00053.html</anchorfile>
      <anchor>7c4c19566ea6648fab99a6788307c42b</anchor>
      <arglist>(AM &amp;arc_map, NM &amp;node_map)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00053.html</anchorfile>
      <anchor>e8346d86811106ea17363c754b7d1f8c</anchor>
      <arglist>(const Key &amp;arc) const </arglist>
    </member>
    <member kind="function">
      <type>Value &amp;</type>
      <name>operator[]</name>
      <anchorfile>a00053.html</anchorfile>
      <anchor>e04f0399db80fbb09db6e916e08cb53a</anchor>
      <arglist>(const Key &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00053.html</anchorfile>
      <anchor>4298356c4811426ffa189423d8165413</anchor>
      <arglist>(const Arc &amp;arc, const Value &amp;val)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SplitNodes::CombinedNodeMap</name>
    <filename>a00054.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>Node</type>
      <name>Key</name>
      <anchorfile>a00054.html</anchorfile>
      <anchor>e05d965da541abbbd35cc532b534496d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>IN::Value</type>
      <name>Value</name>
      <anchorfile>a00054.html</anchorfile>
      <anchor>b01dedd73125983bb1ea31cc9e77b149</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CombinedNodeMap</name>
      <anchorfile>a00054.html</anchorfile>
      <anchor>91d4277520d815686fee8fe613eab57c</anchor>
      <arglist>(IN &amp;in_map, OUT &amp;out_map)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00054.html</anchorfile>
      <anchor>dad303dedbe162e83d5c454f631ab5d7</anchor>
      <arglist>(const Key &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>Value &amp;</type>
      <name>operator[]</name>
      <anchorfile>a00054.html</anchorfile>
      <anchor>fc48ad56cf63a3484c36594a3057a007</anchor>
      <arglist>(const Key &amp;key)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00054.html</anchorfile>
      <anchor>603dc3052fdde5db5980a0846177e561</anchor>
      <arglist>(const Key &amp;key, const Value &amp;value)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ArgParser</name>
    <filename>a00021.html</filename>
    <class kind="class">lemon::ArgParser::RefType</class>
    <member kind="function">
      <type></type>
      <name>ArgParser</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>c53646f9d7508088703ae971557756da</anchor>
      <arglist>(int argc, const char *const *argv)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>parse</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>2b68b45bae933b58b66b151f191720bf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>run</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ff25e191acdc1f1e9bac4af25910bd35</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>commandName</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>7bf048cb0732b56d6b2c063b1a169665</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>given</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>a417e252cac80107c8cfb24f4b53a8ef</anchor>
      <arglist>(std::string op) const </arglist>
    </member>
    <member kind="function">
      <type>RefType</type>
      <name>operator[]</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>a996493529b42f509b8a48bf44c8eaf6</anchor>
      <arglist>(const std::string &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>const std::vector&lt; std::string &gt; &amp;</type>
      <name>files</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>e4ff6b28809bdf1bbff6f2bad5ee41fe</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>intOption</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>85d559c4b20d2f3fce8fd8e75338d9f5</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;help, int value=0, bool obl=false)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>doubleOption</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>0c5536fa3fbccef684622c7c3f77388f</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;help, double value=0, bool obl=false)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>boolOption</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>e4fcf9d73bc7e527ac0db7c9d6a3f4a9</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;help, bool value=false, bool obl=false)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>stringOption</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>2cb1ffd505413d02cd363cbd7f7e6fed</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;help, std::string value=&quot;&quot;, bool obl=false)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>other</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>a8519a140b285cbefa1fa47af0e9a9a6</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;help=&quot;&quot;)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>refOption</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>f72e3b37d1b0d8c06a35ced1e19945da</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;help, int &amp;ref, bool obl=false)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>refOption</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>9551cb0275da0c50091fef78e1fb5a86</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;help, double &amp;ref, bool obl=false)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>refOption</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>154ab534f7b4040ab84c261aebfd33a5</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;help, bool &amp;ref, bool obl=false)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>refOption</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>3e587e44687ddcc3310006f27cbbb0fe</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;help, std::string &amp;ref, bool obl=false)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>optionGroup</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ad20f888d2bc5c590777b753b8b63e90</anchor>
      <arglist>(const std::string &amp;group, const std::string &amp;opt)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>onlyOneGroup</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>1da70c717947a87bcaf0ba40b8cbe411</anchor>
      <arglist>(const std::string &amp;group)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>mandatoryGroup</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>bac885c55c4c5acefd2726d61c89d832</anchor>
      <arglist>(const std::string &amp;group)</arglist>
    </member>
    <member kind="function">
      <type>ArgParser &amp;</type>
      <name>synonym</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>84fdd5f9ed184b3ef96a142590d9c7b3</anchor>
      <arglist>(const std::string &amp;syn, const std::string &amp;opt)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ArgParser::RefType</name>
    <filename>a00224.html</filename>
    <member kind="function">
      <type></type>
      <name>RefType</name>
      <anchorfile>a00224.html</anchorfile>
      <anchor>6cda207aa9b4110a1469a62e2e50b88b</anchor>
      <arglist>(const ArgParser &amp;p, const std::string &amp;n)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator bool</name>
      <anchorfile>a00224.html</anchorfile>
      <anchor>9b3baad8c612d81b96e46f84d7e97580</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator std::string</name>
      <anchorfile>a00224.html</anchorfile>
      <anchor>ec21c2daaac19676aa46134f63b471c1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator double</name>
      <anchorfile>a00224.html</anchorfile>
      <anchor>abab268504345fcb036094bc7b5c4438</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator int</name>
      <anchorfile>a00224.html</anchorfile>
      <anchor>4f4ea421e40bda08a2deca657f640fea</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::BfsDefaultTraits</name>
    <filename>a00026.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; typename Digraph::Arc &gt;</type>
      <name>PredMap</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>2f311e3024ce1a50c00ffc291bce8e85</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>NullMap&lt; typename Digraph::Node, bool &gt;</type>
      <name>ProcessedMap</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>b4c841b7d3ecb344be7b85370b908920</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; bool &gt;</type>
      <name>ReachedMap</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>ffb032b5578e24579000f9899512ae60</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; int &gt;</type>
      <name>DistMap</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>e1b02f76f4c6728d9c92da415fe15f7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static PredMap *</type>
      <name>createPredMap</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>1d05c10ba44d3b5104c17e8181912d2c</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ProcessedMap *</type>
      <name>createProcessedMap</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>ef8f11ee548ae3b61e7c2a0018455569</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ReachedMap *</type>
      <name>createReachedMap</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>1c6c24dc0cfde917210decb6f31be54c</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static DistMap *</type>
      <name>createDistMap</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>c24cfd641a704fa02ebd5d2403125c66</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Bfs</name>
    <filename>a00025.html</filename>
    <templarg>GR</templarg>
    <templarg>TR</templarg>
    <class kind="struct">lemon::Bfs::SetDistMap</class>
    <class kind="struct">lemon::Bfs::SetPredMap</class>
    <class kind="struct">lemon::Bfs::SetProcessedMap</class>
    <class kind="struct">lemon::Bfs::SetReachedMap</class>
    <class kind="struct">lemon::Bfs::SetStandardProcessedMap</class>
    <member kind="typedef">
      <type>TR::Digraph</type>
      <name>Digraph</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>750f38b47fd56ef715849c3bd77fbe77</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::PredMap</type>
      <name>PredMap</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>dc9638061e7acac70160feb0ea49c29c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::DistMap</type>
      <name>DistMap</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>d6d9c2819f215994ded26fbe42e2fd26</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::ReachedMap</type>
      <name>ReachedMap</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>f23453a839e62b3dcc311e0a839e31ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::ProcessedMap</type>
      <name>ProcessedMap</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>43e332c55acc2c5cb99d35ff40accab1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>PredMapPath&lt; Digraph, PredMap &gt;</type>
      <name>Path</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>4d7ae5a0651af9c9cfadd1462718979d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR</type>
      <name>Traits</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>b157e5452122f7a7f73dfda5ed931d69</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Bfs</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>55f525c9a3cc7a3ecb17c51873ba1d14</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Bfs</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>067322e85a116d7670a30085cbff7b25</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Bfs &amp;</type>
      <name>predMap</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>c22804dedcadfd64208f659d6bd89545</anchor>
      <arglist>(PredMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Bfs &amp;</type>
      <name>reachedMap</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>d1dce733ba42cd5b68c9713af742d0a6</anchor>
      <arglist>(ReachedMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Bfs &amp;</type>
      <name>processedMap</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>5d0c6be2ef05e6121193b38146492956</anchor>
      <arglist>(ProcessedMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Bfs &amp;</type>
      <name>distMap</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>3051385a839729dc0c5418be2fa1f085</anchor>
      <arglist>(DistMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addSource</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>c785b6504b67a963b4c20bae90441a77</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>processNextNode</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>dba758047d7378b8a06320d29ce170d7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>processNextNode</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>8f85441d2cf615f40794ac052db5cb18</anchor>
      <arglist>(Node target, bool &amp;reach)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>processNextNode</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>716684ff14dc91b66d9148b562da33d7</anchor>
      <arglist>(const NM &amp;nm, Node &amp;rnode)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>nextNode</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>ca98e3b1fef1a4508df143a24b73b046</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>emptyQueue</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>b6dcd2be02feaff0a95c21824e805445</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>queueSize</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>942d30059e28f60ba6dd1944ab8e416e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>60de64d75454385b23995437f1d72669</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>8b8de9dfc16e4b3cb6b38733e82f6449</anchor>
      <arglist>(Node t)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>start</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>4d3d39ff1805757f57570e6db83630cc</anchor>
      <arglist>(const NodeBoolMap &amp;nm)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>e3f0682c90a4e8a23c259943e899402e</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>run</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>d2c3c4217c98b58a8618397d65d400c8</anchor>
      <arglist>(Node s, Node t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Path</type>
      <name>path</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>6a33507c667ac2bddd240d0baff1a573</anchor>
      <arglist>(Node t) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>dist</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>2e23ddd204b1b00807853d2620a3c1b1</anchor>
      <arglist>(Node v) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>predArc</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>f9fdbb84b58bd3f5daedde027070e056</anchor>
      <arglist>(Node v) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>predNode</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>89bd1c535e3d4fcb9c58afbb54ac47e1</anchor>
      <arglist>(Node v) const </arglist>
    </member>
    <member kind="function">
      <type>const DistMap &amp;</type>
      <name>distMap</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>77feee7321863f24b67813c0ef8dc5c0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const PredMap &amp;</type>
      <name>predMap</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>eec5920cc073b88c4c5f6c46a8f4aa4b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>reached</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>bb98c427556a2afe917e41fe8d3e75bf</anchor>
      <arglist>(Node v) const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::Bfs::SetDistMap</name>
    <filename>a00237.html</filename>
    <templarg></templarg>
    <base>Bfs&lt; Digraph, SetDistMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Bfs::SetPredMap</name>
    <filename>a00246.html</filename>
    <templarg></templarg>
    <base>Bfs&lt; Digraph, SetPredMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Bfs::SetProcessedMap</name>
    <filename>a00251.html</filename>
    <templarg></templarg>
    <base>Bfs&lt; Digraph, SetProcessedMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Bfs::SetReachedMap</name>
    <filename>a00256.html</filename>
    <templarg></templarg>
    <base>Bfs&lt; Digraph, SetReachedMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Bfs::SetStandardProcessedMap</name>
    <filename>a00262.html</filename>
    <base>Bfs&lt; Digraph, SetStandardProcessedMapTraits &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::BfsWizardDefaultTraits</name>
    <filename>a00032.html</filename>
    <templarg>GR</templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; typename Digraph::Arc &gt;</type>
      <name>PredMap</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>2f311e3024ce1a50c00ffc291bce8e85</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>NullMap&lt; typename Digraph::Node, bool &gt;</type>
      <name>ProcessedMap</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>b4c841b7d3ecb344be7b85370b908920</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; bool &gt;</type>
      <name>ReachedMap</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>ffb032b5578e24579000f9899512ae60</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; int &gt;</type>
      <name>DistMap</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>e1b02f76f4c6728d9c92da415fe15f7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>lemon::Path&lt; Digraph &gt;</type>
      <name>Path</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>6989b0a22a99c251a2e7c99cf36605ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static PredMap *</type>
      <name>createPredMap</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>1d05c10ba44d3b5104c17e8181912d2c</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ProcessedMap *</type>
      <name>createProcessedMap</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>ef8f11ee548ae3b61e7c2a0018455569</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ReachedMap *</type>
      <name>createReachedMap</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>1c6c24dc0cfde917210decb6f31be54c</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static DistMap *</type>
      <name>createDistMap</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>c24cfd641a704fa02ebd5d2403125c66</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::BfsWizardBase</name>
    <filename>a00031.html</filename>
    <templarg></templarg>
    <base>BfsWizardDefaultTraits&lt; GR &gt;</base>
    <member kind="function">
      <type></type>
      <name>BfsWizardBase</name>
      <anchorfile>a00031.html</anchorfile>
      <anchor>ad43a7eb3f6d54e0d1cb37f29b8eb08b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BfsWizardBase</name>
      <anchorfile>a00031.html</anchorfile>
      <anchor>d2360a2aeec62c51672a4dcd6615e7fb</anchor>
      <arglist>(const GR &amp;g)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::BfsWizard</name>
    <filename>a00030.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>BfsWizard</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>e8923bde3788f91e3b2bf9191c73bd39</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BfsWizard</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>59efab63f61b1c5e17b0c5b3b30c0afb</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BfsWizard</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>759468eb61ca1b166698056c8a36f5fe</anchor>
      <arglist>(const TR &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>e3f0682c90a4e8a23c259943e899402e</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>run</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>d2c3c4217c98b58a8618397d65d400c8</anchor>
      <arglist>(Node s, Node t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>BfsWizard&lt; SetPredMapBase&lt; T &gt; &gt;</type>
      <name>predMap</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>253e2d4489d8e47c490c59d2390fc9bb</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>BfsWizard&lt; SetReachedMapBase&lt; T &gt; &gt;</type>
      <name>reachedMap</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>c64fe02251caa93296bd2f3e04e99779</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>BfsWizard&lt; SetDistMapBase&lt; T &gt; &gt;</type>
      <name>distMap</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>af5f9fa18b30e8e22f27021b7cce5c19</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>BfsWizard&lt; SetProcessedMapBase&lt; T &gt; &gt;</type>
      <name>processedMap</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>f39ba115a2edf809860a2279b8f78fb9</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>BfsWizard&lt; SetPathBase&lt; T &gt; &gt;</type>
      <name>path</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>f0a1d7dd785e2c41fe25d166f8049710</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>BfsWizard</type>
      <name>dist</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>1c2895f9c7861ca965fc4ec37c4b36e2</anchor>
      <arglist>(const int &amp;d)</arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::Digraph</type>
      <name>Digraph</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>750f38b47fd56ef715849c3bd77fbe77</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::PredMap</type>
      <name>PredMap</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>dc9638061e7acac70160feb0ea49c29c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::DistMap</type>
      <name>DistMap</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>d6d9c2819f215994ded26fbe42e2fd26</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::ReachedMap</type>
      <name>ReachedMap</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>f23453a839e62b3dcc311e0a839e31ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::ProcessedMap</type>
      <name>ProcessedMap</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>43e332c55acc2c5cb99d35ff40accab1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::Path</type>
      <name>Path</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>d6d7757da799220a95281fbee4d3fb8f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::BfsVisitor</name>
    <filename>a00029.html</filename>
    <templarg>GR</templarg>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00029.html</anchorfile>
      <anchor>f763d0c0856194421ebd6ccc73b921c3</anchor>
      <arglist>(const Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reach</name>
      <anchorfile>a00029.html</anchorfile>
      <anchor>4f6dc7007e25f1c20b4fbb8912a83817</anchor>
      <arglist>(const Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>process</name>
      <anchorfile>a00029.html</anchorfile>
      <anchor>a2d367c34d01e4e014e461f40b2f7203</anchor>
      <arglist>(const Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>discover</name>
      <anchorfile>a00029.html</anchorfile>
      <anchor>be70d6cbc4419667e6aa533fc0fa262f</anchor>
      <arglist>(const Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>examine</name>
      <anchorfile>a00029.html</anchorfile>
      <anchor>3656d1f80036aee1dcaa927450b70c2a</anchor>
      <arglist>(const Arc &amp;arc)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::BfsVisitDefaultTraits</name>
    <filename>a00028.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00028.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; bool &gt;</type>
      <name>ReachedMap</name>
      <anchorfile>a00028.html</anchorfile>
      <anchor>ffb032b5578e24579000f9899512ae60</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ReachedMap *</type>
      <name>createReachedMap</name>
      <anchorfile>a00028.html</anchorfile>
      <anchor>1491e8ada6a6d5fdd321e3fee43844e1</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::BfsVisit</name>
    <filename>a00027.html</filename>
    <templarg>GR</templarg>
    <templarg>VS</templarg>
    <templarg>TR</templarg>
    <class kind="struct">lemon::BfsVisit::SetReachedMap</class>
    <member kind="typedef">
      <type>TR</type>
      <name>Traits</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>b157e5452122f7a7f73dfda5ed931d69</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Digraph</type>
      <name>Digraph</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>a6928feef02be4f2a184775d19dc6373</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>VS</type>
      <name>Visitor</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>3bece3b7c0b0189b735a408e533f3d73</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::ReachedMap</type>
      <name>ReachedMap</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>90221be1b9274785db2188548354d2aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BfsVisit</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>e27c446704010b785a05379a1e02811e</anchor>
      <arglist>(const Digraph &amp;digraph, Visitor &amp;visitor)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~BfsVisit</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>8bf7b105577e2894778cc63cbec43a24</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>BfsVisit &amp;</type>
      <name>reachedMap</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>51540af2212fa8760166385adbaa9f34</anchor>
      <arglist>(ReachedMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addSource</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>c785b6504b67a963b4c20bae90441a77</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>processNextNode</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>dba758047d7378b8a06320d29ce170d7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>processNextNode</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>8f85441d2cf615f40794ac052db5cb18</anchor>
      <arglist>(Node target, bool &amp;reach)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>processNextNode</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>716684ff14dc91b66d9148b562da33d7</anchor>
      <arglist>(const NM &amp;nm, Node &amp;rnode)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>nextNode</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>ca98e3b1fef1a4508df143a24b73b046</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>emptyQueue</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>b6dcd2be02feaff0a95c21824e805445</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>queueSize</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>942d30059e28f60ba6dd1944ab8e416e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>60de64d75454385b23995437f1d72669</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>8b8de9dfc16e4b3cb6b38733e82f6449</anchor>
      <arglist>(Node t)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>start</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>868685f8a43c1ea45e173adc3ea47c1f</anchor>
      <arglist>(const NM &amp;nm)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>e3f0682c90a4e8a23c259943e899402e</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>run</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>d2c3c4217c98b58a8618397d65d400c8</anchor>
      <arglist>(Node s, Node t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>reached</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>bb98c427556a2afe917e41fe8d3e75bf</anchor>
      <arglist>(Node v) const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::BfsVisit::SetReachedMap</name>
    <filename>a00253.html</filename>
    <templarg></templarg>
    <base>BfsVisit&lt; Digraph, Visitor, SetReachedMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="class">
    <name>lemon::BinHeap</name>
    <filename>a00033.html</filename>
    <templarg>PR</templarg>
    <templarg>IM</templarg>
    <templarg>Comp</templarg>
    <member kind="enumeration">
      <name>State</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>5d74787dedbc4e11c1ab15bf487e61f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>IN_HEAP</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>5d74787dedbc4e11c1ab15bf487e61f8213759402d071be3f66f8cf86641bb11</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>PRE_HEAP</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>5d74787dedbc4e11c1ab15bf487e61f812f201f9d13d106e81658dad7e7c9c2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>POST_HEAP</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>5d74787dedbc4e11c1ab15bf487e61f8cd215e97251c1085a8b299c068e7172d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>IM</type>
      <name>ItemIntMap</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>c00a2d6f039b6e8ffc0641530bdf5304</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>PR</type>
      <name>Prio</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>644810a55913c9e8b24511758574d6d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>ItemIntMap::Key</type>
      <name>Item</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>70025b32b600038ee2981a3deab1a783</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::pair&lt; Item, Prio &gt;</type>
      <name>Pair</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>616fe456bc4c8eddf813c4f5665e1180</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Comp</type>
      <name>Compare</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>ad6f401d1521e21c6b512b61409f7bf4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BinHeap</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>cf056081005598d56af81013dc122e3f</anchor>
      <arglist>(ItemIntMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BinHeap</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>d37464eb1f70d05e41d8b2f99c3b7b54</anchor>
      <arglist>(ItemIntMap &amp;map, const Compare &amp;comp)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>b8e4e3e2a7bf18888b71bdf9dda0770b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>empty</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>c6e61de369e994009e36f344f99c15ad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>push</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>5b654214309b6f25fceeec04e2b1a016</anchor>
      <arglist>(const Pair &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>push</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>d34f576dafc84f8969e2fecd67cd3a15</anchor>
      <arglist>(const Item &amp;i, const Prio &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>top</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>84adc89415588c3ce11ce526a00070d9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Prio</type>
      <name>prio</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>eea060d345482a9732a96e9021f0c495</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>pop</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>312e7f6c761a199c1369fbe651e084f0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>5fe7eab02681c7735ba173c879de1681</anchor>
      <arglist>(const Item &amp;i)</arglist>
    </member>
    <member kind="function">
      <type>Prio</type>
      <name>operator[]</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>74ccb13d905bf8b608f3426131977cc0</anchor>
      <arglist>(const Item &amp;i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>35c06441ccfac0ed04762113a102e6b3</anchor>
      <arglist>(const Item &amp;i, const Prio &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>decrease</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>301fdc87288afdebf379ae043cf66cd4</anchor>
      <arglist>(const Item &amp;i, const Prio &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>increase</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>958836bae38b188956bcf1345c63eac4</anchor>
      <arglist>(const Item &amp;i, const Prio &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>State</type>
      <name>state</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>613888b340ee362e2f4da6f2e2b97428</anchor>
      <arglist>(const Item &amp;i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>state</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>7d0ca230d438efaad53833701d9cb262</anchor>
      <arglist>(const Item &amp;i, State st)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>replace</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>a3d169f66b376619ebb1825f6c027482</anchor>
      <arglist>(const Item &amp;i, const Item &amp;j)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::CbcMip</name>
    <filename>a00037.html</filename>
    <base>lemon::MipSolver</base>
    <member kind="function">
      <type></type>
      <name>CbcMip</name>
      <anchorfile>a00037.html</anchorfile>
      <anchor>b1ff7c7f55a6ae5f87eeebe0d5a1421c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CbcMip</name>
      <anchorfile>a00037.html</anchorfile>
      <anchor>04420c67edf64119f3df6b79cc7e705d</anchor>
      <arglist>(const CbcMip &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CbcMip</name>
      <anchorfile>a00037.html</anchorfile>
      <anchor>ccd7e3de4fc06851977426ea9f34e83b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CbcMip *</type>
      <name>newSolver</name>
      <anchorfile>a00037.html</anchorfile>
      <anchor>14a0f509c697e8a1ff38d542911a7b0c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CbcMip *</type>
      <name>cloneSolver</name>
      <anchorfile>a00037.html</anchorfile>
      <anchor>1538f13f85436ace01b42de014783a7f</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::CirculationDefaultTraits</name>
    <filename>a00039.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>LM</type>
      <name>LowerMap</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>e46c3221b25227a3b78e9f9a1b3f42ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>UM</type>
      <name>UpperMap</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>96f0ba1f755808fb6478bad8c221187d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>SM</type>
      <name>SupplyMap</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>bddcbdf598a9a95ef03684914e7e9991</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>SupplyMap::Value</type>
      <name>Value</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>87bda177e2c2c008e4662ba93e88a32f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template ArcMap&lt; Value &gt;</type>
      <name>FlowMap</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>438199065fa5e338294d53c559bb957b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>lemon::Elevator&lt; Digraph, typename Digraph::Node &gt;</type>
      <name>Elevator</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>dca253e8246cb3014c17312ce28107ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>lemon::Tolerance&lt; Value &gt;</type>
      <name>Tolerance</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>c8d81c6484d646b8881c72707f2527d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static FlowMap *</type>
      <name>createFlowMap</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>fd79e520abbe90ea86b8013071afb57b</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Elevator *</type>
      <name>createElevator</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>899dadca634616cbf6500efbc71610e5</anchor>
      <arglist>(const Digraph &amp;digraph, int max_level)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Circulation</name>
    <filename>a00038.html</filename>
    <templarg>GR</templarg>
    <templarg>LM</templarg>
    <templarg>UM</templarg>
    <templarg>SM</templarg>
    <templarg>TR</templarg>
    <class kind="struct">lemon::Circulation::SetElevator</class>
    <class kind="struct">lemon::Circulation::SetFlowMap</class>
    <class kind="struct">lemon::Circulation::SetStandardElevator</class>
    <member kind="typedef">
      <type>TR</type>
      <name>Traits</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>b157e5452122f7a7f73dfda5ed931d69</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Digraph</type>
      <name>Digraph</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>a6928feef02be4f2a184775d19dc6373</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Value</type>
      <name>Value</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>1c7478783a00413767196fd8d82ad8fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::LowerMap</type>
      <name>LowerMap</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>be92e2b425d46397f1add6fdd6db6b7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::UpperMap</type>
      <name>UpperMap</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>83be4d86b37c7a43dab64a4b1b5ccb79</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::SupplyMap</type>
      <name>SupplyMap</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>040d59dc3d4879cc8f00ac310308772c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::FlowMap</type>
      <name>FlowMap</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>cbaaf29d0c8168790ab0da45dad92c62</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Elevator</type>
      <name>Elevator</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>078a395cce67cc7938d85be25aa74718</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Tolerance</type>
      <name>Tolerance</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>926a96c583959d256c1316a2aca3ce22</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Circulation</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>766fcc516e43681f77caad90d5e73913</anchor>
      <arglist>(const Digraph &amp;graph, const LowerMap &amp;lower, const UpperMap &amp;upper, const SupplyMap &amp;supply)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Circulation</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>602bddf53e2f8737ac8333233ee6e96d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Circulation &amp;</type>
      <name>lowerMap</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>b6c73f90f08dc7da19a258c7e05674a5</anchor>
      <arglist>(const LowerMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>Circulation &amp;</type>
      <name>upperMap</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>453a1901f76865172e8f1a677d6fece8</anchor>
      <arglist>(const UpperMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>Circulation &amp;</type>
      <name>supplyMap</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>ab294b65a3aa5747c71fd9ead4a71b50</anchor>
      <arglist>(const SupplyMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>Circulation &amp;</type>
      <name>flowMap</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>3dc1b264f452142f2dc77157b86f5155</anchor>
      <arglist>(FlowMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>Circulation &amp;</type>
      <name>elevator</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>bc8e9abf45d71ce9d03f3a559e8edf8e</anchor>
      <arglist>(Elevator &amp;elevator)</arglist>
    </member>
    <member kind="function">
      <type>const Elevator &amp;</type>
      <name>elevator</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>5dc9bfc4e3f3def6bd3daee725ac9d9a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Circulation &amp;</type>
      <name>tolerance</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>19773192cea016ada50d3b9470bf36cd</anchor>
      <arglist>(const Tolerance &amp;tolerance)</arglist>
    </member>
    <member kind="function">
      <type>const Tolerance &amp;</type>
      <name>tolerance</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>0fe880b3588576694de2dbfb22a4e6fc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>greedyInit</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>275cfb5a0d9eeb69e56ec2366e0a4f81</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>start</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>ad5997aaaa2d622f0ca57f8b24a51a7b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>run</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>149ad6701e3e2414cb566bb414029841</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>flow</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>e5735fb5fda2b9e5d130a7628a32737a</anchor>
      <arglist>(const Arc &amp;arc) const </arglist>
    </member>
    <member kind="function">
      <type>const FlowMap &amp;</type>
      <name>flowMap</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>592e6bf75b178e6e189eedf322abcc27</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>barrier</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>aa0fa8205fb370cdc9ee3b33b1cd54a9</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>barrierMap</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>9806258790d19e4c1089bdc0db949aa7</anchor>
      <arglist>(BarrierMap &amp;bar) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>checkFlow</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>3e4c7e285fd08438ba5d265d9daeb094</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>checkBarrier</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>7d893a9a4bd20dff09b5d09d05bb7f26</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::Circulation::SetElevator</name>
    <filename>a00240.html</filename>
    <templarg></templarg>
    <base>Circulation&lt; Digraph, LowerMap, UpperMap, SupplyMap, SetElevatorTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Circulation::SetFlowMap</name>
    <filename>a00243.html</filename>
    <templarg></templarg>
    <base>Circulation&lt; Digraph, LowerMap, UpperMap, SupplyMap, SetFlowMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Circulation::SetStandardElevator</name>
    <filename>a00258.html</filename>
    <templarg></templarg>
    <base>Circulation&lt; Digraph, LowerMap, UpperMap, SupplyMap, SetStandardElevatorTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="class">
    <name>lemon::ClpLp</name>
    <filename>a00045.html</filename>
    <base>lemon::LpSolver</base>
    <member kind="function">
      <type></type>
      <name>ClpLp</name>
      <anchorfile>a00045.html</anchorfile>
      <anchor>f9667a2a88853c4be96b0c2e19416ad2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ClpLp</name>
      <anchorfile>a00045.html</anchorfile>
      <anchor>89f91b42af43524791c4c73c6f377a79</anchor>
      <arglist>(const ClpLp &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~ClpLp</name>
      <anchorfile>a00045.html</anchorfile>
      <anchor>a8060bf28e96f9deb5a0da4d7105230f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual ClpLp *</type>
      <name>newSolver</name>
      <anchorfile>a00045.html</anchorfile>
      <anchor>0d71f4262cbd2c662935050a0bd3630e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual ClpLp *</type>
      <name>cloneSolver</name>
      <anchorfile>a00045.html</anchorfile>
      <anchor>8a2291ac904191f07080b67c59126fcd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>SolveExitStatus</type>
      <name>solvePrimal</name>
      <anchorfile>a00045.html</anchorfile>
      <anchor>0c104ef72fd5fc98036e0a83305bdcb1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>SolveExitStatus</type>
      <name>solveDual</name>
      <anchorfile>a00045.html</anchorfile>
      <anchor>f9e12f11f876ed7feccba1fbd68a0f5b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>SolveExitStatus</type>
      <name>solveBarrier</name>
      <anchorfile>a00045.html</anchorfile>
      <anchor>86f7f5085ed05208610310476906a675</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>clpRow</name>
      <anchorfile>a00045.html</anchorfile>
      <anchor>eb1bfd5600af88c2b22d40800887db78</anchor>
      <arglist>(Row r) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>clpCol</name>
      <anchorfile>a00045.html</anchorfile>
      <anchor>fad7dd3a127354e229ad38226081453f</anchor>
      <arglist>(Col c) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Color</name>
    <filename>a00051.html</filename>
    <member kind="function">
      <type></type>
      <name>Color</name>
      <anchorfile>a00051.html</anchorfile>
      <anchor>1589b83974b42a2f3315624f14c3c92c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Color</name>
      <anchorfile>a00051.html</anchorfile>
      <anchor>c975f1207e7e7caacde357dfea1369c8</anchor>
      <arglist>(double r, double g, double b)</arglist>
    </member>
    <member kind="function">
      <type>double &amp;</type>
      <name>red</name>
      <anchorfile>a00051.html</anchorfile>
      <anchor>76ca6d975a3988ef9d12b7ea867b38ad</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const double &amp;</type>
      <name>red</name>
      <anchorfile>a00051.html</anchorfile>
      <anchor>de4017c9b1d7602d5c2f1b91f5a9cee1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double &amp;</type>
      <name>green</name>
      <anchorfile>a00051.html</anchorfile>
      <anchor>53e0b92699d4680948e29928fa663897</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const double &amp;</type>
      <name>green</name>
      <anchorfile>a00051.html</anchorfile>
      <anchor>2f5e018d8875fac006cc540edcf76147</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double &amp;</type>
      <name>blue</name>
      <anchorfile>a00051.html</anchorfile>
      <anchor>22038a8dc2199101ab5d3826e23a747b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const double &amp;</type>
      <name>blue</name>
      <anchorfile>a00051.html</anchorfile>
      <anchor>cdff982f34c853ff22e638429a003ac4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00051.html</anchorfile>
      <anchor>ad8a1573c2ef17613aef9a536ba7f51d</anchor>
      <arglist>(double r, double g, double b)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Palette</name>
    <filename>a00209.html</filename>
    <base>MapBase&lt; int, Color &gt;</base>
    <member kind="function">
      <type></type>
      <name>Palette</name>
      <anchorfile>a00209.html</anchorfile>
      <anchor>0e6feeba9d414592568f3e816b62f280</anchor>
      <arglist>(bool have_white=false, int num=-1)</arglist>
    </member>
    <member kind="function">
      <type>Color &amp;</type>
      <name>operator[]</name>
      <anchorfile>a00209.html</anchorfile>
      <anchor>dabf831f19d4e9576fb5484afc4a2b4d</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>const Color &amp;</type>
      <name>operator[]</name>
      <anchorfile>a00209.html</anchorfile>
      <anchor>7b375006c489605f87c6dc997fb59006</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00209.html</anchorfile>
      <anchor>d4e5772fa6874dde74565b5f84a07e98</anchor>
      <arglist>(int i, const Color &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>a00209.html</anchorfile>
      <anchor>77e076addeb00430edb72da3f9b134b0</anchor>
      <arglist>(const Color &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resize</name>
      <anchorfile>a00209.html</anchorfile>
      <anchor>4deacfe482590ac13be466b2ff5f347d</anchor>
      <arglist>(int s)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>a00209.html</anchorfile>
      <anchor>b8e4e3e2a7bf18888b71bdf9dda0770b</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::Invalid</name>
    <filename>a00151.html</filename>
  </compound>
  <compound kind="class">
    <name>lemon::DigraphCopy</name>
    <filename>a00084.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>DigraphCopy</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>446ab15561f3b8ed021eb010e9e53ef6</anchor>
      <arglist>(const From &amp;from, To &amp;to)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~DigraphCopy</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>fb01541dd7332dde976064680adb7d27</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>DigraphCopy &amp;</type>
      <name>nodeRef</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>0e00a8bc6e29761785543e0a7c95e05b</anchor>
      <arglist>(NodeRef &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>DigraphCopy &amp;</type>
      <name>nodeCrossRef</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>de0ea901cf440b38fe4fdd4ae0fc8022</anchor>
      <arglist>(NodeCrossRef &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>DigraphCopy &amp;</type>
      <name>nodeMap</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>d59d378e30a5bd83ae07411eaaabfea5</anchor>
      <arglist>(const FromMap &amp;map, ToMap &amp;tmap)</arglist>
    </member>
    <member kind="function">
      <type>DigraphCopy &amp;</type>
      <name>node</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>f0223e0ec4c277528508092be91e6a17</anchor>
      <arglist>(const Node &amp;node, TNode &amp;tnode)</arglist>
    </member>
    <member kind="function">
      <type>DigraphCopy &amp;</type>
      <name>arcRef</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>bcc1b39bbe37995d4cf92593b739504b</anchor>
      <arglist>(ArcRef &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>DigraphCopy &amp;</type>
      <name>arcCrossRef</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>fd3b5b8ac8574f7a31a41fd263a1a3de</anchor>
      <arglist>(ArcCrossRef &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>DigraphCopy &amp;</type>
      <name>arcMap</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>cb10c65e555597455081266a95a4c566</anchor>
      <arglist>(const FromMap &amp;map, ToMap &amp;tmap)</arglist>
    </member>
    <member kind="function">
      <type>DigraphCopy &amp;</type>
      <name>arc</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>7ad45d42947a22cc7c47b2c530a6f654</anchor>
      <arglist>(const Arc &amp;arc, TArc &amp;tarc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GraphCopy</name>
    <filename>a00128.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>GraphCopy</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>2279b087a65e31ed8414893fc247dd29</anchor>
      <arglist>(const From &amp;from, To &amp;to)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~GraphCopy</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>10f931e6776ec70ec988c4fc02bef370</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>nodeRef</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>ded46bc5973794518c004e636e9e730b</anchor>
      <arglist>(NodeRef &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>nodeCrossRef</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>0cc2fbf22d6a10057e8154ca039c9d70</anchor>
      <arglist>(NodeCrossRef &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>nodeMap</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>be271d87a685f627317cce912a66b02e</anchor>
      <arglist>(const FromMap &amp;map, ToMap &amp;tmap)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>node</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>b9178da29ca4df86abb9c729c6f3e354</anchor>
      <arglist>(const Node &amp;node, TNode &amp;tnode)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>arcRef</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>e61cd61bf49d7ddde7f6b0a5508d64f7</anchor>
      <arglist>(ArcRef &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>arcCrossRef</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>087d77c728454dd1733251b2840216ad</anchor>
      <arglist>(ArcCrossRef &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>arcMap</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>36d8ed7dd78ecf864751b33c8fc39873</anchor>
      <arglist>(const FromMap &amp;map, ToMap &amp;tmap)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>arc</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>1a0c870e83efb89541ab51bf35944366</anchor>
      <arglist>(const Arc &amp;arc, TArc &amp;tarc)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>edgeRef</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>28727888601b5757378318a7f6f3d078</anchor>
      <arglist>(EdgeRef &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>edgeCrossRef</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>58aae1de14cb7352d3ac2413a90117ce</anchor>
      <arglist>(EdgeCrossRef &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>edgeMap</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>b2dce368dc1b7ffc1f558a537e6f724a</anchor>
      <arglist>(const FromMap &amp;map, ToMap &amp;tmap)</arglist>
    </member>
    <member kind="function">
      <type>GraphCopy &amp;</type>
      <name>edge</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>fe6d74446b39b082adc66f14fc01f206</anchor>
      <arglist>(const Edge &amp;edge, TEdge &amp;tedge)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00128.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ConArcIt</name>
    <filename>a00057.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>ConArcIt</name>
      <anchorfile>a00057.html</anchorfile>
      <anchor>77d03dd52dcaad7b393c1efbbab63602</anchor>
      <arglist>(const GR &amp;g, Node u, Node v)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ConArcIt</name>
      <anchorfile>a00057.html</anchorfile>
      <anchor>0a41f45a3e529803047fbd033522a2aa</anchor>
      <arglist>(const GR &amp;g, Arc a)</arglist>
    </member>
    <member kind="function">
      <type>ConArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00057.html</anchorfile>
      <anchor>cd427be549d9ab071d71eed9cd02d6ec</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ConEdgeIt</name>
    <filename>a00058.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>ConEdgeIt</name>
      <anchorfile>a00058.html</anchorfile>
      <anchor>09282f7895a4ec3cd5211ece130c8762</anchor>
      <arglist>(const GR &amp;g, Node u, Node v)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ConEdgeIt</name>
      <anchorfile>a00058.html</anchorfile>
      <anchor>66878cccb947904847f417dfdc9de3f5</anchor>
      <arglist>(const GR &amp;g, Edge e)</arglist>
    </member>
    <member kind="function">
      <type>ConEdgeIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00058.html</anchorfile>
      <anchor>db97684eca2bef5f92d3bb6e3f3b083e</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::DynArcLookUp</name>
    <filename>a00097.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00097.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DynArcLookUp</name>
      <anchorfile>a00097.html</anchorfile>
      <anchor>f350d80d0f535f2e22d2759b520b44b7</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>operator()</name>
      <anchorfile>a00097.html</anchorfile>
      <anchor>1305a076cbf538584befb7cd4b2b4e99</anchor>
      <arglist>(Node s, Node t, Arc p=INVALID) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ArcLookUp</name>
    <filename>a00017.html</filename>
    <templarg>GR</templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00017.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcLookUp</name>
      <anchorfile>a00017.html</anchorfile>
      <anchor>c8d5a1b8fa9f0a94a40144de9083bc80</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>refresh</name>
      <anchorfile>a00017.html</anchorfile>
      <anchor>d03e249e4f6d22977dfae5910314ee4e</anchor>
      <arglist>(Node n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>refresh</name>
      <anchorfile>a00017.html</anchorfile>
      <anchor>5f2e190b8261a98c97c2ea4e86670d54</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>operator()</name>
      <anchorfile>a00017.html</anchorfile>
      <anchor>e268c23cfa76e860ab056c6d5ec5e727</anchor>
      <arglist>(Node s, Node t) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::AllArcLookUp</name>
    <filename>a00003.html</filename>
    <templarg></templarg>
    <base>ArcLookUp&lt; GR &gt;</base>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AllArcLookUp</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>da3bb86d0e7763d7fc69c1d179d1d123</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>refresh</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>d03e249e4f6d22977dfae5910314ee4e</anchor>
      <arglist>(Node n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>refresh</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>5f2e190b8261a98c97c2ea4e86670d54</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>operator()</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>48edf5eb52c96e5692395b9239d1e29e</anchor>
      <arglist>(Node s, Node t, Arc prev=INVALID) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Counter</name>
    <filename>a00067.html</filename>
    <member kind="typedef">
      <type>_SubCounter&lt; Counter &gt;</type>
      <name>SubCounter</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>85506a22a53d73d9d211f6a97912b6f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>_NoSubCounter&lt; Counter &gt;</type>
      <name>NoSubCounter</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>1a0261ef4a0c51f79191ad4597e2cd81</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Counter</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>e1666ee5f8d8cf588500c65a9935c4fe</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Counter</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>837a4f6c542af203bc3b5d49d0288220</anchor>
      <arglist>(std::string title, std::ostream &amp;os=std::cerr)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Counter</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>b3194596a4d3b2fa5630e036feff4897</anchor>
      <arglist>(const char *title, std::ostream &amp;os=std::cerr)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Counter</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>d5261fa114af1e8c70c21862d857eaec</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Counter &amp;</type>
      <name>operator++</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>b1598a2acb1c3be3b168855648235e73</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>operator++</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>5526c2c4b0fa541a50ffcaaf25e149a6</anchor>
      <arglist>(int)</arglist>
    </member>
    <member kind="function">
      <type>Counter &amp;</type>
      <name>operator--</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>e495b9da2d2ad8a4b3b6c6479a5a61b8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>operator--</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>5496187b3dcf6064c5c0b158ab6b7264</anchor>
      <arglist>(int)</arglist>
    </member>
    <member kind="function">
      <type>Counter &amp;</type>
      <name>operator+=</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>f8fc68a7c8df80c8abec43da22138159</anchor>
      <arglist>(int c)</arglist>
    </member>
    <member kind="function">
      <type>Counter &amp;</type>
      <name>operator-=</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>c48a256851ae1729e19b1b3c409fc9ce</anchor>
      <arglist>(int c)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>2cd75ed371f4cdabb3cff3d4b77bee84</anchor>
      <arglist>(int c=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator int</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>4f4ea421e40bda08a2deca657f640fea</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::NoCounter</name>
    <filename>a00191.html</filename>
  </compound>
  <compound kind="class">
    <name>lemon::CplexEnv</name>
    <filename>a00069.html</filename>
    <class kind="class">lemon::CplexEnv::LicenseError</class>
    <member kind="function">
      <type></type>
      <name>CplexEnv</name>
      <anchorfile>a00069.html</anchorfile>
      <anchor>02bd581bb5b04ed7f68844e497d8c5f5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CplexEnv</name>
      <anchorfile>a00069.html</anchorfile>
      <anchor>55676c8bcdd5d6168efe436cc8cdc47e</anchor>
      <arglist>(const CplexEnv &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CplexEnv &amp;</type>
      <name>operator=</name>
      <anchorfile>a00069.html</anchorfile>
      <anchor>7523f60d35e342e9a1f70daf83e0ac5c</anchor>
      <arglist>(const CplexEnv &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CplexEnv</name>
      <anchorfile>a00069.html</anchorfile>
      <anchor>3c964016341dd3a4fce8ed1e22f84b4c</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::CplexEnv::LicenseError</name>
    <filename>a00163.html</filename>
    <base>lemon::Exception</base>
    <member kind="function" virtualness="virtual">
      <type>virtual const char *</type>
      <name>what</name>
      <anchorfile>a00163.html</anchorfile>
      <anchor>ff06f49065b54a8a86e02e9a2441a8ba</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::CplexBase</name>
    <filename>a00068.html</filename>
    <base virtualness="virtual">lemon::LpBase</base>
    <member kind="function">
      <type>const CplexEnv &amp;</type>
      <name>env</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>1df4c6fdcec9714c3fe0984374a1e879</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const cpxenv *</type>
      <name>cplexEnv</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>c9821c402b14564694c84aad34d68201</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>cpxenv *</type>
      <name>cplexEnv</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>6898c265c956dea4cd1c62850b4940da</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>cpxlp *</type>
      <name>cplexLp</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>e1d851f69016621c992d1f01c98b9a4a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const cpxlp *</type>
      <name>cplexLp</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>2abeeaa0d636b4705c3487301115f3ef</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::CplexLp</name>
    <filename>a00070.html</filename>
    <base>lemon::LpSolver</base>
    <base>lemon::CplexBase</base>
    <member kind="function">
      <type></type>
      <name>CplexLp</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>f8d078878fc450c02ac2149b2a0f13f3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CplexLp</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>fedf81e8d97a554db5a53f0a3eef3f17</anchor>
      <arglist>(const CplexEnv &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CplexLp</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>69ef0bf0bdc30a3e48cc64a05e13d08a</anchor>
      <arglist>(const CplexLp &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CplexLp</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>5dbdb17742d0a0ffe65d037888f8f684</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CplexLp *</type>
      <name>cloneSolver</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>b902f14b7d737a92da179b33897a09ed</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CplexLp *</type>
      <name>newSolver</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>35f7af63f61c52bb55dfc690e436f99c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>SolveExitStatus</type>
      <name>solvePrimal</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>3316441c156eb3cc78bcb835f3e86c3e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>SolveExitStatus</type>
      <name>solveDual</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>76f2afae9c0dcb0df8d677346129bdfd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>SolveExitStatus</type>
      <name>solveBarrier</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>1e69e9be293c6f7a5433e93d70065a89</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::CplexMip</name>
    <filename>a00071.html</filename>
    <base>lemon::MipSolver</base>
    <base>lemon::CplexBase</base>
    <member kind="function">
      <type></type>
      <name>CplexMip</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>6e68387a4541321452be5b423f00cf4f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CplexMip</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>c5a31290f5306ad26aaabc99ca88e254</anchor>
      <arglist>(const CplexEnv &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CplexMip</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>eb31411f879211d796d5d9dc8a5d8f26</anchor>
      <arglist>(const CplexMip &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CplexMip</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>766aee9d3de7a22a549dd43b4b974224</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CplexMip *</type>
      <name>cloneSolver</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>a0e206ab36331bf7d30c3f2907f70b51</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CplexMip *</type>
      <name>newSolver</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>747a58e5bf7f1b282ec7cd8f372d4a38</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::DfsDefaultTraits</name>
    <filename>a00075.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; typename Digraph::Arc &gt;</type>
      <name>PredMap</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>2f311e3024ce1a50c00ffc291bce8e85</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>NullMap&lt; typename Digraph::Node, bool &gt;</type>
      <name>ProcessedMap</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>b4c841b7d3ecb344be7b85370b908920</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; bool &gt;</type>
      <name>ReachedMap</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>ffb032b5578e24579000f9899512ae60</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; int &gt;</type>
      <name>DistMap</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>e1b02f76f4c6728d9c92da415fe15f7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static PredMap *</type>
      <name>createPredMap</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>1d05c10ba44d3b5104c17e8181912d2c</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ProcessedMap *</type>
      <name>createProcessedMap</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>ef8f11ee548ae3b61e7c2a0018455569</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ReachedMap *</type>
      <name>createReachedMap</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>1c6c24dc0cfde917210decb6f31be54c</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static DistMap *</type>
      <name>createDistMap</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>c24cfd641a704fa02ebd5d2403125c66</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Dfs</name>
    <filename>a00074.html</filename>
    <templarg>GR</templarg>
    <templarg>TR</templarg>
    <class kind="struct">lemon::Dfs::SetDistMap</class>
    <class kind="struct">lemon::Dfs::SetPredMap</class>
    <class kind="struct">lemon::Dfs::SetProcessedMap</class>
    <class kind="struct">lemon::Dfs::SetReachedMap</class>
    <class kind="struct">lemon::Dfs::SetStandardProcessedMap</class>
    <member kind="typedef">
      <type>TR::Digraph</type>
      <name>Digraph</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>750f38b47fd56ef715849c3bd77fbe77</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::PredMap</type>
      <name>PredMap</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>dc9638061e7acac70160feb0ea49c29c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::DistMap</type>
      <name>DistMap</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>d6d9c2819f215994ded26fbe42e2fd26</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::ReachedMap</type>
      <name>ReachedMap</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>f23453a839e62b3dcc311e0a839e31ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::ProcessedMap</type>
      <name>ProcessedMap</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>43e332c55acc2c5cb99d35ff40accab1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>PredMapPath&lt; Digraph, PredMap &gt;</type>
      <name>Path</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>4d7ae5a0651af9c9cfadd1462718979d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR</type>
      <name>Traits</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>b157e5452122f7a7f73dfda5ed931d69</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Dfs</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>77e8d05a88fdbd6eadbc7b562e642c22</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Dfs</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>0853bb5f644e1434ff68eac70b1dd63c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Dfs &amp;</type>
      <name>predMap</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>0ed6678f822688fc2d17e625400819b0</anchor>
      <arglist>(PredMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Dfs &amp;</type>
      <name>reachedMap</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>48fac08b556d545572afe15b39af820c</anchor>
      <arglist>(ReachedMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Dfs &amp;</type>
      <name>processedMap</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>3e278058f34be77a47e2a7ba4f9c12dc</anchor>
      <arglist>(ProcessedMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Dfs &amp;</type>
      <name>distMap</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>67d147d1e125d3a56e49ddd920c1bc98</anchor>
      <arglist>(DistMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addSource</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>c785b6504b67a963b4c20bae90441a77</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>processNextArc</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>773a0c2fc82672c4dd7af546908b8a5c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>OutArcIt</type>
      <name>nextArc</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>4136c3eb28cb12e937c165cb8bb5bf3c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>emptyQueue</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>b6dcd2be02feaff0a95c21824e805445</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>queueSize</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>942d30059e28f60ba6dd1944ab8e416e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>60de64d75454385b23995437f1d72669</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>8b8de9dfc16e4b3cb6b38733e82f6449</anchor>
      <arglist>(Node t)</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>start</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>c335546ef3092c56934f909ca164940e</anchor>
      <arglist>(const ArcBoolMap &amp;am)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>e3f0682c90a4e8a23c259943e899402e</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>run</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>d2c3c4217c98b58a8618397d65d400c8</anchor>
      <arglist>(Node s, Node t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Path</type>
      <name>path</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>6a33507c667ac2bddd240d0baff1a573</anchor>
      <arglist>(Node t) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>dist</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>2e23ddd204b1b00807853d2620a3c1b1</anchor>
      <arglist>(Node v) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>predArc</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>f9fdbb84b58bd3f5daedde027070e056</anchor>
      <arglist>(Node v) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>predNode</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>89bd1c535e3d4fcb9c58afbb54ac47e1</anchor>
      <arglist>(Node v) const </arglist>
    </member>
    <member kind="function">
      <type>const DistMap &amp;</type>
      <name>distMap</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>77feee7321863f24b67813c0ef8dc5c0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const PredMap &amp;</type>
      <name>predMap</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>eec5920cc073b88c4c5f6c46a8f4aa4b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>reached</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>bb98c427556a2afe917e41fe8d3e75bf</anchor>
      <arglist>(Node v) const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::Dfs::SetDistMap</name>
    <filename>a00238.html</filename>
    <templarg></templarg>
    <base>Dfs&lt; Digraph, SetDistMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Dfs::SetPredMap</name>
    <filename>a00247.html</filename>
    <templarg></templarg>
    <base>Dfs&lt; Digraph, SetPredMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Dfs::SetProcessedMap</name>
    <filename>a00250.html</filename>
    <templarg></templarg>
    <base>Dfs&lt; Digraph, SetProcessedMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Dfs::SetReachedMap</name>
    <filename>a00254.html</filename>
    <templarg></templarg>
    <base>Dfs&lt; Digraph, SetReachedMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Dfs::SetStandardProcessedMap</name>
    <filename>a00261.html</filename>
    <base>Dfs&lt; Digraph, SetStandardProcessedMapTraits &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::DfsWizardDefaultTraits</name>
    <filename>a00081.html</filename>
    <templarg>GR</templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; typename Digraph::Arc &gt;</type>
      <name>PredMap</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>2f311e3024ce1a50c00ffc291bce8e85</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>NullMap&lt; typename Digraph::Node, bool &gt;</type>
      <name>ProcessedMap</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>b4c841b7d3ecb344be7b85370b908920</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; bool &gt;</type>
      <name>ReachedMap</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>ffb032b5578e24579000f9899512ae60</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; int &gt;</type>
      <name>DistMap</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>e1b02f76f4c6728d9c92da415fe15f7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>lemon::Path&lt; Digraph &gt;</type>
      <name>Path</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>6989b0a22a99c251a2e7c99cf36605ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static PredMap *</type>
      <name>createPredMap</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>1d05c10ba44d3b5104c17e8181912d2c</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ProcessedMap *</type>
      <name>createProcessedMap</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>ef8f11ee548ae3b61e7c2a0018455569</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ReachedMap *</type>
      <name>createReachedMap</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>1c6c24dc0cfde917210decb6f31be54c</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static DistMap *</type>
      <name>createDistMap</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>c24cfd641a704fa02ebd5d2403125c66</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::DfsWizardBase</name>
    <filename>a00080.html</filename>
    <templarg></templarg>
    <base>DfsWizardDefaultTraits&lt; GR &gt;</base>
    <member kind="function">
      <type></type>
      <name>DfsWizardBase</name>
      <anchorfile>a00080.html</anchorfile>
      <anchor>c6751e3747d9999ff22806a701b0f48b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DfsWizardBase</name>
      <anchorfile>a00080.html</anchorfile>
      <anchor>b58600d263349752f489d95593ca3bc4</anchor>
      <arglist>(const GR &amp;g)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::DfsWizard</name>
    <filename>a00079.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>DfsWizard</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>8496841a286bdc5e6b7804a1c8b7bed9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DfsWizard</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>f2e4b792dede58b8d7f827878dee9a10</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DfsWizard</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>2bf35f5b0270f76877c701f3b87a4fba</anchor>
      <arglist>(const TR &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>e3f0682c90a4e8a23c259943e899402e</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>run</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>d2c3c4217c98b58a8618397d65d400c8</anchor>
      <arglist>(Node s, Node t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>DfsWizard&lt; SetPredMapBase&lt; T &gt; &gt;</type>
      <name>predMap</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>ca5d83d48388626cd278cf188307b86b</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>DfsWizard&lt; SetReachedMapBase&lt; T &gt; &gt;</type>
      <name>reachedMap</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>7dc7719df2d1696fe9dbafb123e58ed8</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>DfsWizard&lt; SetDistMapBase&lt; T &gt; &gt;</type>
      <name>distMap</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>0be991747526c0def864e663fd1e116c</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>DfsWizard&lt; SetProcessedMapBase&lt; T &gt; &gt;</type>
      <name>processedMap</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>e1dbc08f2be38480d10d629e55e89b95</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>DfsWizard&lt; SetPathBase&lt; T &gt; &gt;</type>
      <name>path</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>32c910b02d7760f908bdcde04369fb0d</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>DfsWizard</type>
      <name>dist</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>ef118068d087c63cd0111751ccffa56e</anchor>
      <arglist>(const int &amp;d)</arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::Digraph</type>
      <name>Digraph</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>750f38b47fd56ef715849c3bd77fbe77</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::PredMap</type>
      <name>PredMap</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>dc9638061e7acac70160feb0ea49c29c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::DistMap</type>
      <name>DistMap</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>d6d9c2819f215994ded26fbe42e2fd26</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::ReachedMap</type>
      <name>ReachedMap</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>f23453a839e62b3dcc311e0a839e31ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::ProcessedMap</type>
      <name>ProcessedMap</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>43e332c55acc2c5cb99d35ff40accab1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::Path</type>
      <name>Path</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>d6d7757da799220a95281fbee4d3fb8f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::DfsVisitor</name>
    <filename>a00078.html</filename>
    <templarg>GR</templarg>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>f763d0c0856194421ebd6ccc73b921c3</anchor>
      <arglist>(const Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>stop</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>61325cf6c47c78fe2f978694f9e7bda6</anchor>
      <arglist>(const Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reach</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>4f6dc7007e25f1c20b4fbb8912a83817</anchor>
      <arglist>(const Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>discover</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>be70d6cbc4419667e6aa533fc0fa262f</anchor>
      <arglist>(const Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>examine</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>3656d1f80036aee1dcaa927450b70c2a</anchor>
      <arglist>(const Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>leave</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>37622f5403978798170fd218cdb55647</anchor>
      <arglist>(const Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>backtrack</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>05430d1c630288c497a019594350e94e</anchor>
      <arglist>(const Arc &amp;arc)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::DfsVisitDefaultTraits</name>
    <filename>a00077.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00077.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; bool &gt;</type>
      <name>ReachedMap</name>
      <anchorfile>a00077.html</anchorfile>
      <anchor>ffb032b5578e24579000f9899512ae60</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ReachedMap *</type>
      <name>createReachedMap</name>
      <anchorfile>a00077.html</anchorfile>
      <anchor>1491e8ada6a6d5fdd321e3fee43844e1</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::DfsVisit</name>
    <filename>a00076.html</filename>
    <templarg>GR</templarg>
    <templarg>VS</templarg>
    <templarg>TR</templarg>
    <class kind="struct">lemon::DfsVisit::SetReachedMap</class>
    <member kind="typedef">
      <type>TR</type>
      <name>Traits</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>b157e5452122f7a7f73dfda5ed931d69</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Digraph</type>
      <name>Digraph</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>a6928feef02be4f2a184775d19dc6373</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>VS</type>
      <name>Visitor</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>3bece3b7c0b0189b735a408e533f3d73</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::ReachedMap</type>
      <name>ReachedMap</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>90221be1b9274785db2188548354d2aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DfsVisit</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>b2606d461569b07df9a904f66aadfa82</anchor>
      <arglist>(const Digraph &amp;digraph, Visitor &amp;visitor)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~DfsVisit</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>1b3719df6ef9a6c93579c6b48309fb9a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>DfsVisit &amp;</type>
      <name>reachedMap</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>534f6847b03afac5b47af994d4f6666b</anchor>
      <arglist>(ReachedMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addSource</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>c785b6504b67a963b4c20bae90441a77</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>processNextArc</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>773a0c2fc82672c4dd7af546908b8a5c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>nextArc</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>22c9301865dd8c7dffe8e48cae77823f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>emptyQueue</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>b6dcd2be02feaff0a95c21824e805445</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>queueSize</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>942d30059e28f60ba6dd1944ab8e416e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>60de64d75454385b23995437f1d72669</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>8b8de9dfc16e4b3cb6b38733e82f6449</anchor>
      <arglist>(Node t)</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>start</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>de3539cfc4dd0f5cf01e91b2ebd09b78</anchor>
      <arglist>(const AM &amp;am)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>e3f0682c90a4e8a23c259943e899402e</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>run</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>d2c3c4217c98b58a8618397d65d400c8</anchor>
      <arglist>(Node s, Node t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>reached</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>bb98c427556a2afe917e41fe8d3e75bf</anchor>
      <arglist>(Node v) const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::DfsVisit::SetReachedMap</name>
    <filename>a00255.html</filename>
    <templarg></templarg>
    <base>DfsVisit&lt; Digraph, Visitor, SetReachedMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::DijkstraDefaultOperationTraits</name>
    <filename>a00088.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00088.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Value</type>
      <name>zero</name>
      <anchorfile>a00088.html</anchorfile>
      <anchor>f24efe5c6b0edcb586538222fb5b1024</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Value</type>
      <name>plus</name>
      <anchorfile>a00088.html</anchorfile>
      <anchor>cce469a9728bdb4c09c015dc31a3eb3c</anchor>
      <arglist>(const Value &amp;left, const Value &amp;right)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>less</name>
      <anchorfile>a00088.html</anchorfile>
      <anchor>c142bbf30883f244af50aefbf661bd6a</anchor>
      <arglist>(const Value &amp;left, const Value &amp;right)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::DijkstraDefaultTraits</name>
    <filename>a00089.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>LEN</type>
      <name>LengthMap</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>7398ca1da160bb30ee090866bbfc12ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>LEN::Value</type>
      <name>Value</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>c2ac7ac2928b4cfeafdc493dd44f61c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>DijkstraDefaultOperationTraits&lt; Value &gt;</type>
      <name>OperationTraits</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>44e2d7b3991904d43e2dc64bc7b3994b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; int &gt;</type>
      <name>HeapCrossRef</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>be21d3edcb6bd235d862becfd84c6d92</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>BinHeap&lt; typename LEN::Value, HeapCrossRef, std::less&lt; Value &gt; &gt;</type>
      <name>Heap</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>e9674759c9994950c85735775791c79d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; typename Digraph::Arc &gt;</type>
      <name>PredMap</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>2f311e3024ce1a50c00ffc291bce8e85</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>NullMap&lt; typename Digraph::Node, bool &gt;</type>
      <name>ProcessedMap</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>b4c841b7d3ecb344be7b85370b908920</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; typename LEN::Value &gt;</type>
      <name>DistMap</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>9735fe3ebb1835f94ac433863be10527</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static HeapCrossRef *</type>
      <name>createHeapCrossRef</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>e1638f8612c39f3d981d57056aca2c37</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Heap *</type>
      <name>createHeap</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>01816b6b724aa6d7f99d7ac19d549125</anchor>
      <arglist>(HeapCrossRef &amp;r)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static PredMap *</type>
      <name>createPredMap</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>1d05c10ba44d3b5104c17e8181912d2c</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ProcessedMap *</type>
      <name>createProcessedMap</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>ef8f11ee548ae3b61e7c2a0018455569</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static DistMap *</type>
      <name>createDistMap</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>c24cfd641a704fa02ebd5d2403125c66</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Dijkstra</name>
    <filename>a00087.html</filename>
    <templarg>GR</templarg>
    <templarg>LEN</templarg>
    <templarg>TR</templarg>
    <class kind="struct">lemon::Dijkstra::SetDistMap</class>
    <class kind="struct">lemon::Dijkstra::SetHeap</class>
    <class kind="struct">lemon::Dijkstra::SetOperationTraits</class>
    <class kind="struct">lemon::Dijkstra::SetPredMap</class>
    <class kind="struct">lemon::Dijkstra::SetProcessedMap</class>
    <class kind="struct">lemon::Dijkstra::SetStandardHeap</class>
    <class kind="struct">lemon::Dijkstra::SetStandardProcessedMap</class>
    <member kind="typedef">
      <type>TR::Digraph</type>
      <name>Digraph</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>750f38b47fd56ef715849c3bd77fbe77</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::LengthMap::Value</type>
      <name>Value</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>6f677ff45d75aa12b23a873c02b37256</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::LengthMap</type>
      <name>LengthMap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>c1094a242a4e9be216f0623b0a7381c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::PredMap</type>
      <name>PredMap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>dc9638061e7acac70160feb0ea49c29c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::DistMap</type>
      <name>DistMap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>d6d9c2819f215994ded26fbe42e2fd26</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::ProcessedMap</type>
      <name>ProcessedMap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>43e332c55acc2c5cb99d35ff40accab1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>PredMapPath&lt; Digraph, PredMap &gt;</type>
      <name>Path</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>4d7ae5a0651af9c9cfadd1462718979d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::HeapCrossRef</type>
      <name>HeapCrossRef</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>0dc498f33875179555a71ba9441c400a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::Heap</type>
      <name>Heap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>b1e3f1c47cd4bffb15e3bbc452585f7c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR::OperationTraits</type>
      <name>OperationTraits</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>fda95bfb086e37f03cb0be4ba8725f47</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TR</type>
      <name>Traits</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>b157e5452122f7a7f73dfda5ed931d69</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Dijkstra</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>58fc0895a271a1aa712f66aaf3425b12</anchor>
      <arglist>(const Digraph &amp;g, const LengthMap &amp;length)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Dijkstra</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>9f2cad16e6cb57cad22fe766c9e809ca</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Dijkstra &amp;</type>
      <name>lengthMap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>cca1fc5a2484f8284b37ac8b98a6de89</anchor>
      <arglist>(const LengthMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Dijkstra &amp;</type>
      <name>predMap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>553990801b3f933961ad14abfee00c0e</anchor>
      <arglist>(PredMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Dijkstra &amp;</type>
      <name>processedMap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>6f543d7d3c4b2ab9ecdef5d14f90269d</anchor>
      <arglist>(ProcessedMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Dijkstra &amp;</type>
      <name>distMap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>344715cda707dac050bf32c85923f651</anchor>
      <arglist>(DistMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Dijkstra &amp;</type>
      <name>heap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>7a0403be9c030237c46a9ab320f5bfbe</anchor>
      <arglist>(Heap &amp;hp, HeapCrossRef &amp;cr)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addSource</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>cf3cbe74733e4534c1cebb8383974bae</anchor>
      <arglist>(Node s, Value dst=OperationTraits::zero())</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>processNextNode</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>dba758047d7378b8a06320d29ce170d7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>nextNode</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>ca98e3b1fef1a4508df143a24b73b046</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>emptyQueue</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>b6dcd2be02feaff0a95c21824e805445</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>queueSize</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>942d30059e28f60ba6dd1944ab8e416e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>60de64d75454385b23995437f1d72669</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>8b8de9dfc16e4b3cb6b38733e82f6449</anchor>
      <arglist>(Node t)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>start</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>4d3d39ff1805757f57570e6db83630cc</anchor>
      <arglist>(const NodeBoolMap &amp;nm)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>e3f0682c90a4e8a23c259943e899402e</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>run</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>d2c3c4217c98b58a8618397d65d400c8</anchor>
      <arglist>(Node s, Node t)</arglist>
    </member>
    <member kind="function">
      <type>Path</type>
      <name>path</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>6a33507c667ac2bddd240d0baff1a573</anchor>
      <arglist>(Node t) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>dist</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>70a41051a602b0a9134d128cf1f409f6</anchor>
      <arglist>(Node v) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>predArc</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>f9fdbb84b58bd3f5daedde027070e056</anchor>
      <arglist>(Node v) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>predNode</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>89bd1c535e3d4fcb9c58afbb54ac47e1</anchor>
      <arglist>(Node v) const </arglist>
    </member>
    <member kind="function">
      <type>const DistMap &amp;</type>
      <name>distMap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>77feee7321863f24b67813c0ef8dc5c0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const PredMap &amp;</type>
      <name>predMap</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>eec5920cc073b88c4c5f6c46a8f4aa4b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>reached</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>bb98c427556a2afe917e41fe8d3e75bf</anchor>
      <arglist>(Node v) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>processed</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>376f9f86c0b6bf5fbcf5ee8d76f4cc7a</anchor>
      <arglist>(Node v) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>currentDist</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>514b1faa39e460ebc8e00c90d839f3cf</anchor>
      <arglist>(Node v) const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::Dijkstra::SetDistMap</name>
    <filename>a00239.html</filename>
    <templarg></templarg>
    <base>Dijkstra&lt; Digraph, LengthMap, SetDistMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Dijkstra::SetHeap</name>
    <filename>a00244.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>Dijkstra&lt; Digraph, LengthMap, SetHeapTraits&lt; H, CR &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Dijkstra::SetOperationTraits</name>
    <filename>a00245.html</filename>
    <templarg></templarg>
    <base>Dijkstra&lt; Digraph, LengthMap, SetOperationTraitsTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Dijkstra::SetPredMap</name>
    <filename>a00249.html</filename>
    <templarg></templarg>
    <base>Dijkstra&lt; Digraph, LengthMap, SetPredMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Dijkstra::SetProcessedMap</name>
    <filename>a00252.html</filename>
    <templarg></templarg>
    <base>Dijkstra&lt; Digraph, LengthMap, SetProcessedMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Dijkstra::SetStandardHeap</name>
    <filename>a00259.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>Dijkstra&lt; Digraph, LengthMap, SetStandardHeapTraits&lt; H, CR &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Dijkstra::SetStandardProcessedMap</name>
    <filename>a00260.html</filename>
    <base>Dijkstra&lt; Digraph, LengthMap, SetStandardProcessedMapTraits &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::DijkstraWizardDefaultTraits</name>
    <filename>a00092.html</filename>
    <templarg>GR</templarg>
    <templarg>LEN</templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>LEN</type>
      <name>LengthMap</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>7398ca1da160bb30ee090866bbfc12ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>LEN::Value</type>
      <name>Value</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>c2ac7ac2928b4cfeafdc493dd44f61c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>DijkstraDefaultOperationTraits&lt; Value &gt;</type>
      <name>OperationTraits</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>44e2d7b3991904d43e2dc64bc7b3994b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; int &gt;</type>
      <name>HeapCrossRef</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>be21d3edcb6bd235d862becfd84c6d92</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>BinHeap&lt; Value, typename Digraph::template NodeMap&lt; int &gt;, std::less&lt; Value &gt; &gt;</type>
      <name>Heap</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>7ef292c7753186b672855ef6a2290cff</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; typename Digraph::Arc &gt;</type>
      <name>PredMap</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>2f311e3024ce1a50c00ffc291bce8e85</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>NullMap&lt; typename Digraph::Node, bool &gt;</type>
      <name>ProcessedMap</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>b4c841b7d3ecb344be7b85370b908920</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; typename LEN::Value &gt;</type>
      <name>DistMap</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>9735fe3ebb1835f94ac433863be10527</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>lemon::Path&lt; Digraph &gt;</type>
      <name>Path</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>6989b0a22a99c251a2e7c99cf36605ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static HeapCrossRef *</type>
      <name>createHeapCrossRef</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>e1638f8612c39f3d981d57056aca2c37</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Heap *</type>
      <name>createHeap</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>01816b6b724aa6d7f99d7ac19d549125</anchor>
      <arglist>(HeapCrossRef &amp;r)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static PredMap *</type>
      <name>createPredMap</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>1d05c10ba44d3b5104c17e8181912d2c</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ProcessedMap *</type>
      <name>createProcessedMap</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>ef8f11ee548ae3b61e7c2a0018455569</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static DistMap *</type>
      <name>createDistMap</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>c24cfd641a704fa02ebd5d2403125c66</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::DijkstraWizardBase</name>
    <filename>a00091.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>DijkstraWizardDefaultTraits&lt; GR, LEN &gt;</base>
    <member kind="function">
      <type></type>
      <name>DijkstraWizardBase</name>
      <anchorfile>a00091.html</anchorfile>
      <anchor>33295db5ac017fd3f4aabbabb356973f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DijkstraWizardBase</name>
      <anchorfile>a00091.html</anchorfile>
      <anchor>fb8afa8f01b04960d3177cbd0568713e</anchor>
      <arglist>(const GR &amp;g, const LEN &amp;l)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::DijkstraWizard</name>
    <filename>a00090.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>DijkstraWizard</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>9f26d04d4cbf48a326f1997016e9d413</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DijkstraWizard</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>f5dd28cdef6fc5915eb674832737ee85</anchor>
      <arglist>(const Digraph &amp;g, const LengthMap &amp;l)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DijkstraWizard</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>f83a6b7a96934b5c0efe73294f5d9412</anchor>
      <arglist>(const TR &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>e3f0682c90a4e8a23c259943e899402e</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>run</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>d2c3c4217c98b58a8618397d65d400c8</anchor>
      <arglist>(Node s, Node t)</arglist>
    </member>
    <member kind="function">
      <type>DijkstraWizard&lt; SetPredMapBase&lt; T &gt; &gt;</type>
      <name>predMap</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>61d87a38c8df9ce2561583889ed7e56e</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>DijkstraWizard&lt; SetDistMapBase&lt; T &gt; &gt;</type>
      <name>distMap</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>8c8a3ae0b339a2f2295051927956756f</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>DijkstraWizard&lt; SetProcessedMapBase&lt; T &gt; &gt;</type>
      <name>processedMap</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>765acfb293a8a3af9491cc2370fe9235</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>DijkstraWizard&lt; SetPathBase&lt; T &gt; &gt;</type>
      <name>path</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>b778fcf49e81a1a2842878a4cacb0988</anchor>
      <arglist>(const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>DijkstraWizard</type>
      <name>dist</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>fd921c8909e9b920668ff2f9aa6c2208</anchor>
      <arglist>(const Value &amp;d)</arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::Digraph</type>
      <name>Digraph</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>750f38b47fd56ef715849c3bd77fbe77</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::LengthMap</type>
      <name>LengthMap</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>c1094a242a4e9be216f0623b0a7381c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>LengthMap::Value</type>
      <name>Value</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>57987ef19cf8e5662abe4bdb49dc6ef1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::PredMap</type>
      <name>PredMap</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>dc9638061e7acac70160feb0ea49c29c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::DistMap</type>
      <name>DistMap</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>d6d9c2819f215994ded26fbe42e2fd26</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::ProcessedMap</type>
      <name>ProcessedMap</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>43e332c55acc2c5cb99d35ff40accab1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::Path</type>
      <name>Path</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>d6d7757da799220a95281fbee4d3fb8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>TR::Heap</type>
      <name>Heap</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>b1e3f1c47cd4bffb15e3bbc452585f7c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::DimacsDescriptor</name>
    <filename>a00093.html</filename>
    <member kind="enumeration">
      <name>Type</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>1d1cfd8ffb84e947f82999c682b666a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>NONE</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>1d1cfd8ffb84e947f82999c682b666a7c157bdf0b85a40d2619cbc8bc1ae5fe2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MIN</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>1d1cfd8ffb84e947f82999c682b666a7957e8250f68e7b5677b22397c2c1b51e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MAX</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>1d1cfd8ffb84e947f82999c682b666a7d7e097bda6d981de2520f49fe74c25b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>SP</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>1d1cfd8ffb84e947f82999c682b666a777c32937cb56776e7d96ed4b5e43e06b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MAT</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>1d1cfd8ffb84e947f82999c682b666a7beed243a6ffb8f4f203ba3fb934ab3b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DimacsDescriptor</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>bb1aab8d4a78dbac2c1c69fccf3b337e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>Type</type>
      <name>type</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>b6f4e6d3fde00ce906e46494f60dfe7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nodeNum</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>787bebfa4c764244b11726a4024576a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>edgeNum</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>d6c4c67355fbdfa9035a24b8a3c76dee</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ListArcSet</name>
    <filename>a00165.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>ListArcSet</name>
      <anchorfile>a00165.html</anchorfile>
      <anchor>a282329c1d82b1cb36430d5905c0ae41</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>addArc</name>
      <anchorfile>a00165.html</anchorfile>
      <anchor>b947ea03356504c5c4b3c8ebfed1516a</anchor>
      <arglist>(const Node &amp;s, const Node &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00165.html</anchorfile>
      <anchor>3a2143bdbfc896efc87ea1a34bbcf824</anchor>
      <arglist>(const Arc &amp;a)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ListEdgeSet</name>
    <filename>a00167.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>ListEdgeSet</name>
      <anchorfile>a00167.html</anchorfile>
      <anchor>dda52643f52a89c07675d8f223574921</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>Edge</type>
      <name>addEdge</name>
      <anchorfile>a00167.html</anchorfile>
      <anchor>6192e97afb39b9e2ec4babc88a8aca5f</anchor>
      <arglist>(const Node &amp;u, const Node &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00167.html</anchorfile>
      <anchor>ccff03c7f445a919e57d4ca8e1b2199f</anchor>
      <arglist>(const Edge &amp;e)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SmartArcSet</name>
    <filename>a00267.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>SmartArcSet</name>
      <anchorfile>a00267.html</anchorfile>
      <anchor>695e3ed4a0721ca1e24704aa26ebfff1</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>addArc</name>
      <anchorfile>a00267.html</anchorfile>
      <anchor>b947ea03356504c5c4b3c8ebfed1516a</anchor>
      <arglist>(const Node &amp;s, const Node &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00267.html</anchorfile>
      <anchor>8d985300b138b6c5556ab17ed4df3b38</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SmartEdgeSet</name>
    <filename>a00270.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>SmartEdgeSet</name>
      <anchorfile>a00270.html</anchorfile>
      <anchor>ea25784c832ec83e58bb3e0019c507b3</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>Edge</type>
      <name>addEdge</name>
      <anchorfile>a00270.html</anchorfile>
      <anchor>6192e97afb39b9e2ec4babc88a8aca5f</anchor>
      <arglist>(const Node &amp;u, const Node &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00270.html</anchorfile>
      <anchor>8d985300b138b6c5556ab17ed4df3b38</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Elevator</name>
    <filename>a00103.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>Elevator</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>c5d91754365aee61cba08bc0706fda0f</anchor>
      <arglist>(const GR &amp;graph, int max_level)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Elevator</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>edd805b73161ff74106bb1cbb18ae976</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>activate</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>4db11bfc0b64725808f2cedf6efe85bc</anchor>
      <arglist>(Item i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deactivate</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>6399621b031eeef6b06bf4f25d0922c7</anchor>
      <arglist>(Item i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>active</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>16d8e320d25a950a7ccd3f42fc12d699</anchor>
      <arglist>(Item i) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>operator[]</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>21db8d22216b643992538b26efd49553</anchor>
      <arglist>(Item i) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>onLevel</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>4f42d083c6e02d3bb636f4a9865e17ea</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>emptyLevel</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>7222462ad836aa926e3dc04fc982e329</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>aboveLevel</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>a57b36ed3da61ecce558c68cd214b3b3</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>activesOnLevel</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>7e843e422235ed011d3218242d028cc6</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>activeFree</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>91603b89ccd4b7b3f806a24bdcedcb30</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maxLevel</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>128a4da1b7623c68670a387fa148c65f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>lift</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>9e8cac16792c7bdac1584828690059a5</anchor>
      <arglist>(Item i, int new_level)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>dirtyTopButOne</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>45c945d67da38b2225cf6c34504cdb08</anchor>
      <arglist>(Item i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftToTop</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>f2c6a1f548364b1447a377c311eb948e</anchor>
      <arglist>(int l)</arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>highestActive</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>47eff0f8503f5d8038b31a4b36874c21</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>highestActiveLevel</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>d57dbb9976fbf0bfa412b0dc88776ffc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftHighestActive</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>d448db204a4dc8bcb628933f31a113d5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftHighestActive</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>744a06f7232fcac254c31fe9b2b879b0</anchor>
      <arglist>(int new_level)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftHighestActiveToTop</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>fa70a583d65837a24a1c86bf52a6fdc9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>activeOn</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>9963ab165df3118927670e795da8fc57</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>liftActiveOn</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>4e2a5214802d2fac206445d347757251</anchor>
      <arglist>(int level)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftActiveOn</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>2a5c7fcaf906c207748cbc6a2993ceb3</anchor>
      <arglist>(int level, int new_level)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftActiveToTop</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>e396f5ceb651cfc812eee9737a5fbd79</anchor>
      <arglist>(int level)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initStart</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>92a97cb68d6dbaf545fa72b36dfa3cd1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initAddItem</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>64a9da3e23ab4c7248a6efcde26b1064</anchor>
      <arglist>(Item i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initNewLevel</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>a8880e6363f8b0c2e012a03cb8bc0d77</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initFinish</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>faee4c060888616a45d071530326ea8b</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LinkedElevator</name>
    <filename>a00164.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>LinkedElevator</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>7c6dec431b72da2e9f1ea0296d104f0e</anchor>
      <arglist>(const GR &amp;graph, int max_level)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LinkedElevator</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>a58ae777b13e13505d79f6d58be15e37</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>activate</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>4db11bfc0b64725808f2cedf6efe85bc</anchor>
      <arglist>(Item i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deactivate</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>6399621b031eeef6b06bf4f25d0922c7</anchor>
      <arglist>(Item i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>active</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>16d8e320d25a950a7ccd3f42fc12d699</anchor>
      <arglist>(Item i) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>operator[]</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>21db8d22216b643992538b26efd49553</anchor>
      <arglist>(Item i) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>onLevel</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>4f42d083c6e02d3bb636f4a9865e17ea</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>emptyLevel</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>7222462ad836aa926e3dc04fc982e329</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>aboveLevel</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>a57b36ed3da61ecce558c68cd214b3b3</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>activesOnLevel</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>7e843e422235ed011d3218242d028cc6</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>activeFree</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>91603b89ccd4b7b3f806a24bdcedcb30</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maxLevel</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>128a4da1b7623c68670a387fa148c65f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>lift</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>9e8cac16792c7bdac1584828690059a5</anchor>
      <arglist>(Item i, int new_level)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>dirtyTopButOne</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>45c945d67da38b2225cf6c34504cdb08</anchor>
      <arglist>(Item i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftToTop</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>f2c6a1f548364b1447a377c311eb948e</anchor>
      <arglist>(int l)</arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>highestActive</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>47eff0f8503f5d8038b31a4b36874c21</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>highestActiveLevel</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>d57dbb9976fbf0bfa412b0dc88776ffc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftHighestActive</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>d448db204a4dc8bcb628933f31a113d5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftHighestActive</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>744a06f7232fcac254c31fe9b2b879b0</anchor>
      <arglist>(int new_level)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftHighestActiveToTop</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>fa70a583d65837a24a1c86bf52a6fdc9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>activeOn</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>9963ab165df3118927670e795da8fc57</anchor>
      <arglist>(int l) const </arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>liftActiveOn</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>1275752b5d49412c4cc0dc9cb4d72323</anchor>
      <arglist>(int l)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftActiveOn</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>29269ad15861c513fe6297d4aad41d83</anchor>
      <arglist>(int l, int new_level)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>liftActiveToTop</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>404be93d69dc02dec4b6572745a70921</anchor>
      <arglist>(int l)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initStart</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>92a97cb68d6dbaf545fa72b36dfa3cd1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initAddItem</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>64a9da3e23ab4c7248a6efcde26b1064</anchor>
      <arglist>(Item i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initNewLevel</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>a8880e6363f8b0c2e012a03cb8bc0d77</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initFinish</name>
      <anchorfile>a00164.html</anchorfile>
      <anchor>faee4c060888616a45d071530326ea8b</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Exception</name>
    <filename>a00108.html</filename>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>bfbc23b99b2e78b609d50ac688611236</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>e55b5e05d3195ae27204e3a2395e54e7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const char *</type>
      <name>what</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>ff06f49065b54a8a86e02e9a2441a8ba</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::IoError</name>
    <filename>a00155.html</filename>
    <base>lemon::Exception</base>
    <member kind="function">
      <type></type>
      <name>IoError</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>d86a4dc85ccd17792866cea1da986403</anchor>
      <arglist>(const IoError &amp;error)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IoError</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>01fb1ee8d1d16ccfa24cffa29389d964</anchor>
      <arglist>(const char *message)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IoError</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>f29d4cb0e06420d3609f98509e78166b</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IoError</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>bdc691521b8e780595d1a491a6bcc3be</anchor>
      <arglist>(const char *message, const std::string &amp;file)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IoError</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>000186bf062781204af3940899f1f3ba</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;file)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~IoError</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>aad9306ace6181b2e8a38ba2eedf88d9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>message</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>7a46a349ff3098534da81350a6fec061</anchor>
      <arglist>(const char *message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>message</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>6db7001e8987fe0bed56f41e53dd4fee</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>file</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>51d6a4ade7b21409a4070f3d27c954bf</anchor>
      <arglist>(const std::string &amp;file)</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>message</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>4752bb1bf3e6f79dc5f9a77069a27332</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>file</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>87681515d569e7e8d650e6b54888dd81</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const char *</type>
      <name>what</name>
      <anchorfile>a00155.html</anchorfile>
      <anchor>ff06f49065b54a8a86e02e9a2441a8ba</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::FormatError</name>
    <filename>a00118.html</filename>
    <base>lemon::Exception</base>
    <member kind="function">
      <type></type>
      <name>FormatError</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>a42ee28488e2b7bd6183b748bef48e67</anchor>
      <arglist>(const FormatError &amp;error)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FormatError</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>6d87564747176f0f8a357ccc2ea5f24b</anchor>
      <arglist>(const char *message)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FormatError</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>e14f2a9e1d948374ef4aef96cd37029d</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FormatError</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>0e80334858898227937fec2734861b95</anchor>
      <arglist>(const char *message, const std::string &amp;file, int line=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FormatError</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>82f37328bb116765af91c298a15e62b4</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;file, int line=0)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~FormatError</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>1f920f082438639877ff7f2959715a74</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>line</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>51e985445b7c0be388784e41241e0a96</anchor>
      <arglist>(int line)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>message</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>7a46a349ff3098534da81350a6fec061</anchor>
      <arglist>(const char *message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>message</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>6db7001e8987fe0bed56f41e53dd4fee</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>file</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>51d6a4ade7b21409a4070f3d27c954bf</anchor>
      <arglist>(const std::string &amp;file)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>line</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>a6e2444d9f9eb15566a2c8cb42e45788</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>message</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>4752bb1bf3e6f79dc5f9a77069a27332</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>file</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>87681515d569e7e8d650e6b54888dd81</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const char *</type>
      <name>what</name>
      <anchorfile>a00118.html</anchorfile>
      <anchor>ff06f49065b54a8a86e02e9a2441a8ba</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::DiEulerIt</name>
    <filename>a00082.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>DiEulerIt</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>10a5924de31338bf99468ca7b1ea9156</anchor>
      <arglist>(const GR &amp;gr, typename GR::Node start=INVALID)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Arc</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>9d50d429980c0c2b97fd33e526652023</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>fbd1dc99d3bb91299bdeac8966b4fbc1</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a20cdc6e6b325e231a505a4eb85e3989</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>DiEulerIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a0177ba9ae3d9d831ac7bef57c3e5ed8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>operator++</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>5bdbff064cd6005e3b154c18d3b21df7</anchor>
      <arglist>(int)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::EulerIt</name>
    <filename>a00107.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>EulerIt</name>
      <anchorfile>a00107.html</anchorfile>
      <anchor>deeac6d3cfe48af7b2452144c3fbf6e7</anchor>
      <arglist>(const GR &amp;gr, typename GR::Node start=INVALID)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Arc</name>
      <anchorfile>a00107.html</anchorfile>
      <anchor>fee0d9c078f8e4cf4c3197a0341f864f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Edge</name>
      <anchorfile>a00107.html</anchorfile>
      <anchor>5a25824a51ad86bdf1651c5f988aa81f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00107.html</anchorfile>
      <anchor>9569625b6759eac56d4b4294aba9e5b2</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00107.html</anchorfile>
      <anchor>ab38b74c42b0f2581ed4917fca608efc</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
    <member kind="function">
      <type>EulerIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00107.html</anchorfile>
      <anchor>e0ec7d0bf98e60f122625acb3450928f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>operator++</name>
      <anchorfile>a00107.html</anchorfile>
      <anchor>5bdbff064cd6005e3b154c18d3b21df7</anchor>
      <arglist>(int)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::FullDigraph</name>
    <filename>a00120.html</filename>
    <member kind="function">
      <type></type>
      <name>FullDigraph</name>
      <anchorfile>a00120.html</anchorfile>
      <anchor>8efcbbf06d26f743ebb266ceb4ca1a2a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FullDigraph</name>
      <anchorfile>a00120.html</anchorfile>
      <anchor>79330852030897096b3c56124e78c91b</anchor>
      <arglist>(int n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resize</name>
      <anchorfile>a00120.html</anchorfile>
      <anchor>578be9c59132b8633a67a98c39318777</anchor>
      <arglist>(int n)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>operator()</name>
      <anchorfile>a00120.html</anchorfile>
      <anchor>cbd2e4abdacde884248a2cd28de06aa7</anchor>
      <arglist>(int ix) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>index</name>
      <anchorfile>a00120.html</anchorfile>
      <anchor>110a92ab57218951579bed4495fe8bf6</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>arc</name>
      <anchorfile>a00120.html</anchorfile>
      <anchor>d69cdebb233f0a9db9e20432c3944777</anchor>
      <arglist>(const Node &amp;u, const Node &amp;v) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>nodeNum</name>
      <anchorfile>a00120.html</anchorfile>
      <anchor>f9239a81c840ea4b094b196b6f164fcf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>arcNum</name>
      <anchorfile>a00120.html</anchorfile>
      <anchor>9cda7016f5de299b88f32cf63edee345</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::FullGraph</name>
    <filename>a00121.html</filename>
    <member kind="function">
      <type></type>
      <name>FullGraph</name>
      <anchorfile>a00121.html</anchorfile>
      <anchor>9064d1ac89eab29b8f2951ce080d4e87</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FullGraph</name>
      <anchorfile>a00121.html</anchorfile>
      <anchor>01cb8bb8e0b8f898ffeb33348c48fad2</anchor>
      <arglist>(int n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resize</name>
      <anchorfile>a00121.html</anchorfile>
      <anchor>578be9c59132b8633a67a98c39318777</anchor>
      <arglist>(int n)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>operator()</name>
      <anchorfile>a00121.html</anchorfile>
      <anchor>cbd2e4abdacde884248a2cd28de06aa7</anchor>
      <arglist>(int ix) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>index</name>
      <anchorfile>a00121.html</anchorfile>
      <anchor>110a92ab57218951579bed4495fe8bf6</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>arc</name>
      <anchorfile>a00121.html</anchorfile>
      <anchor>e46c9fd8c0e828a86f8d2a09202badd6</anchor>
      <arglist>(const Node &amp;s, const Node &amp;t) const </arglist>
    </member>
    <member kind="function">
      <type>Edge</type>
      <name>edge</name>
      <anchorfile>a00121.html</anchorfile>
      <anchor>6967f1e83266adfc9bc34ddbedcfb153</anchor>
      <arglist>(const Node &amp;u, const Node &amp;v) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>nodeNum</name>
      <anchorfile>a00121.html</anchorfile>
      <anchor>f9239a81c840ea4b094b196b6f164fcf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>arcNum</name>
      <anchorfile>a00121.html</anchorfile>
      <anchor>9cda7016f5de299b88f32cf63edee345</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>edgeNum</name>
      <anchorfile>a00121.html</anchorfile>
      <anchor>484eca76dda3fe1ceaff8073db17e38e</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GlpkBase</name>
    <filename>a00123.html</filename>
    <base virtualness="virtual">lemon::LpBase</base>
    <member kind="function">
      <type>_solver_bits::VoidPtr</type>
      <name>lpx</name>
      <anchorfile>a00123.html</anchorfile>
      <anchor>df0d2711307da6c7cb6acee24458f699</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>_solver_bits::VoidPtr</type>
      <name>lpx</name>
      <anchorfile>a00123.html</anchorfile>
      <anchor>7deae91370056673f00d768ff2cacd0c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>lpxRow</name>
      <anchorfile>a00123.html</anchorfile>
      <anchor>529c22d2782c36e88390066375fd18ea</anchor>
      <arglist>(Row r) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>lpxCol</name>
      <anchorfile>a00123.html</anchorfile>
      <anchor>978fd544ddf16fe723c4d90184366670</anchor>
      <arglist>(Col c) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GlpkLp</name>
    <filename>a00124.html</filename>
    <base>lemon::LpSolver</base>
    <base>lemon::GlpkBase</base>
    <member kind="function">
      <type></type>
      <name>GlpkLp</name>
      <anchorfile>a00124.html</anchorfile>
      <anchor>8fd9ded5caa626fdf9b1b67e0d4342f6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GlpkLp</name>
      <anchorfile>a00124.html</anchorfile>
      <anchor>bed5d680cd663d1ebf51276c5d40a3c4</anchor>
      <arglist>(const GlpkLp &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual GlpkLp *</type>
      <name>cloneSolver</name>
      <anchorfile>a00124.html</anchorfile>
      <anchor>2f2c4126d10df2126723dab73be4b3fa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual GlpkLp *</type>
      <name>newSolver</name>
      <anchorfile>a00124.html</anchorfile>
      <anchor>ff89c0081b70f9f731386769b6f980fb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>SolveExitStatus</type>
      <name>solvePrimal</name>
      <anchorfile>a00124.html</anchorfile>
      <anchor>48d56f0dbe8b60dadfb9aa6f6e7a458a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>SolveExitStatus</type>
      <name>solveDual</name>
      <anchorfile>a00124.html</anchorfile>
      <anchor>2f2e89d8a3d14f3f86b38a67967df930</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolver</name>
      <anchorfile>a00124.html</anchorfile>
      <anchor>b03beab5ff5c12c7160c325c0b6995d6</anchor>
      <arglist>(bool presolve)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GlpkMip</name>
    <filename>a00125.html</filename>
    <base>lemon::MipSolver</base>
    <base>lemon::GlpkBase</base>
    <member kind="function">
      <type></type>
      <name>GlpkMip</name>
      <anchorfile>a00125.html</anchorfile>
      <anchor>d1601aa1469b52a8fef9e52050a3bc1a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GlpkMip</name>
      <anchorfile>a00125.html</anchorfile>
      <anchor>b1c1ec0ee2201677230ce93a1c815c90</anchor>
      <arglist>(const GlpkMip &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GomoryHu</name>
    <filename>a00126.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <class kind="class">lemon::GomoryHu::MinCutEdgeIt</class>
    <class kind="class">lemon::GomoryHu::MinCutNodeIt</class>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00126.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CAP</type>
      <name>Capacity</name>
      <anchorfile>a00126.html</anchorfile>
      <anchor>772fe38595302c3bf6c4e79765a37a28</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Capacity::Value</type>
      <name>Value</name>
      <anchorfile>a00126.html</anchorfile>
      <anchor>316b6a8145c12ea1dbb1e1e36005a500</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GomoryHu</name>
      <anchorfile>a00126.html</anchorfile>
      <anchor>31ffa12460cb7ae041f29f09fcd9b8f4</anchor>
      <arglist>(const Graph &amp;graph, const Capacity &amp;capacity)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~GomoryHu</name>
      <anchorfile>a00126.html</anchorfile>
      <anchor>bb081203500b7357f28747e675cfd341</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00126.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>predNode</name>
      <anchorfile>a00126.html</anchorfile>
      <anchor>8a047e2d12d7f33ced19c7f71b39278e</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>predValue</name>
      <anchorfile>a00126.html</anchorfile>
      <anchor>67bb03ba4623543e6b08a2f339c1080e</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>rootDist</name>
      <anchorfile>a00126.html</anchorfile>
      <anchor>1723a3299a8a799b1b038ffc6a7a0d4b</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>minCutValue</name>
      <anchorfile>a00126.html</anchorfile>
      <anchor>b6912debf06e1db6c9c267a93368af51</anchor>
      <arglist>(const Node &amp;s, const Node &amp;t) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>minCutMap</name>
      <anchorfile>a00126.html</anchorfile>
      <anchor>1448a0251ea3eef21c4afbf5a1033c95</anchor>
      <arglist>(const Node &amp;s, const Node &amp;t, CutMap &amp;cutMap) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GomoryHu::MinCutEdgeIt</name>
    <filename>a00183.html</filename>
    <member kind="function">
      <type></type>
      <name>MinCutEdgeIt</name>
      <anchorfile>a00183.html</anchorfile>
      <anchor>7462ab82ac3c02f3b6e73b37ebc9eeed</anchor>
      <arglist>(GomoryHu const &amp;gomory, const Node &amp;s, const Node &amp;t, bool side=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator typename Graph::Arc</name>
      <anchorfile>a00183.html</anchorfile>
      <anchor>3bf276f89f021425e167c77f6f16dd90</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator typename Graph::Edge</name>
      <anchorfile>a00183.html</anchorfile>
      <anchor>17b705ba5f1f32fd950a7a8154898053</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>MinCutEdgeIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00183.html</anchorfile>
      <anchor>d55c346699bf86b8b24c89b03c9ad43d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Graph::Arc</type>
      <name>operator++</name>
      <anchorfile>a00183.html</anchorfile>
      <anchor>d262fcc0a3a1a4759f00f42ac0260646</anchor>
      <arglist>(int)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GomoryHu::MinCutNodeIt</name>
    <filename>a00184.html</filename>
    <member kind="function">
      <type></type>
      <name>MinCutNodeIt</name>
      <anchorfile>a00184.html</anchorfile>
      <anchor>aea2c4b8a4f9d05eeaf5e11cc26e81e7</anchor>
      <arglist>(GomoryHu const &amp;gomory, const Node &amp;s, const Node &amp;t, bool side=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator typename Graph::Node</name>
      <anchorfile>a00184.html</anchorfile>
      <anchor>70612da66bb330d0b98094ac8e34372c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>MinCutNodeIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00184.html</anchorfile>
      <anchor>f7c6603223a0d6426a548bf4529fa7a6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Graph::Node</type>
      <name>operator++</name>
      <anchorfile>a00184.html</anchorfile>
      <anchor>d839066a44bc207d648e5f0623601565</anchor>
      <arglist>(int)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::DefaultGraphToEpsTraits</name>
    <filename>a00073.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>DefaultGraphToEpsTraits</name>
      <anchorfile>a00073.html</anchorfile>
      <anchor>5120c56fa09e7484ecd90f9d237ddebd</anchor>
      <arglist>(const GR &amp;gr, std::ostream &amp;ost=std::cout, bool pros=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GraphToEps</name>
    <filename>a00134.html</filename>
    <templarg></templarg>
    <member kind="enumeration">
      <name>NodeShapes</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>30c21e7556892045cb4f5553dcc47ef7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CIRCLE</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>30c21e7556892045cb4f5553dcc47ef7a79c827759ea48f0735386c4b1188911</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>SQUARE</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>30c21e7556892045cb4f5553dcc47ef74233fbf0cafb86abcee94b38d769fc59</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>DIAMOND</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>30c21e7556892045cb4f5553dcc47ef7714f2cc5c292a305e2da3c03bd63916a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MALE</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>30c21e7556892045cb4f5553dcc47ef76e81fe1ef694726dd6f7246cfb42369f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FEMALE</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>30c21e7556892045cb4f5553dcc47ef7debb817413857eea6331734b76793159</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; CoordsTraits&lt; X &gt; &gt;</type>
      <name>coords</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>f72839098eec3c64f56bf7f03ac0ece2</anchor>
      <arglist>(const X &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; NodeSizesTraits&lt; X &gt; &gt;</type>
      <name>nodeSizes</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>06854c31df1f683befe287d30bed0af1</anchor>
      <arglist>(const X &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; NodeShapesTraits&lt; X &gt; &gt;</type>
      <name>nodeShapes</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>97b206153385698dee324aba9803f272</anchor>
      <arglist>(const X &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; NodeTextsTraits&lt; X &gt; &gt;</type>
      <name>nodeTexts</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>b7dd4767aa8076f2074048cbba760b86</anchor>
      <arglist>(const X &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; NodePsTextsTraits&lt; X &gt; &gt;</type>
      <name>nodePsTexts</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>036097f526418c7d02607b074ed74ee7</anchor>
      <arglist>(const X &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; ArcWidthsTraits&lt; X &gt; &gt;</type>
      <name>arcWidths</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>51285b1b1f6585c82c1f1256cfb3df94</anchor>
      <arglist>(const X &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; NodeColorsTraits&lt; X &gt; &gt;</type>
      <name>nodeColors</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>b642b531df840e4358ba0a25bcf7cbe1</anchor>
      <arglist>(const X &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; NodeTextColorsTraits&lt; X &gt; &gt;</type>
      <name>nodeTextColors</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>48d54ccf56f39952c2e47855c5e97cd3</anchor>
      <arglist>(const X &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; ArcColorsTraits&lt; X &gt; &gt;</type>
      <name>arcColors</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>9f6e3cec5c4bf68b55b9003e35747db5</anchor>
      <arglist>(const X &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>nodeScale</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>eac1e05eca87ec3eb33567e31ef1a91c</anchor>
      <arglist>(double d=.01)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>autoNodeScale</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>8d190137dbe4aacfa7d5cfce809d9f75</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>absoluteNodeSizes</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>a370a1dd4b34a5b82ff4c9d79790edd7</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>negateY</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>3104ccdec88f257754d817e4050530ae</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>preScale</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>728bce841b714ae589c110181e8f37f3</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>arcWidthScale</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>bf807dd381deda708eaceac647ddabf5</anchor>
      <arglist>(double d=.003)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>autoArcWidthScale</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>bd98d77a768da5b90d54f2503c43aea9</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>absoluteArcWidths</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>194970341303abd037ff052bf243f241</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>scale</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>e974ab0876d042da85db8a365a2c7fad</anchor>
      <arglist>(double d)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>border</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>6ee956315db169869d5e8b25d322855f</anchor>
      <arglist>(double b=10)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>border</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>3e60b386fd2382cfa00029b1c04ff125</anchor>
      <arglist>(double x, double y)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>drawArrows</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>5927ce2b5f72a0e077efed38c0a95064</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>arrowLength</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>134f596814e662f477366f2c777d8f65</anchor>
      <arglist>(double d=1.0)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>arrowWidth</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>dc664049d0ea2a763e742bc39f9e9b73</anchor>
      <arglist>(double d=.3)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>scaleToA4</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>85b25a0508ef21583dbda20251e675d6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>enableParallel</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>bcdb13d61119d0484bd5e96f9e844c31</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>parArcDist</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>1b00550e4019644f4057d3aaf57726b2</anchor>
      <arglist>(double d)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>hideArcs</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>5ef8417b40049c9d6411a68bd0682262</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>hideNodes</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>0b3ec217b0c0686b46980f812dfdd9ba</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>nodeTextSize</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>38eac3772663e5509aa45322ccd42bbc</anchor>
      <arglist>(double d)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>distantColorNodeTexts</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>01072429fe9bae7a4766665e4c64bfb2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>distantBWNodeTexts</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>fb1df0f6b681202ac8e74a2c9912ebe5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>nodePsTextsPreamble</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>43c32c975a0d66650b56999e739bc7cd</anchor>
      <arglist>(const char *str)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>undirected</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>57f03f8f903d351350e0fd939cf97eb3</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>directed</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>5df7407bacfc9447bb237376cead4218</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>title</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>98feec1651e0184b26c2fb56aef2d18b</anchor>
      <arglist>(const std::string &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>copyright</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>7d899188733cc29477191a0ecd04619a</anchor>
      <arglist>(const std::string &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; ArcWidthsTraits&lt; X &gt; &gt;</type>
      <name>edgeWidths</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>bbbea8d9fec1e8635ac7b03b70ddf0eb</anchor>
      <arglist>(const X &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; ArcColorsTraits&lt; X &gt; &gt;</type>
      <name>edgeColors</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>047fc38cf0de5203bb22274597176f16</anchor>
      <arglist>(const X &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>edgeWidthScale</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>1aa0ca617588f0922679ec9a34b42e8c</anchor>
      <arglist>(double d)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>autoEdgeWidthScale</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>be5b9b9b28139e1eb38d2a67268897ff</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>absoluteEdgeWidths</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>26373d9622dd9292313b81e78381e852</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>parEdgeDist</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>7a58d26082b1c28cb83d7e79e993d21c</anchor>
      <arglist>(double d)</arglist>
    </member>
    <member kind="function">
      <type>GraphToEps&lt; T &gt; &amp;</type>
      <name>hideEdges</name>
      <anchorfile>a00134.html</anchorfile>
      <anchor>9afe60ce30d0481d04268dd0fc4674d2</anchor>
      <arglist>(bool b=true)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GridGraph</name>
    <filename>a00136.html</filename>
    <class kind="class">lemon::GridGraph::ColMap</class>
    <class kind="class">lemon::GridGraph::IndexMap</class>
    <class kind="class">lemon::GridGraph::RowMap</class>
    <member kind="function">
      <type></type>
      <name>GridGraph</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>9592df3bc5a41604bcc6c08b17e6ccce</anchor>
      <arglist>(int width, int height)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resize</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>56bfa8fae91947eb5802d2e208405904</anchor>
      <arglist>(int width, int height)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>operator()</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>fdd1ab1ea88ed5eac1ae78f52b2c1369</anchor>
      <arglist>(int i, int j) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>col</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>c2be310f16fedfdff4a593e50cfe3728</anchor>
      <arglist>(Node n) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>row</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>f590748876d1cdb76d35a89ecc098c27</anchor>
      <arglist>(Node n) const </arglist>
    </member>
    <member kind="function">
      <type>dim2::Point&lt; int &gt;</type>
      <name>pos</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>8f32b26ac5800115467f86ba31aca963</anchor>
      <arglist>(Node n) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>width</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>369399896761e31ae71db57fdd0ba431</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>height</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>e26bcfe2f33f5873dbdfb6948cf1f59f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>right</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>516efb9fb0a2f315d691dd19c130bd63</anchor>
      <arglist>(Node n) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>left</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>f81fa44653a5566024651571fc915bd4</anchor>
      <arglist>(Node n) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>up</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>dde1f2065416cbaaf17563ef322694fa</anchor>
      <arglist>(Node n) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>down</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>de5fc8f1246792a247f5760a869e1d96</anchor>
      <arglist>(Node n) const </arglist>
    </member>
    <member kind="function">
      <type>IndexMap</type>
      <name>indexMap</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>5f999c4f26c4306119a5be087ad88c3d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>RowMap</type>
      <name>rowMap</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>52865a6bd4c2548dbd1ced85b2a1f36f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ColMap</type>
      <name>colMap</name>
      <anchorfile>a00136.html</anchorfile>
      <anchor>4100e090fe6634a5dc491944bafab99a</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GridGraph::ColMap</name>
    <filename>a00050.html</filename>
    <member kind="typedef">
      <type>GridGraph::Node</type>
      <name>Key</name>
      <anchorfile>a00050.html</anchorfile>
      <anchor>89610f8cffa34a4715c0730e05cb8751</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>Value</name>
      <anchorfile>a00050.html</anchorfile>
      <anchor>8940a046b717a18878ab9de4ba32a0b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ColMap</name>
      <anchorfile>a00050.html</anchorfile>
      <anchor>cc4976242d536051e8c575c83d17cf72</anchor>
      <arglist>(const GridGraph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00050.html</anchorfile>
      <anchor>dec40b5924be9b2ac620ace6c4cd8db8</anchor>
      <arglist>(Key key) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GridGraph::IndexMap</name>
    <filename>a00150.html</filename>
    <member kind="typedef">
      <type>GridGraph::Node</type>
      <name>Key</name>
      <anchorfile>a00150.html</anchorfile>
      <anchor>89610f8cffa34a4715c0730e05cb8751</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>dim2::Point&lt; int &gt;</type>
      <name>Value</name>
      <anchorfile>a00150.html</anchorfile>
      <anchor>9b682dd11c28cb3e98421ffb2c4a2240</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IndexMap</name>
      <anchorfile>a00150.html</anchorfile>
      <anchor>7963a1ca4bc817546073da2398d29778</anchor>
      <arglist>(const GridGraph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00150.html</anchorfile>
      <anchor>dec40b5924be9b2ac620ace6c4cd8db8</anchor>
      <arglist>(Key key) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GridGraph::RowMap</name>
    <filename>a00231.html</filename>
    <member kind="typedef">
      <type>GridGraph::Node</type>
      <name>Key</name>
      <anchorfile>a00231.html</anchorfile>
      <anchor>89610f8cffa34a4715c0730e05cb8751</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>Value</name>
      <anchorfile>a00231.html</anchorfile>
      <anchor>8940a046b717a18878ab9de4ba32a0b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RowMap</name>
      <anchorfile>a00231.html</anchorfile>
      <anchor>065a8779432e06c7faa1e2524fed51e1</anchor>
      <arglist>(const GridGraph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00231.html</anchorfile>
      <anchor>dec40b5924be9b2ac620ace6c4cd8db8</anchor>
      <arglist>(Key key) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::HaoOrlin</name>
    <filename>a00137.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CAP</type>
      <name>CapacityMap</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>10f68c1f869f7e1be967acfbd9750290</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TOL</type>
      <name>Tolerance</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>88c54a1e77557bd8734f1729e39b2602</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>HaoOrlin</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>b03a48e815fb5a8af4274cccd705a33c</anchor>
      <arglist>(const Digraph &amp;graph, const CapacityMap &amp;capacity, const Tolerance &amp;tolerance=Tolerance())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>7cb8fc3003223031b35f3ba5fcfb2762</anchor>
      <arglist>(const Node &amp;source)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>calculateOut</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>e495b2edefc81e20b797ee5b125d3955</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>calculateIn</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>3b2a8b9cda4a87593c4fdfc44afa15d7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>429ed28d5b8fc2fd63dde71155213753</anchor>
      <arglist>(const Node &amp;s)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>minCutValue</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>6c236438cfb06210690188638d87b164</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>minCutMap</name>
      <anchorfile>a00137.html</anchorfile>
      <anchor>6f8ab6151ed13708fdf0389442b5ec0c</anchor>
      <arglist>(CutMap &amp;cutMap) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::HypercubeGraph</name>
    <filename>a00140.html</filename>
    <class kind="class">lemon::HypercubeGraph::HyperMap</class>
    <member kind="function">
      <type></type>
      <name>HypercubeGraph</name>
      <anchorfile>a00140.html</anchorfile>
      <anchor>d1e0bae8743a52a5f67cd18ece720412</anchor>
      <arglist>(int dim)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>dimension</name>
      <anchorfile>a00140.html</anchorfile>
      <anchor>e5acbe47d704ceb866e3717e2ac9422f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>projection</name>
      <anchorfile>a00140.html</anchorfile>
      <anchor>a4f046f67ab074dd0e845f6db83e28fe</anchor>
      <arglist>(Node node, int n) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>dimension</name>
      <anchorfile>a00140.html</anchorfile>
      <anchor>1db28f13c190465563114f211fab6b87</anchor>
      <arglist>(Edge edge) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>dimension</name>
      <anchorfile>a00140.html</anchorfile>
      <anchor>968b113f3f1be8e6b7279963ba699c26</anchor>
      <arglist>(Arc arc) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>index</name>
      <anchorfile>a00140.html</anchorfile>
      <anchor>d3bfcd2b6a60981365433762287dd11d</anchor>
      <arglist>(Node node) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>operator()</name>
      <anchorfile>a00140.html</anchorfile>
      <anchor>cbd2e4abdacde884248a2cd28de06aa7</anchor>
      <arglist>(int ix) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>nodeNum</name>
      <anchorfile>a00140.html</anchorfile>
      <anchor>f9239a81c840ea4b094b196b6f164fcf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>edgeNum</name>
      <anchorfile>a00140.html</anchorfile>
      <anchor>484eca76dda3fe1ceaff8073db17e38e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>arcNum</name>
      <anchorfile>a00140.html</anchorfile>
      <anchor>9cda7016f5de299b88f32cf63edee345</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::HypercubeGraph::HyperMap</name>
    <filename>a00141.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>Node</type>
      <name>Key</name>
      <anchorfile>a00141.html</anchorfile>
      <anchor>e05d965da541abbbd35cc532b534496d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>T</type>
      <name>Value</name>
      <anchorfile>a00141.html</anchorfile>
      <anchor>34b57a974fe67a997b7693e6e71cd904</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>HyperMap</name>
      <anchorfile>a00141.html</anchorfile>
      <anchor>cb897ee772dfea382da0adda315f780c</anchor>
      <arglist>(const Graph &amp;graph, It begin, It end, T fv=0, const BF &amp;bf=BF())</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00141.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::DigraphReader</name>
    <filename>a00085.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>DigraphReader</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>a4f1971218572095cd881516767c8cf3</anchor>
      <arglist>(DGR &amp;digraph, std::istream &amp;is=std::cin)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DigraphReader</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>e8b85afb216acee67f9648314e5bb8ed</anchor>
      <arglist>(DGR &amp;digraph, const std::string &amp;fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DigraphReader</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>b60d1946d7d28dc4bf847726c6b68b1a</anchor>
      <arglist>(DGR &amp;digraph, const char *fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~DigraphReader</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>5d63771eeca810885446a254547d2864</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>nodeMap</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>9c4770b88d5ec5785be41ae8cbdf1c6b</anchor>
      <arglist>(const std::string &amp;caption, Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>nodeMap</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>d21093ad533e607fb1141a68d1230b1c</anchor>
      <arglist>(const std::string &amp;caption, Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>arcMap</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>95d4e14f24b1321d16b97ec42823651a</anchor>
      <arglist>(const std::string &amp;caption, Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>arcMap</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>3185373ffc1ab5d4facb1af04966e50e</anchor>
      <arglist>(const std::string &amp;caption, Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>attribute</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>fde7d7ca6e9b93b9599c4f94345980a6</anchor>
      <arglist>(const std::string &amp;caption, Value &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>attribute</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>e20bee99724758343251a7c1d3e07aba</anchor>
      <arglist>(const std::string &amp;caption, Value &amp;value, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>node</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>985b127665d31db2820cbd9fde869a7a</anchor>
      <arglist>(const std::string &amp;caption, Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>arc</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>af97021ab62753411ca5bdd11c8b6891</anchor>
      <arglist>(const std::string &amp;caption, Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>nodes</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>4c23dbc9b67a31251da30029d32d3a3d</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>arcs</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>62c9846f3883712c703c58389a8c24f7</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>attributes</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>f774f0641b0e24afa95f176c518ceb78</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>useNodes</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>ad1ab381a400670e18d076202dad4c4d</anchor>
      <arglist>(const Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>useNodes</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>af4d8af801fdd5bfe398c3908d415694</anchor>
      <arglist>(const Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>useArcs</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>1498e6fa12c721b65327d68765bf3000</anchor>
      <arglist>(const Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>useArcs</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>d8ccee5c5167eb1f53c121bb1aa4fcc1</anchor>
      <arglist>(const Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>skipNodes</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>2c1a0a6fc8364af63b4f446f66e3c675</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>DigraphReader &amp;</type>
      <name>skipArcs</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>db2dec97e14950c802945264144bf7b7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend DigraphReader&lt; TDGR &gt;</type>
      <name>digraphReader</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>g2f805968e7f23de95b1fd2452be1b6b9</anchor>
      <arglist>(TDGR &amp;digraph, std::istream &amp;is)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend DigraphReader&lt; TDGR &gt;</type>
      <name>digraphReader</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>8192673c4b1a70c3f91aa7498f12b8cc</anchor>
      <arglist>(TDGR &amp;digraph, const std::string &amp;fn)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend DigraphReader&lt; TDGR &gt;</type>
      <name>digraphReader</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>46b81e98239e74320c0cdb34ab76f010</anchor>
      <arglist>(TDGR &amp;digraph, const char *fn)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GraphReader</name>
    <filename>a00133.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>GraphReader</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>d0d38d468b2bab5026101734a162702b</anchor>
      <arglist>(GR &amp;graph, std::istream &amp;is=std::cin)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphReader</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>4b7d9443c22683c2efa9edfd85326da1</anchor>
      <arglist>(GR &amp;graph, const std::string &amp;fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphReader</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>4a91fbd433d48e5553bf2f8532eedc2a</anchor>
      <arglist>(GR &amp;graph, const char *fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~GraphReader</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>602ecd8550acd9a47d3e845046dedc46</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>nodeMap</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>1cbac796cc5f7e904fd0526eb444a268</anchor>
      <arglist>(const std::string &amp;caption, Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>nodeMap</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>31613a6c83eb319fdb1fd5f1deac1708</anchor>
      <arglist>(const std::string &amp;caption, Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>edgeMap</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>fba1f5f2a72d58661814e4fa5edcd407</anchor>
      <arglist>(const std::string &amp;caption, Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>edgeMap</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>221ef2b7aec1d0a3979425be8c0f34f0</anchor>
      <arglist>(const std::string &amp;caption, Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>arcMap</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>b5a7c9abd433acdc69604b89ba3e62a2</anchor>
      <arglist>(const std::string &amp;caption, Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>arcMap</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>12caf1da416af65f3bcbba8120ac844f</anchor>
      <arglist>(const std::string &amp;caption, Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>attribute</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>cab86e3f1977e7e22ee62c29465a9f8b</anchor>
      <arglist>(const std::string &amp;caption, Value &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>attribute</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>4dd99c0830c0504165959a146f8ea76d</anchor>
      <arglist>(const std::string &amp;caption, Value &amp;value, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>node</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>935bba5e66fdd35b12fcdbef9a6d9d19</anchor>
      <arglist>(const std::string &amp;caption, Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>edge</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>2a2aa1c80b27367bd2dcf27ded51780e</anchor>
      <arglist>(const std::string &amp;caption, Edge &amp;edge)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>arc</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>57595f8f77dcd876743db6735d98d6a1</anchor>
      <arglist>(const std::string &amp;caption, Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>nodes</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>1d460fd7a3dda23c377cc030273e7a4e</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>edges</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>2325d91e1fffd75860a83e76a0d9dd32</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>attributes</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>f685594e06cd5d28ea88d79f9a9dcb8c</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>useNodes</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>bf8412f75c80778ecc1449d51143a243</anchor>
      <arglist>(const Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>useNodes</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>16f28dc8970b7a7601edddef81081e13</anchor>
      <arglist>(const Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>useEdges</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>2c6527c860b7e27b16f3dc8104a485d0</anchor>
      <arglist>(const Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>useEdges</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>e4d8dad13825893f5ab6b688ee6bbf9d</anchor>
      <arglist>(const Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>skipNodes</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>136054a5ccb371c0fcc1a45294d2a5e6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GraphReader &amp;</type>
      <name>skipEdges</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>a655ba72248a83e55b0f6f5cf1ffb78d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend GraphReader&lt; TGR &gt;</type>
      <name>graphReader</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>gf9cd86326f2bf28c0b7957ad0b94fc0b</anchor>
      <arglist>(TGR &amp;graph, std::istream &amp;is)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend GraphReader&lt; TGR &gt;</type>
      <name>graphReader</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>789f3d04aeea93502c7e78a05513b81f</anchor>
      <arglist>(TGR &amp;graph, const std::string &amp;fn)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend GraphReader&lt; TGR &gt;</type>
      <name>graphReader</name>
      <anchorfile>a00133.html</anchorfile>
      <anchor>fad6b8579998e93b0246a6d44fcac776</anchor>
      <arglist>(TGR &amp;graph, const char *fn)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SectionReader</name>
    <filename>a00234.html</filename>
    <member kind="function">
      <type></type>
      <name>SectionReader</name>
      <anchorfile>a00234.html</anchorfile>
      <anchor>d2864b0251911b45ddb7198c16480b5b</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SectionReader</name>
      <anchorfile>a00234.html</anchorfile>
      <anchor>010d719af4f7dee3d1fb4b94189fec28</anchor>
      <arglist>(const std::string &amp;fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SectionReader</name>
      <anchorfile>a00234.html</anchorfile>
      <anchor>497d304493c028916e5e0c2439d5aadf</anchor>
      <arglist>(const char *fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~SectionReader</name>
      <anchorfile>a00234.html</anchorfile>
      <anchor>06ff4579ab6965f6e6bc29ad7ed3fb8b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>SectionReader &amp;</type>
      <name>sectionLines</name>
      <anchorfile>a00234.html</anchorfile>
      <anchor>8a57066d265f5a302a724eb26c9520ae</anchor>
      <arglist>(const std::string &amp;type, Functor functor)</arglist>
    </member>
    <member kind="function">
      <type>SectionReader &amp;</type>
      <name>sectionStream</name>
      <anchorfile>a00234.html</anchorfile>
      <anchor>30b9aea1189c376985473f8ba0ac3ec0</anchor>
      <arglist>(const std::string &amp;type, Functor functor)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00234.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend SectionReader</type>
      <name>sectionReader</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>g919ce27a20ac3228c90a85310a64fa4c</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend SectionReader</type>
      <name>sectionReader</name>
      <anchorfile>a00234.html</anchorfile>
      <anchor>deb72981c0553e45dd6b5bc63e9c23e8</anchor>
      <arglist>(const std::string &amp;fn)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend SectionReader</type>
      <name>sectionReader</name>
      <anchorfile>a00234.html</anchorfile>
      <anchor>1375da7ad660a4fc871bd6ffab94a5d9</anchor>
      <arglist>(const char *fn)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LgfContents</name>
    <filename>a00162.html</filename>
    <member kind="function">
      <type></type>
      <name>LgfContents</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>354d20288c783068888642817d4201fb</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LgfContents</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>8504ecb1b857fd19df24823e811d3140</anchor>
      <arglist>(const std::string &amp;fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LgfContents</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>df863a29229b03c84b76d174dfc355cb</anchor>
      <arglist>(const char *fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~LgfContents</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>eafd3f08b75c187e55c1c89ec0962248</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>nodeSectionNum</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>0f0562a9333abeba58c88845b159de7c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>nodeSection</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>78e7a2d329efcc77ca146b885820c75b</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>const std::vector&lt; std::string &gt; &amp;</type>
      <name>nodeMapNames</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>92307f04de9d1b9e5dae316db3f5b4d4</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>arcSectionNum</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>5a0b06032f70759300fb4930e01b0cff</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>arcSection</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>496e861ccb7814a611e011a46318bce5</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>const std::vector&lt; std::string &gt; &amp;</type>
      <name>arcMapNames</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>5a1d1325c20b0e302280fd34a9e4da54</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>edgeSectionNum</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>311228579d1d35898483877a3249d1ae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>edgeSection</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>830fe61608acc498369987ba092d0efa</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>const std::vector&lt; std::string &gt; &amp;</type>
      <name>edgeMapNames</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>83f56d761582fe384e0201db89c03f34</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>attributeSectionNum</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>fc01b23cca378311e7e64cf1cb0e4eb2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>attributeSectionNames</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>7b8bf07432f2190469a78c9a517879c7</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>const std::vector&lt; std::string &gt; &amp;</type>
      <name>attributes</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>2ad5b2157e7a47659c89840ed7ac0554</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>extraSectionNum</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>38b8fdd565b6b2c66d96377a4dbf6999</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>extraSection</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>a290e43b6bf815f418e90c08ac473b3e</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00162.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::DigraphWriter</name>
    <filename>a00086.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>DigraphWriter</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>74ad76e5a2254925c5adabd4088819b6</anchor>
      <arglist>(const DGR &amp;digraph, std::ostream &amp;os=std::cout)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DigraphWriter</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>3ee5445ac5f1f877cc947ae7acc609fb</anchor>
      <arglist>(const DGR &amp;digraph, const std::string &amp;fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DigraphWriter</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>1e5c1f2228d2fc8940d99e9cd6d1e582</anchor>
      <arglist>(const DGR &amp;digraph, const char *fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~DigraphWriter</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>2135241b28f87dccd8933a761ea27c6c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>nodeMap</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>b7cf27b7adfd3795b8793f1c6137e6c7</anchor>
      <arglist>(const std::string &amp;caption, const Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>nodeMap</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>fb481c6687e1f26db445489759b86296</anchor>
      <arglist>(const std::string &amp;caption, const Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>arcMap</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>66274cecc8113b71de635fb1d4c4245e</anchor>
      <arglist>(const std::string &amp;caption, const Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>arcMap</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>06582e9047a941f3fffb563b20896073</anchor>
      <arglist>(const std::string &amp;caption, const Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>attribute</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>d03e0db8955e76c86760d1c292e17190</anchor>
      <arglist>(const std::string &amp;caption, const Value &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>attribute</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>bb023769a2436849498c07d1fd9688cf</anchor>
      <arglist>(const std::string &amp;caption, const Value &amp;value, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>node</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>9994c9b8fa00a16560ab6c24c299c93f</anchor>
      <arglist>(const std::string &amp;caption, const Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>arc</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>90fc30773d46a50471993f554e21a4bc</anchor>
      <arglist>(const std::string &amp;caption, const Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>nodes</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>d45ae417635ab38e9f11926e033ce84d</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>arcs</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>30c9c7bebf1120893029cf8016625d5d</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>attributes</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>e7f4dfd9c6254bdb19963abf87676d33</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>skipNodes</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>baa48d206e2c370290d6de539400f885</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>DigraphWriter &amp;</type>
      <name>skipArcs</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>96343892ef6a0d31a1398b739db13ed5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>ostream</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>0df4c16f48d0e3c9c5c0cd8a55d5b183</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend DigraphWriter&lt; TDGR &gt;</type>
      <name>digraphWriter</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>gef02039f61d7e740650a1e8e6274257d</anchor>
      <arglist>(const TDGR &amp;digraph, std::ostream &amp;os)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend DigraphWriter&lt; TDGR &gt;</type>
      <name>digraphWriter</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>2e92b615a799757939d4933975dec05f</anchor>
      <arglist>(const TDGR &amp;digraph, const std::string &amp;fn)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend DigraphWriter&lt; TDGR &gt;</type>
      <name>digraphWriter</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>d3f282e33c4f6cdf506177569c6afa0e</anchor>
      <arglist>(const TDGR &amp;digraph, const char *fn)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::GraphWriter</name>
    <filename>a00135.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>GraphWriter</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>048dd7d5e27e1c30f96e979f92f4ea3d</anchor>
      <arglist>(const GR &amp;graph, std::ostream &amp;os=std::cout)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphWriter</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>6a03481bb09c2fdfe5371fc1cd6e90ea</anchor>
      <arglist>(const GR &amp;graph, const std::string &amp;fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphWriter</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>f23c37b427913cc3a001edb841e4ca85</anchor>
      <arglist>(const GR &amp;graph, const char *fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~GraphWriter</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>6da7b22b11b0cf823181f0e9847e3826</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>nodeMap</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>39d0727a7e8e262c66f6cb59e04f8cf2</anchor>
      <arglist>(const std::string &amp;caption, const Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>nodeMap</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>90fbece3e01f99a6441522b3748a4a27</anchor>
      <arglist>(const std::string &amp;caption, const Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>edgeMap</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>73e8269afd3b286e9e7abfb1f09922a5</anchor>
      <arglist>(const std::string &amp;caption, const Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>edgeMap</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>d1be76ceb9a3be1556e2a131c5f66715</anchor>
      <arglist>(const std::string &amp;caption, const Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>arcMap</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>67305d3e9f4f4a58009b88a49ef4d56c</anchor>
      <arglist>(const std::string &amp;caption, const Map &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>arcMap</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>a1aa6c6b7bcf4da36724277a3271bb7d</anchor>
      <arglist>(const std::string &amp;caption, const Map &amp;map, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>attribute</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>c983592cdc411d2e4bc22b8bf8b2e25d</anchor>
      <arglist>(const std::string &amp;caption, const Value &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>attribute</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>89dc1413a8efd2a435480c18cb4a6bc3</anchor>
      <arglist>(const std::string &amp;caption, const Value &amp;value, const Converter &amp;converter=Converter())</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>node</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>210e737325b8a2d0990a66ff75639bec</anchor>
      <arglist>(const std::string &amp;caption, const Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>edge</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>0a9ebc6f70e74576359e08de9eeafd38</anchor>
      <arglist>(const std::string &amp;caption, const Edge &amp;edge)</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>arc</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>7e6b1f036ccc517c53c26ab6151dadad</anchor>
      <arglist>(const std::string &amp;caption, const Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>nodes</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>d6e3b29651b774389d42facc4a718822</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>edges</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>9f4d25e108365bcc0c8d8df345a4ea32</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>attributes</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>e5ca855de0e76c596f49e8a728af5841</anchor>
      <arglist>(const std::string &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>skipNodes</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>7cac69b6df6f805859aa6382f72a9024</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GraphWriter &amp;</type>
      <name>skipEdges</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>55b1bc7b7d913f1a0a1e05134cede4db</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>ostream</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>0df4c16f48d0e3c9c5c0cd8a55d5b183</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend GraphWriter&lt; TGR &gt;</type>
      <name>graphWriter</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>g68ea72296c298c8b15e51ee6e3de6b4b</anchor>
      <arglist>(const TGR &amp;graph, std::ostream &amp;os)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend GraphWriter&lt; TGR &gt;</type>
      <name>graphWriter</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>3ae256702176a89d3bd678d3790be207</anchor>
      <arglist>(const TGR &amp;graph, const std::string &amp;fn)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend GraphWriter&lt; TGR &gt;</type>
      <name>graphWriter</name>
      <anchorfile>a00135.html</anchorfile>
      <anchor>e17f05aaf26453f2f8ca989cdca77b40</anchor>
      <arglist>(const TGR &amp;graph, const char *fn)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SectionWriter</name>
    <filename>a00235.html</filename>
    <member kind="function">
      <type></type>
      <name>SectionWriter</name>
      <anchorfile>a00235.html</anchorfile>
      <anchor>50c0a7f10e3cb6d13b6fc2623b276c68</anchor>
      <arglist>(std::ostream &amp;os)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SectionWriter</name>
      <anchorfile>a00235.html</anchorfile>
      <anchor>df73bee6769d11d70550d9ed4e7cd024</anchor>
      <arglist>(const std::string &amp;fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SectionWriter</name>
      <anchorfile>a00235.html</anchorfile>
      <anchor>428bbe8f1ff7af0ea09509cb4e37d05d</anchor>
      <arglist>(const char *fn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~SectionWriter</name>
      <anchorfile>a00235.html</anchorfile>
      <anchor>2a9c8674573054e789efd12de05635e2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>SectionWriter &amp;</type>
      <name>sectionLines</name>
      <anchorfile>a00235.html</anchorfile>
      <anchor>5682df807fd1183020f9f5a7755a4f04</anchor>
      <arglist>(const std::string &amp;type, Functor functor)</arglist>
    </member>
    <member kind="function">
      <type>SectionWriter &amp;</type>
      <name>sectionStream</name>
      <anchorfile>a00235.html</anchorfile>
      <anchor>53adf7543b651e9a37a3f045ea0222c2</anchor>
      <arglist>(const std::string &amp;type, Functor functor)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00235.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>ostream</name>
      <anchorfile>a00235.html</anchorfile>
      <anchor>0df4c16f48d0e3c9c5c0cd8a55d5b183</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend SectionWriter</type>
      <name>sectionWriter</name>
      <anchorfile>a00453.html</anchorfile>
      <anchor>g6bc4578acf71f56b06729191b8463779</anchor>
      <arglist>(std::ostream &amp;os)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend SectionWriter</type>
      <name>sectionWriter</name>
      <anchorfile>a00235.html</anchorfile>
      <anchor>f7c98afec48ac900d4af8e89c418ee17</anchor>
      <arglist>(const std::string &amp;fn)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend SectionWriter</type>
      <name>sectionWriter</name>
      <anchorfile>a00235.html</anchorfile>
      <anchor>1b3a56982483d38ab84cd05073a37c0e</anchor>
      <arglist>(const char *fn)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ListDigraph</name>
    <filename>a00166.html</filename>
    <class kind="class">lemon::ListDigraph::Snapshot</class>
    <member kind="function">
      <type></type>
      <name>ListDigraph</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>bff6c0f71f9067a36468f2919edcac51</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>addNode</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>96838566b12a6b04795db38688bad1a0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>addArc</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>b947ea03356504c5c4b3c8ebfed1516a</anchor>
      <arglist>(const Node &amp;s, const Node &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>341935453e37c7bb123e5c05a7678b76</anchor>
      <arglist>(const Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>3a2143bdbfc896efc87ea1a34bbcf824</anchor>
      <arglist>(const Arc &amp;a)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>a088e41ac5858620d94572f4af0b821f</anchor>
      <arglist>(Node n) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>c31ad29081023b5df42a7b08e3b44cb4</anchor>
      <arglist>(Arc a) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>changeTarget</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>bfb074c8c3ead2adee735878dd221ed3</anchor>
      <arglist>(Arc a, Node n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>changeSource</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>20b4f4bf5bad96ec675f077f4b87f349</anchor>
      <arglist>(Arc a, Node n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reverseArc</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>cde0492abf41776fecaee3e44afc07c4</anchor>
      <arglist>(Arc e)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reserveNode</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>28bb4df827e678ae549849be81d88def</anchor>
      <arglist>(int n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reserveArc</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>2489cbecb9d7ff5c9a0b2b5ee46818b5</anchor>
      <arglist>(int m)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>contract</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>0eafaa9830d42f199765879062d495da</anchor>
      <arglist>(Node a, Node b, bool r=true)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>split</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>09a307979f5ac51cc21195285d485bbe</anchor>
      <arglist>(Node n, bool connect=true)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>split</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>6f35063da766a350f79460480a237a83</anchor>
      <arglist>(Arc e)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>ListDigraph</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>d28967a7a041da94e00e954c4e6861e6</anchor>
      <arglist>(const ListDigraph &amp;)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>operator=</name>
      <anchorfile>a00166.html</anchorfile>
      <anchor>85a3026571948df8a0247aa2ce972411</anchor>
      <arglist>(const ListDigraph &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ListDigraph::Snapshot</name>
    <filename>a00274.html</filename>
    <member kind="function">
      <type></type>
      <name>Snapshot</name>
      <anchorfile>a00274.html</anchorfile>
      <anchor>8d35bcf9a5efab747b3b3603ac2861de</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Snapshot</name>
      <anchorfile>a00274.html</anchorfile>
      <anchor>fbd128286802028a6e18638ad055b6db</anchor>
      <arglist>(ListDigraph &amp;_digraph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>save</name>
      <anchorfile>a00274.html</anchorfile>
      <anchor>8cae000554415c148b8a65498b21e7ad</anchor>
      <arglist>(ListDigraph &amp;_digraph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>restore</name>
      <anchorfile>a00274.html</anchorfile>
      <anchor>fd3595051be2709847c2de4352f27cf5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00274.html</anchorfile>
      <anchor>8d985300b138b6c5556ab17ed4df3b38</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ListGraph</name>
    <filename>a00168.html</filename>
    <class kind="class">lemon::ListGraph::Snapshot</class>
    <member kind="function">
      <type></type>
      <name>ListGraph</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>bdab59515ed6ddd23a01d8e20f6d916c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>addNode</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>96838566b12a6b04795db38688bad1a0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Edge</type>
      <name>addEdge</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>409e4da6682b7eadf5ede5ba15b171c5</anchor>
      <arglist>(const Node &amp;s, const Node &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>341935453e37c7bb123e5c05a7678b76</anchor>
      <arglist>(const Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>ccff03c7f445a919e57d4ca8e1b2199f</anchor>
      <arglist>(const Edge &amp;e)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>a088e41ac5858620d94572f4af0b821f</anchor>
      <arglist>(Node n) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>c31ad29081023b5df42a7b08e3b44cb4</anchor>
      <arglist>(Arc a) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>55582c50a716d15504b5680d810bd5dd</anchor>
      <arglist>(Edge e) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>changeU</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>38960b4669bd9a87af8f846c21806c00</anchor>
      <arglist>(Edge e, Node n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>changeV</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>9d5b624a9147ffc63eb9da04fd1a9175</anchor>
      <arglist>(Edge e, Node n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>contract</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>0eafaa9830d42f199765879062d495da</anchor>
      <arglist>(Node a, Node b, bool r=true)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>ListGraph</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>62812b9a8c8bedabc1aeec208cf20d46</anchor>
      <arglist>(const ListGraph &amp;)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>operator=</name>
      <anchorfile>a00168.html</anchorfile>
      <anchor>43d393a97d113d3aa0322292047674bc</anchor>
      <arglist>(const ListGraph &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ListGraph::Snapshot</name>
    <filename>a00273.html</filename>
    <member kind="function">
      <type></type>
      <name>Snapshot</name>
      <anchorfile>a00273.html</anchorfile>
      <anchor>8d35bcf9a5efab747b3b3603ac2861de</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Snapshot</name>
      <anchorfile>a00273.html</anchorfile>
      <anchor>940f7dcf6e3e6a19c2ad4cbe0056ff7c</anchor>
      <arglist>(ListGraph &amp;_graph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>save</name>
      <anchorfile>a00273.html</anchorfile>
      <anchor>fbeda900f4b8b5b218e5b8beffb33b30</anchor>
      <arglist>(ListGraph &amp;_graph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>restore</name>
      <anchorfile>a00273.html</anchorfile>
      <anchor>fd3595051be2709847c2de4352f27cf5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00273.html</anchorfile>
      <anchor>8d985300b138b6c5556ab17ed4df3b38</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase</name>
    <filename>a00171.html</filename>
    <class kind="class">lemon::LpBase::Col</class>
    <class kind="class">lemon::LpBase::ColIt</class>
    <class kind="class">lemon::LpBase::Constr</class>
    <class kind="class">lemon::LpBase::DualExpr</class>
    <class kind="class">lemon::LpBase::Expr</class>
    <class kind="class">lemon::LpBase::Row</class>
    <class kind="class">lemon::LpBase::RowIt</class>
    <member kind="enumeration">
      <name>SolveExitStatus</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>4a5e4b34b14952c4c826e3a859028e31</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>SOLVED</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>4a5e4b34b14952c4c826e3a859028e310a5e81bed8da1cf85236aa6eb7569818</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UNSOLVED</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>4a5e4b34b14952c4c826e3a859028e3138e4b0838cfb137a637dcf0f5217b694</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>Sense</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>c56a6b1edba1f6deaff6fae135e6fd9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MIN</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>c56a6b1edba1f6deaff6fae135e6fd9e957e8250f68e7b5677b22397c2c1b51e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MAX</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>c56a6b1edba1f6deaff6fae135e6fd9ed7e097bda6d981de2520f49fe74c25b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>MessageLevel</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>af5eea9687ad679dc3e504d373e959dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MESSAGE_NOTHING</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>af5eea9687ad679dc3e504d373e959dce7a3f9c19e1a769bc42013b29b931336</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MESSAGE_ERROR</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>af5eea9687ad679dc3e504d373e959dceed68e2821e40e5751af74e449ba1fa7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MESSAGE_WARNING</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>af5eea9687ad679dc3e504d373e959dca0da5a01a18f78c0f248941ad32cc816</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MESSAGE_NORMAL</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>af5eea9687ad679dc3e504d373e959dc3b0309750c8c5bf0ad071cf382f10c67</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MESSAGE_VERBOSE</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>af5eea9687ad679dc3e504d373e959dc0c984e9c0f0f8aeb2ff46a546ce18d56</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>double</type>
      <name>Value</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>566a00621638570a4186414035153a2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LpBase</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>26ecf0974cd1cab0ed58ae67ff295429</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>solverName</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>a36ad09b49f4281a3b3ce71f2c241289</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Col</type>
      <name>addCol</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>ce2f580cd6308e948d9691a01845c8ec</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addColSet</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>2c9d5809f5454ff88569493888711c69</anchor>
      <arglist>(T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>col</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>41f5b9e3a8f92ce2560a62f3e8b0250f</anchor>
      <arglist>(Col c, const DualExpr &amp;e)</arglist>
    </member>
    <member kind="function">
      <type>DualExpr</type>
      <name>col</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>61bc603824e086c53440c385ebc42e53</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="function">
      <type>Col</type>
      <name>addCol</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>7ddfc3691600833b1bd53da9dc71799a</anchor>
      <arglist>(const DualExpr &amp;e, Value o=0)</arglist>
    </member>
    <member kind="function">
      <type>Row</type>
      <name>addRow</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>82ce0d31c840a4b11ad5462c0a5a3fc0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addRowSet</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>76dd777615a51b8f1300353353adc30c</anchor>
      <arglist>(T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>row</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>e099f58d682ae7ddebae4a80975af7ab</anchor>
      <arglist>(Row r, Value l, const Expr &amp;e, Value u)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>row</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>6ae967113c39dad59bc508701671b537</anchor>
      <arglist>(Row r, const Constr &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>Expr</type>
      <name>row</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>74c7bb111b543663d3040c40210b1046</anchor>
      <arglist>(Row r) const </arglist>
    </member>
    <member kind="function">
      <type>Row</type>
      <name>addRow</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>c979aa7dc6bb67dd2784ed61d469e122</anchor>
      <arglist>(Value l, const Expr &amp;e, Value u)</arglist>
    </member>
    <member kind="function">
      <type>Row</type>
      <name>addRow</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>551823fd575a523630b53488d840325b</anchor>
      <arglist>(const Constr &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>b50b2fcd86d231d5aed7228e4b732572</anchor>
      <arglist>(Col c)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>8a5daa2d397c5c1b585711641871971c</anchor>
      <arglist>(Row r)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>colName</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>40cd087446f1edb23bcf21c0cbbddb1e</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>colName</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>926a85f6e1fc4ef4dc9d82dfdf41a954</anchor>
      <arglist>(Col c, const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>Col</type>
      <name>colByName</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>f8d39077bf7fb6fcc066e1266f6a1ccf</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>rowName</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>2796339e099cf560d43433261d108cf4</anchor>
      <arglist>(Row r) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>rowName</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>e0c806c54bb198a105afec8557ccc41b</anchor>
      <arglist>(Row r, const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>Row</type>
      <name>rowByName</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>d011ab6444770df9383c447e60aa550c</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>coeff</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>14e7239a59a1d17bf01f53fe3b89e6f8</anchor>
      <arglist>(Row r, Col c, Value val)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>coeff</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>930c625c649c5047d402478aac891e8c</anchor>
      <arglist>(Row r, Col c) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>colLowerBound</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>cbea661131fafe99de521c22af326c39</anchor>
      <arglist>(Col c, Value value)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>colLowerBound</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>a4d6e3b8659cfd373449359e0c1803b0</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>colLowerBound</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>2bf2a528a411d876be5cad45e417f225</anchor>
      <arglist>(T &amp;t, Value value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>colUpperBound</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>92dc2b50e43b8238917b037b687e66fe</anchor>
      <arglist>(Col c, Value value)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>colUpperBound</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>6f94ed1b3b1fbf8ed64bdad5cb2d5df4</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>colUpperBound</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>ecdb0a340dbdc16aa80e959e22c8018f</anchor>
      <arglist>(T &amp;t, Value value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>colBounds</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>6d4a12e3ebf7ac7bcb4d35a3f18b5422</anchor>
      <arglist>(Col c, Value lower, Value upper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>colBounds</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>e86f414d278fe0623cf643b170d82dd6</anchor>
      <arglist>(T &amp;t, Value lower, Value upper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>rowLowerBound</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>5e454afcea273dccc19a793e9a95aecc</anchor>
      <arglist>(Row r, Value value)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>rowLowerBound</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>0f9508b028d6692acdaa022036a84b15</anchor>
      <arglist>(Row r) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>rowUpperBound</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>41e9d2136a9983180a57390d56f449f5</anchor>
      <arglist>(Row r, Value value)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>rowUpperBound</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>89bfc5c8e24a596a2e2b794a2695f92e</anchor>
      <arglist>(Row r) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>objCoeff</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>e7188a8596024077d2a6e37968c49686</anchor>
      <arglist>(Col c, Value v)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>objCoeff</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>9f8768e82121a924c232d1f5b287eb27</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>obj</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>f2ad43b3dd09721bff1f5bdc40cf7146</anchor>
      <arglist>(const Expr &amp;e)</arglist>
    </member>
    <member kind="function">
      <type>Expr</type>
      <name>obj</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>5017a0af45f30e38575720ae146a95b6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sense</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>e3674508394adc61d4f307fcecae2d70</anchor>
      <arglist>(Sense sense)</arglist>
    </member>
    <member kind="function">
      <type>Sense</type>
      <name>sense</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>236973a073493935ccb5ffc69247fdbe</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>max</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>6a360455761dfb54fa2567a5c2e46b70</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>min</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>0ea60e842310906e02cac7ba70cfb591</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>messageLevel</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>bcb84b14cad394ce094af144f3508d89</anchor>
      <arglist>(MessageLevel level)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>id</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>f34daa7955b659dc2ab4ee97db331c41</anchor>
      <arglist>(const Col &amp;col)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Col</type>
      <name>colFromId</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>accb11eb6305e70717547653560fc7bd</anchor>
      <arglist>(int id)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>id</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>9d1b0b46568a37601e77e137e281b526</anchor>
      <arglist>(const Row &amp;row)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Row</type>
      <name>rowFromId</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>77816a8220e3071b1c6f633ec2d528e5</anchor>
      <arglist>(int id)</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const Value</type>
      <name>INF</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>ae6d881c548a17a1a9e6aa13e3f2c397</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const Value</type>
      <name>NaN</name>
      <anchorfile>a00171.html</anchorfile>
      <anchor>7c8e1716b1e4ce72db4eb41902a0cf2d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase::Col</name>
    <filename>a00048.html</filename>
    <member kind="function">
      <type></type>
      <name>Col</name>
      <anchorfile>a00048.html</anchorfile>
      <anchor>f7991549589751a29539570df31ed273</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Col</name>
      <anchorfile>a00048.html</anchorfile>
      <anchor>5d790ffa8fab4ba0924ab12b5d5dc0ed</anchor>
      <arglist>(const Invalid &amp;)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00048.html</anchorfile>
      <anchor>1fcdaaeb461dc659e8ec6d8511d7e6e6</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00048.html</anchorfile>
      <anchor>fa478aab745002daba63fd6deef517bb</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00048.html</anchorfile>
      <anchor>473d9d668be4211f0be2755393942fd5</anchor>
      <arglist>(Col c) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase::ColIt</name>
    <filename>a00049.html</filename>
    <base>lemon::LpBase::Col</base>
    <member kind="function">
      <type></type>
      <name>ColIt</name>
      <anchorfile>a00049.html</anchorfile>
      <anchor>4ff1d0672e84973c2d1160b1aed6390a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ColIt</name>
      <anchorfile>a00049.html</anchorfile>
      <anchor>71391a941fd206fd2a29dceefaf6a3ca</anchor>
      <arglist>(const LpBase &amp;solver)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ColIt</name>
      <anchorfile>a00049.html</anchorfile>
      <anchor>0ba710f476749e1b36032d4792eb9cb6</anchor>
      <arglist>(const Invalid &amp;)</arglist>
    </member>
    <member kind="function">
      <type>ColIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00049.html</anchorfile>
      <anchor>5c5e13776a43ec2dfb4c2996c3af45c1</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase::Constr</name>
    <filename>a00063.html</filename>
    <member kind="function">
      <type></type>
      <name>Constr</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>576a596598827f586a2ab82ae3e07d31</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Constr</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>623d3a439f88505872161d283b760eee</anchor>
      <arglist>(Value lb, const Expr &amp;e, Value ub)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Expr &amp;</type>
      <name>expr</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>e58c18a4a08116d6ae44604605066507</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Expr &amp;</type>
      <name>expr</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>9e29ddcd48250335b21498997ed26871</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value &amp;</type>
      <name>lowerBound</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>cd1259eac31191e948eeb2c7d988107f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Value &amp;</type>
      <name>lowerBound</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>bf6c2abb7aa0b77b909acd3326858dac</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value &amp;</type>
      <name>upperBound</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>f89ee4ef9d1857a45cb0189a98550b7d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Value &amp;</type>
      <name>upperBound</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>8e5e406d491fe7d35590090771fff576</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>lowerBounded</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>b6159688ba275670d12037875693005a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>upperBounded</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>0b5ad3def717983fbf60c5ca5566d1b4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator&lt;=</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>ba05c19e3728edb9820af4891bd9e514</anchor>
      <arglist>(const LpBase::Expr &amp;e, const LpBase::Expr &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator&lt;=</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>3a6d494d96bd77cd35d2aefbf0d817d6</anchor>
      <arglist>(const LpBase::Value &amp;e, const LpBase::Expr &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator&lt;=</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>cb01b31e7969cba02f0bdb9e510630c1</anchor>
      <arglist>(const LpBase::Expr &amp;e, const LpBase::Value &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator&gt;=</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>78444c6379b70fe3160f65a7df0b0db8</anchor>
      <arglist>(const LpBase::Expr &amp;e, const LpBase::Expr &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator&gt;=</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>d1a4be06877b77f023219ea329646f70</anchor>
      <arglist>(const LpBase::Value &amp;e, const LpBase::Expr &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator&gt;=</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>5c753591c58902ddfffac5926b3312f2</anchor>
      <arglist>(const LpBase::Expr &amp;e, const LpBase::Value &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator==</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>33cd989df3b6f11babefd0aa1f0e116a</anchor>
      <arglist>(const LpBase::Expr &amp;e, const LpBase::Value &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator==</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>bb6f822ab9e2703ee89d33560c4a31e8</anchor>
      <arglist>(const LpBase::Expr &amp;e, const LpBase::Expr &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator&lt;=</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>f2a4b8210c5e3ffaea64c34723c25ad2</anchor>
      <arglist>(const LpBase::Value &amp;n, const LpBase::Constr &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator&lt;=</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>9632e192a5e7ea45bf320dfdb41a6a46</anchor>
      <arglist>(const LpBase::Constr &amp;c, const LpBase::Value &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator&gt;=</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>bcef5c1c71de61abbdfd7948a38dae9e</anchor>
      <arglist>(const LpBase::Value &amp;n, const LpBase::Constr &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Constr</type>
      <name>operator&gt;=</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>1c2da2fff256f10cdeec8cd0967433c1</anchor>
      <arglist>(const LpBase::Constr &amp;c, const LpBase::Value &amp;n)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase::DualExpr</name>
    <filename>a00095.html</filename>
    <class kind="class">lemon::LpBase::DualExpr::CoeffIt</class>
    <class kind="class">lemon::LpBase::DualExpr::ConstCoeffIt</class>
    <member kind="typedef">
      <type>LpBase::Row</type>
      <name>Key</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>1d3263d18e745e004485ffd0c52769ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>LpBase::Value</type>
      <name>Value</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>032494d683f041c4f7747c474a044c30</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DualExpr</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>b17c2c35bd5ff5f16f10a42bb5a8871e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DualExpr</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>81ae7566dd55a7fa596dce65e2108807</anchor>
      <arglist>(const Row &amp;r)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>6095e9431f15568c02f142a25e0b6573</anchor>
      <arglist>(const Row &amp;r) const </arglist>
    </member>
    <member kind="function">
      <type>Value &amp;</type>
      <name>operator[]</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>ccddbed99b053afecd40ea92867c54b8</anchor>
      <arglist>(const Row &amp;r)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>6435de676808f8a7c88ea1e1c3950796</anchor>
      <arglist>(const Row &amp;r, const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>simplify</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>a0a8cc3641e02a96fa39d303b0f88b3d</anchor>
      <arglist>(Value epsilon=0.0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>DualExpr &amp;</type>
      <name>operator+=</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>88004d09920b531f523044e88c0b88f1</anchor>
      <arglist>(const DualExpr &amp;e)</arglist>
    </member>
    <member kind="function">
      <type>DualExpr &amp;</type>
      <name>operator-=</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>d60ab715a7a90eb935f81966f120671a</anchor>
      <arglist>(const DualExpr &amp;e)</arglist>
    </member>
    <member kind="function">
      <type>DualExpr &amp;</type>
      <name>operator*=</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>e9262dea3610bff7f0aa5482430eee09</anchor>
      <arglist>(const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>DualExpr &amp;</type>
      <name>operator/=</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>2f0e09da752f2eafb4f8071ecfadea0e</anchor>
      <arglist>(const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::DualExpr</type>
      <name>operator+</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>c0e09c29a447377931b706ebf8714d58</anchor>
      <arglist>(const LpBase::DualExpr &amp;a, const LpBase::DualExpr &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::DualExpr</type>
      <name>operator-</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>0276c63464b89f00e8c32688837f49a6</anchor>
      <arglist>(const LpBase::DualExpr &amp;a, const LpBase::DualExpr &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::DualExpr</type>
      <name>operator*</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>e3b285a79edb32b9c8f9561e9bd20889</anchor>
      <arglist>(const LpBase::DualExpr &amp;a, const LpBase::Value &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::DualExpr</type>
      <name>operator*</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>de4dde11224713e0fe6bd57b4370b57e</anchor>
      <arglist>(const LpBase::Value &amp;a, const LpBase::DualExpr &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::DualExpr</type>
      <name>operator/</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>de6fa4ea6e3d09bfb1845dd304556969</anchor>
      <arglist>(const LpBase::DualExpr &amp;a, const LpBase::Value &amp;b)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase::DualExpr::CoeffIt</name>
    <filename>a00047.html</filename>
    <member kind="function">
      <type></type>
      <name>CoeffIt</name>
      <anchorfile>a00047.html</anchorfile>
      <anchor>f1d4bd086fbc47d2f405b90977ea211c</anchor>
      <arglist>(DualExpr &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Row</name>
      <anchorfile>a00047.html</anchorfile>
      <anchor>c29d373219762ca5ef8ff030149d316e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value &amp;</type>
      <name>operator*</name>
      <anchorfile>a00047.html</anchorfile>
      <anchor>153207f2745305ea9d834e3ad5ad99ca</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Value &amp;</type>
      <name>operator*</name>
      <anchorfile>a00047.html</anchorfile>
      <anchor>6e19374418db2abf9484578d9718c383</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoeffIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00047.html</anchorfile>
      <anchor>039a874d8a262be17f9ce7ac3d169081</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00047.html</anchorfile>
      <anchor>9569625b6759eac56d4b4294aba9e5b2</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00047.html</anchorfile>
      <anchor>ab38b74c42b0f2581ed4917fca608efc</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase::DualExpr::ConstCoeffIt</name>
    <filename>a00060.html</filename>
    <member kind="function">
      <type></type>
      <name>ConstCoeffIt</name>
      <anchorfile>a00060.html</anchorfile>
      <anchor>906f76a122f4e52776d7f108a8bfabed</anchor>
      <arglist>(const DualExpr &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Row</name>
      <anchorfile>a00060.html</anchorfile>
      <anchor>c29d373219762ca5ef8ff030149d316e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const Value &amp;</type>
      <name>operator*</name>
      <anchorfile>a00060.html</anchorfile>
      <anchor>6e19374418db2abf9484578d9718c383</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ConstCoeffIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00060.html</anchorfile>
      <anchor>7b10f56647ec34e697101749cae7fa92</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00060.html</anchorfile>
      <anchor>9569625b6759eac56d4b4294aba9e5b2</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00060.html</anchorfile>
      <anchor>ab38b74c42b0f2581ed4917fca608efc</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase::Expr</name>
    <filename>a00109.html</filename>
    <class kind="class">lemon::LpBase::Expr::CoeffIt</class>
    <class kind="class">lemon::LpBase::Expr::ConstCoeffIt</class>
    <member kind="typedef">
      <type>LpBase::Col</type>
      <name>Key</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>bfaaaa30fd6c82b0a45041e2f5ba58b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>LpBase::Value</type>
      <name>Value</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>032494d683f041c4f7747c474a044c30</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Expr</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>bc3c54e9ecf73d4054d8ef45fdbb6a54</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Expr</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>0c91aa73ea30ab88abdac335b18de1f8</anchor>
      <arglist>(const Col &amp;c)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Expr</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>692099ab161f0bae46c205b7fff4ec32</anchor>
      <arglist>(const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>a29eb47420c060a4488e3d5f99884e5b</anchor>
      <arglist>(const Col &amp;c) const </arglist>
    </member>
    <member kind="function">
      <type>Value &amp;</type>
      <name>operator[]</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>cde22c2fa83e22b68b4062954a94c4d3</anchor>
      <arglist>(const Col &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>45f0a1a4ccd25f30c350e03c025c99d4</anchor>
      <arglist>(const Col &amp;c, const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>Value &amp;</type>
      <name>operator*</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>153207f2745305ea9d834e3ad5ad99ca</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Value &amp;</type>
      <name>operator*</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>6e19374418db2abf9484578d9718c383</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>simplify</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>a0a8cc3641e02a96fa39d303b0f88b3d</anchor>
      <arglist>(Value epsilon=0.0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Expr &amp;</type>
      <name>operator+=</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>7da807a52dd8199a4b6f0e095c5e3466</anchor>
      <arglist>(const Expr &amp;e)</arglist>
    </member>
    <member kind="function">
      <type>Expr &amp;</type>
      <name>operator-=</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>ea5575e382c1eb0514af4a7b6b371af7</anchor>
      <arglist>(const Expr &amp;e)</arglist>
    </member>
    <member kind="function">
      <type>Expr &amp;</type>
      <name>operator*=</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>450a32ebed0e3d97cf73a3fd2d3f5228</anchor>
      <arglist>(const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>Expr &amp;</type>
      <name>operator/=</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>41962013d47d284a5785143ff306ac8a</anchor>
      <arglist>(const Value &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Expr</type>
      <name>operator+</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>96cb600aab6d3a1cd79355e5e7ef7615</anchor>
      <arglist>(const LpBase::Expr &amp;a, const LpBase::Expr &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Expr</type>
      <name>operator-</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>c2b4b41c4a456533dfcf1d00150ada41</anchor>
      <arglist>(const LpBase::Expr &amp;a, const LpBase::Expr &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Expr</type>
      <name>operator*</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>6994c853574fe469fa348cd5ac359363</anchor>
      <arglist>(const LpBase::Expr &amp;a, const LpBase::Value &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Expr</type>
      <name>operator*</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>5b279048337eb708f35fda03684bd20d</anchor>
      <arglist>(const LpBase::Value &amp;a, const LpBase::Expr &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>LpBase::Expr</type>
      <name>operator/</name>
      <anchorfile>a00109.html</anchorfile>
      <anchor>91e1c295db53e190588c1b5e8e1be009</anchor>
      <arglist>(const LpBase::Expr &amp;a, const LpBase::Value &amp;b)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase::Expr::CoeffIt</name>
    <filename>a00046.html</filename>
    <member kind="function">
      <type></type>
      <name>CoeffIt</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>86138e097d2ae2c09acd7c2f2e595574</anchor>
      <arglist>(Expr &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Col</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>400973c92863296a1f0f627b85da2b17</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value &amp;</type>
      <name>operator*</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>153207f2745305ea9d834e3ad5ad99ca</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Value &amp;</type>
      <name>operator*</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>6e19374418db2abf9484578d9718c383</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoeffIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>039a874d8a262be17f9ce7ac3d169081</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>9569625b6759eac56d4b4294aba9e5b2</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>ab38b74c42b0f2581ed4917fca608efc</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase::Expr::ConstCoeffIt</name>
    <filename>a00059.html</filename>
    <member kind="function">
      <type></type>
      <name>ConstCoeffIt</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>d50df4e747412a8515c4ed98e63d6f95</anchor>
      <arglist>(const Expr &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Col</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>400973c92863296a1f0f627b85da2b17</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const Value &amp;</type>
      <name>operator*</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>6e19374418db2abf9484578d9718c383</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ConstCoeffIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>7b10f56647ec34e697101749cae7fa92</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>9569625b6759eac56d4b4294aba9e5b2</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>ab38b74c42b0f2581ed4917fca608efc</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase::Row</name>
    <filename>a00229.html</filename>
    <member kind="function">
      <type></type>
      <name>Row</name>
      <anchorfile>a00229.html</anchorfile>
      <anchor>be1b058f05829f61c8f6615f83e2b7ad</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Row</name>
      <anchorfile>a00229.html</anchorfile>
      <anchor>afee000c5e8b9efb815aded749b2ed28</anchor>
      <arglist>(const Invalid &amp;)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00229.html</anchorfile>
      <anchor>f54ae35ab90085c0bd6c8e5f77dd81e3</anchor>
      <arglist>(Row r) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00229.html</anchorfile>
      <anchor>caddfe4b03925d3f0383192aeb62ed8f</anchor>
      <arglist>(Row r) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00229.html</anchorfile>
      <anchor>36bf7f6869ae44a643a13974ff345b5f</anchor>
      <arglist>(Row r) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpBase::RowIt</name>
    <filename>a00230.html</filename>
    <base>lemon::LpBase::Row</base>
    <member kind="function">
      <type></type>
      <name>RowIt</name>
      <anchorfile>a00230.html</anchorfile>
      <anchor>a63559ada5687aa42b7db278d5c9bc36</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RowIt</name>
      <anchorfile>a00230.html</anchorfile>
      <anchor>764ebc325a013cf3a42b5633990c1f69</anchor>
      <arglist>(const LpBase &amp;solver)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RowIt</name>
      <anchorfile>a00230.html</anchorfile>
      <anchor>3e23648dfcbb187c8609760fff95075c</anchor>
      <arglist>(const Invalid &amp;)</arglist>
    </member>
    <member kind="function">
      <type>RowIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00230.html</anchorfile>
      <anchor>d30aa1e690e8ca3e0dfcb0c0273e0b39</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpSolver</name>
    <filename>a00173.html</filename>
    <base virtualness="virtual">lemon::LpBase</base>
    <member kind="enumeration">
      <name>ProblemType</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UNDEFINED</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e605159e8a4c32319fd69b5d151369d93</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>INFEASIBLE</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e2884fa43446c0cbc9c7a9b74d41d7483</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FEASIBLE</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e03f919221217f95d21a593a7120165e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>OPTIMAL</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e2579881e7c83261bc21bafb5a5c92cad</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UNBOUNDED</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e6c65123d1b5b01632a477661055b01ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>VarStatus</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>86b9012e22aaac73c64bb7247dcb44c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>BASIC</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>86b9012e22aaac73c64bb7247dcb44c513c45196813cb44e6e81e9c48a5ec1b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FREE</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>86b9012e22aaac73c64bb7247dcb44c5cc62d1576546f3245237e1b232d838b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>LOWER</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>86b9012e22aaac73c64bb7247dcb44c5a1017e9b343135a54a98b6f479354d16</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UPPER</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>86b9012e22aaac73c64bb7247dcb44c5e704d5d328a8522a6193aa3efb28c724</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FIXED</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>86b9012e22aaac73c64bb7247dcb44c59b5eccb7f8f027c46f2018d07c74f384</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual LpSolver *</type>
      <name>newSolver</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>96ce83198455cdbef6be4e5b4b16eaca</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual LpSolver *</type>
      <name>cloneSolver</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>7bd26c359e95549b3ab91a4abde8973e</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function">
      <type>SolveExitStatus</type>
      <name>solve</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>1963be740c5f32bb76dfc9df4c2de376</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>ProblemType</type>
      <name>primalType</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>2b4368f93440f5b674a509841415e554</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ProblemType</type>
      <name>dualType</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>8da27523270c3cab58202dde93e4f7b5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>primal</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>37740a7c4bc53157fe6a1135f30ac24f</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>primal</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>6c2cdad752fe07e91e06b7a421befe58</anchor>
      <arglist>(const Expr &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>primalRay</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>900d1bf30c187c50d4e1a982c41505db</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>dual</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>22e9480f94d2224f1a0080e227028e9d</anchor>
      <arglist>(Row r) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>dual</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>a65b30070f24573eb53f13dd7b7b8cb4</anchor>
      <arglist>(const DualExpr &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>dualRay</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>e8c931c73226d2f3cb5afb18d3b9550b</anchor>
      <arglist>(Row r) const </arglist>
    </member>
    <member kind="function">
      <type>VarStatus</type>
      <name>colStatus</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>773b4758545a7cae47ac1c5ab51fa5ff</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="function">
      <type>VarStatus</type>
      <name>rowStatus</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>1f4b3adb0e49c66d163d92b010d3b29c</anchor>
      <arglist>(Row r) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>primal</name>
      <anchorfile>a00173.html</anchorfile>
      <anchor>3a2e2a739c1350d23ca0b7edb2cad814</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MipSolver</name>
    <filename>a00186.html</filename>
    <base virtualness="virtual">lemon::LpBase</base>
    <member kind="enumeration">
      <name>ColTypes</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>2abb63446fb6107f8e2f4dbe9de2e082</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>REAL</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>2abb63446fb6107f8e2f4dbe9de2e082053cb139f4b2333482449705c529b1e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>INTEGER</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>2abb63446fb6107f8e2f4dbe9de2e0825a063e265d2ac903b6808e9f6e73ec46</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>colType</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>a7e3fcdb8e8840d5a4ce7a74a143de8c</anchor>
      <arglist>(Col c, ColTypes col_type)</arglist>
    </member>
    <member kind="function">
      <type>ColTypes</type>
      <name>colType</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>976be9b0ed7194fa58e57c847b2a3508</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="enumeration">
      <name>ProblemType</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UNDEFINED</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e605159e8a4c32319fd69b5d151369d93</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>INFEASIBLE</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e2884fa43446c0cbc9c7a9b74d41d7483</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FEASIBLE</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e03f919221217f95d21a593a7120165e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>OPTIMAL</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e2579881e7c83261bc21bafb5a5c92cad</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UNBOUNDED</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e6c65123d1b5b01632a477661055b01ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual MipSolver *</type>
      <name>newSolver</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>d14b234174aeb1168ee9169136b66367</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual MipSolver *</type>
      <name>cloneSolver</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>36358eade1031f6be344751a8fd16437</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function">
      <type>SolveExitStatus</type>
      <name>solve</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>1963be740c5f32bb76dfc9df4c2de376</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>ProblemType</type>
      <name>type</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>085c0dfe4f0aa0442c9a31b99e396b90</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>sol</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>13b82b692292e7d694033d89451a0199</anchor>
      <arglist>(Col c) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>sol</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>b805549c7229890a9557278d06e85d42</anchor>
      <arglist>(const Expr &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>solValue</name>
      <anchorfile>a00186.html</anchorfile>
      <anchor>39b170fcdf246f7e8bc0dea2423a8647</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SkeletonSolverBase</name>
    <filename>a00266.html</filename>
    <base virtualness="virtual">lemon::LpBase</base>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual int</type>
      <name>_addCol</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>3e4122bd62080a37ffabaa0f7ee71a3f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual int</type>
      <name>_addRow</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>63fe2e2764e33f96cff12ef717850bf5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_eraseCol</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>8d9ae2ab8f081f2675ac1861d7b92539</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_eraseRow</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>7a88deb35973e379fe613b64fefd9df3</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_getColName</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>63f7bce5e007a36f3a233536fdff26c0</anchor>
      <arglist>(int col, std::string &amp;name) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setColName</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>e9742b34c4c4f8421d24ae35b40165c8</anchor>
      <arglist>(int col, const std::string &amp;name)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual int</type>
      <name>_colByName</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>17bb426f44e7c688f246c131ecdaf4a0</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_getRowName</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>9af6ac589bdbb728d958a6b818f4646f</anchor>
      <arglist>(int row, std::string &amp;name) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setRowName</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>32c0b5b3985527308084f26565a23375</anchor>
      <arglist>(int row, const std::string &amp;name)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual int</type>
      <name>_rowByName</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>deb6051ec6fdce28df4bbf8c33a4ec10</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setRowCoeffs</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>86bcc074e1232efc1633cf70e595da73</anchor>
      <arglist>(int i, ExprIterator b, ExprIterator e)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_getRowCoeffs</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>a3cc8250dff7bcaa6860cd992c69ad0a</anchor>
      <arglist>(int i, InsertIterator b) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setColCoeffs</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>eafde598054ed90d572089322454f5a6</anchor>
      <arglist>(int i, ExprIterator b, ExprIterator e)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_getColCoeffs</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>498d597aea8e87dae9379353f884cfce</anchor>
      <arglist>(int i, InsertIterator b) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setCoeff</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>0f76d7b2ec4142034cb71d94a1f87dab</anchor>
      <arglist>(int row, int col, Value value)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getCoeff</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>776ef1e45d36050e9c02b91bb48189c7</anchor>
      <arglist>(int row, int col) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setColLowerBound</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>6271da636bd2c7034f8502506532809e</anchor>
      <arglist>(int i, Value value)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getColLowerBound</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>2a225402c718cd88bfcec3ed0805e1f2</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setColUpperBound</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>cca7a1a24c08d2e4386a767bdabdeb08</anchor>
      <arglist>(int i, Value value)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getColUpperBound</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>5914ea33ddc2ff8ee5a0ec2499993351</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setRowLowerBound</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>826ed7e1b4ed98483dc828237007bbbf</anchor>
      <arglist>(int i, Value value)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getRowLowerBound</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>4acfbc8622c1ee4c89d55bd95b93f11e</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setRowUpperBound</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>854b5e280e4d6bd253f67dd1dfe69644</anchor>
      <arglist>(int i, Value value)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getRowUpperBound</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>d9acc2e19b0b9eb76bc50bd3e7dee486</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setObjCoeffs</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>290835cf8024c4138a97edc3df5ce1b1</anchor>
      <arglist>(ExprIterator b, ExprIterator e)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_getObjCoeffs</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>1ddd17c0d2c1d2e53fe426198689b559</anchor>
      <arglist>(InsertIterator b) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setObjCoeff</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>454737987b57b0808888de1f89ec1f60</anchor>
      <arglist>(int i, Value obj_coef)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getObjCoeff</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>1223311f7cd9caba18c3c23f06a0e832</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_setSense</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>452e8c0672accb14f50a95d3247681d1</anchor>
      <arglist>(Sense)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Sense</type>
      <name>_getSense</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>3280d7cae82147d475f5d2aef30b24c1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_clear</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>da6768be8b067492c3f7e6e7861091e2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_messageLevel</name>
      <anchorfile>a00266.html</anchorfile>
      <anchor>bf7ba538e76b92c679544a92fa538ba0</anchor>
      <arglist>(MessageLevel)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LpSkeleton</name>
    <filename>a00172.html</filename>
    <base>lemon::LpSolver</base>
    <base>lemon::SkeletonSolverBase</base>
    <member kind="function">
      <type></type>
      <name>LpSkeleton</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>ac6b7af1622599f20a58aaa053c7b6b4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual LpSkeleton *</type>
      <name>newSolver</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>171f2a55ab80dccf76871df17d295e80</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual LpSkeleton *</type>
      <name>cloneSolver</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>477a3f86cb9586d3fe2e86696fbc1057</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual SolveExitStatus</type>
      <name>_solve</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>fb62ef2dec9579d5a2b4ebb5e482e3bd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getPrimal</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>b7cf4b16ee827a1242c2bf4dffd567b1</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getDual</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>79d7bc323f4cd04d281db30aa9f4cc86</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getPrimalValue</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>62395345f6e54d83c8d5893d3eeb62b2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getPrimalRay</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>2967d2e7c0d0042f06dbdb14f04db3a6</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getDualRay</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>ba482fdb5d1505edaf5d64ee96e45516</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual ProblemType</type>
      <name>_getPrimalType</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>add94f7c5114597072bc0fd5ade8e6b5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual ProblemType</type>
      <name>_getDualType</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>0a8d2cb6a0d932a0add661d71c79fb74</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual VarStatus</type>
      <name>_getColStatus</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>55507e823e01466e2378cf6fd5ff7054</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual VarStatus</type>
      <name>_getRowStatus</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>2218b837cfab857010050d16e55ba5c8</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual const char *</type>
      <name>_solverName</name>
      <anchorfile>a00172.html</anchorfile>
      <anchor>d7a89ea622e84d4605d402e33f3361ee</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MipSkeleton</name>
    <filename>a00185.html</filename>
    <base>lemon::MipSolver</base>
    <base>lemon::SkeletonSolverBase</base>
    <member kind="function">
      <type></type>
      <name>MipSkeleton</name>
      <anchorfile>a00185.html</anchorfile>
      <anchor>4e33e7af142273f190acd7dfcb5ad77b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual MipSkeleton *</type>
      <name>newSolver</name>
      <anchorfile>a00185.html</anchorfile>
      <anchor>e38cc0909b8588d7d846b7ea9ee1cbd3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual MipSkeleton *</type>
      <name>cloneSolver</name>
      <anchorfile>a00185.html</anchorfile>
      <anchor>e912ff8ff000b017a3de2d17379b19d4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual SolveExitStatus</type>
      <name>_solve</name>
      <anchorfile>a00185.html</anchorfile>
      <anchor>05bc99cbe46a5d442c2039f21ab5ac00</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getSol</name>
      <anchorfile>a00185.html</anchorfile>
      <anchor>2b7149d1e248123feadfdc471a6af6a2</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual Value</type>
      <name>_getSolValue</name>
      <anchorfile>a00185.html</anchorfile>
      <anchor>6c54141d3e417b7afea31b9917c52c30</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual ProblemType</type>
      <name>_getType</name>
      <anchorfile>a00185.html</anchorfile>
      <anchor>866f80ce66ff5b0b0657d9434dd366a1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual const char *</type>
      <name>_solverName</name>
      <anchorfile>a00185.html</anchorfile>
      <anchor>d7a89ea622e84d4605d402e33f3361ee</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MapBase</name>
    <filename>a00174.html</filename>
    <templarg>K</templarg>
    <templarg>V</templarg>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00174.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00174.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::NullMap</name>
    <filename>a00203.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; K, V &gt;</base>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00203.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00203.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00203.html</anchorfile>
      <anchor>a6290a1f49db2643ba24d02f87cbf429</anchor>
      <arglist>(const Key &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00203.html</anchorfile>
      <anchor>46b66c881c64c81af62a9616946a2f3a</anchor>
      <arglist>(const Key &amp;, const Value &amp;)</arglist>
    </member>
    <member kind="function">
      <type>NullMap&lt; K, V &gt;</type>
      <name>nullMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>gdc125dd59f361134ee8e5a59802ea2e2</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ConstMap</name>
    <filename>a00061.html</filename>
    <templarg>K</templarg>
    <templarg>V</templarg>
    <base>MapBase&lt; K, V &gt;</base>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00061.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00061.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ConstMap</name>
      <anchorfile>a00061.html</anchorfile>
      <anchor>9532675f587433570548ee24a2aa440e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ConstMap</name>
      <anchorfile>a00061.html</anchorfile>
      <anchor>c27f63c7329551a5b298813c13e1b599</anchor>
      <arglist>(const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00061.html</anchorfile>
      <anchor>a6290a1f49db2643ba24d02f87cbf429</anchor>
      <arglist>(const Key &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00061.html</anchorfile>
      <anchor>46b66c881c64c81af62a9616946a2f3a</anchor>
      <arglist>(const Key &amp;, const Value &amp;)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setAll</name>
      <anchorfile>a00061.html</anchorfile>
      <anchor>745398d41d98d0e81a74af611430004f</anchor>
      <arglist>(const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>ConstMap&lt; K, V &gt;</type>
      <name>constMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g19fc5eeb6094d25f6c72e273e0b1e776</anchor>
      <arglist>(const V &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>ConstMap&lt; K, Const&lt; V, v &gt; &gt;</type>
      <name>constMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g1004623cdfc30601d45b45eff02294d0</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ConstMap&lt; K, Const&lt; V, v &gt; &gt;</name>
    <filename>a00062.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg>v</templarg>
    <base>MapBase&lt; K, V &gt;</base>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00062.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00062.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ConstMap</name>
      <anchorfile>a00062.html</anchorfile>
      <anchor>9532675f587433570548ee24a2aa440e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00062.html</anchorfile>
      <anchor>a6290a1f49db2643ba24d02f87cbf429</anchor>
      <arglist>(const Key &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00062.html</anchorfile>
      <anchor>46b66c881c64c81af62a9616946a2f3a</anchor>
      <arglist>(const Key &amp;, const Value &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::IdentityMap</name>
    <filename>a00144.html</filename>
    <templarg></templarg>
    <base>MapBase&lt; T, T &gt;</base>
    <member kind="typedef">
      <type>T</type>
      <name>Key</name>
      <anchorfile>a00144.html</anchorfile>
      <anchor>a232325a6d03d31df8f241b597083063</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>T</type>
      <name>Value</name>
      <anchorfile>a00144.html</anchorfile>
      <anchor>34b57a974fe67a997b7693e6e71cd904</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00144.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>IdentityMap&lt; T &gt;</type>
      <name>identityMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g74d66a12052a62f85aaffa4fcdc17cb9</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::RangeMap</name>
    <filename>a00220.html</filename>
    <templarg>V</templarg>
    <base>MapBase&lt; int, V &gt;</base>
    <member kind="typedef">
      <type>int</type>
      <name>Key</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>c11959cb1c3b09fd9b188d07365c3031</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Vector::reference</type>
      <name>Reference</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>ed67788dc2eb8e9e979fbbbcb89c5adc</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Vector::const_reference</type>
      <name>ConstReference</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>10a58ef4f080a86d304c7f001791b8e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RangeMap</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>721cc8008a224e2d8ec682e70b93be33</anchor>
      <arglist>(int size=0, const Value &amp;value=Value())</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RangeMap</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>e9c12b775ad7bec1b6144d6be9101d82</anchor>
      <arglist>(const std::vector&lt; V1 &gt; &amp;vector)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RangeMap</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>db311d2ab91a506c4bd00d1620b65f4f</anchor>
      <arglist>(const RangeMap&lt; V1 &gt; &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>f4b57d21919c42d55af03391f91a1c08</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resize</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>f8911559a8f65e0f6cd045a549b5e29c</anchor>
      <arglist>(int size, const Value &amp;value=Value())</arglist>
    </member>
    <member kind="function">
      <type>Reference</type>
      <name>operator[]</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>d78f54d95302f2e1c40c9e69609d5646</anchor>
      <arglist>(const Key &amp;k)</arglist>
    </member>
    <member kind="function">
      <type>ConstReference</type>
      <name>operator[]</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>73758693b0a336ba3038083f14ac8699</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00220.html</anchorfile>
      <anchor>6ac83be911521f604e18f3df0d678213</anchor>
      <arglist>(const Key &amp;k, const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>RangeMap&lt; V &gt;</type>
      <name>rangeMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g6648195c0df738a0c3101524e55243dc</anchor>
      <arglist>(int size=0, const V &amp;value=V())</arglist>
    </member>
    <member kind="function">
      <type>RangeMap&lt; V &gt;</type>
      <name>rangeMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g9b43968e329c49b394424cdfd56e61a1</anchor>
      <arglist>(const std::vector&lt; V &gt; &amp;vector)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SparseMap</name>
    <filename>a00278.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; K, V &gt;</base>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00278.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00278.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Value &amp;</type>
      <name>Reference</name>
      <anchorfile>a00278.html</anchorfile>
      <anchor>e36c42dfba46c6fd74d7d85a4fc27d57</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>const Value &amp;</type>
      <name>ConstReference</name>
      <anchorfile>a00278.html</anchorfile>
      <anchor>33899f17b36f4c01c39f333a0a6f989b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SparseMap</name>
      <anchorfile>a00278.html</anchorfile>
      <anchor>9237ea5d58e005fbe1b48632340f2039</anchor>
      <arglist>(const Value &amp;value=Value())</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SparseMap</name>
      <anchorfile>a00278.html</anchorfile>
      <anchor>0d2f163c81ae809fa8d8f7044e50d28e</anchor>
      <arglist>(const std::map&lt; Key, V1, Comp1 &gt; &amp;map, const Value &amp;value=Value())</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SparseMap</name>
      <anchorfile>a00278.html</anchorfile>
      <anchor>049f33dd48237b19950df1f69622c1f3</anchor>
      <arglist>(const SparseMap&lt; Key, V1, Comp1 &gt; &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>Reference</type>
      <name>operator[]</name>
      <anchorfile>a00278.html</anchorfile>
      <anchor>d78f54d95302f2e1c40c9e69609d5646</anchor>
      <arglist>(const Key &amp;k)</arglist>
    </member>
    <member kind="function">
      <type>ConstReference</type>
      <name>operator[]</name>
      <anchorfile>a00278.html</anchorfile>
      <anchor>73758693b0a336ba3038083f14ac8699</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00278.html</anchorfile>
      <anchor>6ac83be911521f604e18f3df0d678213</anchor>
      <arglist>(const Key &amp;k, const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setAll</name>
      <anchorfile>a00278.html</anchorfile>
      <anchor>745398d41d98d0e81a74af611430004f</anchor>
      <arglist>(const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>SparseMap&lt; K, V, Compare &gt;</type>
      <name>sparseMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g22355d327475f1a36b63b7270b9f07b2</anchor>
      <arglist>(const V &amp;value=V())</arglist>
    </member>
    <member kind="function">
      <type>SparseMap&lt; K, V, Compare &gt;</type>
      <name>sparseMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g22955912798ba75ea8bca22e408b8807</anchor>
      <arglist>(const std::map&lt; K, V, Compare &gt; &amp;map, const V &amp;value=V())</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ComposeMap</name>
    <filename>a00056.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M2::Key, M1::Value &gt;</base>
    <member kind="typedef">
      <type>M2::Key</type>
      <name>Key</name>
      <anchorfile>a00056.html</anchorfile>
      <anchor>481aeb3578c6cc4719982a3e64501d19</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M1::Value</type>
      <name>Value</name>
      <anchorfile>a00056.html</anchorfile>
      <anchor>087615b851ee4b1f01463e4589205d3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ComposeMap</name>
      <anchorfile>a00056.html</anchorfile>
      <anchor>ff637d186f992a6d4e8c16d29b8eb7f2</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>MapTraits&lt; M1 &gt;::ConstReturnValue</type>
      <name>operator[]</name>
      <anchorfile>a00056.html</anchorfile>
      <anchor>347f53776a286f7b576ebb129dab7434</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>ComposeMap&lt; M1, M2 &gt;</type>
      <name>composeMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g168cb183ff15d3610151ab5044c70ce2</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::CombineMap</name>
    <filename>a00055.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M1::Key, V &gt;</base>
    <member kind="typedef">
      <type>M1::Key</type>
      <name>Key</name>
      <anchorfile>a00055.html</anchorfile>
      <anchor>c16c774bd2b82a45854c3ac66248de01</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00055.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CombineMap</name>
      <anchorfile>a00055.html</anchorfile>
      <anchor>051804ca4d865d945af7f8430325d60e</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2, const F &amp;f=F())</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00055.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>CombineMap&lt; M1, M2, F, V &gt;</type>
      <name>combineMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gb2f07629cac63828dbdab26e754e0767</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2, const F &amp;f)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::FunctorToMap</name>
    <filename>a00122.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; K, V &gt;</base>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00122.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00122.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FunctorToMap</name>
      <anchorfile>a00122.html</anchorfile>
      <anchor>dc7f90e90bf894dc1bddda3846a6f294</anchor>
      <arglist>(const F &amp;f=F())</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00122.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>FunctorToMap&lt; F, K, V &gt;</type>
      <name>functorToMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g963668011830914036e0246f7ee060ac</anchor>
      <arglist>(const F &amp;f)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MapToFunctor</name>
    <filename>a00177.html</filename>
    <templarg></templarg>
    <base>MapBase&lt; M::Key, M::Value &gt;</base>
    <member kind="typedef">
      <type>M::Key</type>
      <name>Key</name>
      <anchorfile>a00177.html</anchorfile>
      <anchor>8c5e944591efa8f8f0d64cf81950abcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M::Value</type>
      <name>Value</name>
      <anchorfile>a00177.html</anchorfile>
      <anchor>7054917e50868b3b4c3d0899fc4b422e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MapToFunctor</name>
      <anchorfile>a00177.html</anchorfile>
      <anchor>33bd2b0fdb3a620641e9f9870b418107</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator()</name>
      <anchorfile>a00177.html</anchorfile>
      <anchor>70fbf333c59f69eff6e3371f111c3dd1</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00177.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>MapToFunctor&lt; M &gt;</type>
      <name>mapToFunctor</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g9141e0ff40567f9d640db2c22485c28a</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ConvertMap</name>
    <filename>a00066.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M::Key, V &gt;</base>
    <member kind="typedef">
      <type>M::Key</type>
      <name>Key</name>
      <anchorfile>a00066.html</anchorfile>
      <anchor>8c5e944591efa8f8f0d64cf81950abcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00066.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ConvertMap</name>
      <anchorfile>a00066.html</anchorfile>
      <anchor>79f8b6b95303fb618dae9c43330b5ee4</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00066.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>ConvertMap&lt; M, V &gt;</type>
      <name>convertMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g47b4b4266339a3a8178c64d70958246d</anchor>
      <arglist>(const M &amp;map)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ForkMap</name>
    <filename>a00117.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M1::Key, M1::Value &gt;</base>
    <member kind="typedef">
      <type>M1::Key</type>
      <name>Key</name>
      <anchorfile>a00117.html</anchorfile>
      <anchor>c16c774bd2b82a45854c3ac66248de01</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M1::Value</type>
      <name>Value</name>
      <anchorfile>a00117.html</anchorfile>
      <anchor>087615b851ee4b1f01463e4589205d3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ForkMap</name>
      <anchorfile>a00117.html</anchorfile>
      <anchor>34d7446a3070acc4225e771654bce223</anchor>
      <arglist>(M1 &amp;m1, M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00117.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00117.html</anchorfile>
      <anchor>6ac83be911521f604e18f3df0d678213</anchor>
      <arglist>(const Key &amp;k, const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>ForkMap&lt; M1, M2 &gt;</type>
      <name>forkMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g09ff2fa37a6ca994c6d79a21942a50d2</anchor>
      <arglist>(M1 &amp;m1, M2 &amp;m2)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::AddMap</name>
    <filename>a00002.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M1::Key, M1::Value &gt;</base>
    <member kind="typedef">
      <type>M1::Key</type>
      <name>Key</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>c16c774bd2b82a45854c3ac66248de01</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M1::Value</type>
      <name>Value</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>087615b851ee4b1f01463e4589205d3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AddMap</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>326f279be4e7fccf9d30345b8a868297</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>AddMap&lt; M1, M2 &gt;</type>
      <name>addMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gbb8ca6bab7c32b12e1671497ac774d5d</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SubMap</name>
    <filename>a00283.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M1::Key, M1::Value &gt;</base>
    <member kind="typedef">
      <type>M1::Key</type>
      <name>Key</name>
      <anchorfile>a00283.html</anchorfile>
      <anchor>c16c774bd2b82a45854c3ac66248de01</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M1::Value</type>
      <name>Value</name>
      <anchorfile>a00283.html</anchorfile>
      <anchor>087615b851ee4b1f01463e4589205d3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SubMap</name>
      <anchorfile>a00283.html</anchorfile>
      <anchor>d1392c25e50f359abea9f21e249a83d1</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00283.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>SubMap&lt; M1, M2 &gt;</type>
      <name>subMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gd1a93a51897caa4c921635a9fa51cef9</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MulMap</name>
    <filename>a00187.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M1::Key, M1::Value &gt;</base>
    <member kind="typedef">
      <type>M1::Key</type>
      <name>Key</name>
      <anchorfile>a00187.html</anchorfile>
      <anchor>c16c774bd2b82a45854c3ac66248de01</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M1::Value</type>
      <name>Value</name>
      <anchorfile>a00187.html</anchorfile>
      <anchor>087615b851ee4b1f01463e4589205d3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MulMap</name>
      <anchorfile>a00187.html</anchorfile>
      <anchor>ab3ad71e03084dd7fb88dab99170b62d</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00187.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>MulMap&lt; M1, M2 &gt;</type>
      <name>mulMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>ge53cd3bc6adcb7381407f33f065142a5</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::DivMap</name>
    <filename>a00094.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M1::Key, M1::Value &gt;</base>
    <member kind="typedef">
      <type>M1::Key</type>
      <name>Key</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>c16c774bd2b82a45854c3ac66248de01</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M1::Value</type>
      <name>Value</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>087615b851ee4b1f01463e4589205d3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DivMap</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>4f76bcb3d309ff7e3d3dc8aef4c15f12</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>DivMap&lt; M1, M2 &gt;</type>
      <name>divMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g5ed3699166ac2cb68e69d4722ebc4df7</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ShiftMap</name>
    <filename>a00263.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M::Key, M::Value &gt;</base>
    <member kind="typedef">
      <type>M::Key</type>
      <name>Key</name>
      <anchorfile>a00263.html</anchorfile>
      <anchor>8c5e944591efa8f8f0d64cf81950abcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M::Value</type>
      <name>Value</name>
      <anchorfile>a00263.html</anchorfile>
      <anchor>7054917e50868b3b4c3d0899fc4b422e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ShiftMap</name>
      <anchorfile>a00263.html</anchorfile>
      <anchor>61298980a1e6c958993b583e99a973bb</anchor>
      <arglist>(const M &amp;m, const C &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00263.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>ShiftMap&lt; M, C &gt;</type>
      <name>shiftMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g179d41790ba45a63238666fd8437f763</anchor>
      <arglist>(const M &amp;m, const C &amp;v)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ShiftWriteMap</name>
    <filename>a00264.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M::Key, M::Value &gt;</base>
    <member kind="typedef">
      <type>M::Key</type>
      <name>Key</name>
      <anchorfile>a00264.html</anchorfile>
      <anchor>8c5e944591efa8f8f0d64cf81950abcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M::Value</type>
      <name>Value</name>
      <anchorfile>a00264.html</anchorfile>
      <anchor>7054917e50868b3b4c3d0899fc4b422e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ShiftWriteMap</name>
      <anchorfile>a00264.html</anchorfile>
      <anchor>d6fc2174dc74be26fdd310c4657f6be8</anchor>
      <arglist>(M &amp;m, const C &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00264.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00264.html</anchorfile>
      <anchor>6ac83be911521f604e18f3df0d678213</anchor>
      <arglist>(const Key &amp;k, const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>ShiftWriteMap&lt; M, C &gt;</type>
      <name>shiftWriteMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gd25e797238b5fe75bec91c1937c2f99d</anchor>
      <arglist>(M &amp;m, const C &amp;v)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ScaleMap</name>
    <filename>a00232.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M::Key, M::Value &gt;</base>
    <member kind="typedef">
      <type>M::Key</type>
      <name>Key</name>
      <anchorfile>a00232.html</anchorfile>
      <anchor>8c5e944591efa8f8f0d64cf81950abcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M::Value</type>
      <name>Value</name>
      <anchorfile>a00232.html</anchorfile>
      <anchor>7054917e50868b3b4c3d0899fc4b422e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ScaleMap</name>
      <anchorfile>a00232.html</anchorfile>
      <anchor>0b6202055fa4ee5ac6c956e9a35e4046</anchor>
      <arglist>(const M &amp;m, const C &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00232.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>ScaleMap&lt; M, C &gt;</type>
      <name>scaleMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g50af6b7bd9b56fa08bdda3e4e84a9541</anchor>
      <arglist>(const M &amp;m, const C &amp;v)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ScaleWriteMap</name>
    <filename>a00233.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M::Key, M::Value &gt;</base>
    <member kind="typedef">
      <type>M::Key</type>
      <name>Key</name>
      <anchorfile>a00233.html</anchorfile>
      <anchor>8c5e944591efa8f8f0d64cf81950abcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M::Value</type>
      <name>Value</name>
      <anchorfile>a00233.html</anchorfile>
      <anchor>7054917e50868b3b4c3d0899fc4b422e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ScaleWriteMap</name>
      <anchorfile>a00233.html</anchorfile>
      <anchor>2834c89c1978860c3e99cd810af8e83e</anchor>
      <arglist>(M &amp;m, const C &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00233.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00233.html</anchorfile>
      <anchor>6ac83be911521f604e18f3df0d678213</anchor>
      <arglist>(const Key &amp;k, const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>ScaleWriteMap&lt; M, C &gt;</type>
      <name>scaleWriteMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>ga23c144c639d693478a6660cf388cf87</anchor>
      <arglist>(M &amp;m, const C &amp;v)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::NegMap</name>
    <filename>a00188.html</filename>
    <templarg></templarg>
    <base>MapBase&lt; M::Key, M::Value &gt;</base>
    <member kind="typedef">
      <type>M::Key</type>
      <name>Key</name>
      <anchorfile>a00188.html</anchorfile>
      <anchor>8c5e944591efa8f8f0d64cf81950abcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M::Value</type>
      <name>Value</name>
      <anchorfile>a00188.html</anchorfile>
      <anchor>7054917e50868b3b4c3d0899fc4b422e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NegMap</name>
      <anchorfile>a00188.html</anchorfile>
      <anchor>77e1df4d3a894decde69bbeb33fb1512</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00188.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>NegMap&lt; M &gt;</type>
      <name>negMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g8d293b8674b498629cd4f5cd73431330</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::NegWriteMap</name>
    <filename>a00189.html</filename>
    <templarg></templarg>
    <base>MapBase&lt; M::Key, M::Value &gt;</base>
    <member kind="typedef">
      <type>M::Key</type>
      <name>Key</name>
      <anchorfile>a00189.html</anchorfile>
      <anchor>8c5e944591efa8f8f0d64cf81950abcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M::Value</type>
      <name>Value</name>
      <anchorfile>a00189.html</anchorfile>
      <anchor>7054917e50868b3b4c3d0899fc4b422e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NegWriteMap</name>
      <anchorfile>a00189.html</anchorfile>
      <anchor>ded98ea981ece293ceb6acd2909cc4c6</anchor>
      <arglist>(M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00189.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00189.html</anchorfile>
      <anchor>6ac83be911521f604e18f3df0d678213</anchor>
      <arglist>(const Key &amp;k, const Value &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>NegWriteMap&lt; M &gt;</type>
      <name>negWriteMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gb8b568a6081147ff0df4d6720095a80d</anchor>
      <arglist>(M &amp;m)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::AbsMap</name>
    <filename>a00001.html</filename>
    <templarg></templarg>
    <base>MapBase&lt; M::Key, M::Value &gt;</base>
    <member kind="typedef">
      <type>M::Key</type>
      <name>Key</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>8c5e944591efa8f8f0d64cf81950abcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>M::Value</type>
      <name>Value</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>7054917e50868b3b4c3d0899fc4b422e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AbsMap</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>878b0bf60ce4b839b2c3f08e1d3f84d0</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>AbsMap&lt; M &gt;</type>
      <name>absMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>ga3f892034fca5ba41151aebdba69e41c</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::TrueMap</name>
    <filename>a00293.html</filename>
    <templarg></templarg>
    <base>MapBase&lt; K, bool &gt;</base>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00293.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>Value</name>
      <anchorfile>a00293.html</anchorfile>
      <anchor>af2b2f1e7bba19591a125e352c5d7bda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00293.html</anchorfile>
      <anchor>a6290a1f49db2643ba24d02f87cbf429</anchor>
      <arglist>(const Key &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>TrueMap&lt; K &gt;</type>
      <name>trueMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g9346464be299b8b31ec5319144ea5302</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::FalseMap</name>
    <filename>a00113.html</filename>
    <templarg></templarg>
    <base>MapBase&lt; K, bool &gt;</base>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00113.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>Value</name>
      <anchorfile>a00113.html</anchorfile>
      <anchor>af2b2f1e7bba19591a125e352c5d7bda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00113.html</anchorfile>
      <anchor>a6290a1f49db2643ba24d02f87cbf429</anchor>
      <arglist>(const Key &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>FalseMap&lt; K &gt;</type>
      <name>falseMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g30371344e499cec60db176b5b35f6cd0</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::AndMap</name>
    <filename>a00006.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M1::Key, bool &gt;</base>
    <member kind="typedef">
      <type>M1::Key</type>
      <name>Key</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>c16c774bd2b82a45854c3ac66248de01</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>Value</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>af2b2f1e7bba19591a125e352c5d7bda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AndMap</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>87163ce7561166f71889a12898e73d6e</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>AndMap&lt; M1, M2 &gt;</type>
      <name>andMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g33f98a39433f441c03b1945d2de4403f</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::OrMap</name>
    <filename>a00205.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M1::Key, bool &gt;</base>
    <member kind="typedef">
      <type>M1::Key</type>
      <name>Key</name>
      <anchorfile>a00205.html</anchorfile>
      <anchor>c16c774bd2b82a45854c3ac66248de01</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>Value</name>
      <anchorfile>a00205.html</anchorfile>
      <anchor>af2b2f1e7bba19591a125e352c5d7bda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OrMap</name>
      <anchorfile>a00205.html</anchorfile>
      <anchor>cb8ad38827ee87a102f1dde2bf01b5be</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00205.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>OrMap&lt; M1, M2 &gt;</type>
      <name>orMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g87d0aa36f6a8a59b23f692d03376761d</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::NotMap</name>
    <filename>a00201.html</filename>
    <templarg></templarg>
    <base>MapBase&lt; M::Key, bool &gt;</base>
    <member kind="typedef">
      <type>M::Key</type>
      <name>Key</name>
      <anchorfile>a00201.html</anchorfile>
      <anchor>8c5e944591efa8f8f0d64cf81950abcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>Value</name>
      <anchorfile>a00201.html</anchorfile>
      <anchor>af2b2f1e7bba19591a125e352c5d7bda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NotMap</name>
      <anchorfile>a00201.html</anchorfile>
      <anchor>3214b2dda6a2ef1b4df34d5e18ef9684</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00201.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>NotMap&lt; M &gt;</type>
      <name>notMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g03804878900222f6828e1ee4f921a083</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::NotWriteMap</name>
    <filename>a00202.html</filename>
    <templarg></templarg>
    <base>MapBase&lt; M::Key, bool &gt;</base>
    <member kind="typedef">
      <type>M::Key</type>
      <name>Key</name>
      <anchorfile>a00202.html</anchorfile>
      <anchor>8c5e944591efa8f8f0d64cf81950abcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>Value</name>
      <anchorfile>a00202.html</anchorfile>
      <anchor>af2b2f1e7bba19591a125e352c5d7bda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NotWriteMap</name>
      <anchorfile>a00202.html</anchorfile>
      <anchor>b7b3cbfe44e63462d1977bb7557260aa</anchor>
      <arglist>(M &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00202.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00202.html</anchorfile>
      <anchor>01004ab61d918108487eb1fa915a1408</anchor>
      <arglist>(const Key &amp;k, bool v)</arglist>
    </member>
    <member kind="function">
      <type>NotWriteMap&lt; M &gt;</type>
      <name>notWriteMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g6d3efe02540c14e706214993a061111e</anchor>
      <arglist>(M &amp;m)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::EqualMap</name>
    <filename>a00104.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M1::Key, bool &gt;</base>
    <member kind="typedef">
      <type>M1::Key</type>
      <name>Key</name>
      <anchorfile>a00104.html</anchorfile>
      <anchor>c16c774bd2b82a45854c3ac66248de01</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>Value</name>
      <anchorfile>a00104.html</anchorfile>
      <anchor>af2b2f1e7bba19591a125e352c5d7bda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>EqualMap</name>
      <anchorfile>a00104.html</anchorfile>
      <anchor>f6ecf4896a47999967b6622d3f50b5b1</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00104.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>EqualMap&lt; M1, M2 &gt;</type>
      <name>equalMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>gb064d43b2f6ccb5ea5fe99ba5f53015d</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LessMap</name>
    <filename>a00161.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; M1::Key, bool &gt;</base>
    <member kind="typedef">
      <type>M1::Key</type>
      <name>Key</name>
      <anchorfile>a00161.html</anchorfile>
      <anchor>c16c774bd2b82a45854c3ac66248de01</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>Value</name>
      <anchorfile>a00161.html</anchorfile>
      <anchor>af2b2f1e7bba19591a125e352c5d7bda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LessMap</name>
      <anchorfile>a00161.html</anchorfile>
      <anchor>3992271974bb62dd5632a068a68b5ea6</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00161.html</anchorfile>
      <anchor>b9c10eb597919bcf35b4c1d1a1e23ddc</anchor>
      <arglist>(const Key &amp;k) const </arglist>
    </member>
    <member kind="function">
      <type>LessMap&lt; M1, M2 &gt;</type>
      <name>lessMap</name>
      <anchorfile>a00432.html</anchorfile>
      <anchor>g6d57040954dfe0188f02540fe8cf3319</anchor>
      <arglist>(const M1 &amp;m1, const M2 &amp;m2)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::LoggerBoolMap</name>
    <filename>a00170.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; KEY, bool &gt;</base>
    <member kind="typedef">
      <type>KEY</type>
      <name>Key</name>
      <anchorfile>a00170.html</anchorfile>
      <anchor>815df26dac2d60318c7a52fe953a5393</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>bool</type>
      <name>Value</name>
      <anchorfile>a00170.html</anchorfile>
      <anchor>af2b2f1e7bba19591a125e352c5d7bda</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>IT</type>
      <name>Iterator</name>
      <anchorfile>a00170.html</anchorfile>
      <anchor>504647bc0d34a0cb950f3a7ed5b09e96</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LoggerBoolMap</name>
      <anchorfile>a00170.html</anchorfile>
      <anchor>bd96d3b6eafdabd483815279dd66ade6</anchor>
      <arglist>(Iterator it)</arglist>
    </member>
    <member kind="function">
      <type>Iterator</type>
      <name>begin</name>
      <anchorfile>a00170.html</anchorfile>
      <anchor>83c9fd7252c97810a9aaf392e10a00d7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Iterator</type>
      <name>end</name>
      <anchorfile>a00170.html</anchorfile>
      <anchor>7c19587af55f61dbff765ec7e291e583</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00170.html</anchorfile>
      <anchor>fe66b402603ec089dc4f592643678634</anchor>
      <arglist>(const Key &amp;key, Value value)</arglist>
    </member>
    <member kind="function">
      <type>LoggerBoolMap&lt; Iterator &gt;</type>
      <name>loggerBoolMap</name>
      <anchorfile>a00430.html</anchorfile>
      <anchor>g21ca379ec2c92eccd71b76df0a9eee8c</anchor>
      <arglist>(Iterator it)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::IdMap</name>
    <filename>a00145.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <base>MapBase&lt; K, int &gt;</base>
    <class kind="class">lemon::IdMap::InverseMap</class>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00145.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>K</type>
      <name>Item</name>
      <anchorfile>a00145.html</anchorfile>
      <anchor>c4edd56c103a7549d76b3a6b5c7bdf6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00145.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>Value</name>
      <anchorfile>a00145.html</anchorfile>
      <anchor>8940a046b717a18878ab9de4ba32a0b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IdMap</name>
      <anchorfile>a00145.html</anchorfile>
      <anchor>fc1a3f0b1c5b351753dd801082e222c0</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>operator[]</name>
      <anchorfile>a00145.html</anchorfile>
      <anchor>ecef094bb1f045fa0732617f1ac3e350</anchor>
      <arglist>(const Item &amp;item) const </arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>operator()</name>
      <anchorfile>a00145.html</anchorfile>
      <anchor>50fe2e7d2c4ca7050d4c0a45e6ad3edd</anchor>
      <arglist>(int id)</arglist>
    </member>
    <member kind="function">
      <type>InverseMap</type>
      <name>inverse</name>
      <anchorfile>a00145.html</anchorfile>
      <anchor>7fa3f7683483927bd075b32ea99bb173</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::IdMap::InverseMap</name>
    <filename>a00152.html</filename>
    <member kind="function">
      <type></type>
      <name>InverseMap</name>
      <anchorfile>a00152.html</anchorfile>
      <anchor>b97b60a9dd4d5b9924459f2ba4e1d176</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InverseMap</name>
      <anchorfile>a00152.html</anchorfile>
      <anchor>6e34ce40f71eaf31a4a2afa127711e50</anchor>
      <arglist>(const IdMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>operator[]</name>
      <anchorfile>a00152.html</anchorfile>
      <anchor>650ff021ce87aa07a3a3702274b8a651</anchor>
      <arglist>(int id) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::CrossRefMap</name>
    <filename>a00072.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <class kind="class">lemon::CrossRefMap::InverseMap</class>
    <class kind="class">lemon::CrossRefMap::ValueIterator</class>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>K</type>
      <name>Item</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>c4edd56c103a7549d76b3a6b5c7bdf6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CrossRefMap</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>62edc095423badf36c020aa9c2cf3f1d</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>ValueIterator</type>
      <name>beginValue</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>c0e6dbf08c61168e04e7705ecf0e1bb6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ValueIterator</type>
      <name>endValue</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>7ba4ee59e953ac1a2c8d615b66bf36db</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>886b9abb160f57c5857d573bbd323b93</anchor>
      <arglist>(const Key &amp;key, const Value &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>MapTraits&lt; Map &gt;::ConstReturnValue</type>
      <name>operator[]</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>3b82df5471db1260dc190ea568de1c46</anchor>
      <arglist>(const Key &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>Key</type>
      <name>operator()</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>0e391b5e0260ae3fd8c1695c12428364</anchor>
      <arglist>(const Value &amp;val) const </arglist>
    </member>
    <member kind="function">
      <type>InverseMap</type>
      <name>inverse</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>7fa3f7683483927bd075b32ea99bb173</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>erase</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>c1c16351f196b9ac3a755a4974eaf304</anchor>
      <arglist>(const Key &amp;key)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>erase</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>3ad67ed0febc0f7c2ac4f9efc368c51b</anchor>
      <arglist>(const std::vector&lt; Key &gt; &amp;keys)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>clear</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>ae048282c7011eedc2e0492f6421ea73</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::CrossRefMap::InverseMap</name>
    <filename>a00153.html</filename>
    <member kind="typedef">
      <type>CrossRefMap::Key</type>
      <name>Value</name>
      <anchorfile>a00153.html</anchorfile>
      <anchor>d6328bf624a7984fe6648c70b1f4aeca</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CrossRefMap::Value</type>
      <name>Key</name>
      <anchorfile>a00153.html</anchorfile>
      <anchor>dd5ec20c5c456fcd5faee1564f378ff0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InverseMap</name>
      <anchorfile>a00153.html</anchorfile>
      <anchor>804ea26c79fa8994c79ef4b3eb615bf8</anchor>
      <arglist>(const CrossRefMap &amp;inverted)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00153.html</anchorfile>
      <anchor>dad303dedbe162e83d5c454f631ab5d7</anchor>
      <arglist>(const Key &amp;key) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::CrossRefMap::ValueIterator</name>
    <filename>a00297.html</filename>
  </compound>
  <compound kind="class">
    <name>lemon::RangeIdMap</name>
    <filename>a00219.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <class kind="class">lemon::RangeIdMap::InverseMap</class>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>K</type>
      <name>Item</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>c4edd56c103a7549d76b3a6b5c7bdf6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>Value</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>8940a046b717a18878ab9de4ba32a0b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RangeIdMap</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>5186d7da8a6e71d0dd74806b043fc3a8</anchor>
      <arglist>(const Graph &amp;gr)</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>size</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>90ca964ebcc1b02bbcde225edd49e812</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>swap</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>f90921c6ed51125c78327455d73edb8a</anchor>
      <arglist>(const Item &amp;p, const Item &amp;q)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>operator[]</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>ecef094bb1f045fa0732617f1ac3e350</anchor>
      <arglist>(const Item &amp;item) const </arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>operator()</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>fa30cb86a0024898ca9e62abf7c0ad14</anchor>
      <arglist>(int id) const </arglist>
    </member>
    <member kind="function">
      <type>const InverseMap</type>
      <name>inverse</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>7e3fc8c3c9fa5001ccb7cf6a3dcb2861</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>add</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>8f19d579f2966225d0000e559d187a38</anchor>
      <arglist>(const Item &amp;item)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>add</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>26843673979cea029c22db6d5561cb37</anchor>
      <arglist>(const std::vector&lt; Item &gt; &amp;items)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>erase</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>35bd4365da9b50a6366a56b11d5f2770</anchor>
      <arglist>(const Item &amp;item)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>erase</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>7eb1154153f7d324cc5fdb4ca09d2684</anchor>
      <arglist>(const std::vector&lt; Item &gt; &amp;items)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>build</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>9d887f6f043a4b7c3388ca7aba0b070c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>clear</name>
      <anchorfile>a00219.html</anchorfile>
      <anchor>ae048282c7011eedc2e0492f6421ea73</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::RangeIdMap::InverseMap</name>
    <filename>a00154.html</filename>
    <member kind="typedef">
      <type>RangeIdMap::Key</type>
      <name>Value</name>
      <anchorfile>a00154.html</anchorfile>
      <anchor>fe9c60964cee7f7f4930fad88d3d9c50</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>RangeIdMap::Value</type>
      <name>Key</name>
      <anchorfile>a00154.html</anchorfile>
      <anchor>f5e10bcf703e7e794c22d57f96fa15ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InverseMap</name>
      <anchorfile>a00154.html</anchorfile>
      <anchor>b6a873b35176f0e02362652d2681e164</anchor>
      <arglist>(const RangeIdMap &amp;inverted)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00154.html</anchorfile>
      <anchor>dad303dedbe162e83d5c454f631ab5d7</anchor>
      <arglist>(const Key &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>size</name>
      <anchorfile>a00154.html</anchorfile>
      <anchor>90ca964ebcc1b02bbcde225edd49e812</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SourceMap</name>
    <filename>a00277.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR::Arc</type>
      <name>Key</name>
      <anchorfile>a00277.html</anchorfile>
      <anchor>1b9ec95bf71a770498c3f39011dc7584</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GR::Node</type>
      <name>Value</name>
      <anchorfile>a00277.html</anchorfile>
      <anchor>6916336d24762bcc49ad82b62e3638ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SourceMap</name>
      <anchorfile>a00277.html</anchorfile>
      <anchor>e910d37157c386e1655004ae148ff5dd</anchor>
      <arglist>(const GR &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00277.html</anchorfile>
      <anchor>e8346d86811106ea17363c754b7d1f8c</anchor>
      <arglist>(const Key &amp;arc) const </arglist>
    </member>
    <member kind="function">
      <type>SourceMap&lt; GR &gt;</type>
      <name>sourceMap</name>
      <anchorfile>a00431.html</anchorfile>
      <anchor>gee3916bbb46530ddc62cf39e1801325c</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::TargetMap</name>
    <filename>a00285.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR::Arc</type>
      <name>Key</name>
      <anchorfile>a00285.html</anchorfile>
      <anchor>1b9ec95bf71a770498c3f39011dc7584</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GR::Node</type>
      <name>Value</name>
      <anchorfile>a00285.html</anchorfile>
      <anchor>6916336d24762bcc49ad82b62e3638ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>TargetMap</name>
      <anchorfile>a00285.html</anchorfile>
      <anchor>231a3ef4b3f4db426e0068d99ef04031</anchor>
      <arglist>(const GR &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00285.html</anchorfile>
      <anchor>4306661a8ab05b3cc91765703011e604</anchor>
      <arglist>(const Key &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>TargetMap&lt; GR &gt;</type>
      <name>targetMap</name>
      <anchorfile>a00431.html</anchorfile>
      <anchor>g96e38231848a32a3a8ec5626b2e68b3c</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ForwardMap</name>
    <filename>a00119.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>ForwardMap</name>
      <anchorfile>a00119.html</anchorfile>
      <anchor>ca82dbed2eb0ffb8552c017000ba1512</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00119.html</anchorfile>
      <anchor>dad303dedbe162e83d5c454f631ab5d7</anchor>
      <arglist>(const Key &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>ForwardMap&lt; GR &gt;</type>
      <name>forwardMap</name>
      <anchorfile>a00431.html</anchorfile>
      <anchor>g9b42700e1bf75a2d81827c5d8e63cafb</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::BackwardMap</name>
    <filename>a00022.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>BackwardMap</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>2269f2ca0a7263cdbf0c9f471533212c</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>dad303dedbe162e83d5c454f631ab5d7</anchor>
      <arglist>(const Key &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>BackwardMap&lt; GR &gt;</type>
      <name>backwardMap</name>
      <anchorfile>a00431.html</anchorfile>
      <anchor>g6e0ca8c5fa9c4bfb97a0d1bc8296fd54</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::InDegMap</name>
    <filename>a00149.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00149.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::Node</type>
      <name>Key</name>
      <anchorfile>a00149.html</anchorfile>
      <anchor>0835dd1b238d00fbb4cea5cd42df661f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>Value</name>
      <anchorfile>a00149.html</anchorfile>
      <anchor>8940a046b717a18878ab9de4ba32a0b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InDegMap</name>
      <anchorfile>a00149.html</anchorfile>
      <anchor>2ef38f21a0b2ac5818982bec1f8686a9</anchor>
      <arglist>(const Digraph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>operator[]</name>
      <anchorfile>a00149.html</anchorfile>
      <anchor>7622385a3ef0fd9027b08a6897523df8</anchor>
      <arglist>(const Key &amp;key) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::OutDegMap</name>
    <filename>a00208.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00208.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::Node</type>
      <name>Key</name>
      <anchorfile>a00208.html</anchorfile>
      <anchor>0835dd1b238d00fbb4cea5cd42df661f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>Value</name>
      <anchorfile>a00208.html</anchorfile>
      <anchor>8940a046b717a18878ab9de4ba32a0b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OutDegMap</name>
      <anchorfile>a00208.html</anchorfile>
      <anchor>c231c2cc4864b04ef7f6ba9da5b4d2a6</anchor>
      <arglist>(const Digraph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>operator[]</name>
      <anchorfile>a00208.html</anchorfile>
      <anchor>7622385a3ef0fd9027b08a6897523df8</anchor>
      <arglist>(const Key &amp;key) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::PotentialDifferenceMap</name>
    <filename>a00215.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR::Arc</type>
      <name>Key</name>
      <anchorfile>a00215.html</anchorfile>
      <anchor>1b9ec95bf71a770498c3f39011dc7584</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>POT::Value</type>
      <name>Value</name>
      <anchorfile>a00215.html</anchorfile>
      <anchor>af7ce360ed8d2e75550a849ba1f1d6f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PotentialDifferenceMap</name>
      <anchorfile>a00215.html</anchorfile>
      <anchor>626fe962c4c170ddc2df7c15f3e23101</anchor>
      <arglist>(const GR &amp;gr, const POT &amp;potential)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00215.html</anchorfile>
      <anchor>e8346d86811106ea17363c754b7d1f8c</anchor>
      <arglist>(const Key &amp;arc) const </arglist>
    </member>
    <member kind="function">
      <type>PotentialDifferenceMap&lt; GR, POT &gt;</type>
      <name>potentialDifferenceMap</name>
      <anchorfile>a00431.html</anchorfile>
      <anchor>g06e9797d62087031a2a1baedefafbadf</anchor>
      <arglist>(const GR &amp;gr, const POT &amp;potential)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MaxMatching</name>
    <filename>a00178.html</filename>
    <templarg></templarg>
    <member kind="enumeration">
      <name>Status</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>67a0db04d321a74b7e7fcfd3f1a3f70b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>EVEN</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>67a0db04d321a74b7e7fcfd3f1a3f70b8487756fbc720579906f0ae1738f0fcc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MATCHED</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>67a0db04d321a74b7e7fcfd3f1a3f70bfa51dda37faacd52f548681cd05d4e31</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ODD</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>67a0db04d321a74b7e7fcfd3f1a3f70ba29cedab858353a26006af9db7cd1ed8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UNMATCHED</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>67a0db04d321a74b7e7fcfd3f1a3f70b1281ac5b7ba5b42a08b8e2dfee5ec74c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Graph::template NodeMap&lt; typename Graph::Arc &gt;</type>
      <name>MatchingMap</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>5e14c7d4f5b49a6771fef21bece0139c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Graph::template NodeMap&lt; Status &gt;</type>
      <name>StatusMap</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>aa57c1a28da6573c042ce3650072e142</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MaxMatching</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>b3291930ed44de4eebbb080515043759</anchor>
      <arglist>(const Graph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>greedyInit</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>275cfb5a0d9eeb69e56ec2366e0a4f81</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>matchingInit</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>290115e24e97bb3877edd0adbb175c2a</anchor>
      <arglist>(const MatchingMap &amp;matching)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>startSparse</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>772e8152b771c82330948686df4f9a6c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>startDense</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>4965a741e7b39a4efd41965bfa67f6f1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>matchingSize</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>8be6dc29de917e74b2fcf7d452ec033d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>matching</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>bf1eb92cb4e4d10b12e48cf3ff6b3376</anchor>
      <arglist>(const Edge &amp;edge) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>matching</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>6540aef98681e1c6008a4ebb73ed9fca</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>const MatchingMap &amp;</type>
      <name>matchingMap</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>e64828409d8b38c61c9d134d3f139390</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>mate</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>bf77e4d0ed0d04d110752c1bfe472361</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>status</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>aa9d7af7830feff1d4609e3a964da2a0</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>const StatusMap &amp;</type>
      <name>statusMap</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>6cbfccb6bec81c99c0ffd7aae9d12da4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>barrier</name>
      <anchorfile>a00178.html</anchorfile>
      <anchor>8541a5e8f7b43d29302687c5cca13259</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MaxWeightedMatching</name>
    <filename>a00179.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <class kind="class">lemon::MaxWeightedMatching::BlossomIt</class>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>WM</type>
      <name>WeightMap</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>8cc0487bc8ca8ef9b236b960bc0b9b81</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>WeightMap::Value</type>
      <name>Value</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>db23f34f07ad6e93c0e6fba266a31c0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Graph::template NodeMap&lt; typename Graph::Arc &gt;</type>
      <name>MatchingMap</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>5e14c7d4f5b49a6771fef21bece0139c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MaxWeightedMatching</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>452a6b17d04688ef7953307630053964</anchor>
      <arglist>(const Graph &amp;graph, const WeightMap &amp;weight)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>60de64d75454385b23995437f1d72669</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>matchingWeight</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>12587b4fe2da66ed88589d45c935f2fd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>matchingSize</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>8be6dc29de917e74b2fcf7d452ec033d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>matching</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>bf1eb92cb4e4d10b12e48cf3ff6b3376</anchor>
      <arglist>(const Edge &amp;edge) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>matching</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>20e6a43cb0c6c60ec37d39c627b8b662</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>const MatchingMap &amp;</type>
      <name>matchingMap</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>e64828409d8b38c61c9d134d3f139390</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>mate</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>2c22f3aacc61db6b0ced1bc14357cc28</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>dualValue</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>f25ac7031ef96fbd26792f44d764fcb1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>nodeValue</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>c8e57fc9bc7adf2aaabf3cc01c1cad3d</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>blossomNum</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>697582da59c33715b80a8ce1c3d4317a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>blossomSize</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>b55a362f19866affc286ad2217f9e45e</anchor>
      <arglist>(int k) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>blossomValue</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>c555ec5c478ee2f5aa1ed5901535f430</anchor>
      <arglist>(int k) const </arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>dualScale</name>
      <anchorfile>a00179.html</anchorfile>
      <anchor>eb1562496ec67b935620a87b899897bd</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MaxWeightedMatching::BlossomIt</name>
    <filename>a00034.html</filename>
    <member kind="function">
      <type></type>
      <name>BlossomIt</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>3dd505f88ec29604988016120d67394f</anchor>
      <arglist>(const MaxWeightedMatching &amp;algorithm, int variable)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Node</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>842a9fc933bc21c81d8d2790744a19b3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>BlossomIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>ba2eb057ccea67a09607a0f92cdb0895</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>9569625b6759eac56d4b4294aba9e5b2</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>ab38b74c42b0f2581ed4917fca608efc</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MaxWeightedPerfectMatching</name>
    <filename>a00180.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <class kind="class">lemon::MaxWeightedPerfectMatching::BlossomIt</class>
    <member kind="typedef">
      <type>GR</type>
      <name>Graph</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>2a51ae337b207f01f1c904f5eb2aa98a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>WM</type>
      <name>WeightMap</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>8cc0487bc8ca8ef9b236b960bc0b9b81</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>WeightMap::Value</type>
      <name>Value</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>db23f34f07ad6e93c0e6fba266a31c0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Graph::template NodeMap&lt; typename Graph::Arc &gt;</type>
      <name>MatchingMap</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>5e14c7d4f5b49a6771fef21bece0139c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MaxWeightedPerfectMatching</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>a7a5d4dac99e37dca9ad075f287cbd82</anchor>
      <arglist>(const Graph &amp;graph, const WeightMap &amp;weight)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>start</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>ad5997aaaa2d622f0ca57f8b24a51a7b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>run</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>149ad6701e3e2414cb566bb414029841</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>matchingWeight</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>12587b4fe2da66ed88589d45c935f2fd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>matching</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>bf1eb92cb4e4d10b12e48cf3ff6b3376</anchor>
      <arglist>(const Edge &amp;edge) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>matching</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>20e6a43cb0c6c60ec37d39c627b8b662</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>const MatchingMap &amp;</type>
      <name>matchingMap</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>e64828409d8b38c61c9d134d3f139390</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>mate</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>2c22f3aacc61db6b0ced1bc14357cc28</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>dualValue</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>f25ac7031ef96fbd26792f44d764fcb1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>nodeValue</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>c8e57fc9bc7adf2aaabf3cc01c1cad3d</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>blossomNum</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>697582da59c33715b80a8ce1c3d4317a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>blossomSize</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>b55a362f19866affc286ad2217f9e45e</anchor>
      <arglist>(int k) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>blossomValue</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>c555ec5c478ee2f5aa1ed5901535f430</anchor>
      <arglist>(int k) const </arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>dualScale</name>
      <anchorfile>a00180.html</anchorfile>
      <anchor>eb1562496ec67b935620a87b899897bd</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MaxWeightedPerfectMatching::BlossomIt</name>
    <filename>a00035.html</filename>
    <member kind="function">
      <type></type>
      <name>BlossomIt</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>49d55c2465fffab373ffc96db71ca616</anchor>
      <arglist>(const MaxWeightedPerfectMatching &amp;algorithm, int variable)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Node</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>842a9fc933bc21c81d8d2790744a19b3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>BlossomIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>ba2eb057ccea67a09607a0f92cdb0895</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>9569625b6759eac56d4b4294aba9e5b2</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>ab38b74c42b0f2581ed4917fca608efc</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::MinCostArborescenceDefaultTraits</name>
    <filename>a00182.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00182.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CM</type>
      <name>CostMap</name>
      <anchorfile>a00182.html</anchorfile>
      <anchor>0e4fe3b3bd126f464294f661c36a61d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CostMap::Value</type>
      <name>Value</name>
      <anchorfile>a00182.html</anchorfile>
      <anchor>27a2ae8fda0566f686c0dda8668521e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template ArcMap&lt; bool &gt;</type>
      <name>ArborescenceMap</name>
      <anchorfile>a00182.html</anchorfile>
      <anchor>e8f0298807da399a43d45454dfffa284</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template NodeMap&lt; typename Digraph::Arc &gt;</type>
      <name>PredMap</name>
      <anchorfile>a00182.html</anchorfile>
      <anchor>2f311e3024ce1a50c00ffc291bce8e85</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ArborescenceMap *</type>
      <name>createArborescenceMap</name>
      <anchorfile>a00182.html</anchorfile>
      <anchor>8f0cf18871b7513ccab683de17ca12eb</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static PredMap *</type>
      <name>createPredMap</name>
      <anchorfile>a00182.html</anchorfile>
      <anchor>5a1853d9d7d8464c6e4215b441f12496</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MinCostArborescence</name>
    <filename>a00181.html</filename>
    <templarg>GR</templarg>
    <templarg>CM</templarg>
    <templarg>TR</templarg>
    <class kind="class">lemon::MinCostArborescence::DualIt</class>
    <class kind="struct">lemon::MinCostArborescence::SetArborescenceMap</class>
    <class kind="struct">lemon::MinCostArborescence::SetPredMap</class>
    <member kind="typedef">
      <type>TR</type>
      <name>Traits</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>b157e5452122f7a7f73dfda5ed931d69</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Digraph</type>
      <name>Digraph</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>a6928feef02be4f2a184775d19dc6373</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::CostMap</type>
      <name>CostMap</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>4394ecf7ca38a61b918f4e4ff3d3c9c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Value</type>
      <name>Value</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>1c7478783a00413767196fd8d82ad8fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::PredMap</type>
      <name>PredMap</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>d1a405d28d1b2733ec664409307be04f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::ArborescenceMap</type>
      <name>ArborescenceMap</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>a1d4b4fc0b1e1e05a9aa150eb1471a9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MinCostArborescence</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>7e8813f46384642dc4348622a966461a</anchor>
      <arglist>(const Digraph &amp;digraph, const CostMap &amp;cost)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~MinCostArborescence</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>322a9f09bc8ca050537f92270b93957b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>MinCostArborescence &amp;</type>
      <name>arborescenceMap</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>4ca9693e035b544f115fd28740ff9b32</anchor>
      <arglist>(ArborescenceMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>MinCostArborescence &amp;</type>
      <name>predMap</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>3419e79c5d19a90e3314c35c9e098515</anchor>
      <arglist>(PredMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addSource</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>8208ca7e6b5405b60a9ae85a6f6394d4</anchor>
      <arglist>(Node source)</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>processNextNode</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>dba758047d7378b8a06320d29ce170d7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>queueSize</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>942d30059e28f60ba6dd1944ab8e416e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>emptyQueue</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>b6dcd2be02feaff0a95c21824e805445</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>60de64d75454385b23995437f1d72669</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>e3f0682c90a4e8a23c259943e899402e</anchor>
      <arglist>(Node s)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>arborescenceCost</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>44ff8fc36ab4b216075492f99c8219ea</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>arborescence</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>88d0e574ade6f15fb5ed7053b9921f18</anchor>
      <arglist>(Arc arc) const </arglist>
    </member>
    <member kind="function">
      <type>const ArborescenceMap &amp;</type>
      <name>arborescenceMap</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>b46fe0e79ac4ffc1910aad8ced0c9381</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>pred</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>0981af70b09fd000fedb2e967a43c457</anchor>
      <arglist>(Node node) const </arglist>
    </member>
    <member kind="function">
      <type>const PredMap &amp;</type>
      <name>predMap</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>eec5920cc073b88c4c5f6c46a8f4aa4b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>reached</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>063160dd2c130caeda302ce654df168e</anchor>
      <arglist>(Node node) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>processed</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>ac3e5dbaa1cbbd0445323268dcd22fa6</anchor>
      <arglist>(Node node) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>dualNum</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>0924c41e37c01605ef7ef3df9d39f6f9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>dualValue</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>f25ac7031ef96fbd26792f44d764fcb1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>dualSize</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>09601d9525ed3626569f5726ff1b52e3</anchor>
      <arglist>(int k) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>dualValue</name>
      <anchorfile>a00181.html</anchorfile>
      <anchor>a7f2ad412ee56aefc61aa99a7c917208</anchor>
      <arglist>(int k) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::MinCostArborescence::DualIt</name>
    <filename>a00096.html</filename>
    <member kind="function">
      <type></type>
      <name>DualIt</name>
      <anchorfile>a00096.html</anchorfile>
      <anchor>52f8825715c02cfb462e404479ee9378</anchor>
      <arglist>(const MinCostArborescence &amp;algorithm, int variable)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Node</name>
      <anchorfile>a00096.html</anchorfile>
      <anchor>842a9fc933bc21c81d8d2790744a19b3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>DualIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00096.html</anchorfile>
      <anchor>0a697e264a03d65ee9d17e36b9c94d17</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00096.html</anchorfile>
      <anchor>9569625b6759eac56d4b4294aba9e5b2</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00096.html</anchorfile>
      <anchor>ab38b74c42b0f2581ed4917fca608efc</anchor>
      <arglist>(Invalid) const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::MinCostArborescence::SetArborescenceMap</name>
    <filename>a00236.html</filename>
    <templarg></templarg>
    <base>MinCostArborescence&lt; Digraph, CostMap, SetArborescenceMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::MinCostArborescence::SetPredMap</name>
    <filename>a00248.html</filename>
    <templarg></templarg>
    <base>MinCostArborescence&lt; Digraph, CostMap, SetPredMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="class">
    <name>lemon::NetworkSimplex</name>
    <filename>a00190.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="enumeration">
      <name>ProblemType</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>INFEASIBLE</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e2884fa43446c0cbc9c7a9b74d41d7483</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>OPTIMAL</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e2579881e7c83261bc21bafb5a5c92cad</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UNBOUNDED</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>4c669cb1cb4d98dfea944e9ceec7d33e6c65123d1b5b01632a477661055b01ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>SupplyType</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>c860a45e09c68fb71f723d392c3161ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>GEQ</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>c860a45e09c68fb71f723d392c3161ac99705e9593e3e5c078150b293c86561e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>LEQ</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>c860a45e09c68fb71f723d392c3161ac5eba1d52e68e10fb2dafcb363cec49b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>PivotRule</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>310d3a1fcfff1e099330ae372abc73c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FIRST_ELIGIBLE</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>310d3a1fcfff1e099330ae372abc73c27c31211b575a3beecd934538c4419b25</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>BEST_ELIGIBLE</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>310d3a1fcfff1e099330ae372abc73c2c89b3377964fb28026f9b814f30c210b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>BLOCK_SEARCH</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>310d3a1fcfff1e099330ae372abc73c2fa32a0f9c82dc3e2e1000b6086d3fc1c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CANDIDATE_LIST</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>310d3a1fcfff1e099330ae372abc73c28d608c7bc486d1c56c7e5a5a6c6a616d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ALTERING_LIST</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>310d3a1fcfff1e099330ae372abc73c276eceb36ac871a19d694999a965f877a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>C</type>
      <name>Cost</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>deb81a88e6bbaf933ca20ea3bbba7a2c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NetworkSimplex</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>652f6cd1dab2f56e8a0a29460e8e4fd4</anchor>
      <arglist>(const GR &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>NetworkSimplex &amp;</type>
      <name>lowerMap</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>46572604302ad5db507870c2b11ab004</anchor>
      <arglist>(const LowerMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>NetworkSimplex &amp;</type>
      <name>upperMap</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>ce99ce94ec9eaa2f7e329d3547a5cb4e</anchor>
      <arglist>(const UpperMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>NetworkSimplex &amp;</type>
      <name>costMap</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>f171423e38437c26404bd1f2ece0c83c</anchor>
      <arglist>(const CostMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>NetworkSimplex &amp;</type>
      <name>supplyMap</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>524e7594df8980eb0edcca44a2625769</anchor>
      <arglist>(const SupplyMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>NetworkSimplex &amp;</type>
      <name>stSupply</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>c622127c2be52342b9becb61ff28e56e</anchor>
      <arglist>(const Node &amp;s, const Node &amp;t, Value k)</arglist>
    </member>
    <member kind="function">
      <type>NetworkSimplex &amp;</type>
      <name>supplyType</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>40c06e412dc08e6a541abbcf97385b1e</anchor>
      <arglist>(SupplyType supply_type)</arglist>
    </member>
    <member kind="function">
      <type>ProblemType</type>
      <name>run</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>591276ea8f1afbe9d24bd1c1d48b0f53</anchor>
      <arglist>(PivotRule pivot_rule=BLOCK_SEARCH)</arglist>
    </member>
    <member kind="function">
      <type>NetworkSimplex &amp;</type>
      <name>reset</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>88086127469093e19a9a024bbf60c360</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Number</type>
      <name>totalCost</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>53bae6343367680cea8a37c2f6cd17b3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>flow</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>7fc53c0667fdfb95dadd6a302f045941</anchor>
      <arglist>(const Arc &amp;a) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>flowMap</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>0bacd75ff1778bdc0a2c3ab0d0d00f36</anchor>
      <arglist>(FlowMap &amp;map) const </arglist>
    </member>
    <member kind="function">
      <type>Cost</type>
      <name>potential</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>c97efaa2b500bb002f116bf4ba7c9b0b</anchor>
      <arglist>(const Node &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>potentialMap</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>5011146c7986300cf2a684fb7aa8419a</anchor>
      <arglist>(PotentialMap &amp;map) const </arglist>
    </member>
    <member kind="variable">
      <type>const Value</type>
      <name>INF</name>
      <anchorfile>a00190.html</anchorfile>
      <anchor>601ffd79f13610daacadd46f973507c5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Path</name>
    <filename>a00211.html</filename>
    <templarg></templarg>
    <class kind="class">lemon::Path::ArcIt</class>
    <member kind="function">
      <type></type>
      <name>Path</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>aa44fef284bec9041f7eb22b921c6174</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Path</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>f5bfe56b948c1ac8261d41052bd459ec</anchor>
      <arglist>(const CPath &amp;cpath)</arglist>
    </member>
    <member kind="function">
      <type>Path &amp;</type>
      <name>operator=</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>078906b734c1e9874043bc142341f63f</anchor>
      <arglist>(const CPath &amp;cpath)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>length</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>57b988236ee6a3a5e572d126d3fbccc1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>empty</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>c6e61de369e994009e36f344f99c15ad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>nth</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>40bd5d3f5c1200b7430a09569320c0e6</anchor>
      <arglist>(int n) const </arglist>
    </member>
    <member kind="function">
      <type>ArcIt</type>
      <name>nthIt</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>7aef22363af219db2fb2136423dfd207</anchor>
      <arglist>(int n) const </arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>front</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>cce4f891e39fef226fc1322298f7dded</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addFront</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>3c4a7c086d4884b898681043173b2152</anchor>
      <arglist>(const Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>eraseFront</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>320672c73f9043f74530d5b2e65d5f6e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>back</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>cd3438da82c46a6162afdbe68d4004fa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addBack</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>7de8be578587027ac5fda4e89a016f4d</anchor>
      <arglist>(const Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>eraseBack</name>
      <anchorfile>a00211.html</anchorfile>
      <anchor>8a5e7d5f5c01f29344ec6d80d3a38e4d</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Path::ArcIt</name>
    <filename>a00014.html</filename>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>19f73b24f93bc2400778a66262f4fc78</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>3bde563f0bba0fc91cc7255fed9b682c</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>cb2907ea0f330b931889617b60cf7098</anchor>
      <arglist>(const Path &amp;_path)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator const Arc &amp;</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>5b3207456ade9e2a3bd54b8fcad46874</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a8838b5e6d2c1404b0b07dfc23ff9564</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>b99496b83b0eb7c521e2e2870e672f19</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>1504a913237c9d8b6cc779d93b0b4595</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>4f5ef730c1d98bdf5beb2442f2137946</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SimplePath</name>
    <filename>a00265.html</filename>
    <templarg>GR</templarg>
    <class kind="class">lemon::SimplePath::ArcIt</class>
    <member kind="function">
      <type></type>
      <name>SimplePath</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>d1c463cc944f26b376fd4531e6ffafef</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SimplePath</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>1e1f1f7d6adc139966b8729b1a792dfd</anchor>
      <arglist>(const CPath &amp;cpath)</arglist>
    </member>
    <member kind="function">
      <type>SimplePath &amp;</type>
      <name>operator=</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>959e1d2fdcc41a54bb22c8d99a764980</anchor>
      <arglist>(const CPath &amp;cpath)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>length</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>57b988236ee6a3a5e572d126d3fbccc1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>empty</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>c6e61de369e994009e36f344f99c15ad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>nth</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>40bd5d3f5c1200b7430a09569320c0e6</anchor>
      <arglist>(int n) const </arglist>
    </member>
    <member kind="function">
      <type>ArcIt</type>
      <name>nthIt</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>7aef22363af219db2fb2136423dfd207</anchor>
      <arglist>(int n) const </arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>front</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>cce4f891e39fef226fc1322298f7dded</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>back</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>cd3438da82c46a6162afdbe68d4004fa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addBack</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>7de8be578587027ac5fda4e89a016f4d</anchor>
      <arglist>(const Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>eraseBack</name>
      <anchorfile>a00265.html</anchorfile>
      <anchor>8a5e7d5f5c01f29344ec6d80d3a38e4d</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SimplePath::ArcIt</name>
    <filename>a00013.html</filename>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>19f73b24f93bc2400778a66262f4fc78</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>3bde563f0bba0fc91cc7255fed9b682c</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>d9dcb25456b012d997873a88812f6a4a</anchor>
      <arglist>(const SimplePath &amp;_path)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator const Arc &amp;</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>5b3207456ade9e2a3bd54b8fcad46874</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>a8838b5e6d2c1404b0b07dfc23ff9564</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>b99496b83b0eb7c521e2e2870e672f19</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>1504a913237c9d8b6cc779d93b0b4595</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>4f5ef730c1d98bdf5beb2442f2137946</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>d48a60279e86c5c6895cf18ae9e207b7</anchor>
      <arglist>(const SimplePath &amp;_path, int _idx)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ListPath</name>
    <filename>a00169.html</filename>
    <templarg></templarg>
    <class kind="class">lemon::ListPath::ArcIt</class>
    <member kind="function">
      <type></type>
      <name>ListPath</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>fb2e858c85748c945eb07160709a2085</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ListPath</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>de81b1198bd5c2134e42ca1ebd9ce743</anchor>
      <arglist>(const CPath &amp;cpath)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~ListPath</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>bcdd5fb0921608bca68617f2347759c3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>ListPath &amp;</type>
      <name>operator=</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>ad7aa28f264beb63ef27afc12565ad26</anchor>
      <arglist>(const CPath &amp;cpath)</arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>nth</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>40bd5d3f5c1200b7430a09569320c0e6</anchor>
      <arglist>(int n) const </arglist>
    </member>
    <member kind="function">
      <type>ArcIt</type>
      <name>nthIt</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>7aef22363af219db2fb2136423dfd207</anchor>
      <arglist>(int n) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>length</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>57b988236ee6a3a5e572d126d3fbccc1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>empty</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>c6e61de369e994009e36f344f99c15ad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>front</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>cce4f891e39fef226fc1322298f7dded</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addFront</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>3c4a7c086d4884b898681043173b2152</anchor>
      <arglist>(const Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>eraseFront</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>320672c73f9043f74530d5b2e65d5f6e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>back</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>cd3438da82c46a6162afdbe68d4004fa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addBack</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>7de8be578587027ac5fda4e89a016f4d</anchor>
      <arglist>(const Arc &amp;arc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>eraseBack</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>8a5e7d5f5c01f29344ec6d80d3a38e4d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spliceBack</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>8681523079be0663aa35658c6047069e</anchor>
      <arglist>(ListPath &amp;tpath)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spliceFront</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>2509b0749adb05475436e999f02604c2</anchor>
      <arglist>(ListPath &amp;tpath)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>splice</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>8e76221f3637b0f805fd2096334b228d</anchor>
      <arglist>(ArcIt it, ListPath &amp;tpath)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>split</name>
      <anchorfile>a00169.html</anchorfile>
      <anchor>c2b6f1c082fbba008bcb01d02b841dfd</anchor>
      <arglist>(ArcIt it, ListPath &amp;tpath)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ListPath::ArcIt</name>
    <filename>a00015.html</filename>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>19f73b24f93bc2400778a66262f4fc78</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>3bde563f0bba0fc91cc7255fed9b682c</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>c74dc943db48800993e5564636657fc7</anchor>
      <arglist>(const ListPath &amp;_path)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator const Arc &amp;</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>5b3207456ade9e2a3bd54b8fcad46874</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a8838b5e6d2c1404b0b07dfc23ff9564</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>b99496b83b0eb7c521e2e2870e672f19</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>1504a913237c9d8b6cc779d93b0b4595</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>4f5ef730c1d98bdf5beb2442f2137946</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::StaticPath</name>
    <filename>a00280.html</filename>
    <templarg></templarg>
    <class kind="class">lemon::StaticPath::ArcIt</class>
    <member kind="function">
      <type></type>
      <name>StaticPath</name>
      <anchorfile>a00280.html</anchorfile>
      <anchor>64b9be3c43f4d02196741ece4eb8338f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>StaticPath</name>
      <anchorfile>a00280.html</anchorfile>
      <anchor>7099114c41181b5b8271c147a03477a1</anchor>
      <arglist>(const CPath &amp;cpath)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~StaticPath</name>
      <anchorfile>a00280.html</anchorfile>
      <anchor>0843a4a3667350525ac3758cd357937e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>StaticPath &amp;</type>
      <name>operator=</name>
      <anchorfile>a00280.html</anchorfile>
      <anchor>fc7df2e7c12af0c6d2af5910916098ab</anchor>
      <arglist>(const CPath &amp;cpath)</arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>nth</name>
      <anchorfile>a00280.html</anchorfile>
      <anchor>40bd5d3f5c1200b7430a09569320c0e6</anchor>
      <arglist>(int n) const </arglist>
    </member>
    <member kind="function">
      <type>ArcIt</type>
      <name>nthIt</name>
      <anchorfile>a00280.html</anchorfile>
      <anchor>7aef22363af219db2fb2136423dfd207</anchor>
      <arglist>(int n) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>length</name>
      <anchorfile>a00280.html</anchorfile>
      <anchor>57b988236ee6a3a5e572d126d3fbccc1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>empty</name>
      <anchorfile>a00280.html</anchorfile>
      <anchor>84fa9c43bfdea9c745c5cb66aa856368</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00280.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>front</name>
      <anchorfile>a00280.html</anchorfile>
      <anchor>cce4f891e39fef226fc1322298f7dded</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const Arc &amp;</type>
      <name>back</name>
      <anchorfile>a00280.html</anchorfile>
      <anchor>cd3438da82c46a6162afdbe68d4004fa</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::StaticPath::ArcIt</name>
    <filename>a00016.html</filename>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>19f73b24f93bc2400778a66262f4fc78</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>3bde563f0bba0fc91cc7255fed9b682c</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>ca18f95e8af73633930dd81159a02392</anchor>
      <arglist>(const StaticPath &amp;_path)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator const Arc &amp;</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>5b3207456ade9e2a3bd54b8fcad46874</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>a8838b5e6d2c1404b0b07dfc23ff9564</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>b99496b83b0eb7c521e2e2870e672f19</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>1504a913237c9d8b6cc779d93b0b4595</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>4f5ef730c1d98bdf5beb2442f2137946</anchor>
      <arglist>(const ArcIt &amp;e) const </arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>e8a75a9d6f59688d3819d1575f575269</anchor>
      <arglist>(const StaticPath &amp;_path, int _idx)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::PathNodeIt</name>
    <filename>a00213.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>PathNodeIt</name>
      <anchorfile>a00213.html</anchorfile>
      <anchor>cec47d873e115073fc9745d186ff012f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PathNodeIt</name>
      <anchorfile>a00213.html</anchorfile>
      <anchor>6fb8b1bcfaec0fc9824717bf51e01acc</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PathNodeIt</name>
      <anchorfile>a00213.html</anchorfile>
      <anchor>0885125d0105b43832ff3b92dee6ff16</anchor>
      <arglist>(const Digraph &amp;digraph, const Path &amp;path)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PathNodeIt</name>
      <anchorfile>a00213.html</anchorfile>
      <anchor>cf9a6360e9a9397a59a790ce851e2c18</anchor>
      <arglist>(const Digraph &amp;digraph, const Path &amp;path, const Node &amp;src)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Node</name>
      <anchorfile>a00213.html</anchorfile>
      <anchor>842a9fc933bc21c81d8d2790744a19b3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>PathNodeIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00213.html</anchorfile>
      <anchor>ca942aedc1d9d4f5c5c2518b39dea1c1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00213.html</anchorfile>
      <anchor>8574b3e61bad0a75f0935e201bbba2df</anchor>
      <arglist>(const PathNodeIt &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00213.html</anchorfile>
      <anchor>fcd5b2e7fd3fd9791e91f52f61d15ba1</anchor>
      <arglist>(const PathNodeIt &amp;n) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00213.html</anchorfile>
      <anchor>46534a0c3727c836ee9d683162efbac3</anchor>
      <arglist>(const PathNodeIt &amp;n) const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::PreflowDefaultTraits</name>
    <filename>a00217.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00217.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CAP</type>
      <name>CapacityMap</name>
      <anchorfile>a00217.html</anchorfile>
      <anchor>10f68c1f869f7e1be967acfbd9750290</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CapacityMap::Value</type>
      <name>Value</name>
      <anchorfile>a00217.html</anchorfile>
      <anchor>f751c878eda966b90fa4fdf5f1915eff</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::template ArcMap&lt; Value &gt;</type>
      <name>FlowMap</name>
      <anchorfile>a00217.html</anchorfile>
      <anchor>438199065fa5e338294d53c559bb957b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>LinkedElevator&lt; Digraph, typename Digraph::Node &gt;</type>
      <name>Elevator</name>
      <anchorfile>a00217.html</anchorfile>
      <anchor>a6c09e8a486968dae9271e39da3531e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>lemon::Tolerance&lt; Value &gt;</type>
      <name>Tolerance</name>
      <anchorfile>a00217.html</anchorfile>
      <anchor>c8d81c6484d646b8881c72707f2527d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static FlowMap *</type>
      <name>createFlowMap</name>
      <anchorfile>a00217.html</anchorfile>
      <anchor>fd79e520abbe90ea86b8013071afb57b</anchor>
      <arglist>(const Digraph &amp;digraph)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Elevator *</type>
      <name>createElevator</name>
      <anchorfile>a00217.html</anchorfile>
      <anchor>899dadca634616cbf6500efbc71610e5</anchor>
      <arglist>(const Digraph &amp;digraph, int max_level)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Preflow</name>
    <filename>a00216.html</filename>
    <templarg>GR</templarg>
    <templarg>CAP</templarg>
    <templarg>TR</templarg>
    <class kind="struct">lemon::Preflow::SetElevator</class>
    <class kind="struct">lemon::Preflow::SetFlowMap</class>
    <class kind="struct">lemon::Preflow::SetStandardElevator</class>
    <member kind="typedef">
      <type>TR</type>
      <name>Traits</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>b157e5452122f7a7f73dfda5ed931d69</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Digraph</type>
      <name>Digraph</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>a6928feef02be4f2a184775d19dc6373</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::CapacityMap</type>
      <name>CapacityMap</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>9eeec06f304c4b0e7ec59d26ea8698a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Value</type>
      <name>Value</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>1c7478783a00413767196fd8d82ad8fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::FlowMap</type>
      <name>FlowMap</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>cbaaf29d0c8168790ab0da45dad92c62</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Elevator</type>
      <name>Elevator</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>078a395cce67cc7938d85be25aa74718</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Traits::Tolerance</type>
      <name>Tolerance</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>926a96c583959d256c1316a2aca3ce22</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Preflow</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>bb9534957b62032a1e5dea5e3ccad75e</anchor>
      <arglist>(const Digraph &amp;digraph, const CapacityMap &amp;capacity, Node source, Node target)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Preflow</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>ae9e836f55f8d8db0bc72c67e17ef572</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Preflow &amp;</type>
      <name>capacityMap</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>9b7ca5903950236a26e5fa32593e505f</anchor>
      <arglist>(const CapacityMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>Preflow &amp;</type>
      <name>flowMap</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>3434242e32c2a3e201e6d43638ff6177</anchor>
      <arglist>(FlowMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>Preflow &amp;</type>
      <name>source</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>0d6e4c5d67ec721cbb7362f5dab94106</anchor>
      <arglist>(const Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>Preflow &amp;</type>
      <name>target</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>14481b7125441edac1f572b9931ec444</anchor>
      <arglist>(const Node &amp;node)</arglist>
    </member>
    <member kind="function">
      <type>Preflow &amp;</type>
      <name>elevator</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>cc28831f6c4aef33f7c47a30bbb093cd</anchor>
      <arglist>(Elevator &amp;elevator)</arglist>
    </member>
    <member kind="function">
      <type>const Elevator &amp;</type>
      <name>elevator</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>5dc9bfc4e3f3def6bd3daee725ac9d9a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Preflow &amp;</type>
      <name>tolerance</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>967e0e131fa859a777927c55ac3ef35b</anchor>
      <arglist>(const Tolerance &amp;tolerance)</arglist>
    </member>
    <member kind="function">
      <type>const Tolerance &amp;</type>
      <name>tolerance</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>0fe880b3588576694de2dbfb22a4e6fc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>02fd73d861ef2e4aabb38c0c9ff82947</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>init</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>5bc111915c61a34eba56de3a4756c8b6</anchor>
      <arglist>(const FlowMap &amp;flowMap)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>startFirstPhase</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>ecbfdfd060020b8d84e202b78a345e8c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>startSecondPhase</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>5fce99cb4a842b1941dbfe4518a05251</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>13a43e6d814de94978c515cb084873b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>runMinCut</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>ba782387e4460a7c34c9227fafb53697</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>flowValue</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>8213f5ab8f2d11b368bd26833c570d55</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>flow</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>e5735fb5fda2b9e5d130a7628a32737a</anchor>
      <arglist>(const Arc &amp;arc) const </arglist>
    </member>
    <member kind="function">
      <type>const FlowMap &amp;</type>
      <name>flowMap</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>592e6bf75b178e6e189eedf322abcc27</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>minCut</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>7069e6ac25c7ac3cc4f0b79bf7166ff2</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>minCutMap</name>
      <anchorfile>a00216.html</anchorfile>
      <anchor>5063e1b784000f2828c094fab0fd9866</anchor>
      <arglist>(CutMap &amp;cutMap) const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lemon::Preflow::SetElevator</name>
    <filename>a00241.html</filename>
    <templarg></templarg>
    <base>Preflow&lt; Digraph, CapacityMap, SetElevatorTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Preflow::SetFlowMap</name>
    <filename>a00242.html</filename>
    <templarg></templarg>
    <base>Preflow&lt; Digraph, CapacityMap, SetFlowMapTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="struct">
    <name>lemon::Preflow::SetStandardElevator</name>
    <filename>a00257.html</filename>
    <templarg></templarg>
    <base>Preflow&lt; Digraph, CapacityMap, SetStandardElevatorTraits&lt; T &gt; &gt;</base>
  </compound>
  <compound kind="class">
    <name>lemon::Random</name>
    <filename>a00218.html</filename>
    <member kind="function">
      <type></type>
      <name>Random</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>a0cd23a16025cfeef7b8810eeb2a5d36</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Random</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>857ff5a7d27498071544a861a07af43c</anchor>
      <arglist>(Number seed)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Random</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>c015235b4ca152b5d9d77cc7002c17e4</anchor>
      <arglist>(Iterator begin, Iterator end)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Random</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>f1ba64a55666953d210a357ffd756bf5</anchor>
      <arglist>(const Random &amp;other)</arglist>
    </member>
    <member kind="function">
      <type>Random &amp;</type>
      <name>operator=</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>e07ab74cd27370d349856ec4cec76636</anchor>
      <arglist>(const Random &amp;other)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>seed</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>4d9a294b95ac5078b5d40787d05f14cb</anchor>
      <arglist>(Number seed)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>seed</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>dc4ba0755f19d7c6a5e069f45f2b1efc</anchor>
      <arglist>(Iterator begin, Iterator end)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>seed</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>e1156b8d9e442e441de700f93387143d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>seedFromFile</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>8979ee04f314da77eeb0ae6c41099f37</anchor>
      <arglist>(const std::string &amp;file=&quot;/dev/urandom&quot;, int offset=0)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>seedFromTime</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>d51cc770b46c582ed9c749aae3c16ff7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Number</type>
      <name>real</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>47bb96727c24e9d1404fd104673ddf7d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>real</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>ed2f4e5cf96cab689db5c63ca873a2ed</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>operator()</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>8ba58318b0e39bc2e453dd4dcac1f00f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>operator()</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>f8bac0a1d7352cde11539b6eff1a9586</anchor>
      <arglist>(double b)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>operator()</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>6b23b0cb075fcf514a070e3b52a177d7</anchor>
      <arglist>(double a, double b)</arglist>
    </member>
    <member kind="function">
      <type>Number</type>
      <name>integer</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>f0ad2da47b90bd2d9c66e882f7f65f20</anchor>
      <arglist>(Number b)</arglist>
    </member>
    <member kind="function">
      <type>Number</type>
      <name>integer</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>ebc2459828dd3a7607f968d4239837cf</anchor>
      <arglist>(Number a, Number b)</arglist>
    </member>
    <member kind="function">
      <type>Number</type>
      <name>operator[]</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>d295b208ce4678b935bf90ae1ecdfb6e</anchor>
      <arglist>(Number b)</arglist>
    </member>
    <member kind="function">
      <type>Number</type>
      <name>uinteger</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>435286b104693571118651d4666314ed</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>uinteger</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>f3f9bbf681335dde647d38699e0656da</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Number</type>
      <name>integer</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>2b4d49eba4d4f9b74f8fa0098e5a971b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>integer</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>1d91bf3b439b10037a6baf4c0018058e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>boolean</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>54a211ac234172119d7ee8f89cb53750</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>boolean</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>f8d119e90f605fb7776372bc083e0ec7</anchor>
      <arglist>(double p)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>gauss</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>61ccd72f7e2d2f65912807eda4c8643c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>gauss</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>56f2cac96664ba4cd0c6f4190e9982d4</anchor>
      <arglist>(double mean, double std_dev)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>lognormal</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>8271908f36362e4fac31dc5e98b7ad96</anchor>
      <arglist>(double n_mean, double n_std_dev)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>lognormal</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>526688ca8edd8f4bcad89c5bb89ee91b</anchor>
      <arglist>(const std::pair&lt; double, double &gt; &amp;params)</arglist>
    </member>
    <member kind="function">
      <type>std::pair&lt; double, double &gt;</type>
      <name>lognormalParamsFromMD</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>bd25cd785c5560b0bd89895dfad522cd</anchor>
      <arglist>(double mean, double std_dev)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>lognormalMD</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>d2b1e1b11a298b3cbdcc9207ebeca6b2</anchor>
      <arglist>(double mean, double std_dev)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>exponential</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>09fcc5d10338daea8484d7ab76307a64</anchor>
      <arglist>(double lambda=1.0)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>gamma</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>feefca159809b5989ff2916d6c23b0f6</anchor>
      <arglist>(int k)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>gamma</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>d93f49275dfaf442e9f8104f17394b2f</anchor>
      <arglist>(double k, double theta=1.0)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>weibull</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>c97b6292abddbc837251a1bcef58b3be</anchor>
      <arglist>(double k, double lambda)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>pareto</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>36b13c3dbd28b5a3684b0f5393b89c43</anchor>
      <arglist>(double k, double x_min)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>poisson</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>eef9d0d8d5af1d58208c767bd2947d9d</anchor>
      <arglist>(double lambda)</arglist>
    </member>
    <member kind="function">
      <type>dim2::Point&lt; double &gt;</type>
      <name>disc</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>bb3756a57ddd7ba851e502e7b279ba69</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>dim2::Point&lt; double &gt;</type>
      <name>gauss2</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>e3bc4202a01710ad288ed64b1e4b7cce</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>dim2::Point&lt; double &gt;</type>
      <name>exponential2</name>
      <anchorfile>a00218.html</anchorfile>
      <anchor>f2fb3ecd150d233ab1eb49755e0e50db</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SmartDigraphBase</name>
    <filename>a00269.html</filename>
  </compound>
  <compound kind="class">
    <name>lemon::SmartDigraph</name>
    <filename>a00268.html</filename>
    <class kind="class">lemon::SmartDigraph::Snapshot</class>
    <member kind="function">
      <type></type>
      <name>SmartDigraph</name>
      <anchorfile>a00268.html</anchorfile>
      <anchor>7fbeec3c0903b621368577f4a6802066</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>addNode</name>
      <anchorfile>a00268.html</anchorfile>
      <anchor>96838566b12a6b04795db38688bad1a0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>addArc</name>
      <anchorfile>a00268.html</anchorfile>
      <anchor>b947ea03356504c5c4b3c8ebfed1516a</anchor>
      <arglist>(const Node &amp;s, const Node &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reserveNode</name>
      <anchorfile>a00268.html</anchorfile>
      <anchor>28bb4df827e678ae549849be81d88def</anchor>
      <arglist>(int n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reserveArc</name>
      <anchorfile>a00268.html</anchorfile>
      <anchor>2489cbecb9d7ff5c9a0b2b5ee46818b5</anchor>
      <arglist>(int m)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00268.html</anchorfile>
      <anchor>a088e41ac5858620d94572f4af0b821f</anchor>
      <arglist>(Node n) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00268.html</anchorfile>
      <anchor>c31ad29081023b5df42a7b08e3b44cb4</anchor>
      <arglist>(Arc a) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00268.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>split</name>
      <anchorfile>a00268.html</anchorfile>
      <anchor>09a307979f5ac51cc21195285d485bbe</anchor>
      <arglist>(Node n, bool connect=true)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>SmartDigraph</name>
      <anchorfile>a00268.html</anchorfile>
      <anchor>4fd186151b026519546e7cfa7daae084</anchor>
      <arglist>(const SmartDigraph &amp;)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>operator=</name>
      <anchorfile>a00268.html</anchorfile>
      <anchor>2f041f84a01a853d371869103f47fbd9</anchor>
      <arglist>(const SmartDigraph &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SmartDigraph::Snapshot</name>
    <filename>a00275.html</filename>
    <member kind="function">
      <type></type>
      <name>Snapshot</name>
      <anchorfile>a00275.html</anchorfile>
      <anchor>8d35bcf9a5efab747b3b3603ac2861de</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Snapshot</name>
      <anchorfile>a00275.html</anchorfile>
      <anchor>420bebb38b3825415723d283e1ee1a58</anchor>
      <arglist>(SmartDigraph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>save</name>
      <anchorfile>a00275.html</anchorfile>
      <anchor>c4915493a67a5b6df654bace471e35d6</anchor>
      <arglist>(SmartDigraph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>restore</name>
      <anchorfile>a00275.html</anchorfile>
      <anchor>fd3595051be2709847c2de4352f27cf5</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SmartGraph</name>
    <filename>a00271.html</filename>
    <class kind="class">lemon::SmartGraph::Snapshot</class>
    <member kind="function">
      <type></type>
      <name>SmartGraph</name>
      <anchorfile>a00271.html</anchorfile>
      <anchor>97519188f8091a7925f4a51402a439f1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>addNode</name>
      <anchorfile>a00271.html</anchorfile>
      <anchor>96838566b12a6b04795db38688bad1a0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Edge</type>
      <name>addEdge</name>
      <anchorfile>a00271.html</anchorfile>
      <anchor>409e4da6682b7eadf5ede5ba15b171c5</anchor>
      <arglist>(const Node &amp;s, const Node &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00271.html</anchorfile>
      <anchor>a088e41ac5858620d94572f4af0b821f</anchor>
      <arglist>(Node n) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00271.html</anchorfile>
      <anchor>c31ad29081023b5df42a7b08e3b44cb4</anchor>
      <arglist>(Arc a) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>valid</name>
      <anchorfile>a00271.html</anchorfile>
      <anchor>55582c50a716d15504b5680d810bd5dd</anchor>
      <arglist>(Edge e) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00271.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>SmartGraph</name>
      <anchorfile>a00271.html</anchorfile>
      <anchor>76cea0ca28eed58d2a963c1f60186789</anchor>
      <arglist>(const SmartGraph &amp;)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>operator=</name>
      <anchorfile>a00271.html</anchorfile>
      <anchor>4c57d7f73f2bb09d8376f6d39ba6484c</anchor>
      <arglist>(const SmartGraph &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SmartGraph::Snapshot</name>
    <filename>a00272.html</filename>
    <member kind="function">
      <type></type>
      <name>Snapshot</name>
      <anchorfile>a00272.html</anchorfile>
      <anchor>8d35bcf9a5efab747b3b3603ac2861de</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Snapshot</name>
      <anchorfile>a00272.html</anchorfile>
      <anchor>8dc007458aabb5b0044a07dcae6979bf</anchor>
      <arglist>(SmartGraph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>save</name>
      <anchorfile>a00272.html</anchorfile>
      <anchor>bc94c3994f14aff6a3a6ac21c7413c6d</anchor>
      <arglist>(SmartGraph &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>restore</name>
      <anchorfile>a00272.html</anchorfile>
      <anchor>fd3595051be2709847c2de4352f27cf5</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::SoplexLp</name>
    <filename>a00276.html</filename>
    <base>lemon::LpSolver</base>
    <member kind="function">
      <type></type>
      <name>SoplexLp</name>
      <anchorfile>a00276.html</anchorfile>
      <anchor>5f0bc2548006cf473f17dc3f9aa01efd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SoplexLp</name>
      <anchorfile>a00276.html</anchorfile>
      <anchor>661b9d88b07c1915e69d7df71006109f</anchor>
      <arglist>(const SoplexLp &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~SoplexLp</name>
      <anchorfile>a00276.html</anchorfile>
      <anchor>651535f3d8d9e7ffaf8fe4ceb302ac25</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual SoplexLp *</type>
      <name>newSolver</name>
      <anchorfile>a00276.html</anchorfile>
      <anchor>7a7dae9e1c48237e3607aa4c15b584f4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual SoplexLp *</type>
      <name>cloneSolver</name>
      <anchorfile>a00276.html</anchorfile>
      <anchor>17c2b50d8058446bc37e82f3282a8f52</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Suurballe</name>
    <filename>a00284.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>LEN</type>
      <name>LengthMap</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>7398ca1da160bb30ee090866bbfc12ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>LengthMap::Value</type>
      <name>Length</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>544757ed56c7059f0f6f352b22802c62</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GR::ArcMap&lt; int &gt;</type>
      <name>FlowMap</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>18181c50983a40b31a0d6e288d986da8</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GR::NodeMap&lt; Length &gt;</type>
      <name>PotentialMap</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>11d865fdae9e8228ea49ae637ab64f8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>SimplePath&lt; GR &gt;</type>
      <name>Path</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>ede19a0219bfacac9531c8df84862bb3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Suurballe</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>31b52817725a0f1a2154d6e2b026274e</anchor>
      <arglist>(const Digraph &amp;graph, const LengthMap &amp;length)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Suurballe</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>924d52afb136fc92220a5878bcc26523</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Suurballe &amp;</type>
      <name>flowMap</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>d5ca17abd16a49b8b1cdc9c415c086a4</anchor>
      <arglist>(FlowMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>Suurballe &amp;</type>
      <name>potentialMap</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>1f9e7fc3cfeed3c7300017c7f00197bd</anchor>
      <arglist>(PotentialMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>run</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>775a143b5042d2c415c51cd1ea8924af</anchor>
      <arglist>(const Node &amp;s, const Node &amp;t, int k=2)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>3449f65c3633e80616102966e1af6bb4</anchor>
      <arglist>(const Node &amp;s)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>findFlow</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>7ed86f61c420946041091655b6cb9763</anchor>
      <arglist>(const Node &amp;t, int k=2)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>findPaths</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>dd7bd37fa586f6ddfa0fbe28f0e9afc4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Length</type>
      <name>totalLength</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>76e7b99dd475e8124524986e4e19a633</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>flow</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>d10191a43bc967094d3b43fcb47d230b</anchor>
      <arglist>(const Arc &amp;arc) const </arglist>
    </member>
    <member kind="function">
      <type>const FlowMap &amp;</type>
      <name>flowMap</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>592e6bf75b178e6e189eedf322abcc27</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Length</type>
      <name>potential</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>bf8f3df22d4e5bdc3ad368aa595e16e8</anchor>
      <arglist>(const Node &amp;node) const </arglist>
    </member>
    <member kind="function">
      <type>const PotentialMap &amp;</type>
      <name>potentialMap</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>0c903745ae34dd8732160c905909b705</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>pathNum</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>5ac78f51b4219ee8e89f22a35f8b52a2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const Path &amp;</type>
      <name>path</name>
      <anchorfile>a00284.html</anchorfile>
      <anchor>db338bb7401d5571eed87771ee5f70ad</anchor>
      <arglist>(int i) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::TimeStamp</name>
    <filename>a00288.html</filename>
    <member kind="function">
      <type>void</type>
      <name>stamp</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>130b5680827b41a6316cf211b7b1dc08</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>TimeStamp</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>1ae4a35dfcd8d6cd86ac79b723b463be</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>TimeStamp</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>b653dbae07ad3594dcb2b3804df01da0</anchor>
      <arglist>(void *)</arglist>
    </member>
    <member kind="function">
      <type>TimeStamp &amp;</type>
      <name>reset</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>3c8a00141996e34136e2025bd9d2b10e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>TimeStamp &amp;</type>
      <name>operator+=</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>dc75cb1de5387aab879cb9004f06a25e</anchor>
      <arglist>(const TimeStamp &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>TimeStamp</type>
      <name>operator+</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>ccedb0361dcf41a63f5693b0c55e89b3</anchor>
      <arglist>(const TimeStamp &amp;b) const </arglist>
    </member>
    <member kind="function">
      <type>TimeStamp &amp;</type>
      <name>operator-=</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>7d6e8adcd702074cb50f8129541df576</anchor>
      <arglist>(const TimeStamp &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>TimeStamp</type>
      <name>operator-</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>4ba7ff9185e2857e8ebc30167ba07cf0</anchor>
      <arglist>(const TimeStamp &amp;b) const </arglist>
    </member>
    <member kind="function">
      <type>TimeStamp &amp;</type>
      <name>operator*=</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>1211cb3b2213896c0381b70e5f763ad4</anchor>
      <arglist>(double b)</arglist>
    </member>
    <member kind="function">
      <type>TimeStamp</type>
      <name>operator*</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>4d835dff9a6f3daa19a3ad3854a91df7</anchor>
      <arglist>(double b) const </arglist>
    </member>
    <member kind="function">
      <type>TimeStamp &amp;</type>
      <name>operator/=</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>645205eb5cd5dd12d0a462ff44b713c4</anchor>
      <arglist>(double b)</arglist>
    </member>
    <member kind="function">
      <type>TimeStamp</type>
      <name>operator/</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>6471bd23752200acaaccc1fd0cfafce2</anchor>
      <arglist>(double b) const </arglist>
    </member>
    <member kind="function">
      <type>TimeStamp</type>
      <name>ellapsed</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>265d7f3c059c8d533d24ddf8a7bcb098</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>userTime</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>f92ff91e68d9b6c064cefc82ed9b3410</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>systemTime</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>41f1f0abb1c20b68824f0d4c76bc8c6d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>cUserTime</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>3c24d1228c8ac9b97b235b5b7cda44aa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>cSystemTime</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>2fc96e86f905484f3598824d80dc97db</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>realTime</name>
      <anchorfile>a00288.html</anchorfile>
      <anchor>8112e0d1c5a7ebdc4f0a0ffd7a7b2865</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="friend">
      <type>friend std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>a00450.html</anchorfile>
      <anchor>g7a8fc6a89c07865a42b13d6a2bbcbbcb</anchor>
      <arglist>(std::ostream &amp;os, const TimeStamp &amp;t)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Timer</name>
    <filename>a00286.html</filename>
    <member kind="function">
      <type></type>
      <name>Timer</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>7c4639dcdcc80169f9886b41077e106a</anchor>
      <arglist>(bool run=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>d20897c5c8bd47f5d4005989bead0e55</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>60de64d75454385b23995437f1d72669</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>stop</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>8c528baf37154d347366083f0f816846</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>halt</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>de0430439247877006d7df950f94918a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>running</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>6ed60de270944f49abc9787d2c59552c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>restart</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>22ee094ca3f45aa4156b97d34fe678bf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>userTime</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>f92ff91e68d9b6c064cefc82ed9b3410</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>systemTime</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>41f1f0abb1c20b68824f0d4c76bc8c6d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>cUserTime</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>3c24d1228c8ac9b97b235b5b7cda44aa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>cSystemTime</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>2fc96e86f905484f3598824d80dc97db</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>realTime</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>8112e0d1c5a7ebdc4f0a0ffd7a7b2865</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator TimeStamp</name>
      <anchorfile>a00286.html</anchorfile>
      <anchor>c61443176236152c37a5bd92c9d7de70</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::TimeReport</name>
    <filename>a00287.html</filename>
    <base>lemon::Timer</base>
    <member kind="function">
      <type></type>
      <name>TimeReport</name>
      <anchorfile>a00287.html</anchorfile>
      <anchor>def84ab0405cf58d47fa387068654cb8</anchor>
      <arglist>(std::string title, std::ostream &amp;os=std::cerr, bool run=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~TimeReport</name>
      <anchorfile>a00287.html</anchorfile>
      <anchor>1ea3883865c76b665de24924840b23a4</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::NoTimeReport</name>
    <filename>a00200.html</filename>
    <member kind="function">
      <type></type>
      <name>NoTimeReport</name>
      <anchorfile>a00200.html</anchorfile>
      <anchor>fadca87f9c0f5ac66a2e9d129b048b24</anchor>
      <arglist>(std::string, std::ostream &amp;, bool)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NoTimeReport</name>
      <anchorfile>a00200.html</anchorfile>
      <anchor>8b3531073f0165921962368e43e52879</anchor>
      <arglist>(std::string, std::ostream &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NoTimeReport</name>
      <anchorfile>a00200.html</anchorfile>
      <anchor>bf4a0472aacec05cbce3c0ee9a6facbe</anchor>
      <arglist>(std::string)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~NoTimeReport</name>
      <anchorfile>a00200.html</anchorfile>
      <anchor>d65b9018e923edec80720246bdc0d379</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Tolerance</name>
    <filename>a00289.html</filename>
    <templarg></templarg>
    <member kind="function" static="yes">
      <type>static Value</type>
      <name>zero</name>
      <anchorfile>a00289.html</anchorfile>
      <anchor>f24efe5c6b0edcb586538222fb5b1024</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>less</name>
      <anchorfile>a00289.html</anchorfile>
      <anchor>2a96b31f1a042a6b099f1252a7e88d8d</anchor>
      <arglist>(Value a, Value b)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>different</name>
      <anchorfile>a00289.html</anchorfile>
      <anchor>3347b4c48c20385af1e0a3bcae825a70</anchor>
      <arglist>(Value a, Value b)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>positive</name>
      <anchorfile>a00289.html</anchorfile>
      <anchor>48f208e5bc36bd09cb0e38c65bbc864d</anchor>
      <arglist>(Value a)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>negative</name>
      <anchorfile>a00289.html</anchorfile>
      <anchor>0f3a83e592e31a91b5ae73b221180c25</anchor>
      <arglist>(Value a)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>nonZero</name>
      <anchorfile>a00289.html</anchorfile>
      <anchor>9568ce99a273f6c593ff87ecdaae9c40</anchor>
      <arglist>(Value a)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Tolerance&lt; float &gt;</name>
    <filename>a00291.html</filename>
    <member kind="typedef">
      <type>float</type>
      <name>Value</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>18cf6695002fb1308814915b24fb1c4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Tolerance</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>9fb4f4c61a9530cf159b6d73e0a1e74f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Tolerance</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>1a95b75b6f3503b87ff886003df8503a</anchor>
      <arglist>(float e)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>epsilon</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>4a93fbfc9aad2fb1ffa93afb7bd1d906</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>epsilon</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>7ddb109de0243aa3f74ba0e2fc79e42c</anchor>
      <arglist>(Value e)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>less</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>0145e107e424776598c3b92054e7d514</anchor>
      <arglist>(Value a, Value b) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>different</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>682f13280f07ed5f3c2a0170722834b5</anchor>
      <arglist>(Value a, Value b) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>positive</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>35b4bc655e8cb11eabf4518e70108707</anchor>
      <arglist>(Value a) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>negative</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>5374b335ff173589e1c995fc47e94dc3</anchor>
      <arglist>(Value a) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>nonZero</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>ca7609eae2d3d03d6793ce617dc5dc56</anchor>
      <arglist>(Value a) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Value</type>
      <name>defaultEpsilon</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>786f69286455a315d2e866777f56e384</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>defaultEpsilon</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>00e9ca3993439df8046386e963c17f96</anchor>
      <arglist>(Value e)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Value</type>
      <name>zero</name>
      <anchorfile>a00291.html</anchorfile>
      <anchor>f24efe5c6b0edcb586538222fb5b1024</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Tolerance&lt; double &gt;</name>
    <filename>a00290.html</filename>
    <member kind="typedef">
      <type>double</type>
      <name>Value</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>566a00621638570a4186414035153a2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Tolerance</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>9fb4f4c61a9530cf159b6d73e0a1e74f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Tolerance</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>9945dec0f6e6f3cf19597b2f7fa313b1</anchor>
      <arglist>(double e)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>epsilon</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>4a93fbfc9aad2fb1ffa93afb7bd1d906</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>epsilon</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>7ddb109de0243aa3f74ba0e2fc79e42c</anchor>
      <arglist>(Value e)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>less</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>0145e107e424776598c3b92054e7d514</anchor>
      <arglist>(Value a, Value b) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>different</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>682f13280f07ed5f3c2a0170722834b5</anchor>
      <arglist>(Value a, Value b) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>positive</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>35b4bc655e8cb11eabf4518e70108707</anchor>
      <arglist>(Value a) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>negative</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>5374b335ff173589e1c995fc47e94dc3</anchor>
      <arglist>(Value a) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>nonZero</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>ca7609eae2d3d03d6793ce617dc5dc56</anchor>
      <arglist>(Value a) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Value</type>
      <name>defaultEpsilon</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>786f69286455a315d2e866777f56e384</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>defaultEpsilon</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>00e9ca3993439df8046386e963c17f96</anchor>
      <arglist>(Value e)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Value</type>
      <name>zero</name>
      <anchorfile>a00290.html</anchorfile>
      <anchor>f24efe5c6b0edcb586538222fb5b1024</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::Tolerance&lt; long double &gt;</name>
    <filename>a00292.html</filename>
    <member kind="typedef">
      <type>long double</type>
      <name>Value</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>0b5c2e30f1eb10bc1d304bb91ec99a7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Tolerance</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>9fb4f4c61a9530cf159b6d73e0a1e74f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Tolerance</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>4a809dde05056773bbf1414bf3bfa8bf</anchor>
      <arglist>(long double e)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>epsilon</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>4a93fbfc9aad2fb1ffa93afb7bd1d906</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>epsilon</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>7ddb109de0243aa3f74ba0e2fc79e42c</anchor>
      <arglist>(Value e)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>less</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>0145e107e424776598c3b92054e7d514</anchor>
      <arglist>(Value a, Value b) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>different</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>682f13280f07ed5f3c2a0170722834b5</anchor>
      <arglist>(Value a, Value b) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>positive</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>35b4bc655e8cb11eabf4518e70108707</anchor>
      <arglist>(Value a) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>negative</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>5374b335ff173589e1c995fc47e94dc3</anchor>
      <arglist>(Value a) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>nonZero</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>ca7609eae2d3d03d6793ce617dc5dc56</anchor>
      <arglist>(Value a) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Value</type>
      <name>defaultEpsilon</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>786f69286455a315d2e866777f56e384</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>defaultEpsilon</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>00e9ca3993439df8046386e963c17f96</anchor>
      <arglist>(Value e)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Value</type>
      <name>zero</name>
      <anchorfile>a00292.html</anchorfile>
      <anchor>f24efe5c6b0edcb586538222fb5b1024</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::UnionFind</name>
    <filename>a00295.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>IM</type>
      <name>ItemIntMap</name>
      <anchorfile>a00295.html</anchorfile>
      <anchor>c00a2d6f039b6e8ffc0641530bdf5304</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>ItemIntMap::Key</type>
      <name>Item</name>
      <anchorfile>a00295.html</anchorfile>
      <anchor>70025b32b600038ee2981a3deab1a783</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>UnionFind</name>
      <anchorfile>a00295.html</anchorfile>
      <anchor>23eb9f42b64ad319bd13e7b39921f9e1</anchor>
      <arglist>(ItemIntMap &amp;m)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>find</name>
      <anchorfile>a00295.html</anchorfile>
      <anchor>9208c8cfd1194fc80116f291ca20cc8a</anchor>
      <arglist>(const Item &amp;a)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00295.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>insert</name>
      <anchorfile>a00295.html</anchorfile>
      <anchor>68ecd7821136d47f859542f44ef40138</anchor>
      <arglist>(const Item &amp;a)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>join</name>
      <anchorfile>a00295.html</anchorfile>
      <anchor>5dffafed442e04c5a749b3790d35a888</anchor>
      <arglist>(const Item &amp;a, const Item &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>a00295.html</anchorfile>
      <anchor>edb3794e47f1fcb65a6902b79ea679d3</anchor>
      <arglist>(const Item &amp;a)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::UnionFindEnum</name>
    <filename>a00296.html</filename>
    <templarg></templarg>
    <class kind="class">lemon::UnionFindEnum::ClassIt</class>
    <class kind="class">lemon::UnionFindEnum::ItemIt</class>
    <member kind="typedef">
      <type>IM</type>
      <name>ItemIntMap</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>c00a2d6f039b6e8ffc0641530bdf5304</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>ItemIntMap::Key</type>
      <name>Item</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>70025b32b600038ee2981a3deab1a783</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>insert</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>59869ec643e4a302e58808739ad532ec</anchor>
      <arglist>(const Item &amp;item)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>insert</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>1762b08b3b35b8b1a3d1f6d26f389fd1</anchor>
      <arglist>(const Item &amp;item, int cls)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>find</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>0399c7e0e67a14c77712826eebec2f53</anchor>
      <arglist>(const Item &amp;item) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>join</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>d536c4d31fc031687d301a8eff68699c</anchor>
      <arglist>(const Item &amp;a, const Item &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>bfd9f0171d7d0d20bfbef3050ec118ab</anchor>
      <arglist>(int cls) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>split</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>f63fdc55f882427953a7a8a348e49642</anchor>
      <arglist>(int cls)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>b71f9a526f9dfa9ded1fdd78189c3e37</anchor>
      <arglist>(const Item &amp;item)</arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>item</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>ec7e0ba8ac366acc669d28335d03e38e</anchor>
      <arglist>(int cls) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>eraseClass</name>
      <anchorfile>a00296.html</anchorfile>
      <anchor>9cafaae8d2a6a124f500d5cd774c10fc</anchor>
      <arglist>(int cls)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::UnionFindEnum::ClassIt</name>
    <filename>a00040.html</filename>
    <member kind="function">
      <type></type>
      <name>ClassIt</name>
      <anchorfile>a00040.html</anchorfile>
      <anchor>9a83e3dd1807f0728359469863a5ae14</anchor>
      <arglist>(const UnionFindEnum &amp;ufe)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ClassIt</name>
      <anchorfile>a00040.html</anchorfile>
      <anchor>73864ec018608788129b8166ccf2635e</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>ClassIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00040.html</anchorfile>
      <anchor>52b7eaf8769c0839d58b44a4fa4b39f0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator int</name>
      <anchorfile>a00040.html</anchorfile>
      <anchor>6a6c0aaee2d785b48a1e57710ceb586b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00040.html</anchorfile>
      <anchor>efa8e3edb2c7c9d0819b4c9765117d5e</anchor>
      <arglist>(const ClassIt &amp;i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00040.html</anchorfile>
      <anchor>9d7715efaa0bd2d11de33da881e26698</anchor>
      <arglist>(const ClassIt &amp;i)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::UnionFindEnum::ItemIt</name>
    <filename>a00156.html</filename>
    <member kind="function">
      <type></type>
      <name>ItemIt</name>
      <anchorfile>a00156.html</anchorfile>
      <anchor>71eb2d37629fe1d7a9f7b0f418c4ff16</anchor>
      <arglist>(const UnionFindEnum &amp;ufe, int cls)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ItemIt</name>
      <anchorfile>a00156.html</anchorfile>
      <anchor>c94a8a34f023735fd20c6a684974d344</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>ItemIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00156.html</anchorfile>
      <anchor>fcf841b3d25f7aa896fe7edad455091a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator const Item &amp;</name>
      <anchorfile>a00156.html</anchorfile>
      <anchor>36af9c387ac91c423473c4aa488a39c4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00156.html</anchorfile>
      <anchor>83ee20b3b68aeb5de19bf71cf2c9f17d</anchor>
      <arglist>(const ItemIt &amp;i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00156.html</anchorfile>
      <anchor>8806a1b76a91c170b552cb92a95b7da0</anchor>
      <arglist>(const ItemIt &amp;i)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ExtendFindEnum</name>
    <filename>a00112.html</filename>
    <templarg></templarg>
    <class kind="class">lemon::ExtendFindEnum::ClassIt</class>
    <class kind="class">lemon::ExtendFindEnum::ItemIt</class>
    <member kind="typedef">
      <type>IM</type>
      <name>ItemIntMap</name>
      <anchorfile>a00112.html</anchorfile>
      <anchor>c00a2d6f039b6e8ffc0641530bdf5304</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>ItemIntMap::Key</type>
      <name>Item</name>
      <anchorfile>a00112.html</anchorfile>
      <anchor>70025b32b600038ee2981a3deab1a783</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ExtendFindEnum</name>
      <anchorfile>a00112.html</anchorfile>
      <anchor>b94083163188959d9fcfa940c6e640b7</anchor>
      <arglist>(ItemIntMap &amp;_index)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>insert</name>
      <anchorfile>a00112.html</anchorfile>
      <anchor>59869ec643e4a302e58808739ad532ec</anchor>
      <arglist>(const Item &amp;item)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>insert</name>
      <anchorfile>a00112.html</anchorfile>
      <anchor>1762b08b3b35b8b1a3d1f6d26f389fd1</anchor>
      <arglist>(const Item &amp;item, int cls)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00112.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>find</name>
      <anchorfile>a00112.html</anchorfile>
      <anchor>0399c7e0e67a14c77712826eebec2f53</anchor>
      <arglist>(const Item &amp;item) const </arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>item</name>
      <anchorfile>a00112.html</anchorfile>
      <anchor>ec7e0ba8ac366acc669d28335d03e38e</anchor>
      <arglist>(int cls) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00112.html</anchorfile>
      <anchor>b71f9a526f9dfa9ded1fdd78189c3e37</anchor>
      <arglist>(const Item &amp;item)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>eraseClass</name>
      <anchorfile>a00112.html</anchorfile>
      <anchor>2174a725c3a68a866b49d43142bc3d29</anchor>
      <arglist>(int cdx)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ExtendFindEnum::ClassIt</name>
    <filename>a00041.html</filename>
    <member kind="function">
      <type></type>
      <name>ClassIt</name>
      <anchorfile>a00041.html</anchorfile>
      <anchor>6f28b94b741b9597ea12d1a02d5c0d6c</anchor>
      <arglist>(const ExtendFindEnum &amp;ufe)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ClassIt</name>
      <anchorfile>a00041.html</anchorfile>
      <anchor>73864ec018608788129b8166ccf2635e</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>ClassIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00041.html</anchorfile>
      <anchor>52b7eaf8769c0839d58b44a4fa4b39f0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator int</name>
      <anchorfile>a00041.html</anchorfile>
      <anchor>6a6c0aaee2d785b48a1e57710ceb586b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00041.html</anchorfile>
      <anchor>efa8e3edb2c7c9d0819b4c9765117d5e</anchor>
      <arglist>(const ClassIt &amp;i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00041.html</anchorfile>
      <anchor>9d7715efaa0bd2d11de33da881e26698</anchor>
      <arglist>(const ClassIt &amp;i)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::ExtendFindEnum::ItemIt</name>
    <filename>a00157.html</filename>
    <member kind="function">
      <type></type>
      <name>ItemIt</name>
      <anchorfile>a00157.html</anchorfile>
      <anchor>7812fb6c628cec851fb9afb1883972a5</anchor>
      <arglist>(const ExtendFindEnum &amp;ufe, int cls)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ItemIt</name>
      <anchorfile>a00157.html</anchorfile>
      <anchor>c94a8a34f023735fd20c6a684974d344</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>ItemIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00157.html</anchorfile>
      <anchor>fcf841b3d25f7aa896fe7edad455091a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator const Item &amp;</name>
      <anchorfile>a00157.html</anchorfile>
      <anchor>36af9c387ac91c423473c4aa488a39c4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00157.html</anchorfile>
      <anchor>83ee20b3b68aeb5de19bf71cf2c9f17d</anchor>
      <arglist>(const ItemIt &amp;i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00157.html</anchorfile>
      <anchor>8806a1b76a91c170b552cb92a95b7da0</anchor>
      <arglist>(const ItemIt &amp;i)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::HeapUnionFind</name>
    <filename>a00139.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <class kind="class">lemon::HeapUnionFind::ClassIt</class>
    <class kind="class">lemon::HeapUnionFind::ItemIt</class>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>IM::Key</type>
      <name>Item</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>b34456f9e53eda26e5c605cb223fbcd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>IM</type>
      <name>ItemIntMap</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>c00a2d6f039b6e8ffc0641530bdf5304</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Comp</type>
      <name>Compare</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>ad6f401d1521e21c6b512b61409f7bf4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>alive</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>3bb9253425f75a8d88aa56ec70f254a0</anchor>
      <arglist>(int cls) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>trivial</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>9ed92927df756293536843ebb2fd5e39</anchor>
      <arglist>(int cls) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>HeapUnionFind</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>20106e1c71629a9fe8f6067d4871da84</anchor>
      <arglist>(ItemIntMap &amp;_index)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>insert</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>daf61646dea527f772f4e1711d59e35e</anchor>
      <arglist>(const Item &amp;item, const Value &amp;prio)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>find</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>0399c7e0e67a14c77712826eebec2f53</anchor>
      <arglist>(const Item &amp;item) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>join</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>10933fc9d40208c4d581ae5ad6ac6c3e</anchor>
      <arglist>(Iterator begin, Iterator end)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>split</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>c0f63a2678c5d3b2772933748b0ab345</anchor>
      <arglist>(int cls, Iterator out)</arglist>
    </member>
    <member kind="function">
      <type>const Value &amp;</type>
      <name>operator[]</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>d3cdce83a10b031eb28d0f2d1fcba450</anchor>
      <arglist>(const Item &amp;item) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>76d53a876dd67ecdc8832920937b7c3f</anchor>
      <arglist>(const Item &amp;item, const Value &amp;prio)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>increase</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>7b49a405fa19af1ac4ab69e669ad7842</anchor>
      <arglist>(const Item &amp;item, const Value &amp;prio)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>decrease</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>4be49210bae8aa13f625b3846f6e7242</anchor>
      <arglist>(const Item &amp;item, const Value &amp;prio)</arglist>
    </member>
    <member kind="function">
      <type>const Value &amp;</type>
      <name>classPrio</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>2b93e2981172fa0ed63e5fc6e6e65754</anchor>
      <arglist>(int cls) const </arglist>
    </member>
    <member kind="function">
      <type>const Item &amp;</type>
      <name>classTop</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>8924e2f5509951ceb92b9c5c5bff7f7f</anchor>
      <arglist>(int cls) const </arglist>
    </member>
    <member kind="function">
      <type>const Item &amp;</type>
      <name>classRep</name>
      <anchorfile>a00139.html</anchorfile>
      <anchor>4bc6dba7d781cad8595b94bda3737df3</anchor>
      <arglist>(int id) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::HeapUnionFind::ClassIt</name>
    <filename>a00042.html</filename>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00042.html</anchorfile>
      <anchor>efa8e3edb2c7c9d0819b4c9765117d5e</anchor>
      <arglist>(const ClassIt &amp;i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00042.html</anchorfile>
      <anchor>9d7715efaa0bd2d11de33da881e26698</anchor>
      <arglist>(const ClassIt &amp;i)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::HeapUnionFind::ItemIt</name>
    <filename>a00158.html</filename>
    <member kind="function">
      <type></type>
      <name>ItemIt</name>
      <anchorfile>a00158.html</anchorfile>
      <anchor>f9815f2a938cdcf66550d6f5ba407275</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>ItemIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00158.html</anchorfile>
      <anchor>fcf841b3d25f7aa896fe7edad455091a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator const Item &amp;</name>
      <anchorfile>a00158.html</anchorfile>
      <anchor>36af9c387ac91c423473c4aa488a39c4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00158.html</anchorfile>
      <anchor>83ee20b3b68aeb5de19bf71cf2c9f17d</anchor>
      <arglist>(const ItemIt &amp;i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00158.html</anchorfile>
      <anchor>8806a1b76a91c170b552cb92a95b7da0</anchor>
      <arglist>(const ItemIt &amp;i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00158.html</anchorfile>
      <anchor>fbd1dc99d3bb91299bdeac8966b4fbc1</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00158.html</anchorfile>
      <anchor>a20cdc6e6b325e231a505a4eb85e3989</anchor>
      <arglist>(Invalid)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lemon::concepts</name>
    <filename>a00422.html</filename>
    <class kind="class">lemon::concepts::Digraph</class>
    <class kind="class">lemon::concepts::Graph</class>
    <class kind="class">lemon::concepts::GraphItem</class>
    <class kind="class">lemon::concepts::BaseDigraphComponent</class>
    <class kind="class">lemon::concepts::BaseGraphComponent</class>
    <class kind="class">lemon::concepts::IDableDigraphComponent</class>
    <class kind="class">lemon::concepts::IDableGraphComponent</class>
    <class kind="class">lemon::concepts::GraphItemIt</class>
    <class kind="class">lemon::concepts::GraphIncIt</class>
    <class kind="class">lemon::concepts::IterableDigraphComponent</class>
    <class kind="class">lemon::concepts::IterableGraphComponent</class>
    <class kind="class">lemon::concepts::AlterableDigraphComponent</class>
    <class kind="class">lemon::concepts::AlterableGraphComponent</class>
    <class kind="class">lemon::concepts::GraphMap</class>
    <class kind="class">lemon::concepts::MappableDigraphComponent</class>
    <class kind="class">lemon::concepts::MappableGraphComponent</class>
    <class kind="class">lemon::concepts::ExtendableDigraphComponent</class>
    <class kind="class">lemon::concepts::ExtendableGraphComponent</class>
    <class kind="class">lemon::concepts::ErasableDigraphComponent</class>
    <class kind="class">lemon::concepts::ErasableGraphComponent</class>
    <class kind="class">lemon::concepts::ClearableDigraphComponent</class>
    <class kind="class">lemon::concepts::ClearableGraphComponent</class>
    <class kind="class">lemon::concepts::Heap</class>
    <class kind="class">lemon::concepts::ReadMap</class>
    <class kind="class">lemon::concepts::WriteMap</class>
    <class kind="class">lemon::concepts::ReadWriteMap</class>
    <class kind="class">lemon::concepts::ReferenceMap</class>
    <class kind="class">lemon::concepts::Path</class>
    <class kind="class">lemon::concepts::PathDumper</class>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Digraph</name>
    <filename>a00083.html</filename>
    <class kind="class">lemon::concepts::Digraph::Arc</class>
    <class kind="class">lemon::concepts::Digraph::ArcIt</class>
    <class kind="class">lemon::concepts::Digraph::ArcMap</class>
    <class kind="class">lemon::concepts::Digraph::InArcIt</class>
    <class kind="class">lemon::concepts::Digraph::Node</class>
    <class kind="class">lemon::concepts::Digraph::NodeIt</class>
    <class kind="class">lemon::concepts::Digraph::NodeMap</class>
    <class kind="class">lemon::concepts::Digraph::OutArcIt</class>
    <member kind="function">
      <type></type>
      <name>Digraph</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>fe9cc0c764291ff93356bb374e237128</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>target</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>ad5e4a458a3078ff7d2669db9e9a371f</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>source</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>f17b3519cfc9258e3e7aee685958c1e4</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>id</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>846b30a16b421338012940a52b718b1d</anchor>
      <arglist>(Node) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>id</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>5140868ea355635affbd108914b2dccd</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>nodeFromId</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>c062562f00e551ac6e440e95a15609d7</anchor>
      <arglist>(int) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>arcFromId</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>58a883bb07c2bbd1819a277b21bf2964</anchor>
      <arglist>(int) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maxNodeId</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>22ee4829c9dcc9859917b3480b8af19a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maxArcId</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>6b94cf2706afb7c936a4f6b97b430edf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>baseNode</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>3445bf026452215ad19333a499afc613</anchor>
      <arglist>(const InArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>runningNode</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>7a1a12c0acd7e4ee2481a8f900e0d672</anchor>
      <arglist>(const InArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>baseNode</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>b10cce7c966f098d0802b651da2b0d49</anchor>
      <arglist>(const OutArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>runningNode</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>15a56f51941bf8376aa809dc3d9bec96</anchor>
      <arglist>(const OutArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>oppositeNode</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>611c261fc201674bb62114df682eb35f</anchor>
      <arglist>(const Node &amp;, const Arc &amp;) const </arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>Digraph</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>e18fda3a8b7402d1b913eb3d0eb64b98</anchor>
      <arglist>(const Digraph &amp;)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>operator=</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>86ceef2fe8f8ba68f9a9eec1c99cdad2</anchor>
      <arglist>(const Digraph &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Digraph::Arc</name>
    <filename>a00007.html</filename>
    <member kind="function">
      <type></type>
      <name>Arc</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>9da2daa646ba83cf05f2911e9b711395</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Arc</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>5373a1c1375de29fc15d04784ff78c15</anchor>
      <arglist>(const Arc &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Arc</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>c8e547177cf6043a0f6801ef10d4a3c3</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>7ba951aaae03c58a728fe64c864c7f91</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>0ec526d3fabcfdb7fdd2d7ccc975d0dd</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>18d3e57a712ba654ce4aa89b823633ec</anchor>
      <arglist>(Arc) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Digraph::ArcIt</name>
    <filename>a00009.html</filename>
    <base>lemon::concepts::Digraph::Arc</base>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>19f73b24f93bc2400778a66262f4fc78</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>362cddf54aed5ce978e49d334776250b</anchor>
      <arglist>(const ArcIt &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>3bde563f0bba0fc91cc7255fed9b682c</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>9fdb90bbc22caeb9bb716bb8c5d6e35c</anchor>
      <arglist>(const Digraph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>8b331a60890a7a9d4ed7254fc44062a0</anchor>
      <arglist>(const Digraph &amp;, const Arc &amp;)</arglist>
    </member>
    <member kind="function">
      <type>ArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a8838b5e6d2c1404b0b07dfc23ff9564</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Digraph::ArcMap</name>
    <filename>a00018.html</filename>
    <templarg></templarg>
    <base>ReferenceMap&lt; Arc, T, T &amp;, const T &amp; &gt;</base>
    <member kind="function">
      <type></type>
      <name>ArcMap</name>
      <anchorfile>a00018.html</anchorfile>
      <anchor>52ad4fa4ff6bb5c6078ff647814144fb</anchor>
      <arglist>(const Digraph &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcMap</name>
      <anchorfile>a00018.html</anchorfile>
      <anchor>2ade9cf15a9db3bc363f27f43204a65d</anchor>
      <arglist>(const Digraph &amp;, T)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>ArcMap</name>
      <anchorfile>a00018.html</anchorfile>
      <anchor>8b5b7779352631b2f1e496cd8b6a0825</anchor>
      <arglist>(const ArcMap &amp;em)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>ArcMap &amp;</type>
      <name>operator=</name>
      <anchorfile>a00018.html</anchorfile>
      <anchor>677fe241a6de3a78b9f70032466ea0d0</anchor>
      <arglist>(const CMap &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Digraph::InArcIt</name>
    <filename>a00146.html</filename>
    <base>lemon::concepts::Digraph::Arc</base>
    <member kind="function">
      <type></type>
      <name>InArcIt</name>
      <anchorfile>a00146.html</anchorfile>
      <anchor>5b73f3d25b822a4402bae491770c69b7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InArcIt</name>
      <anchorfile>a00146.html</anchorfile>
      <anchor>ed9bfdcea39717444f32ab67b7c63445</anchor>
      <arglist>(const InArcIt &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InArcIt</name>
      <anchorfile>a00146.html</anchorfile>
      <anchor>590d5ea53f43dae27e42986018c206fa</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InArcIt</name>
      <anchorfile>a00146.html</anchorfile>
      <anchor>ab21dd710a5b92d22d9ecf57493f6036</anchor>
      <arglist>(const Digraph &amp;, const Node &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InArcIt</name>
      <anchorfile>a00146.html</anchorfile>
      <anchor>3798c4d1638b96a6a9207b62d3d526fb</anchor>
      <arglist>(const Digraph &amp;, const Arc &amp;)</arglist>
    </member>
    <member kind="function">
      <type>InArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00146.html</anchorfile>
      <anchor>c495b69e37cf4ccf75cade0b52ccb08d</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Digraph::Node</name>
    <filename>a00192.html</filename>
    <member kind="function">
      <type></type>
      <name>Node</name>
      <anchorfile>a00192.html</anchorfile>
      <anchor>0d313fac56abd7ebe58a17f1530b879e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Node</name>
      <anchorfile>a00192.html</anchorfile>
      <anchor>7d0d5367c2bd0ffca74029c22845c3b4</anchor>
      <arglist>(const Node &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Node</name>
      <anchorfile>a00192.html</anchorfile>
      <anchor>9a0be9b2de82c808eb4e89a217e35f55</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00192.html</anchorfile>
      <anchor>012ce897fb32e39d99c1cd300227504a</anchor>
      <arglist>(Node) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00192.html</anchorfile>
      <anchor>6fc9deb2a19387e8840cf5cb90c7835d</anchor>
      <arglist>(Node) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00192.html</anchorfile>
      <anchor>05af619da9adfb04fde63e00ae776eb8</anchor>
      <arglist>(Node) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Digraph::NodeIt</name>
    <filename>a00195.html</filename>
    <base>lemon::concepts::Digraph::Node</base>
    <member kind="function">
      <type></type>
      <name>NodeIt</name>
      <anchorfile>a00195.html</anchorfile>
      <anchor>986ea351c3f6f20b80e3c6224e5495cc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeIt</name>
      <anchorfile>a00195.html</anchorfile>
      <anchor>a97d7bb0a0a6d9abf3d4da3af90e0da8</anchor>
      <arglist>(const NodeIt &amp;n)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeIt</name>
      <anchorfile>a00195.html</anchorfile>
      <anchor>e4608fb56651c0a20495054ea7ae84e0</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeIt</name>
      <anchorfile>a00195.html</anchorfile>
      <anchor>412e8e22a87fd351f1876474d1d95a18</anchor>
      <arglist>(const Digraph &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeIt</name>
      <anchorfile>a00195.html</anchorfile>
      <anchor>09534657364bfafe0ecc987f9b1fee83</anchor>
      <arglist>(const Digraph &amp;, const Node &amp;)</arglist>
    </member>
    <member kind="function">
      <type>NodeIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00195.html</anchorfile>
      <anchor>745151a2b1ec6f321751d9291dcd4f79</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Digraph::NodeMap</name>
    <filename>a00196.html</filename>
    <templarg></templarg>
    <base>ReferenceMap&lt; Node, T, T &amp;, const T &amp; &gt;</base>
    <member kind="function">
      <type></type>
      <name>NodeMap</name>
      <anchorfile>a00196.html</anchorfile>
      <anchor>7eba1602f2ea7506a81e8983db6063af</anchor>
      <arglist>(const Digraph &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeMap</name>
      <anchorfile>a00196.html</anchorfile>
      <anchor>a626b2c1f7b1dca7ef781110339eaf07</anchor>
      <arglist>(const Digraph &amp;, T)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>NodeMap</name>
      <anchorfile>a00196.html</anchorfile>
      <anchor>f7e39537603ab1c0ec715d2ba2f8cbba</anchor>
      <arglist>(const NodeMap &amp;nm)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>NodeMap &amp;</type>
      <name>operator=</name>
      <anchorfile>a00196.html</anchorfile>
      <anchor>a667d7a1e0e82a9914047c23c6abb92a</anchor>
      <arglist>(const CMap &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Digraph::OutArcIt</name>
    <filename>a00207.html</filename>
    <base>lemon::concepts::Digraph::Arc</base>
    <member kind="function">
      <type></type>
      <name>OutArcIt</name>
      <anchorfile>a00207.html</anchorfile>
      <anchor>63481864a724a2879e46fa4f4202a743</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OutArcIt</name>
      <anchorfile>a00207.html</anchorfile>
      <anchor>471d9e55da47e420563ab67c82641476</anchor>
      <arglist>(const OutArcIt &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OutArcIt</name>
      <anchorfile>a00207.html</anchorfile>
      <anchor>e66c85834955281519f2c62b2e202785</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OutArcIt</name>
      <anchorfile>a00207.html</anchorfile>
      <anchor>2538bac8a29dcbf54eaf04617f03d2cd</anchor>
      <arglist>(const Digraph &amp;, const Node &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OutArcIt</name>
      <anchorfile>a00207.html</anchorfile>
      <anchor>32f5b45bd9852040f6de8066cfd816d7</anchor>
      <arglist>(const Digraph &amp;, const Arc &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OutArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00207.html</anchorfile>
      <anchor>e220200f09a52e317d02403d5d2c8a16</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph</name>
    <filename>a00127.html</filename>
    <class kind="class">lemon::concepts::Graph::Arc</class>
    <class kind="class">lemon::concepts::Graph::ArcIt</class>
    <class kind="class">lemon::concepts::Graph::ArcMap</class>
    <class kind="class">lemon::concepts::Graph::Edge</class>
    <class kind="class">lemon::concepts::Graph::EdgeIt</class>
    <class kind="class">lemon::concepts::Graph::EdgeMap</class>
    <class kind="class">lemon::concepts::Graph::InArcIt</class>
    <class kind="class">lemon::concepts::Graph::IncEdgeIt</class>
    <class kind="class">lemon::concepts::Graph::Node</class>
    <class kind="class">lemon::concepts::Graph::NodeIt</class>
    <class kind="class">lemon::concepts::Graph::NodeMap</class>
    <class kind="class">lemon::concepts::Graph::OutArcIt</class>
    <member kind="typedef">
      <type>True</type>
      <name>UndirectedTag</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>ede403f4c863f2249a26de6a33fd47ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>direct</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>5d4026f8aa623088ebf940618c6abb35</anchor>
      <arglist>(const Edge &amp;, const Node &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>direct</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>4b6177bf5b5c548bbbf2a626cc1e0979</anchor>
      <arglist>(const Edge &amp;, bool) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>direction</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>5ece07eb3bf823690a2d379d8ad514b5</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>oppositeArc</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>e1119df1efda911e44ecf3f8bc306f18</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>oppositeNode</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>d1389886a660ae25e86e3b1a1fab0f50</anchor>
      <arglist>(Node, Edge) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>u</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>debdb2db53887c1708db7e1ff5289140</anchor>
      <arglist>(Edge) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>v</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>8a2993711daf1e0a074f11cd23a85f80</anchor>
      <arglist>(Edge) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>source</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>f17b3519cfc9258e3e7aee685958c1e4</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>target</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>ad5e4a458a3078ff7d2669db9e9a371f</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>id</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>846b30a16b421338012940a52b718b1d</anchor>
      <arglist>(Node) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>id</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>40361e9f8781664704993f16066fdfdb</anchor>
      <arglist>(Edge) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>id</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>5140868ea355635affbd108914b2dccd</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>nodeFromId</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>c062562f00e551ac6e440e95a15609d7</anchor>
      <arglist>(int) const </arglist>
    </member>
    <member kind="function">
      <type>Edge</type>
      <name>edgeFromId</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>dc3a4a3eddb6ba32c2c849ee4ff2f45a</anchor>
      <arglist>(int) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>arcFromId</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>58a883bb07c2bbd1819a277b21bf2964</anchor>
      <arglist>(int) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maxNodeId</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>22ee4829c9dcc9859917b3480b8af19a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maxEdgeId</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>fa71fa16b2498a294c93c5aea4bd1c5e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maxArcId</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>6b94cf2706afb7c936a4f6b97b430edf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>baseNode</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>6d8bbad97cc608cae74777f1a03a8693</anchor>
      <arglist>(OutArcIt e) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>runningNode</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>3c0d55169f56592b2df35631e7e76b95</anchor>
      <arglist>(OutArcIt e) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>baseNode</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>9bd9d99ce3d4c5a859f8fb4508949854</anchor>
      <arglist>(InArcIt e) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>runningNode</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>c0cea0502ad0f9b7e50977a22f49e3cf</anchor>
      <arglist>(InArcIt e) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>baseNode</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>8ff86881f5d40a7227653a1787e04920</anchor>
      <arglist>(IncEdgeIt) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>runningNode</name>
      <anchorfile>a00127.html</anchorfile>
      <anchor>c5bb7b8d8f1f0ad7bcdaa55c2d161f99</anchor>
      <arglist>(IncEdgeIt) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::Arc</name>
    <filename>a00008.html</filename>
    <member kind="function">
      <type></type>
      <name>Arc</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>9da2daa646ba83cf05f2911e9b711395</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Arc</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>5373a1c1375de29fc15d04784ff78c15</anchor>
      <arglist>(const Arc &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Arc</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>c8e547177cf6043a0f6801ef10d4a3c3</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>7ba951aaae03c58a728fe64c864c7f91</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>0ec526d3fabcfdb7fdd2d7ccc975d0dd</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>18d3e57a712ba654ce4aa89b823633ec</anchor>
      <arglist>(Arc) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Edge</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>5a25824a51ad86bdf1651c5f988aa81f</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::ArcIt</name>
    <filename>a00010.html</filename>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>19f73b24f93bc2400778a66262f4fc78</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>362cddf54aed5ce978e49d334776250b</anchor>
      <arglist>(const ArcIt &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>3bde563f0bba0fc91cc7255fed9b682c</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>c38b8cd9b9df08c67e7bd8bbb02ac09b</anchor>
      <arglist>(const Graph &amp;g)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>94562cb075a97a6834cbb69fc5066a67</anchor>
      <arglist>(const Graph &amp;, const Arc &amp;)</arglist>
    </member>
    <member kind="function">
      <type>ArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>a8838b5e6d2c1404b0b07dfc23ff9564</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::ArcMap</name>
    <filename>a00019.html</filename>
    <templarg></templarg>
    <base>ReferenceMap&lt; Arc, T, T &amp;, const T &amp; &gt;</base>
    <member kind="function">
      <type></type>
      <name>ArcMap</name>
      <anchorfile>a00019.html</anchorfile>
      <anchor>5271b8a8cf0b7a38df385e8f74ef0e86</anchor>
      <arglist>(const Graph &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcMap</name>
      <anchorfile>a00019.html</anchorfile>
      <anchor>b93b09d11305d46b9b46a2f1aa8a4c86</anchor>
      <arglist>(const Graph &amp;, T)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>ArcMap</name>
      <anchorfile>a00019.html</anchorfile>
      <anchor>8b5b7779352631b2f1e496cd8b6a0825</anchor>
      <arglist>(const ArcMap &amp;em)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>ArcMap &amp;</type>
      <name>operator=</name>
      <anchorfile>a00019.html</anchorfile>
      <anchor>677fe241a6de3a78b9f70032466ea0d0</anchor>
      <arglist>(const CMap &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::Edge</name>
    <filename>a00098.html</filename>
    <member kind="function">
      <type></type>
      <name>Edge</name>
      <anchorfile>a00098.html</anchorfile>
      <anchor>91715a2ea8b8478224859ee6ee42ed87</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Edge</name>
      <anchorfile>a00098.html</anchorfile>
      <anchor>05e10aa476d938bdc8767728e5e524dd</anchor>
      <arglist>(const Edge &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Edge</name>
      <anchorfile>a00098.html</anchorfile>
      <anchor>4c15ff1fe447a33c5ba30c34b114df23</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00098.html</anchorfile>
      <anchor>a73e66d80a2f5ffbf88992b3aa6e68d4</anchor>
      <arglist>(Edge) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00098.html</anchorfile>
      <anchor>29c8801ae414ed09645a4904a7f5f61d</anchor>
      <arglist>(Edge) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00098.html</anchorfile>
      <anchor>4728af69bd6b38230a043c8d27e26e3d</anchor>
      <arglist>(Edge) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::EdgeIt</name>
    <filename>a00100.html</filename>
    <base>lemon::concepts::Graph::Edge</base>
    <member kind="function">
      <type></type>
      <name>EdgeIt</name>
      <anchorfile>a00100.html</anchorfile>
      <anchor>367509fe9aa557c1b7acdf86a9e4fb8b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>EdgeIt</name>
      <anchorfile>a00100.html</anchorfile>
      <anchor>1ed0fcb6fd8e9654a5c8531d2d31ecbb</anchor>
      <arglist>(const EdgeIt &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>EdgeIt</name>
      <anchorfile>a00100.html</anchorfile>
      <anchor>984660b6f8ca41cc1257f012f97f1a50</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>EdgeIt</name>
      <anchorfile>a00100.html</anchorfile>
      <anchor>5b832c64cc7e26a264cddbca7dbce41c</anchor>
      <arglist>(const Graph &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>EdgeIt</name>
      <anchorfile>a00100.html</anchorfile>
      <anchor>d8a7dee5bf5794f9f544f1bc94c284a8</anchor>
      <arglist>(const Graph &amp;, const Edge &amp;)</arglist>
    </member>
    <member kind="function">
      <type>EdgeIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00100.html</anchorfile>
      <anchor>c76a4784e377b27dbcc0a66ddda42c01</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::EdgeMap</name>
    <filename>a00101.html</filename>
    <templarg></templarg>
    <base>ReferenceMap&lt; Edge, T, T &amp;, const T &amp; &gt;</base>
    <member kind="function">
      <type></type>
      <name>EdgeMap</name>
      <anchorfile>a00101.html</anchorfile>
      <anchor>168c15d8d818b62e17ffec8bbafb1acb</anchor>
      <arglist>(const Graph &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>EdgeMap</name>
      <anchorfile>a00101.html</anchorfile>
      <anchor>924256b62bbfbe886d1eb9a79d3f7670</anchor>
      <arglist>(const Graph &amp;, T)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>EdgeMap</name>
      <anchorfile>a00101.html</anchorfile>
      <anchor>c8f04bd14f33e7130c8190d667639f86</anchor>
      <arglist>(const EdgeMap &amp;em)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>EdgeMap &amp;</type>
      <name>operator=</name>
      <anchorfile>a00101.html</anchorfile>
      <anchor>5d8010c74679b0d91ddde1196eeed600</anchor>
      <arglist>(const CMap &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::InArcIt</name>
    <filename>a00147.html</filename>
    <base>lemon::concepts::Graph::Arc</base>
    <member kind="function">
      <type></type>
      <name>InArcIt</name>
      <anchorfile>a00147.html</anchorfile>
      <anchor>5b73f3d25b822a4402bae491770c69b7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InArcIt</name>
      <anchorfile>a00147.html</anchorfile>
      <anchor>ed9bfdcea39717444f32ab67b7c63445</anchor>
      <arglist>(const InArcIt &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InArcIt</name>
      <anchorfile>a00147.html</anchorfile>
      <anchor>590d5ea53f43dae27e42986018c206fa</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InArcIt</name>
      <anchorfile>a00147.html</anchorfile>
      <anchor>02e3b30ddc4b2a7ab91224f8b427eb63</anchor>
      <arglist>(const Graph &amp;g, const Node &amp;n)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>InArcIt</name>
      <anchorfile>a00147.html</anchorfile>
      <anchor>d59a0a7a1c0a17780b723307cb32c456</anchor>
      <arglist>(const Graph &amp;, const Arc &amp;)</arglist>
    </member>
    <member kind="function">
      <type>InArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00147.html</anchorfile>
      <anchor>c495b69e37cf4ccf75cade0b52ccb08d</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::IncEdgeIt</name>
    <filename>a00148.html</filename>
    <base>lemon::concepts::Graph::Edge</base>
    <member kind="function">
      <type></type>
      <name>IncEdgeIt</name>
      <anchorfile>a00148.html</anchorfile>
      <anchor>e34db5c99fa583501bc36ede95ce0437</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IncEdgeIt</name>
      <anchorfile>a00148.html</anchorfile>
      <anchor>acdd46c687cee03076515c8922b5c1db</anchor>
      <arglist>(const IncEdgeIt &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IncEdgeIt</name>
      <anchorfile>a00148.html</anchorfile>
      <anchor>ea2396f494e392d2583c3be19e562b77</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IncEdgeIt</name>
      <anchorfile>a00148.html</anchorfile>
      <anchor>df9a4cfeb46ee0503e0015681773c69a</anchor>
      <arglist>(const Graph &amp;, const Node &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IncEdgeIt</name>
      <anchorfile>a00148.html</anchorfile>
      <anchor>f74baba478726d981bf2c8cb406d4921</anchor>
      <arglist>(const Graph &amp;, const Edge &amp;)</arglist>
    </member>
    <member kind="function">
      <type>IncEdgeIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00148.html</anchorfile>
      <anchor>fd56b09535c0e18e9bd871531497797c</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::Node</name>
    <filename>a00193.html</filename>
    <member kind="function">
      <type></type>
      <name>Node</name>
      <anchorfile>a00193.html</anchorfile>
      <anchor>0d313fac56abd7ebe58a17f1530b879e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Node</name>
      <anchorfile>a00193.html</anchorfile>
      <anchor>7d0d5367c2bd0ffca74029c22845c3b4</anchor>
      <arglist>(const Node &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Node</name>
      <anchorfile>a00193.html</anchorfile>
      <anchor>9a0be9b2de82c808eb4e89a217e35f55</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00193.html</anchorfile>
      <anchor>012ce897fb32e39d99c1cd300227504a</anchor>
      <arglist>(Node) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00193.html</anchorfile>
      <anchor>6fc9deb2a19387e8840cf5cb90c7835d</anchor>
      <arglist>(Node) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00193.html</anchorfile>
      <anchor>05af619da9adfb04fde63e00ae776eb8</anchor>
      <arglist>(Node) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::NodeIt</name>
    <filename>a00194.html</filename>
    <member kind="function">
      <type></type>
      <name>NodeIt</name>
      <anchorfile>a00194.html</anchorfile>
      <anchor>986ea351c3f6f20b80e3c6224e5495cc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeIt</name>
      <anchorfile>a00194.html</anchorfile>
      <anchor>a97d7bb0a0a6d9abf3d4da3af90e0da8</anchor>
      <arglist>(const NodeIt &amp;n)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeIt</name>
      <anchorfile>a00194.html</anchorfile>
      <anchor>e4608fb56651c0a20495054ea7ae84e0</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeIt</name>
      <anchorfile>a00194.html</anchorfile>
      <anchor>805994a5236afcfc7e900e14c18efb5d</anchor>
      <arglist>(const Graph &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeIt</name>
      <anchorfile>a00194.html</anchorfile>
      <anchor>baa980899cf9405d7b65150755e9b6d4</anchor>
      <arglist>(const Graph &amp;, const Node &amp;)</arglist>
    </member>
    <member kind="function">
      <type>NodeIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00194.html</anchorfile>
      <anchor>745151a2b1ec6f321751d9291dcd4f79</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::NodeMap</name>
    <filename>a00197.html</filename>
    <templarg></templarg>
    <base>ReferenceMap&lt; Node, T, T &amp;, const T &amp; &gt;</base>
    <member kind="function">
      <type></type>
      <name>NodeMap</name>
      <anchorfile>a00197.html</anchorfile>
      <anchor>64623b25777e859cd446774b6c314c33</anchor>
      <arglist>(const Graph &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeMap</name>
      <anchorfile>a00197.html</anchorfile>
      <anchor>a994ef92e61070970e4df539a5b92dc9</anchor>
      <arglist>(const Graph &amp;, T)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>NodeMap</name>
      <anchorfile>a00197.html</anchorfile>
      <anchor>f7e39537603ab1c0ec715d2ba2f8cbba</anchor>
      <arglist>(const NodeMap &amp;nm)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>NodeMap &amp;</type>
      <name>operator=</name>
      <anchorfile>a00197.html</anchorfile>
      <anchor>a667d7a1e0e82a9914047c23c6abb92a</anchor>
      <arglist>(const CMap &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Graph::OutArcIt</name>
    <filename>a00206.html</filename>
    <base>lemon::concepts::Graph::Arc</base>
    <member kind="function">
      <type></type>
      <name>OutArcIt</name>
      <anchorfile>a00206.html</anchorfile>
      <anchor>63481864a724a2879e46fa4f4202a743</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OutArcIt</name>
      <anchorfile>a00206.html</anchorfile>
      <anchor>471d9e55da47e420563ab67c82641476</anchor>
      <arglist>(const OutArcIt &amp;e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OutArcIt</name>
      <anchorfile>a00206.html</anchorfile>
      <anchor>e66c85834955281519f2c62b2e202785</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OutArcIt</name>
      <anchorfile>a00206.html</anchorfile>
      <anchor>00d18a0ac564c38da9fc99a1baedf14c</anchor>
      <arglist>(const Graph &amp;n, const Node &amp;g)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OutArcIt</name>
      <anchorfile>a00206.html</anchorfile>
      <anchor>ff32060a435b7ffe67a17a7736e9127f</anchor>
      <arglist>(const Graph &amp;, const Arc &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OutArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00206.html</anchorfile>
      <anchor>e220200f09a52e317d02403d5d2c8a16</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::GraphItem</name>
    <filename>a00130.html</filename>
    <member kind="function">
      <type></type>
      <name>GraphItem</name>
      <anchorfile>a00130.html</anchorfile>
      <anchor>ddc79191559edeabf17f9aaaa379d65c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphItem</name>
      <anchorfile>a00130.html</anchorfile>
      <anchor>a009ad8dc27673e336aa9b34f56b6e45</anchor>
      <arglist>(const GraphItem &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphItem</name>
      <anchorfile>a00130.html</anchorfile>
      <anchor>0037239921556cd688a4cc9a6b7b726f</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>GraphItem &amp;</type>
      <name>operator=</name>
      <anchorfile>a00130.html</anchorfile>
      <anchor>5b20b906a9d9d43e64ded0f7472467c7</anchor>
      <arglist>(const GraphItem &amp;)</arglist>
    </member>
    <member kind="function">
      <type>GraphItem &amp;</type>
      <name>operator=</name>
      <anchorfile>a00130.html</anchorfile>
      <anchor>3bf66158fd6071e0e7769dfeed6bd8d1</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00130.html</anchorfile>
      <anchor>063e178cb22e1f055c1153bbb8c529bc</anchor>
      <arglist>(const GraphItem &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00130.html</anchorfile>
      <anchor>cb8d2c96854d29ccb912f7a389877c09</anchor>
      <arglist>(const GraphItem &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00130.html</anchorfile>
      <anchor>dcc4be7e45023422b5c176c594b08d71</anchor>
      <arglist>(const GraphItem &amp;) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::BaseDigraphComponent</name>
    <filename>a00023.html</filename>
    <member kind="typedef">
      <type>GraphItem&lt;&apos;n&apos;&gt;</type>
      <name>Node</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>90c88d4c17b799f8cb75ebf0a35f4b45</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GraphItem&lt;&apos;a&apos;&gt;</type>
      <name>Arc</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>17b854e59506b0707d2716c7108a0124</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>source</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>60427cac03d2b78cf4faed7c8ff8df8e</anchor>
      <arglist>(const Arc &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>target</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>960d11c98e7592c29a05d8574d91a1e1</anchor>
      <arglist>(const Arc &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>oppositeNode</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>611c261fc201674bb62114df682eb35f</anchor>
      <arglist>(const Node &amp;, const Arc &amp;) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::BaseGraphComponent</name>
    <filename>a00024.html</filename>
    <base>lemon::concepts::BaseDigraphComponent</base>
    <class kind="class">lemon::concepts::BaseGraphComponent::Edge</class>
    <member kind="function">
      <type>Node</type>
      <name>u</name>
      <anchorfile>a00024.html</anchorfile>
      <anchor>4a0fde436b9aad4134e1bf84341c6e67</anchor>
      <arglist>(const Edge &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>v</name>
      <anchorfile>a00024.html</anchorfile>
      <anchor>7dbe4cfbdf46dc6a40c3361ee9ab9c81</anchor>
      <arglist>(const Edge &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>direct</name>
      <anchorfile>a00024.html</anchorfile>
      <anchor>4b6177bf5b5c548bbbf2a626cc1e0979</anchor>
      <arglist>(const Edge &amp;, bool) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>direct</name>
      <anchorfile>a00024.html</anchorfile>
      <anchor>5d4026f8aa623088ebf940618c6abb35</anchor>
      <arglist>(const Edge &amp;, const Node &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>direction</name>
      <anchorfile>a00024.html</anchorfile>
      <anchor>19667f0f6e694d5a9711ca2111147473</anchor>
      <arglist>(const Arc &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>oppositeArc</name>
      <anchorfile>a00024.html</anchorfile>
      <anchor>ee7764232afab8e782c84fe813cb65df</anchor>
      <arglist>(const Arc &amp;) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::BaseGraphComponent::Edge</name>
    <filename>a00099.html</filename>
    <base>GraphItem&lt;&apos;e&apos;&gt;</base>
    <member kind="function">
      <type></type>
      <name>Edge</name>
      <anchorfile>a00099.html</anchorfile>
      <anchor>91715a2ea8b8478224859ee6ee42ed87</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Edge</name>
      <anchorfile>a00099.html</anchorfile>
      <anchor>05e10aa476d938bdc8767728e5e524dd</anchor>
      <arglist>(const Edge &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Edge</name>
      <anchorfile>a00099.html</anchorfile>
      <anchor>4c15ff1fe447a33c5ba30c34b114df23</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Edge</name>
      <anchorfile>a00099.html</anchorfile>
      <anchor>159c394fe9804b8efb784f7151e5bf82</anchor>
      <arglist>(const Arc &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::IDableDigraphComponent</name>
    <filename>a00142.html</filename>
    <templarg>BAS</templarg>
    <member kind="function">
      <type>int</type>
      <name>id</name>
      <anchorfile>a00142.html</anchorfile>
      <anchor>5f0c56e53fb90f859d22d45abfb6ee2b</anchor>
      <arglist>(const Node &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>nodeFromId</name>
      <anchorfile>a00142.html</anchorfile>
      <anchor>c062562f00e551ac6e440e95a15609d7</anchor>
      <arglist>(int) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>id</name>
      <anchorfile>a00142.html</anchorfile>
      <anchor>fd45d1418e059c067e3a648b862fdd30</anchor>
      <arglist>(const Arc &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>arcFromId</name>
      <anchorfile>a00142.html</anchorfile>
      <anchor>58a883bb07c2bbd1819a277b21bf2964</anchor>
      <arglist>(int) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maxNodeId</name>
      <anchorfile>a00142.html</anchorfile>
      <anchor>22ee4829c9dcc9859917b3480b8af19a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maxArcId</name>
      <anchorfile>a00142.html</anchorfile>
      <anchor>6b94cf2706afb7c936a4f6b97b430edf</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::IDableGraphComponent</name>
    <filename>a00143.html</filename>
    <templarg></templarg>
    <base>IDableDigraphComponent&lt; BAS &gt;</base>
    <member kind="function">
      <type>int</type>
      <name>id</name>
      <anchorfile>a00143.html</anchorfile>
      <anchor>d519877cb339d0460223d37916d08672</anchor>
      <arglist>(const Edge &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Edge</type>
      <name>edgeFromId</name>
      <anchorfile>a00143.html</anchorfile>
      <anchor>dc3a4a3eddb6ba32c2c849ee4ff2f45a</anchor>
      <arglist>(int) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maxEdgeId</name>
      <anchorfile>a00143.html</anchorfile>
      <anchor>fa71fa16b2498a294c93c5aea4bd1c5e</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::GraphItemIt</name>
    <filename>a00131.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>GraphItemIt</name>
      <anchorfile>a00131.html</anchorfile>
      <anchor>cfcfa98b379e0464ef00f3d78bb70d0e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphItemIt</name>
      <anchorfile>a00131.html</anchorfile>
      <anchor>17274619ee8e0bcf7f5e9381f1ccc8d4</anchor>
      <arglist>(const GraphItemIt &amp;it)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphItemIt</name>
      <anchorfile>a00131.html</anchorfile>
      <anchor>9a7a333aaec82d1e1155381c69f29162</anchor>
      <arglist>(const GR &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphItemIt</name>
      <anchorfile>a00131.html</anchorfile>
      <anchor>8d5d10baef4798ddfc3a13669da49c45</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>GraphItemIt &amp;</type>
      <name>operator=</name>
      <anchorfile>a00131.html</anchorfile>
      <anchor>68532251c9cba526d3110151535eef72</anchor>
      <arglist>(const GraphItemIt &amp;)</arglist>
    </member>
    <member kind="function">
      <type>GraphItemIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00131.html</anchorfile>
      <anchor>fcff4a2fb279bec74692e105212e7bef</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00131.html</anchorfile>
      <anchor>866ad4c0d786a28689226e84170d8a41</anchor>
      <arglist>(const GraphItemIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00131.html</anchorfile>
      <anchor>460337a635ffb52a41f20ef60353c0b1</anchor>
      <arglist>(const GraphItemIt &amp;) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::GraphIncIt</name>
    <filename>a00129.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <templarg>sel</templarg>
    <member kind="function">
      <type></type>
      <name>GraphIncIt</name>
      <anchorfile>a00129.html</anchorfile>
      <anchor>22a7a460818da92ce6d2221439a3b22a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphIncIt</name>
      <anchorfile>a00129.html</anchorfile>
      <anchor>d82ba99734592401b5add691c846bb2c</anchor>
      <arglist>(const GraphIncIt &amp;it)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphIncIt</name>
      <anchorfile>a00129.html</anchorfile>
      <anchor>88c15f8f8a74c8718d785aea76f9ce8a</anchor>
      <arglist>(const GR &amp;, const Base &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphIncIt</name>
      <anchorfile>a00129.html</anchorfile>
      <anchor>e40351291ee4baa092d4ed2f7c5e9965</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type>GraphIncIt &amp;</type>
      <name>operator=</name>
      <anchorfile>a00129.html</anchorfile>
      <anchor>5ba4779ba432ac93c39d53b080405540</anchor>
      <arglist>(const GraphIncIt &amp;)</arglist>
    </member>
    <member kind="function">
      <type>GraphIncIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00129.html</anchorfile>
      <anchor>dc2a7ca00385a31a33522708d9455785</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00129.html</anchorfile>
      <anchor>a37ed24ce7ed37c4b3551d69c4759f9d</anchor>
      <arglist>(const GraphIncIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00129.html</anchorfile>
      <anchor>f3f6f72ad68bdbc03cee5f9fc65ae570</anchor>
      <arglist>(const GraphIncIt &amp;) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::IterableDigraphComponent</name>
    <filename>a00159.html</filename>
    <templarg>BAS</templarg>
    <member kind="typedef">
      <type>GraphItemIt&lt; Digraph, Node &gt;</type>
      <name>NodeIt</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>6257071492599b2dbdfae34d4773c4ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GraphItemIt&lt; Digraph, Arc &gt;</type>
      <name>ArcIt</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>024b650da6297bc362c5c45a1ec1393a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GraphIncIt&lt; Digraph, Arc, Node, &apos;i&apos;&gt;</type>
      <name>InArcIt</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>4af9ca720fd4e16255dcdeb8f2c3d8e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GraphIncIt&lt; Digraph, Arc, Node, &apos;o&apos;&gt;</type>
      <name>OutArcIt</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>fa242dd42944ff445f049a70c180c2f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>baseNode</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>3445bf026452215ad19333a499afc613</anchor>
      <arglist>(const InArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>runningNode</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>7a1a12c0acd7e4ee2481a8f900e0d672</anchor>
      <arglist>(const InArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>baseNode</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>b10cce7c966f098d0802b651da2b0d49</anchor>
      <arglist>(const OutArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>runningNode</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>15a56f51941bf8376aa809dc3d9bec96</anchor>
      <arglist>(const OutArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>first</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>97c6de267e85f88edc2eb0986a2c9e13</anchor>
      <arglist>(Node &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>next</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>b167d12d5b01179092578a705d11ee4d</anchor>
      <arglist>(Node &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>first</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>9846d653d1b9c0bf98ee248ced38f1da</anchor>
      <arglist>(Arc &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>next</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>c09e643904e3ab206bb27747987c65ec</anchor>
      <arglist>(Arc &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>firstIn</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>a0773f055b540af1cde0c30969cb059e</anchor>
      <arglist>(Arc &amp;, const Node &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>nextIn</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>642f6b8a3a3ad16e3b55dcca9bac582e</anchor>
      <arglist>(Arc &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>firstOut</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>ec77ff092a466e787e9fa33a84d3dbf9</anchor>
      <arglist>(Arc &amp;, const Node &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>nextOut</name>
      <anchorfile>a00159.html</anchorfile>
      <anchor>51006f1a03829c2068d143cdfe80f238</anchor>
      <arglist>(Arc &amp;) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::IterableGraphComponent</name>
    <filename>a00160.html</filename>
    <templarg></templarg>
    <base>IterableDigraphComponent&lt; BAS &gt;</base>
    <member kind="typedef">
      <type>GraphItemIt&lt; Graph, Edge &gt;</type>
      <name>EdgeIt</name>
      <anchorfile>a00160.html</anchorfile>
      <anchor>c447d197226538192ba5820304c275fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>GraphIncIt&lt; Graph, Edge, Node, &apos;e&apos;&gt;</type>
      <name>IncEdgeIt</name>
      <anchorfile>a00160.html</anchorfile>
      <anchor>4bf94d3f391ecccbe5126d93e418fb6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>baseNode</name>
      <anchorfile>a00160.html</anchorfile>
      <anchor>0e138a6d229c2e931212ef4060c4de1c</anchor>
      <arglist>(const IncEdgeIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>Node</type>
      <name>runningNode</name>
      <anchorfile>a00160.html</anchorfile>
      <anchor>77d03441a807a1b26d909f9bfcf7c852</anchor>
      <arglist>(const IncEdgeIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>first</name>
      <anchorfile>a00160.html</anchorfile>
      <anchor>8860f5c80cefbb5ef5c5eeb4550e4eb5</anchor>
      <arglist>(Edge &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>next</name>
      <anchorfile>a00160.html</anchorfile>
      <anchor>f8565bc6ee45621553c79797a4c6b099</anchor>
      <arglist>(Edge &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>firstInc</name>
      <anchorfile>a00160.html</anchorfile>
      <anchor>762df57675d092a207afd9b43556691a</anchor>
      <arglist>(Edge &amp;, bool &amp;, const Node &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>nextInc</name>
      <anchorfile>a00160.html</anchorfile>
      <anchor>8f1b07c1145586fa0c5b32cdbc1897fb</anchor>
      <arglist>(Edge &amp;, bool &amp;) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::AlterableDigraphComponent</name>
    <filename>a00004.html</filename>
    <templarg>BAS</templarg>
    <member kind="typedef">
      <type>AlterationNotifier&lt; AlterableDigraphComponent, Node &gt;</type>
      <name>NodeNotifier</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>08fc2a908c6f70feec8d4c38260b26b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>AlterationNotifier&lt; AlterableDigraphComponent, Arc &gt;</type>
      <name>ArcNotifier</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>f37d09f18f08f584b74cbe6fa99b4773</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>NodeNotifier &amp;</type>
      <name>notifier</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>b1f181a6aac329f45816360e120da601</anchor>
      <arglist>(Node) const </arglist>
    </member>
    <member kind="function">
      <type>ArcNotifier &amp;</type>
      <name>notifier</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>e4078e8be6d775e198f0217f30e46347</anchor>
      <arglist>(Arc) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::AlterableGraphComponent</name>
    <filename>a00005.html</filename>
    <templarg></templarg>
    <base>AlterableDigraphComponent&lt; BAS &gt;</base>
    <member kind="typedef">
      <type>AlterationNotifier&lt; AlterableGraphComponent, Edge &gt;</type>
      <name>EdgeNotifier</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>8e089f2afb622f7ce8033ec8dde6e885</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>EdgeNotifier &amp;</type>
      <name>notifier</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>fd16f47c8d07e33fcab801d8c565d4ea</anchor>
      <arglist>(Edge) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::GraphMap</name>
    <filename>a00132.html</filename>
    <templarg>GR</templarg>
    <templarg>K</templarg>
    <templarg>V</templarg>
    <base>ReferenceMap&lt; K, V, V &amp;, const V &amp; &gt;</base>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00132.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>V</type>
      <name>Value</name>
      <anchorfile>a00132.html</anchorfile>
      <anchor>6c1768456283cc436140a9ffae849dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Value &amp;</type>
      <name>Reference</name>
      <anchorfile>a00132.html</anchorfile>
      <anchor>e36c42dfba46c6fd74d7d85a4fc27d57</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>const Value &amp;</type>
      <name>ConstReference</name>
      <anchorfile>a00132.html</anchorfile>
      <anchor>33899f17b36f4c01c39f333a0a6f989b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphMap</name>
      <anchorfile>a00132.html</anchorfile>
      <anchor>12ff6931f03d1c6d324f2c29fe7cf206</anchor>
      <arglist>(const GR &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>GraphMap</name>
      <anchorfile>a00132.html</anchorfile>
      <anchor>1f126a3fc1d22face02b15fc9a091ace</anchor>
      <arglist>(const GR &amp;, const Value &amp;)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>GraphMap</name>
      <anchorfile>a00132.html</anchorfile>
      <anchor>b57bf8ab328b83859f80c6ae379bb4a8</anchor>
      <arglist>(const GraphMap &amp;)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>GraphMap &amp;</type>
      <name>operator=</name>
      <anchorfile>a00132.html</anchorfile>
      <anchor>e9bff1bdcb3396b95454616ded616f09</anchor>
      <arglist>(const CMap &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::MappableDigraphComponent</name>
    <filename>a00175.html</filename>
    <templarg>BAS</templarg>
    <class kind="class">lemon::concepts::MappableDigraphComponent::ArcMap</class>
    <class kind="class">lemon::concepts::MappableDigraphComponent::NodeMap</class>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::MappableDigraphComponent::ArcMap</name>
    <filename>a00020.html</filename>
    <templarg></templarg>
    <base>GraphMap&lt; MappableDigraphComponent, Arc, V &gt;</base>
    <member kind="function">
      <type></type>
      <name>ArcMap</name>
      <anchorfile>a00020.html</anchorfile>
      <anchor>475275c7f2626cb2bc41fd5253e629bb</anchor>
      <arglist>(const MappableDigraphComponent &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcMap</name>
      <anchorfile>a00020.html</anchorfile>
      <anchor>c80a16d015e7b8ddd5b5ca930e5888a8</anchor>
      <arglist>(const MappableDigraphComponent &amp;digraph, const V &amp;value)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>ArcMap</name>
      <anchorfile>a00020.html</anchorfile>
      <anchor>4ad524297cf426d506ac218711d6010a</anchor>
      <arglist>(const ArcMap &amp;nm)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>ArcMap &amp;</type>
      <name>operator=</name>
      <anchorfile>a00020.html</anchorfile>
      <anchor>677fe241a6de3a78b9f70032466ea0d0</anchor>
      <arglist>(const CMap &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::MappableDigraphComponent::NodeMap</name>
    <filename>a00198.html</filename>
    <templarg></templarg>
    <base>GraphMap&lt; MappableDigraphComponent, Node, V &gt;</base>
    <member kind="function">
      <type></type>
      <name>NodeMap</name>
      <anchorfile>a00198.html</anchorfile>
      <anchor>06da3fde50cb6440a5b753c0ac7342ac</anchor>
      <arglist>(const MappableDigraphComponent &amp;digraph)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeMap</name>
      <anchorfile>a00198.html</anchorfile>
      <anchor>7c09fb299675aff0560c6c9b7638dccb</anchor>
      <arglist>(const MappableDigraphComponent &amp;digraph, const V &amp;value)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>NodeMap</name>
      <anchorfile>a00198.html</anchorfile>
      <anchor>f7e39537603ab1c0ec715d2ba2f8cbba</anchor>
      <arglist>(const NodeMap &amp;nm)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>NodeMap &amp;</type>
      <name>operator=</name>
      <anchorfile>a00198.html</anchorfile>
      <anchor>a667d7a1e0e82a9914047c23c6abb92a</anchor>
      <arglist>(const CMap &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::MappableGraphComponent</name>
    <filename>a00176.html</filename>
    <templarg></templarg>
    <base>MappableDigraphComponent&lt; BAS &gt;</base>
    <class kind="class">lemon::concepts::MappableGraphComponent::EdgeMap</class>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::MappableGraphComponent::EdgeMap</name>
    <filename>a00102.html</filename>
    <templarg></templarg>
    <base>GraphMap&lt; MappableGraphComponent, Edge, V &gt;</base>
    <member kind="function">
      <type></type>
      <name>EdgeMap</name>
      <anchorfile>a00102.html</anchorfile>
      <anchor>6ae16313000b2c034cfb6184dcd58f99</anchor>
      <arglist>(const MappableGraphComponent &amp;graph)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>EdgeMap</name>
      <anchorfile>a00102.html</anchorfile>
      <anchor>250e683cf375f94ad2a3181ecf35335f</anchor>
      <arglist>(const MappableGraphComponent &amp;graph, const V &amp;value)</arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>EdgeMap</name>
      <anchorfile>a00102.html</anchorfile>
      <anchor>53231e3e624ba6d6f5c17ea58c5b7a75</anchor>
      <arglist>(const EdgeMap &amp;nm)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>EdgeMap &amp;</type>
      <name>operator=</name>
      <anchorfile>a00102.html</anchorfile>
      <anchor>5d8010c74679b0d91ddde1196eeed600</anchor>
      <arglist>(const CMap &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::ExtendableDigraphComponent</name>
    <filename>a00110.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type>Node</type>
      <name>addNode</name>
      <anchorfile>a00110.html</anchorfile>
      <anchor>96838566b12a6b04795db38688bad1a0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Arc</type>
      <name>addArc</name>
      <anchorfile>a00110.html</anchorfile>
      <anchor>dd1d7458968c541deb2e77847700c818</anchor>
      <arglist>(const Node &amp;, const Node &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::ExtendableGraphComponent</name>
    <filename>a00111.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type>Node</type>
      <name>addNode</name>
      <anchorfile>a00111.html</anchorfile>
      <anchor>96838566b12a6b04795db38688bad1a0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Edge</type>
      <name>addEdge</name>
      <anchorfile>a00111.html</anchorfile>
      <anchor>0b9661080220dd051f3ac469581a793e</anchor>
      <arglist>(const Node &amp;, const Node &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::ErasableDigraphComponent</name>
    <filename>a00105.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00105.html</anchorfile>
      <anchor>b5aaeeb174f6471064c2eddced9e5c5e</anchor>
      <arglist>(const Node &amp;)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00105.html</anchorfile>
      <anchor>401f60df04443410c880f0dc9c1d899e</anchor>
      <arglist>(const Arc &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::ErasableGraphComponent</name>
    <filename>a00106.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00106.html</anchorfile>
      <anchor>b5aaeeb174f6471064c2eddced9e5c5e</anchor>
      <arglist>(const Node &amp;)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00106.html</anchorfile>
      <anchor>2adc2f58e71cb69dac3c9a5d211e2d82</anchor>
      <arglist>(const Edge &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::ClearableDigraphComponent</name>
    <filename>a00043.html</filename>
    <templarg>BAS</templarg>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00043.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::ClearableGraphComponent</name>
    <filename>a00044.html</filename>
    <templarg></templarg>
    <base>ClearableDigraphComponent&lt; BAS &gt;</base>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00044.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Heap</name>
    <filename>a00138.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="enumeration">
      <name>State</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>5d74787dedbc4e11c1ab15bf487e61f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>IN_HEAP</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>5d74787dedbc4e11c1ab15bf487e61f8213759402d071be3f66f8cf86641bb11</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>PRE_HEAP</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>5d74787dedbc4e11c1ab15bf487e61f812f201f9d13d106e81658dad7e7c9c2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>POST_HEAP</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>5d74787dedbc4e11c1ab15bf487e61f8cd215e97251c1085a8b299c068e7172d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>IM</type>
      <name>ItemIntMap</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>c00a2d6f039b6e8ffc0641530bdf5304</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>PR</type>
      <name>Prio</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>644810a55913c9e8b24511758574d6d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>ItemIntMap::Key</type>
      <name>Item</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>70025b32b600038ee2981a3deab1a783</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Heap</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>9b8fa9d5935f50e2052fb7c6ddf0cebb</anchor>
      <arglist>(ItemIntMap &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>b8e4e3e2a7bf18888b71bdf9dda0770b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>empty</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>c6e61de369e994009e36f344f99c15ad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>push</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>d34f576dafc84f8969e2fecd67cd3a15</anchor>
      <arglist>(const Item &amp;i, const Prio &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>Item</type>
      <name>top</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>84adc89415588c3ce11ce526a00070d9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Prio</type>
      <name>prio</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>eea060d345482a9732a96e9021f0c495</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>pop</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>312e7f6c761a199c1369fbe651e084f0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>erase</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>5fe7eab02681c7735ba173c879de1681</anchor>
      <arglist>(const Item &amp;i)</arglist>
    </member>
    <member kind="function">
      <type>Prio</type>
      <name>operator[]</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>74ccb13d905bf8b608f3426131977cc0</anchor>
      <arglist>(const Item &amp;i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>35c06441ccfac0ed04762113a102e6b3</anchor>
      <arglist>(const Item &amp;i, const Prio &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>decrease</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>301fdc87288afdebf379ae043cf66cd4</anchor>
      <arglist>(const Item &amp;i, const Prio &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>increase</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>958836bae38b188956bcf1345c63eac4</anchor>
      <arglist>(const Item &amp;i, const Prio &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>State</type>
      <name>state</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>613888b340ee362e2f4da6f2e2b97428</anchor>
      <arglist>(const Item &amp;i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>state</name>
      <anchorfile>a00138.html</anchorfile>
      <anchor>7d0ca230d438efaad53833701d9cb262</anchor>
      <arglist>(const Item &amp;i, State st)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::ReadMap</name>
    <filename>a00221.html</filename>
    <templarg>K</templarg>
    <templarg>T</templarg>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00221.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>T</type>
      <name>Value</name>
      <anchorfile>a00221.html</anchorfile>
      <anchor>34b57a974fe67a997b7693e6e71cd904</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00221.html</anchorfile>
      <anchor>a6290a1f49db2643ba24d02f87cbf429</anchor>
      <arglist>(const Key &amp;) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::WriteMap</name>
    <filename>a00298.html</filename>
    <templarg>K</templarg>
    <templarg>T</templarg>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00298.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>T</type>
      <name>Value</name>
      <anchorfile>a00298.html</anchorfile>
      <anchor>34b57a974fe67a997b7693e6e71cd904</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00298.html</anchorfile>
      <anchor>46b66c881c64c81af62a9616946a2f3a</anchor>
      <arglist>(const Key &amp;, const Value &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>WriteMap</name>
      <anchorfile>a00298.html</anchorfile>
      <anchor>7988a5ffa9402369408447cb21be1123</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::ReadWriteMap</name>
    <filename>a00222.html</filename>
    <templarg>K</templarg>
    <templarg>T</templarg>
    <base>ReadMap&lt; K, T &gt;</base>
    <base>WriteMap&lt; K, T &gt;</base>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00222.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>T</type>
      <name>Value</name>
      <anchorfile>a00222.html</anchorfile>
      <anchor>34b57a974fe67a997b7693e6e71cd904</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>operator[]</name>
      <anchorfile>a00222.html</anchorfile>
      <anchor>a6290a1f49db2643ba24d02f87cbf429</anchor>
      <arglist>(const Key &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00222.html</anchorfile>
      <anchor>46b66c881c64c81af62a9616946a2f3a</anchor>
      <arglist>(const Key &amp;, const Value &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::ReferenceMap</name>
    <filename>a00223.html</filename>
    <templarg>K</templarg>
    <templarg>T</templarg>
    <templarg>R</templarg>
    <templarg>CR</templarg>
    <base>ReadWriteMap&lt; K, T &gt;</base>
    <member kind="typedef">
      <type>True</type>
      <name>ReferenceMapTag</name>
      <anchorfile>a00223.html</anchorfile>
      <anchor>804d338ca2e0e71ef0572ec61ddefffd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>K</type>
      <name>Key</name>
      <anchorfile>a00223.html</anchorfile>
      <anchor>2cfe904ef7579cf511b9fcb14420539b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>T</type>
      <name>Value</name>
      <anchorfile>a00223.html</anchorfile>
      <anchor>34b57a974fe67a997b7693e6e71cd904</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>R</type>
      <name>Reference</name>
      <anchorfile>a00223.html</anchorfile>
      <anchor>6e8175a8d0f7ccdbabe88aea29e38777</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CR</type>
      <name>ConstReference</name>
      <anchorfile>a00223.html</anchorfile>
      <anchor>aaf1f16bcbee97f0cb6727e10901222b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Reference</type>
      <name>operator[]</name>
      <anchorfile>a00223.html</anchorfile>
      <anchor>3fdb5b25c67b183083ebd86dba491e50</anchor>
      <arglist>(const Key &amp;)</arglist>
    </member>
    <member kind="function">
      <type>ConstReference</type>
      <name>operator[]</name>
      <anchorfile>a00223.html</anchorfile>
      <anchor>5bc6c95392afea89d3938c86932964d9</anchor>
      <arglist>(const Key &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>a00223.html</anchorfile>
      <anchor>1eb01f50386db49765cdf4edbe951369</anchor>
      <arglist>(const Key &amp;k, const Value &amp;t)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Path</name>
    <filename>a00210.html</filename>
    <templarg></templarg>
    <class kind="class">lemon::concepts::Path::ArcIt</class>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00210.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::Arc</type>
      <name>Arc</name>
      <anchorfile>a00210.html</anchorfile>
      <anchor>a8ea0a3f0dead55bd6038b930df40763</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Path</name>
      <anchorfile>a00210.html</anchorfile>
      <anchor>aa44fef284bec9041f7eb22b921c6174</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Path</name>
      <anchorfile>a00210.html</anchorfile>
      <anchor>f5bfe56b948c1ac8261d41052bd459ec</anchor>
      <arglist>(const CPath &amp;cpath)</arglist>
    </member>
    <member kind="function">
      <type>Path &amp;</type>
      <name>operator=</name>
      <anchorfile>a00210.html</anchorfile>
      <anchor>078906b734c1e9874043bc142341f63f</anchor>
      <arglist>(const CPath &amp;cpath)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>length</name>
      <anchorfile>a00210.html</anchorfile>
      <anchor>57b988236ee6a3a5e572d126d3fbccc1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>empty</name>
      <anchorfile>a00210.html</anchorfile>
      <anchor>c6e61de369e994009e36f344f99c15ad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00210.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::Path::ArcIt</name>
    <filename>a00012.html</filename>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>19f73b24f93bc2400778a66262f4fc78</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>3bde563f0bba0fc91cc7255fed9b682c</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>5cefdc7b7a666e1a0faa5d23de25ef32</anchor>
      <arglist>(const Path &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Arc</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>fee0d9c078f8e4cf4c3197a0341f864f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a8838b5e6d2c1404b0b07dfc23ff9564</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>5911b1d91ce601b6fb5a69d07e67d709</anchor>
      <arglist>(const ArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>c54d4f962dd2c3cbe320381e468aea0d</anchor>
      <arglist>(const ArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>f64ef0275947af3ac36bb9acab6e07d3</anchor>
      <arglist>(const ArcIt &amp;) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::PathDumper</name>
    <filename>a00212.html</filename>
    <templarg></templarg>
    <class kind="class">lemon::concepts::PathDumper::ArcIt</class>
    <class kind="class">lemon::concepts::PathDumper::RevArcIt</class>
    <member kind="typedef">
      <type>GR</type>
      <name>Digraph</name>
      <anchorfile>a00212.html</anchorfile>
      <anchor>f108349b07bd3b361cfa1387c19395ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Digraph::Arc</type>
      <name>Arc</name>
      <anchorfile>a00212.html</anchorfile>
      <anchor>a8ea0a3f0dead55bd6038b930df40763</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>False</type>
      <name>RevPathTag</name>
      <anchorfile>a00212.html</anchorfile>
      <anchor>747b77ad858f28fae6dcfa1cd13c6915</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>length</name>
      <anchorfile>a00212.html</anchorfile>
      <anchor>57b988236ee6a3a5e572d126d3fbccc1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>empty</name>
      <anchorfile>a00212.html</anchorfile>
      <anchor>c6e61de369e994009e36f344f99c15ad</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::PathDumper::ArcIt</name>
    <filename>a00011.html</filename>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00011.html</anchorfile>
      <anchor>19f73b24f93bc2400778a66262f4fc78</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00011.html</anchorfile>
      <anchor>3bde563f0bba0fc91cc7255fed9b682c</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ArcIt</name>
      <anchorfile>a00011.html</anchorfile>
      <anchor>31eaf7cec0be2ee774ad71a30d669b9a</anchor>
      <arglist>(const PathDumper &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Arc</name>
      <anchorfile>a00011.html</anchorfile>
      <anchor>fee0d9c078f8e4cf4c3197a0341f864f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00011.html</anchorfile>
      <anchor>a8838b5e6d2c1404b0b07dfc23ff9564</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00011.html</anchorfile>
      <anchor>5911b1d91ce601b6fb5a69d07e67d709</anchor>
      <arglist>(const ArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00011.html</anchorfile>
      <anchor>c54d4f962dd2c3cbe320381e468aea0d</anchor>
      <arglist>(const ArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00011.html</anchorfile>
      <anchor>f64ef0275947af3ac36bb9acab6e07d3</anchor>
      <arglist>(const ArcIt &amp;) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::concepts::PathDumper::RevArcIt</name>
    <filename>a00227.html</filename>
    <member kind="function">
      <type></type>
      <name>RevArcIt</name>
      <anchorfile>a00227.html</anchorfile>
      <anchor>c46e3052081d5657ff0e7526db165bc7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RevArcIt</name>
      <anchorfile>a00227.html</anchorfile>
      <anchor>c677c8e5033a8b0125f6a7c2e1c6d2c5</anchor>
      <arglist>(Invalid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RevArcIt</name>
      <anchorfile>a00227.html</anchorfile>
      <anchor>8d8d20e4a92c9539aae154d524518a2e</anchor>
      <arglist>(const PathDumper &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator Arc</name>
      <anchorfile>a00227.html</anchorfile>
      <anchor>fee0d9c078f8e4cf4c3197a0341f864f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>RevArcIt &amp;</type>
      <name>operator++</name>
      <anchorfile>a00227.html</anchorfile>
      <anchor>63bf133c4b11c93307d474be1d880835</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00227.html</anchorfile>
      <anchor>66bec6b70e23f1553360288f36f5a810</anchor>
      <arglist>(const RevArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00227.html</anchorfile>
      <anchor>3bd72f71e6f6bfbf8089066ab4780f7a</anchor>
      <arglist>(const RevArcIt &amp;) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>a00227.html</anchorfile>
      <anchor>cb16d1a5028583bed9f13b1cb9ce801d</anchor>
      <arglist>(const RevArcIt &amp;) const </arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lemon::dim2</name>
    <filename>a00424.html</filename>
    <class kind="class">lemon::dim2::Point</class>
    <class kind="class">lemon::dim2::Box</class>
    <class kind="class">lemon::dim2::XMap</class>
    <class kind="class">lemon::dim2::ConstXMap</class>
    <class kind="class">lemon::dim2::YMap</class>
    <class kind="class">lemon::dim2::ConstYMap</class>
    <class kind="class">lemon::dim2::NormSquareMap</class>
  </compound>
  <compound kind="class">
    <name>lemon::dim2::Point</name>
    <filename>a00214.html</filename>
    <templarg>T</templarg>
    <member kind="function">
      <type></type>
      <name>Point</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>e08c5f0c5b4c75a3e0f33dada5f2fcba</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Point</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>3162fb50e7b7cd26fe1d2800525779f3</anchor>
      <arglist>(T a, T b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>b8e4e3e2a7bf18888b71bdf9dda0770b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>T &amp;</type>
      <name>operator[]</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>2a322a02b87eab70bf5d7d1f6e1b8842</anchor>
      <arglist>(int idx)</arglist>
    </member>
    <member kind="function">
      <type>const T &amp;</type>
      <name>operator[]</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>21e58a4115e3fe476f7a6677c14c54fa</anchor>
      <arglist>(int idx) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Point</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>f6f69c73d4f5843847054ae80c1464b0</anchor>
      <arglist>(const Point&lt; TT &gt; &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>normSquare</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>6b5e0f4e99552b002fa639bc7c3018aa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt; &amp;</type>
      <name>operator+=</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>d54bef7b607263200a84b35da544902f</anchor>
      <arglist>(const Point&lt; T &gt; &amp;u)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt; &amp;</type>
      <name>operator-=</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>70e2bd6a1cd9f63691426e3a59c23677</anchor>
      <arglist>(const Point&lt; T &gt; &amp;u)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt; &amp;</type>
      <name>operator*=</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>5fbf8dcc45005bc420eb5d23d511c9cd</anchor>
      <arglist>(const T &amp;u)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt; &amp;</type>
      <name>operator/=</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>a790e91303048d21d3d9483d541f461c</anchor>
      <arglist>(const T &amp;u)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>operator*</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>c8a06184a9e10556961cb04b82a86f6b</anchor>
      <arglist>(const Point&lt; T &gt; &amp;u) const </arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>operator+</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>23ddbecbecad8b3660976fde4bb3f34e</anchor>
      <arglist>(const Point&lt; T &gt; &amp;u) const </arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>operator-</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>e047546c9ba773cf13cc8b8fe5814a9a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>operator-</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>ef4b600f9d6172c958c5c870d2d7f196</anchor>
      <arglist>(const Point&lt; T &gt; &amp;u) const </arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>operator*</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>34594688a40d8a74eaaab726eab4bd84</anchor>
      <arglist>(const T &amp;u) const </arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>operator/</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>dc77a4365cbcbddeb89bee2e6e7f7e60</anchor>
      <arglist>(const T &amp;u) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>560620860e6f8c5862c1a6709d8617a8</anchor>
      <arglist>(const Point&lt; T &gt; &amp;u) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>442a7b905f6394ca1da1c4905c92ccee</anchor>
      <arglist>(Point u) const </arglist>
    </member>
    <member kind="variable">
      <type>T</type>
      <name>x</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>9a4f74af87a76a4c3dcb729cb0e68f8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>T</type>
      <name>y</name>
      <anchorfile>a00214.html</anchorfile>
      <anchor>1cb2b5ea04251d543e49356ef54eb853</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>makePoint</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gf2251ac71816d6d1a01977a41c1e6f5b</anchor>
      <arglist>(const T &amp;x, const T &amp;y)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>operator*</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gbfc65ded0f794f4e5903048252a8cc79</anchor>
      <arglist>(const T &amp;u, const Point&lt; T &gt; &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g2dd3eccf5dece76c03bc6d1c2f348643</anchor>
      <arglist>(std::istream &amp;is, Point&lt; T &gt; &amp;z)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g551d5cc0501d3499faff3e3ba728f32a</anchor>
      <arglist>(std::ostream &amp;os, const Point&lt; T &gt; &amp;z)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>rot90</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g3e6760608b4dfa6eb771a7c02bee5892</anchor>
      <arglist>(const Point&lt; T &gt; &amp;z)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>rot180</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g908068d1858d89712f7b55c3a0f2a375</anchor>
      <arglist>(const Point&lt; T &gt; &amp;z)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>rot270</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g729ffd49ccc19dcd23cb057b8c550e0a</anchor>
      <arglist>(const Point&lt; T &gt; &amp;z)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::dim2::Box</name>
    <filename>a00036.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>Box</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>dea1928c15ca1eb88b6619bc554911b5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Box</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>29f3af2cc22fcfbd9974ca460192aca3</anchor>
      <arglist>(Point&lt; T &gt; a)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Box</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>2cff8d3592817baa693975c17f90f271</anchor>
      <arglist>(Point&lt; T &gt; a, Point&lt; T &gt; b)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Box</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>7a3358e8b1634d15a1c4cad13900b90c</anchor>
      <arglist>(T l, T b, T r, T t)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>empty</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>c6e61de369e994009e36f344f99c15ad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>c8bb3912a3ce86b15842e79d0b421204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>bottomLeft</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>a2afed5cf72d3e18f01c984b31dda08e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>bottomLeft</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>e35f29394ae0783f42032fac7f18b2aa</anchor>
      <arglist>(Point&lt; T &gt; p)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>topRight</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>c46479c697a699cf70d5b1c14b27ebd8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>topRight</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>05f5ce4ee3e3faa2a62cf5f10316691a</anchor>
      <arglist>(Point&lt; T &gt; p)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>bottomRight</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>c5f3974b10edac39b4b8f0dbf5f7616a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>bottomRight</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>57339debce32855309ce6298d892a154</anchor>
      <arglist>(Point&lt; T &gt; p)</arglist>
    </member>
    <member kind="function">
      <type>Point&lt; T &gt;</type>
      <name>topLeft</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>4d2c4a6352a93f76b5264556128cfa96</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>topLeft</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>a350b1414024d9e005d0dce6450bf83b</anchor>
      <arglist>(Point&lt; T &gt; p)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>bottom</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>68c6d5345f8792612d338b1a871e650e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>bottom</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>2bf62d47c1595eb5a3f4787cb2fb55ab</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>top</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>11f99b5e7cf8cd195d042c56eac8d809</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>top</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>6a04460707328bc1639fcd9e2ce1a9de</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>left</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>bdc4b3e668f051306fcac4394853e155</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>left</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>5f3b7939831ee6d17e2b881e5db9c741</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>right</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>1484626fadc35eeef3296f77ba903104</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>right</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>83bf8f14a6687649ee8df9c6a4de2273</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>height</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>d2dce3cfd627b1c99fcabe5072bcc215</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>width</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>8496240bd85c1aee34c207e272ba3969</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>inside</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>a1ec5d72a932243e49f67c23373b08d4</anchor>
      <arglist>(const Point&lt; T &gt; &amp;u) const </arglist>
    </member>
    <member kind="function">
      <type>Box &amp;</type>
      <name>add</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>e1b0a6f64ab6d14c3cea52b861daa6e4</anchor>
      <arglist>(const Point&lt; T &gt; &amp;u)</arglist>
    </member>
    <member kind="function">
      <type>Box &amp;</type>
      <name>add</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>8ea76f3252630757a9fcd2e2dadbc499</anchor>
      <arglist>(const Box &amp;u)</arglist>
    </member>
    <member kind="function">
      <type>Box</type>
      <name>operator&amp;</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>2ec46aa087c222c7f29d25f6e3003a58</anchor>
      <arglist>(const Box &amp;u) const </arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gd9dd0880428a70a38bc5d4daa1930da7</anchor>
      <arglist>(std::istream &amp;is, Box&lt; T &gt; &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>geec02f85273c8923fcb14c9a38df5eb1</anchor>
      <arglist>(std::ostream &amp;os, const Box&lt; T &gt; &amp;b)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::dim2::XMap</name>
    <filename>a00299.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>XMap</name>
      <anchorfile>a00299.html</anchorfile>
      <anchor>70c36f0d3b618043d829adc868b85072</anchor>
      <arglist>(M &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>XMap&lt; M &gt;</type>
      <name>xMap</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>gd9409d1cd73ef6041dfd7e2ef985caad</anchor>
      <arglist>(M &amp;m)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::dim2::ConstXMap</name>
    <filename>a00064.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>ConstXMap</name>
      <anchorfile>a00064.html</anchorfile>
      <anchor>ad7bf966e445ba324e3cdab5a2fb4389</anchor>
      <arglist>(const M &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>ConstXMap&lt; M &gt;</type>
      <name>xMap</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g6149b65448f13dabba937f9decbd6f20</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::dim2::YMap</name>
    <filename>a00300.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>YMap</name>
      <anchorfile>a00300.html</anchorfile>
      <anchor>b5f6a5b671ba21e3dd3164a28c6d4d1f</anchor>
      <arglist>(M &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>YMap&lt; M &gt;</type>
      <name>yMap</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g42c015a3cec9a5cdaf8c76340e8a7570</anchor>
      <arglist>(M &amp;m)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::dim2::ConstYMap</name>
    <filename>a00065.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>ConstYMap</name>
      <anchorfile>a00065.html</anchorfile>
      <anchor>2119ed51dd7d2b9b643ea062b545342f</anchor>
      <arglist>(const M &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>ConstYMap&lt; M &gt;</type>
      <name>yMap</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g45e163c72ccfdc68808cc69f803449ed</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lemon::dim2::NormSquareMap</name>
    <filename>a00199.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>NormSquareMap</name>
      <anchorfile>a00199.html</anchorfile>
      <anchor>addccaa22b50c9fae82ab2d0e174f027</anchor>
      <arglist>(const M &amp;map)</arglist>
    </member>
    <member kind="function">
      <type>NormSquareMap&lt; M &gt;</type>
      <name>normSquareMap</name>
      <anchorfile>a00449.html</anchorfile>
      <anchor>g6120711d506a51dddca09c0ea5607cb3</anchor>
      <arglist>(const M &amp;m)</arglist>
    </member>
  </compound>
  <compound kind="dir">
    <name>lemon/bits/</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/bits/</path>
    <filename>dir_ffd5ec475e8c6b1e9e55abf0d672fe0c.html</filename>
    <file>alteration_notifier.h</file>
    <file>array_map.h</file>
    <file>bezier.h</file>
    <file>default_map.h</file>
    <file>edge_set_extender.h</file>
    <file>enable_if.h</file>
    <file>graph_adaptor_extender.h</file>
    <file>graph_extender.h</file>
    <file>map_extender.h</file>
    <file>path_dump.h</file>
    <file>solver_bits.h</file>
    <file>traits.h</file>
    <file>variant.h</file>
    <file>vector_map.h</file>
    <file>windows.cc</file>
    <file>windows.h</file>
  </compound>
  <compound kind="dir">
    <name>lemon/concepts/</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/concepts/</path>
    <filename>dir_083884b0c5423edf9db694b76128106a.html</filename>
    <file>digraph.h</file>
    <file>graph.h</file>
    <file>graph_components.h</file>
    <file>heap.h</file>
    <file>maps.h</file>
    <file>path.h</file>
  </compound>
  <compound kind="dir">
    <name>demo/</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/demo/</path>
    <filename>dir_12211e2b749fba0c0698cc335d9553f3.html</filename>
    <file>arg_parser_demo.cc</file>
    <file>graph_to_eps_demo.cc</file>
    <file>lgf_demo.cc</file>
  </compound>
  <compound kind="dir">
    <name>doc/</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/doc/</path>
    <filename>dir_24fa41e166f8a3a6343aaabcbbd6ac30.html</filename>
    <file>template.h</file>
  </compound>
  <compound kind="dir">
    <name>lemon/</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/lemon/</path>
    <filename>dir_94ea5476c6c78d93f5a18a404c0a5de9.html</filename>
    <dir>lemon/bits/</dir>
    <dir>lemon/concepts/</dir>
    <file>adaptors.h</file>
    <file>arg_parser.cc</file>
    <file>arg_parser.h</file>
    <file>assert.h</file>
    <file>base.cc</file>
    <file>bfs.h</file>
    <file>bin_heap.h</file>
    <file>cbc.cc</file>
    <file>cbc.h</file>
    <file>circulation.h</file>
    <file>clp.cc</file>
    <file>clp.h</file>
    <file>color.cc</file>
    <file>color.h</file>
    <file>concept_check.h</file>
    <file>config.h</file>
    <file>connectivity.h</file>
    <file>core.h</file>
    <file>counter.h</file>
    <file>cplex.cc</file>
    <file>cplex.h</file>
    <file>dfs.h</file>
    <file>dijkstra.h</file>
    <file>dim2.h</file>
    <file>dimacs.h</file>
    <file>edge_set.h</file>
    <file>elevator.h</file>
    <file>error.h</file>
    <file>euler.h</file>
    <file>full_graph.h</file>
    <file>glpk.cc</file>
    <file>glpk.h</file>
    <file>gomory_hu.h</file>
    <file>graph_to_eps.h</file>
    <file>grid_graph.h</file>
    <file>hao_orlin.h</file>
    <file>hypercube_graph.h</file>
    <file>kruskal.h</file>
    <file>lgf_reader.h</file>
    <file>lgf_writer.h</file>
    <file>list_graph.h</file>
    <file>lp.h</file>
    <file>lp_base.cc</file>
    <file>lp_base.h</file>
    <file>lp_skeleton.cc</file>
    <file>lp_skeleton.h</file>
    <file>maps.h</file>
    <file>matching.h</file>
    <file>math.h</file>
    <file>min_cost_arborescence.h</file>
    <file>nauty_reader.h</file>
    <file>network_simplex.h</file>
    <file>path.h</file>
    <file>preflow.h</file>
    <file>radix_sort.h</file>
    <file>random.cc</file>
    <file>random.h</file>
    <file>smart_graph.h</file>
    <file>soplex.cc</file>
    <file>soplex.h</file>
    <file>suurballe.h</file>
    <file>time_measure.h</file>
    <file>tolerance.h</file>
    <file>unionfind.h</file>
  </compound>
  <compound kind="dir">
    <name>test/</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/test/</path>
    <filename>dir_98b999bb03cef93b91c100abc9391c16.html</filename>
    <file>test_tools.h</file>
  </compound>
  <compound kind="dir">
    <name>tools/</name>
    <path>/home/alpar/projects/LEMON/hg/lemon-1.1.2/tools/</path>
    <filename>dir_e28b5feea2ae8768b89b71fcfce18dc2.html</filename>
    <file>dimacs-solver.cc</file>
    <file>dimacs-to-lgf.cc</file>
    <file>lgf-gen.cc</file>
  </compound>
</tagfile>
